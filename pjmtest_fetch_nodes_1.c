#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_1(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* ServerType ********/


case 2004 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{
	universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2004;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Specifies the current status and capabilities of the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

puts("finished adding node >> 2004 \n");

if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_2004_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_2004_retrieved_reference_45_inverse_0);
*Node_2004_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_45_inverse_0_typed_id,Node_2004_ref_node_target_id_45_inverse_0);
*Node_2004_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_45_inverse_true_0_is_inverse, Node_2004_ref_node_target_id_45_inverse_0);
*Node_2004_retrieved_reference_45_inverse_0_target_id = 58;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_45_inverse_0_target_id, Node_2004_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_45_inverse_0->references,"1",Node_2004_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2004_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2004_retrieved_reference_46_0);
*Node_2004_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_46_0_typed_id,Node_2004_ref_node_target_id_46_0);
*Node_2004_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_46_false_0_is_inverse, Node_2004_ref_node_target_id_46_0);
*Node_2004_retrieved_reference_46_0_target_id = 2005;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_46_0_target_id, Node_2004_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_46_0->references,"1",Node_2004_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_46_1;
ishidaopcua_NODE* Node_2004_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2004_retrieved_reference_46_1);
*Node_2004_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_46_1_typed_id,Node_2004_ref_node_target_id_46_1);
*Node_2004_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_46_false_1_is_inverse, Node_2004_ref_node_target_id_46_1);
*Node_2004_retrieved_reference_46_1_target_id = 2006;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_46_1_target_id, Node_2004_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_46_1->references,"2",Node_2004_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_46_2;
ishidaopcua_NODE* Node_2004_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2004_retrieved_reference_46_2);
*Node_2004_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_46_2_typed_id,Node_2004_ref_node_target_id_46_2);
*Node_2004_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_46_false_2_is_inverse, Node_2004_ref_node_target_id_46_2);
*Node_2004_retrieved_reference_46_2_target_id = 15003;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_46_2_target_id, Node_2004_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_46_2->references,"3",Node_2004_ref_node_target_id_46_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_0);
*Node_2004_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_0_typed_id,Node_2004_ref_node_target_id_47_0);
*Node_2004_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_0_is_inverse, Node_2004_ref_node_target_id_47_0);
*Node_2004_retrieved_reference_47_0_target_id = 2007;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_0_target_id, Node_2004_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_0->references,"1",Node_2004_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_46_3;
ishidaopcua_NODE* Node_2004_ref_node_target_id_46_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2004_retrieved_reference_46_3);
*Node_2004_retrieved_reference_46_3_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_46_3_typed_id,Node_2004_ref_node_target_id_46_3);
*Node_2004_retrieved_reference_46_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_46_false_3_is_inverse, Node_2004_ref_node_target_id_46_3);
*Node_2004_retrieved_reference_46_3_target_id = 2008;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_46_3_target_id, Node_2004_ref_node_target_id_46_3);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_46_3->references,"4",Node_2004_ref_node_target_id_46_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_46_4;
ishidaopcua_NODE* Node_2004_ref_node_target_id_46_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2004_retrieved_reference_46_4);
*Node_2004_retrieved_reference_46_4_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_46_4_typed_id,Node_2004_ref_node_target_id_46_4);
*Node_2004_retrieved_reference_46_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_46_false_4_is_inverse, Node_2004_ref_node_target_id_46_4);
*Node_2004_retrieved_reference_46_4_target_id = 2742;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_46_4_target_id, Node_2004_ref_node_target_id_46_4);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_46_4->references,"5",Node_2004_ref_node_target_id_46_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_46_5;
ishidaopcua_NODE* Node_2004_ref_node_target_id_46_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2004_retrieved_reference_46_5);
*Node_2004_retrieved_reference_46_5_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_46_5_typed_id,Node_2004_ref_node_target_id_46_5);
*Node_2004_retrieved_reference_46_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_46_false_5_is_inverse, Node_2004_ref_node_target_id_46_5);
*Node_2004_retrieved_reference_46_5_target_id = 12882;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_46_5_target_id, Node_2004_ref_node_target_id_46_5);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_46_5->references,"6",Node_2004_ref_node_target_id_46_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_46_6;
ishidaopcua_NODE* Node_2004_ref_node_target_id_46_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_46_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2004_retrieved_reference_46_6);
*Node_2004_retrieved_reference_46_6_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_46_6_typed_id,Node_2004_ref_node_target_id_46_6);
*Node_2004_retrieved_reference_46_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_46_false_6_is_inverse, Node_2004_ref_node_target_id_46_6);
*Node_2004_retrieved_reference_46_6_target_id = 17612;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_46_6_target_id, Node_2004_ref_node_target_id_46_6);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_46_6->references,"7",Node_2004_ref_node_target_id_46_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_1);
*Node_2004_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_1_typed_id,Node_2004_ref_node_target_id_47_1);
*Node_2004_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_1_is_inverse, Node_2004_ref_node_target_id_47_1);
*Node_2004_retrieved_reference_47_1_target_id = 2009;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_1_target_id, Node_2004_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_1->references,"2",Node_2004_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_2);
*Node_2004_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_2_typed_id,Node_2004_ref_node_target_id_47_2);
*Node_2004_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_2_is_inverse, Node_2004_ref_node_target_id_47_2);
*Node_2004_retrieved_reference_47_2_target_id = 2010;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_2_target_id, Node_2004_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_2->references,"3",Node_2004_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_3);
*Node_2004_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_3_typed_id,Node_2004_ref_node_target_id_47_3);
*Node_2004_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_3_is_inverse, Node_2004_ref_node_target_id_47_3);
*Node_2004_retrieved_reference_47_3_target_id = 2011;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_3_target_id, Node_2004_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_3->references,"4",Node_2004_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_4);
*Node_2004_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_4_typed_id,Node_2004_ref_node_target_id_47_4);
*Node_2004_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_4_is_inverse, Node_2004_ref_node_target_id_47_4);
*Node_2004_retrieved_reference_47_4_target_id = 2012;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_4_target_id, Node_2004_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_4->references,"5",Node_2004_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_5;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_5);
*Node_2004_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_5_typed_id,Node_2004_ref_node_target_id_47_5);
*Node_2004_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_5_is_inverse, Node_2004_ref_node_target_id_47_5);
*Node_2004_retrieved_reference_47_5_target_id = 11527;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_5_target_id, Node_2004_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_5->references,"6",Node_2004_ref_node_target_id_47_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_6;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_6);
*Node_2004_retrieved_reference_47_6_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_6_typed_id,Node_2004_ref_node_target_id_47_6);
*Node_2004_retrieved_reference_47_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_6_is_inverse, Node_2004_ref_node_target_id_47_6);
*Node_2004_retrieved_reference_47_6_target_id = 11489;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_6_target_id, Node_2004_ref_node_target_id_47_6);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_6->references,"7",Node_2004_ref_node_target_id_47_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_7;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_7);
*Node_2004_retrieved_reference_47_7_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_7_typed_id,Node_2004_ref_node_target_id_47_7);
*Node_2004_retrieved_reference_47_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_7_is_inverse, Node_2004_ref_node_target_id_47_7);
*Node_2004_retrieved_reference_47_7_target_id = 12871;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_7_target_id, Node_2004_ref_node_target_id_47_7);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_7->references,"8",Node_2004_ref_node_target_id_47_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_8;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_8);
*Node_2004_retrieved_reference_47_8_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_8_typed_id,Node_2004_ref_node_target_id_47_8);
*Node_2004_retrieved_reference_47_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_8_is_inverse, Node_2004_ref_node_target_id_47_8);
*Node_2004_retrieved_reference_47_8_target_id = 12746;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_8_target_id, Node_2004_ref_node_target_id_47_8);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_8->references,"9",Node_2004_ref_node_target_id_47_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2004_retrieved_reference_47_9;
ishidaopcua_NODE* Node_2004_ref_node_target_id_47_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2004_retrieved_reference_47_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2004_retrieved_reference_47_9);
*Node_2004_retrieved_reference_47_9_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2004_retrieved_reference_47_9_typed_id,Node_2004_ref_node_target_id_47_9);
*Node_2004_retrieved_reference_47_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2004_retrieved_reference_47_false_9_is_inverse, Node_2004_ref_node_target_id_47_9);
*Node_2004_retrieved_reference_47_9_target_id = 12883;
ishidaopcua_node_set_target_id(Node_2004_retrieved_reference_47_9_target_id, Node_2004_ref_node_target_id_47_9);
ishidaeutz_put_hashmap(Node_2004_retrieved_reference_47_9->references,"10",Node_2004_ref_node_target_id_47_9); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerDiagnosticsSummaryType ********/


case 2150 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2150;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerDiagnosticsSummaryType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerDiagnosticsSummaryType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 859;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 2150 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_2150_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_2150_retrieved_reference_45_inverse_0);
*Node_2150_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_45_inverse_0_typed_id,Node_2150_ref_node_target_id_45_inverse_0);
*Node_2150_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_45_inverse_true_0_is_inverse, Node_2150_ref_node_target_id_45_inverse_0);
*Node_2150_retrieved_reference_45_inverse_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_45_inverse_0_target_id, Node_2150_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_45_inverse_0->references,"1",Node_2150_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_0);
*Node_2150_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_0_typed_id,Node_2150_ref_node_target_id_47_0);
*Node_2150_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_0_is_inverse, Node_2150_ref_node_target_id_47_0);
*Node_2150_retrieved_reference_47_0_target_id = 2151;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_0_target_id, Node_2150_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_0->references,"1",Node_2150_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_1);
*Node_2150_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_1_typed_id,Node_2150_ref_node_target_id_47_1);
*Node_2150_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_1_is_inverse, Node_2150_ref_node_target_id_47_1);
*Node_2150_retrieved_reference_47_1_target_id = 2152;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_1_target_id, Node_2150_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_1->references,"2",Node_2150_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_2);
*Node_2150_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_2_typed_id,Node_2150_ref_node_target_id_47_2);
*Node_2150_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_2_is_inverse, Node_2150_ref_node_target_id_47_2);
*Node_2150_retrieved_reference_47_2_target_id = 2153;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_2_target_id, Node_2150_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_2->references,"3",Node_2150_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_3);
*Node_2150_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_3_typed_id,Node_2150_ref_node_target_id_47_3);
*Node_2150_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_3_is_inverse, Node_2150_ref_node_target_id_47_3);
*Node_2150_retrieved_reference_47_3_target_id = 2154;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_3_target_id, Node_2150_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_3->references,"4",Node_2150_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_4);
*Node_2150_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_4_typed_id,Node_2150_ref_node_target_id_47_4);
*Node_2150_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_4_is_inverse, Node_2150_ref_node_target_id_47_4);
*Node_2150_retrieved_reference_47_4_target_id = 2155;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_4_target_id, Node_2150_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_4->references,"5",Node_2150_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_5;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_5);
*Node_2150_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_5_typed_id,Node_2150_ref_node_target_id_47_5);
*Node_2150_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_5_is_inverse, Node_2150_ref_node_target_id_47_5);
*Node_2150_retrieved_reference_47_5_target_id = 2156;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_5_target_id, Node_2150_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_5->references,"6",Node_2150_ref_node_target_id_47_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_6;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_6);
*Node_2150_retrieved_reference_47_6_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_6_typed_id,Node_2150_ref_node_target_id_47_6);
*Node_2150_retrieved_reference_47_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_6_is_inverse, Node_2150_ref_node_target_id_47_6);
*Node_2150_retrieved_reference_47_6_target_id = 2157;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_6_target_id, Node_2150_ref_node_target_id_47_6);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_6->references,"7",Node_2150_ref_node_target_id_47_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_7;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_7);
*Node_2150_retrieved_reference_47_7_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_7_typed_id,Node_2150_ref_node_target_id_47_7);
*Node_2150_retrieved_reference_47_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_7_is_inverse, Node_2150_ref_node_target_id_47_7);
*Node_2150_retrieved_reference_47_7_target_id = 2159;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_7_target_id, Node_2150_ref_node_target_id_47_7);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_7->references,"8",Node_2150_ref_node_target_id_47_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_8;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_8);
*Node_2150_retrieved_reference_47_8_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_8_typed_id,Node_2150_ref_node_target_id_47_8);
*Node_2150_retrieved_reference_47_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_8_is_inverse, Node_2150_ref_node_target_id_47_8);
*Node_2150_retrieved_reference_47_8_target_id = 2160;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_8_target_id, Node_2150_ref_node_target_id_47_8);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_8->references,"9",Node_2150_ref_node_target_id_47_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_9;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_9);
*Node_2150_retrieved_reference_47_9_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_9_typed_id,Node_2150_ref_node_target_id_47_9);
*Node_2150_retrieved_reference_47_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_9_is_inverse, Node_2150_ref_node_target_id_47_9);
*Node_2150_retrieved_reference_47_9_target_id = 2161;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_9_target_id, Node_2150_ref_node_target_id_47_9);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_9->references,"10",Node_2150_ref_node_target_id_47_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_10;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_10);
*Node_2150_retrieved_reference_47_10_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_10_typed_id,Node_2150_ref_node_target_id_47_10);
*Node_2150_retrieved_reference_47_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_10_is_inverse, Node_2150_ref_node_target_id_47_10);
*Node_2150_retrieved_reference_47_10_target_id = 2162;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_10_target_id, Node_2150_ref_node_target_id_47_10);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_10->references,"11",Node_2150_ref_node_target_id_47_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2150_retrieved_reference_47_11;
ishidaopcua_NODE* Node_2150_ref_node_target_id_47_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2150_retrieved_reference_47_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2150_retrieved_reference_47_11);
*Node_2150_retrieved_reference_47_11_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2150_retrieved_reference_47_11_typed_id,Node_2150_ref_node_target_id_47_11);
*Node_2150_retrieved_reference_47_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2150_retrieved_reference_47_false_11_is_inverse, Node_2150_ref_node_target_id_47_11);
*Node_2150_retrieved_reference_47_11_target_id = 2163;
ishidaopcua_node_set_target_id(Node_2150_retrieved_reference_47_11_target_id, Node_2150_ref_node_target_id_47_11);
ishidaeutz_put_hashmap(Node_2150_retrieved_reference_47_11->references,"12",Node_2150_ref_node_target_id_47_11); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SecurityRejectedSessionCount ********/


case 2154 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2154;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SecurityRejectedSessionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SecurityRejectedSessionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2154 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2154_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2154_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2154_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2154_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2154_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2154_retrieved_reference_47_inverse_0);
*Node_2154_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2154_retrieved_reference_47_inverse_0_typed_id,Node_2154_ref_node_target_id_47_inverse_0);
*Node_2154_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2154_retrieved_reference_47_inverse_true_0_is_inverse, Node_2154_ref_node_target_id_47_inverse_0);
*Node_2154_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2154_retrieved_reference_47_inverse_0_target_id, Node_2154_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2154_retrieved_reference_47_inverse_0->references,"1",Node_2154_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2154_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2154_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2154_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2154_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2154_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2154_retrieved_reference_40_0);
*Node_2154_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2154_retrieved_reference_40_0_typed_id,Node_2154_ref_node_target_id_40_0);
*Node_2154_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2154_retrieved_reference_40_false_0_is_inverse, Node_2154_ref_node_target_id_40_0);
*Node_2154_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2154_retrieved_reference_40_0_target_id, Node_2154_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2154_retrieved_reference_40_0->references,"1",Node_2154_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2154_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2154_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2154_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2154_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2154_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2154_retrieved_reference_37_0);
*Node_2154_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2154_retrieved_reference_37_0_typed_id,Node_2154_ref_node_target_id_37_0);
*Node_2154_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2154_retrieved_reference_37_false_0_is_inverse, Node_2154_ref_node_target_id_37_0);
*Node_2154_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2154_retrieved_reference_37_0_target_id, Node_2154_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2154_retrieved_reference_37_0->references,"1",Node_2154_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* PublishingIntervalCount ********/


case 2159 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2159;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("PublishingIntervalCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("PublishingIntervalCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2159 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2159_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2159_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2159_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2159_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2159_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2159_retrieved_reference_47_inverse_0);
*Node_2159_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2159_retrieved_reference_47_inverse_0_typed_id,Node_2159_ref_node_target_id_47_inverse_0);
*Node_2159_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2159_retrieved_reference_47_inverse_true_0_is_inverse, Node_2159_ref_node_target_id_47_inverse_0);
*Node_2159_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2159_retrieved_reference_47_inverse_0_target_id, Node_2159_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2159_retrieved_reference_47_inverse_0->references,"1",Node_2159_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2159_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2159_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2159_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2159_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2159_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2159_retrieved_reference_40_0);
*Node_2159_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2159_retrieved_reference_40_0_typed_id,Node_2159_ref_node_target_id_40_0);
*Node_2159_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2159_retrieved_reference_40_false_0_is_inverse, Node_2159_ref_node_target_id_40_0);
*Node_2159_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2159_retrieved_reference_40_0_target_id, Node_2159_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2159_retrieved_reference_40_0->references,"1",Node_2159_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2159_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2159_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2159_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2159_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2159_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2159_retrieved_reference_37_0);
*Node_2159_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2159_retrieved_reference_37_0_typed_id,Node_2159_ref_node_target_id_37_0);
*Node_2159_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2159_retrieved_reference_37_false_0_is_inverse, Node_2159_ref_node_target_id_37_0);
*Node_2159_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2159_retrieved_reference_37_0_target_id, Node_2159_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2159_retrieved_reference_37_0->references,"1",Node_2159_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SecurityRejectedRequestsCount ********/


case 2162 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2162;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SecurityRejectedRequestsCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SecurityRejectedRequestsCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2162 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2162_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2162_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2162_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2162_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2162_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2162_retrieved_reference_47_inverse_0);
*Node_2162_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2162_retrieved_reference_47_inverse_0_typed_id,Node_2162_ref_node_target_id_47_inverse_0);
*Node_2162_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2162_retrieved_reference_47_inverse_true_0_is_inverse, Node_2162_ref_node_target_id_47_inverse_0);
*Node_2162_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2162_retrieved_reference_47_inverse_0_target_id, Node_2162_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2162_retrieved_reference_47_inverse_0->references,"1",Node_2162_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2162_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2162_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2162_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2162_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2162_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2162_retrieved_reference_40_0);
*Node_2162_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2162_retrieved_reference_40_0_typed_id,Node_2162_ref_node_target_id_40_0);
*Node_2162_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2162_retrieved_reference_40_false_0_is_inverse, Node_2162_ref_node_target_id_40_0);
*Node_2162_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2162_retrieved_reference_40_0_target_id, Node_2162_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2162_retrieved_reference_40_0->references,"1",Node_2162_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2162_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2162_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2162_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2162_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2162_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2162_retrieved_reference_37_0);
*Node_2162_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2162_retrieved_reference_37_0_typed_id,Node_2162_ref_node_target_id_37_0);
*Node_2162_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2162_retrieved_reference_37_false_0_is_inverse, Node_2162_ref_node_target_id_37_0);
*Node_2162_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2162_retrieved_reference_37_0_target_id, Node_2162_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2162_retrieved_reference_37_0->references,"1",Node_2162_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* RejectedRequestsCount ********/


case 2163 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2163;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("RejectedRequestsCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("RejectedRequestsCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2163 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2163_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2163_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2163_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2163_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2163_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2163_retrieved_reference_47_inverse_0);
*Node_2163_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2163_retrieved_reference_47_inverse_0_typed_id,Node_2163_ref_node_target_id_47_inverse_0);
*Node_2163_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2163_retrieved_reference_47_inverse_true_0_is_inverse, Node_2163_ref_node_target_id_47_inverse_0);
*Node_2163_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2163_retrieved_reference_47_inverse_0_target_id, Node_2163_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2163_retrieved_reference_47_inverse_0->references,"1",Node_2163_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2163_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2163_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2163_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2163_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2163_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2163_retrieved_reference_40_0);
*Node_2163_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2163_retrieved_reference_40_0_typed_id,Node_2163_ref_node_target_id_40_0);
*Node_2163_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2163_retrieved_reference_40_false_0_is_inverse, Node_2163_ref_node_target_id_40_0);
*Node_2163_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2163_retrieved_reference_40_0_target_id, Node_2163_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2163_retrieved_reference_40_0->references,"1",Node_2163_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2163_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2163_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2163_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2163_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2163_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2163_retrieved_reference_37_0);
*Node_2163_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2163_retrieved_reference_37_0_typed_id,Node_2163_ref_node_target_id_37_0);
*Node_2163_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2163_retrieved_reference_37_false_0_is_inverse, Node_2163_ref_node_target_id_37_0);
*Node_2163_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2163_retrieved_reference_37_0_target_id, Node_2163_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2163_retrieved_reference_37_0->references,"1",Node_2163_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Server ********/


case 2253 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2253;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Server", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Server", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 2253 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_2253_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2253_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2253_retrieved_reference_40_0);
*Node_2253_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_40_0_typed_id,Node_2253_ref_node_target_id_40_0);
*Node_2253_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_40_false_0_is_inverse, Node_2253_ref_node_target_id_40_0);
*Node_2253_retrieved_reference_40_0_target_id = 2004;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_40_0_target_id, Node_2253_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_40_0->references,"1",Node_2253_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_2253_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_2253_retrieved_reference_35_inverse_0);
*Node_2253_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_35_inverse_0_typed_id,Node_2253_ref_node_target_id_35_inverse_0);
*Node_2253_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_35_inverse_true_0_is_inverse, Node_2253_ref_node_target_id_35_inverse_0);
*Node_2253_retrieved_reference_35_inverse_0_target_id = 85;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_35_inverse_0_target_id, Node_2253_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_35_inverse_0->references,"1",Node_2253_ref_node_target_id_35_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2253_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2253_retrieved_reference_46_0);
*Node_2253_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_46_0_typed_id,Node_2253_ref_node_target_id_46_0);
*Node_2253_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_46_false_0_is_inverse, Node_2253_ref_node_target_id_46_0);
*Node_2253_retrieved_reference_46_0_target_id = 2254;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_46_0_target_id, Node_2253_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_46_0->references,"1",Node_2253_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_46_1;
ishidaopcua_NODE* Node_2253_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2253_retrieved_reference_46_1);
*Node_2253_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_46_1_typed_id,Node_2253_ref_node_target_id_46_1);
*Node_2253_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_46_false_1_is_inverse, Node_2253_ref_node_target_id_46_1);
*Node_2253_retrieved_reference_46_1_target_id = 2255;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_46_1_target_id, Node_2253_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_46_1->references,"2",Node_2253_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_0);
*Node_2253_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_0_typed_id,Node_2253_ref_node_target_id_47_0);
*Node_2253_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_0_is_inverse, Node_2253_ref_node_target_id_47_0);
*Node_2253_retrieved_reference_47_0_target_id = 2256;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_0_target_id, Node_2253_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_0->references,"1",Node_2253_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_46_2;
ishidaopcua_NODE* Node_2253_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2253_retrieved_reference_46_2);
*Node_2253_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_46_2_typed_id,Node_2253_ref_node_target_id_46_2);
*Node_2253_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_46_false_2_is_inverse, Node_2253_ref_node_target_id_46_2);
*Node_2253_retrieved_reference_46_2_target_id = 2267;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_46_2_target_id, Node_2253_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_46_2->references,"3",Node_2253_ref_node_target_id_46_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_46_3;
ishidaopcua_NODE* Node_2253_ref_node_target_id_46_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2253_retrieved_reference_46_3);
*Node_2253_retrieved_reference_46_3_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_46_3_typed_id,Node_2253_ref_node_target_id_46_3);
*Node_2253_retrieved_reference_46_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_46_false_3_is_inverse, Node_2253_ref_node_target_id_46_3);
*Node_2253_retrieved_reference_46_3_target_id = 2994;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_46_3_target_id, Node_2253_ref_node_target_id_46_3);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_46_3->references,"4",Node_2253_ref_node_target_id_46_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_46_4;
ishidaopcua_NODE* Node_2253_ref_node_target_id_46_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2253_retrieved_reference_46_4);
*Node_2253_retrieved_reference_46_4_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_46_4_typed_id,Node_2253_ref_node_target_id_46_4);
*Node_2253_retrieved_reference_46_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_46_false_4_is_inverse, Node_2253_ref_node_target_id_46_4);
*Node_2253_retrieved_reference_46_4_target_id = 12885;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_46_4_target_id, Node_2253_ref_node_target_id_46_4);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_46_4->references,"5",Node_2253_ref_node_target_id_46_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_1);
*Node_2253_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_1_typed_id,Node_2253_ref_node_target_id_47_1);
*Node_2253_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_1_is_inverse, Node_2253_ref_node_target_id_47_1);
*Node_2253_retrieved_reference_47_1_target_id = 2268;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_1_target_id, Node_2253_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_1->references,"2",Node_2253_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_2);
*Node_2253_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_2_typed_id,Node_2253_ref_node_target_id_47_2);
*Node_2253_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_2_is_inverse, Node_2253_ref_node_target_id_47_2);
*Node_2253_retrieved_reference_47_2_target_id = 2274;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_2_target_id, Node_2253_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_2->references,"3",Node_2253_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_3);
*Node_2253_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_3_typed_id,Node_2253_ref_node_target_id_47_3);
*Node_2253_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_3_is_inverse, Node_2253_ref_node_target_id_47_3);
*Node_2253_retrieved_reference_47_3_target_id = 2295;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_3_target_id, Node_2253_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_3->references,"4",Node_2253_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_4);
*Node_2253_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_4_typed_id,Node_2253_ref_node_target_id_47_4);
*Node_2253_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_4_is_inverse, Node_2253_ref_node_target_id_47_4);
*Node_2253_retrieved_reference_47_4_target_id = 2296;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_4_target_id, Node_2253_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_4->references,"5",Node_2253_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_5;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_5);
*Node_2253_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_5_typed_id,Node_2253_ref_node_target_id_47_5);
*Node_2253_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_5_is_inverse, Node_2253_ref_node_target_id_47_5);
*Node_2253_retrieved_reference_47_5_target_id = 11715;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_5_target_id, Node_2253_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_5->references,"6",Node_2253_ref_node_target_id_47_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_6;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_6);
*Node_2253_retrieved_reference_47_6_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_6_typed_id,Node_2253_ref_node_target_id_47_6);
*Node_2253_retrieved_reference_47_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_6_is_inverse, Node_2253_ref_node_target_id_47_6);
*Node_2253_retrieved_reference_47_6_target_id = 11492;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_6_target_id, Node_2253_ref_node_target_id_47_6);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_6->references,"7",Node_2253_ref_node_target_id_47_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_7;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_7);
*Node_2253_retrieved_reference_47_7_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_7_typed_id,Node_2253_ref_node_target_id_47_7);
*Node_2253_retrieved_reference_47_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_7_is_inverse, Node_2253_ref_node_target_id_47_7);
*Node_2253_retrieved_reference_47_7_target_id = 12873;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_7_target_id, Node_2253_ref_node_target_id_47_7);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_7->references,"8",Node_2253_ref_node_target_id_47_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_8;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_8);
*Node_2253_retrieved_reference_47_8_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_8_typed_id,Node_2253_ref_node_target_id_47_8);
*Node_2253_retrieved_reference_47_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_8_is_inverse, Node_2253_ref_node_target_id_47_8);
*Node_2253_retrieved_reference_47_8_target_id = 12749;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_8_target_id, Node_2253_ref_node_target_id_47_8);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_8->references,"9",Node_2253_ref_node_target_id_47_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_9;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_9);
*Node_2253_retrieved_reference_47_9_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_9_typed_id,Node_2253_ref_node_target_id_47_9);
*Node_2253_retrieved_reference_47_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_9_is_inverse, Node_2253_ref_node_target_id_47_9);
*Node_2253_retrieved_reference_47_9_target_id = 12886;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_9_target_id, Node_2253_ref_node_target_id_47_9);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_9->references,"10",Node_2253_ref_node_target_id_47_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_46_5;
ishidaopcua_NODE* Node_2253_ref_node_target_id_46_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_46_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2253_retrieved_reference_46_5);
*Node_2253_retrieved_reference_46_5_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_46_5_typed_id,Node_2253_ref_node_target_id_46_5);
*Node_2253_retrieved_reference_46_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_46_false_5_is_inverse, Node_2253_ref_node_target_id_46_5);
*Node_2253_retrieved_reference_46_5_target_id = 16313;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_46_5_target_id, Node_2253_ref_node_target_id_46_5);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_46_5->references,"6",Node_2253_ref_node_target_id_46_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_10;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_10);
*Node_2253_retrieved_reference_47_10_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_10_typed_id,Node_2253_ref_node_target_id_47_10);
*Node_2253_retrieved_reference_47_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_10_is_inverse, Node_2253_ref_node_target_id_47_10);
*Node_2253_retrieved_reference_47_10_target_id = 12637;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_10_target_id, Node_2253_ref_node_target_id_47_10);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_10->references,"11",Node_2253_ref_node_target_id_47_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2253_retrieved_reference_47_11;
ishidaopcua_NODE* Node_2253_ref_node_target_id_47_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2253_retrieved_reference_47_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2253_retrieved_reference_47_11);
*Node_2253_retrieved_reference_47_11_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2253_retrieved_reference_47_11_typed_id,Node_2253_ref_node_target_id_47_11);
*Node_2253_retrieved_reference_47_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2253_retrieved_reference_47_false_11_is_inverse, Node_2253_ref_node_target_id_47_11);
*Node_2253_retrieved_reference_47_11_target_id = 14443;
ishidaopcua_node_set_target_id(Node_2253_retrieved_reference_47_11_target_id, Node_2253_ref_node_target_id_47_11);
ishidaeutz_put_hashmap(Node_2253_retrieved_reference_47_11->references,"12",Node_2253_ref_node_target_id_47_11); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerCapabilities ********/


case 2268 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2268;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerCapabilities", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerCapabilities", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes capabilities supported by the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 2268 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2268_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2268_retrieved_reference_47_inverse_0);
*Node_2268_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_47_inverse_0_typed_id,Node_2268_ref_node_target_id_47_inverse_0);
*Node_2268_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_47_inverse_true_0_is_inverse, Node_2268_ref_node_target_id_47_inverse_0);
*Node_2268_retrieved_reference_47_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_47_inverse_0_target_id, Node_2268_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_47_inverse_0->references,"1",Node_2268_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2268_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2268_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2268_retrieved_reference_40_0);
*Node_2268_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_40_0_typed_id,Node_2268_ref_node_target_id_40_0);
*Node_2268_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_40_false_0_is_inverse, Node_2268_ref_node_target_id_40_0);
*Node_2268_retrieved_reference_40_0_target_id = 2013;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_40_0_target_id, Node_2268_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_40_0->references,"1",Node_2268_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_0);
*Node_2268_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_0_typed_id,Node_2268_ref_node_target_id_46_0);
*Node_2268_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_0_is_inverse, Node_2268_ref_node_target_id_46_0);
*Node_2268_retrieved_reference_46_0_target_id = 2269;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_0_target_id, Node_2268_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_0->references,"1",Node_2268_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_1;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_1);
*Node_2268_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_1_typed_id,Node_2268_ref_node_target_id_46_1);
*Node_2268_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_1_is_inverse, Node_2268_ref_node_target_id_46_1);
*Node_2268_retrieved_reference_46_1_target_id = 2271;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_1_target_id, Node_2268_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_1->references,"2",Node_2268_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_2;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_2);
*Node_2268_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_2_typed_id,Node_2268_ref_node_target_id_46_2);
*Node_2268_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_2_is_inverse, Node_2268_ref_node_target_id_46_2);
*Node_2268_retrieved_reference_46_2_target_id = 2272;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_2_target_id, Node_2268_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_2->references,"3",Node_2268_ref_node_target_id_46_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_3;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_3);
*Node_2268_retrieved_reference_46_3_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_3_typed_id,Node_2268_ref_node_target_id_46_3);
*Node_2268_retrieved_reference_46_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_3_is_inverse, Node_2268_ref_node_target_id_46_3);
*Node_2268_retrieved_reference_46_3_target_id = 2735;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_3_target_id, Node_2268_ref_node_target_id_46_3);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_3->references,"4",Node_2268_ref_node_target_id_46_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_4;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_4);
*Node_2268_retrieved_reference_46_4_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_4_typed_id,Node_2268_ref_node_target_id_46_4);
*Node_2268_retrieved_reference_46_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_4_is_inverse, Node_2268_ref_node_target_id_46_4);
*Node_2268_retrieved_reference_46_4_target_id = 2736;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_4_target_id, Node_2268_ref_node_target_id_46_4);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_4->references,"5",Node_2268_ref_node_target_id_46_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_5;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_5);
*Node_2268_retrieved_reference_46_5_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_5_typed_id,Node_2268_ref_node_target_id_46_5);
*Node_2268_retrieved_reference_46_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_5_is_inverse, Node_2268_ref_node_target_id_46_5);
*Node_2268_retrieved_reference_46_5_target_id = 2737;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_5_target_id, Node_2268_ref_node_target_id_46_5);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_5->references,"6",Node_2268_ref_node_target_id_46_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_6;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_6);
*Node_2268_retrieved_reference_46_6_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_6_typed_id,Node_2268_ref_node_target_id_46_6);
*Node_2268_retrieved_reference_46_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_6_is_inverse, Node_2268_ref_node_target_id_46_6);
*Node_2268_retrieved_reference_46_6_target_id = 3704;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_6_target_id, Node_2268_ref_node_target_id_46_6);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_6->references,"7",Node_2268_ref_node_target_id_46_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_7;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_7);
*Node_2268_retrieved_reference_46_7_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_7_typed_id,Node_2268_ref_node_target_id_46_7);
*Node_2268_retrieved_reference_46_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_7_is_inverse, Node_2268_ref_node_target_id_46_7);
*Node_2268_retrieved_reference_46_7_target_id = 11702;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_7_target_id, Node_2268_ref_node_target_id_46_7);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_7->references,"8",Node_2268_ref_node_target_id_46_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_8;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_8);
*Node_2268_retrieved_reference_46_8_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_8_typed_id,Node_2268_ref_node_target_id_46_8);
*Node_2268_retrieved_reference_46_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_8_is_inverse, Node_2268_ref_node_target_id_46_8);
*Node_2268_retrieved_reference_46_8_target_id = 11703;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_8_target_id, Node_2268_ref_node_target_id_46_8);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_8->references,"9",Node_2268_ref_node_target_id_46_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_46_9;
ishidaopcua_NODE* Node_2268_ref_node_target_id_46_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_46_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2268_retrieved_reference_46_9);
*Node_2268_retrieved_reference_46_9_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_46_9_typed_id,Node_2268_ref_node_target_id_46_9);
*Node_2268_retrieved_reference_46_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_46_false_9_is_inverse, Node_2268_ref_node_target_id_46_9);
*Node_2268_retrieved_reference_46_9_target_id = 12911;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_46_9_target_id, Node_2268_ref_node_target_id_46_9);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_46_9->references,"10",Node_2268_ref_node_target_id_46_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2268_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2268_retrieved_reference_47_0);
*Node_2268_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_47_0_typed_id,Node_2268_ref_node_target_id_47_0);
*Node_2268_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_47_false_0_is_inverse, Node_2268_ref_node_target_id_47_0);
*Node_2268_retrieved_reference_47_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_47_0_target_id, Node_2268_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_47_0->references,"1",Node_2268_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2268_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2268_retrieved_reference_47_1);
*Node_2268_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_47_1_typed_id,Node_2268_ref_node_target_id_47_1);
*Node_2268_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_47_false_1_is_inverse, Node_2268_ref_node_target_id_47_1);
*Node_2268_retrieved_reference_47_1_target_id = 2996;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_47_1_target_id, Node_2268_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_47_1->references,"2",Node_2268_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2268_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2268_retrieved_reference_47_2);
*Node_2268_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_47_2_typed_id,Node_2268_ref_node_target_id_47_2);
*Node_2268_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_47_false_2_is_inverse, Node_2268_ref_node_target_id_47_2);
*Node_2268_retrieved_reference_47_2_target_id = 2997;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_47_2_target_id, Node_2268_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_47_2->references,"3",Node_2268_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2268_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2268_retrieved_reference_47_3);
*Node_2268_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_47_3_typed_id,Node_2268_ref_node_target_id_47_3);
*Node_2268_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_47_false_3_is_inverse, Node_2268_ref_node_target_id_47_3);
*Node_2268_retrieved_reference_47_3_target_id = 15606;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_47_3_target_id, Node_2268_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_47_3->references,"4",Node_2268_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2268_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2268_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2268_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2268_retrieved_reference_47_4);
*Node_2268_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2268_retrieved_reference_47_4_typed_id,Node_2268_ref_node_target_id_47_4);
*Node_2268_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2268_retrieved_reference_47_false_4_is_inverse, Node_2268_ref_node_target_id_47_4);
*Node_2268_retrieved_reference_47_4_target_id = 11192;
ishidaopcua_node_set_target_id(Node_2268_retrieved_reference_47_4_target_id, Node_2268_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2268_retrieved_reference_47_4->references,"5",Node_2268_ref_node_target_id_47_4); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* GetMonitoredItems ********/


case 11492 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11492;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_METHOD;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("GetMonitoredItems", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("GetMonitoredItems", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_executable = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_executable = 1;
ishidaopcua_node_set_executable(universal_executable, universal_node);
universal_user_executable = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_user_executable = 1;
ishidaopcua_node_set_user_executable(universal_user_executable, universal_node);
puts("finished adding node >> 11492 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11492_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_11492_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11492_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_11492_retrieved_reference_47_inverse_0);
*Node_11492_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_11492_retrieved_reference_47_inverse_0_typed_id,Node_11492_ref_node_target_id_47_inverse_0);
*Node_11492_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11492_retrieved_reference_47_inverse_true_0_is_inverse, Node_11492_ref_node_target_id_47_inverse_0);
*Node_11492_retrieved_reference_47_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_11492_retrieved_reference_47_inverse_0_target_id, Node_11492_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_11492_retrieved_reference_47_inverse_0->references,"1",Node_11492_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11492_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11492_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11492_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11492_retrieved_reference_40_0);
*Node_11492_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11492_retrieved_reference_40_0_typed_id,Node_11492_ref_node_target_id_40_0);
*Node_11492_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11492_retrieved_reference_40_false_0_is_inverse, Node_11492_ref_node_target_id_40_0);
*Node_11492_retrieved_reference_40_0_target_id = 11489;
ishidaopcua_node_set_target_id(Node_11492_retrieved_reference_40_0_target_id, Node_11492_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11492_retrieved_reference_40_0->references,"1",Node_11492_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11492_retrieved_reference_46_0;
ishidaopcua_NODE* Node_11492_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11492_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11492_retrieved_reference_46_0);
*Node_11492_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11492_retrieved_reference_46_0_typed_id,Node_11492_ref_node_target_id_46_0);
*Node_11492_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11492_retrieved_reference_46_false_0_is_inverse, Node_11492_ref_node_target_id_46_0);
*Node_11492_retrieved_reference_46_0_target_id = 11493;
ishidaopcua_node_set_target_id(Node_11492_retrieved_reference_46_0_target_id, Node_11492_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_11492_retrieved_reference_46_0->references,"1",Node_11492_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11492_retrieved_reference_46_1;
ishidaopcua_NODE* Node_11492_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11492_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11492_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11492_retrieved_reference_46_1);
*Node_11492_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11492_retrieved_reference_46_1_typed_id,Node_11492_ref_node_target_id_46_1);
*Node_11492_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11492_retrieved_reference_46_false_1_is_inverse, Node_11492_ref_node_target_id_46_1);
*Node_11492_retrieved_reference_46_1_target_id = 11494;
ishidaopcua_node_set_target_id(Node_11492_retrieved_reference_46_1_target_id, Node_11492_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_11492_retrieved_reference_46_1->references,"2",Node_11492_ref_node_target_id_46_1); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* OperationLimits ********/


case 11704 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11704;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("OperationLimits", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("OperationLimits", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Defines the limits supported by the server for different operations.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 11704 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_11704_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_11704_retrieved_reference_47_inverse_0);
*Node_11704_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_47_inverse_0_typed_id,Node_11704_ref_node_target_id_47_inverse_0);
*Node_11704_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_47_inverse_true_0_is_inverse, Node_11704_ref_node_target_id_47_inverse_0);
*Node_11704_retrieved_reference_47_inverse_0_target_id = 2268;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_47_inverse_0_target_id, Node_11704_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_47_inverse_0->references,"1",Node_11704_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11704_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11704_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11704_retrieved_reference_40_0);
*Node_11704_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_40_0_typed_id,Node_11704_ref_node_target_id_40_0);
*Node_11704_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_40_false_0_is_inverse, Node_11704_ref_node_target_id_40_0);
*Node_11704_retrieved_reference_40_0_target_id = 11564;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_40_0_target_id, Node_11704_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_40_0->references,"1",Node_11704_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_0;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_0);
*Node_11704_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_0_typed_id,Node_11704_ref_node_target_id_46_0);
*Node_11704_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_0_is_inverse, Node_11704_ref_node_target_id_46_0);
*Node_11704_retrieved_reference_46_0_target_id = 11705;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_0_target_id, Node_11704_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_0->references,"1",Node_11704_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_1;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_1);
*Node_11704_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_1_typed_id,Node_11704_ref_node_target_id_46_1);
*Node_11704_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_1_is_inverse, Node_11704_ref_node_target_id_46_1);
*Node_11704_retrieved_reference_46_1_target_id = 12165;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_1_target_id, Node_11704_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_1->references,"2",Node_11704_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_2;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_2);
*Node_11704_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_2_typed_id,Node_11704_ref_node_target_id_46_2);
*Node_11704_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_2_is_inverse, Node_11704_ref_node_target_id_46_2);
*Node_11704_retrieved_reference_46_2_target_id = 12166;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_2_target_id, Node_11704_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_2->references,"3",Node_11704_ref_node_target_id_46_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_3;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_3);
*Node_11704_retrieved_reference_46_3_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_3_typed_id,Node_11704_ref_node_target_id_46_3);
*Node_11704_retrieved_reference_46_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_3_is_inverse, Node_11704_ref_node_target_id_46_3);
*Node_11704_retrieved_reference_46_3_target_id = 11707;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_3_target_id, Node_11704_ref_node_target_id_46_3);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_3->references,"4",Node_11704_ref_node_target_id_46_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_4;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_4);
*Node_11704_retrieved_reference_46_4_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_4_typed_id,Node_11704_ref_node_target_id_46_4);
*Node_11704_retrieved_reference_46_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_4_is_inverse, Node_11704_ref_node_target_id_46_4);
*Node_11704_retrieved_reference_46_4_target_id = 12167;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_4_target_id, Node_11704_ref_node_target_id_46_4);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_4->references,"5",Node_11704_ref_node_target_id_46_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_5;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_5);
*Node_11704_retrieved_reference_46_5_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_5_typed_id,Node_11704_ref_node_target_id_46_5);
*Node_11704_retrieved_reference_46_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_5_is_inverse, Node_11704_ref_node_target_id_46_5);
*Node_11704_retrieved_reference_46_5_target_id = 12168;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_5_target_id, Node_11704_ref_node_target_id_46_5);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_5->references,"6",Node_11704_ref_node_target_id_46_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_6;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_6);
*Node_11704_retrieved_reference_46_6_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_6_typed_id,Node_11704_ref_node_target_id_46_6);
*Node_11704_retrieved_reference_46_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_6_is_inverse, Node_11704_ref_node_target_id_46_6);
*Node_11704_retrieved_reference_46_6_target_id = 11709;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_6_target_id, Node_11704_ref_node_target_id_46_6);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_6->references,"7",Node_11704_ref_node_target_id_46_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_7;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_7);
*Node_11704_retrieved_reference_46_7_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_7_typed_id,Node_11704_ref_node_target_id_46_7);
*Node_11704_retrieved_reference_46_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_7_is_inverse, Node_11704_ref_node_target_id_46_7);
*Node_11704_retrieved_reference_46_7_target_id = 11710;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_7_target_id, Node_11704_ref_node_target_id_46_7);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_7->references,"8",Node_11704_ref_node_target_id_46_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_8;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_8);
*Node_11704_retrieved_reference_46_8_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_8_typed_id,Node_11704_ref_node_target_id_46_8);
*Node_11704_retrieved_reference_46_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_8_is_inverse, Node_11704_ref_node_target_id_46_8);
*Node_11704_retrieved_reference_46_8_target_id = 11711;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_8_target_id, Node_11704_ref_node_target_id_46_8);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_8->references,"9",Node_11704_ref_node_target_id_46_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_9;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_9);
*Node_11704_retrieved_reference_46_9_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_9_typed_id,Node_11704_ref_node_target_id_46_9);
*Node_11704_retrieved_reference_46_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_9_is_inverse, Node_11704_ref_node_target_id_46_9);
*Node_11704_retrieved_reference_46_9_target_id = 11712;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_9_target_id, Node_11704_ref_node_target_id_46_9);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_9->references,"10",Node_11704_ref_node_target_id_46_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_10;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_10);
*Node_11704_retrieved_reference_46_10_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_10_typed_id,Node_11704_ref_node_target_id_46_10);
*Node_11704_retrieved_reference_46_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_10_is_inverse, Node_11704_ref_node_target_id_46_10);
*Node_11704_retrieved_reference_46_10_target_id = 11713;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_10_target_id, Node_11704_ref_node_target_id_46_10);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_10->references,"11",Node_11704_ref_node_target_id_46_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11704_retrieved_reference_46_11;
ishidaopcua_NODE* Node_11704_ref_node_target_id_46_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11704_retrieved_reference_46_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11704_retrieved_reference_46_11);
*Node_11704_retrieved_reference_46_11_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11704_retrieved_reference_46_11_typed_id,Node_11704_ref_node_target_id_46_11);
*Node_11704_retrieved_reference_46_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11704_retrieved_reference_46_false_11_is_inverse, Node_11704_ref_node_target_id_46_11);
*Node_11704_retrieved_reference_46_11_target_id = 11714;
ishidaopcua_node_set_target_id(Node_11704_retrieved_reference_46_11_target_id, Node_11704_ref_node_target_id_46_11);
ishidaeutz_put_hashmap(Node_11704_retrieved_reference_46_11->references,"12",Node_11704_ref_node_target_id_46_11); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/
