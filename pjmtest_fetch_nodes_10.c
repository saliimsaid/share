#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_10(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* DataTypeDescriptionType ********/


case 69 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 69;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DataTypeDescriptionType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DataTypeDescriptionType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for variable that represents the description of a data type encoding.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 69 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_69_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_69_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_69_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_69_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_69_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_69_retrieved_reference_45_inverse_0);
*Node_69_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_69_retrieved_reference_45_inverse_0_typed_id,Node_69_ref_node_target_id_45_inverse_0);
*Node_69_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_69_retrieved_reference_45_inverse_true_0_is_inverse, Node_69_ref_node_target_id_45_inverse_0);
*Node_69_retrieved_reference_45_inverse_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_69_retrieved_reference_45_inverse_0_target_id, Node_69_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_69_retrieved_reference_45_inverse_0->references,"1",Node_69_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_69_retrieved_reference_46_0;
ishidaopcua_NODE* Node_69_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_69_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_69_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_69_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_69_retrieved_reference_46_0);
*Node_69_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_69_retrieved_reference_46_0_typed_id,Node_69_ref_node_target_id_46_0);
*Node_69_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_69_retrieved_reference_46_false_0_is_inverse, Node_69_ref_node_target_id_46_0);
*Node_69_retrieved_reference_46_0_target_id = 104;
ishidaopcua_node_set_target_id(Node_69_retrieved_reference_46_0_target_id, Node_69_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_69_retrieved_reference_46_0->references,"1",Node_69_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_69_retrieved_reference_46_1;
ishidaopcua_NODE* Node_69_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_69_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_69_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_69_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_69_retrieved_reference_46_1);
*Node_69_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_69_retrieved_reference_46_1_typed_id,Node_69_ref_node_target_id_46_1);
*Node_69_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_69_retrieved_reference_46_false_1_is_inverse, Node_69_ref_node_target_id_46_1);
*Node_69_retrieved_reference_46_1_target_id = 105;
ishidaopcua_node_set_target_id(Node_69_retrieved_reference_46_1_target_id, Node_69_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_69_retrieved_reference_46_1->references,"2",Node_69_ref_node_target_id_46_1); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DataTypeDictionaryType ********/


case 72 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 72;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DataTypeDictionaryType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DataTypeDictionaryType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for variable that represents the collection of data type decriptions.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 15;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 72 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_72_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_72_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_72_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_72_retrieved_reference_45_inverse_0);
*Node_72_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_72_retrieved_reference_45_inverse_0_typed_id,Node_72_ref_node_target_id_45_inverse_0);
*Node_72_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_72_retrieved_reference_45_inverse_true_0_is_inverse, Node_72_ref_node_target_id_45_inverse_0);
*Node_72_retrieved_reference_45_inverse_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_72_retrieved_reference_45_inverse_0_target_id, Node_72_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_72_retrieved_reference_45_inverse_0->references,"1",Node_72_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_72_retrieved_reference_46_0;
ishidaopcua_NODE* Node_72_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_72_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_72_retrieved_reference_46_0);
*Node_72_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_72_retrieved_reference_46_0_typed_id,Node_72_ref_node_target_id_46_0);
*Node_72_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_72_retrieved_reference_46_false_0_is_inverse, Node_72_ref_node_target_id_46_0);
*Node_72_retrieved_reference_46_0_target_id = 106;
ishidaopcua_node_set_target_id(Node_72_retrieved_reference_46_0_target_id, Node_72_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_72_retrieved_reference_46_0->references,"1",Node_72_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_72_retrieved_reference_46_1;
ishidaopcua_NODE* Node_72_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_72_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_72_retrieved_reference_46_1);
*Node_72_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_72_retrieved_reference_46_1_typed_id,Node_72_ref_node_target_id_46_1);
*Node_72_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_72_retrieved_reference_46_false_1_is_inverse, Node_72_ref_node_target_id_46_1);
*Node_72_retrieved_reference_46_1_target_id = 107;
ishidaopcua_node_set_target_id(Node_72_retrieved_reference_46_1_target_id, Node_72_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_72_retrieved_reference_46_1->references,"2",Node_72_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_72_retrieved_reference_46_2;
ishidaopcua_NODE* Node_72_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_72_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_72_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_72_retrieved_reference_46_2);
*Node_72_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_72_retrieved_reference_46_2_typed_id,Node_72_ref_node_target_id_46_2);
*Node_72_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_72_retrieved_reference_46_false_2_is_inverse, Node_72_ref_node_target_id_46_2);
*Node_72_retrieved_reference_46_2_target_id = 15001;
ishidaopcua_node_set_target_id(Node_72_retrieved_reference_46_2_target_id, Node_72_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_72_retrieved_reference_46_2->references,"3",Node_72_ref_node_target_id_46_2); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DataTypeVersion ********/


case 104 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 104;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DataTypeVersion", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DataTypeVersion", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The version number for the data type description.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 104 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_104_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_104_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_104_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_104_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_104_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_104_retrieved_reference_46_inverse_0);
*Node_104_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_104_retrieved_reference_46_inverse_0_typed_id,Node_104_ref_node_target_id_46_inverse_0);
*Node_104_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_104_retrieved_reference_46_inverse_true_0_is_inverse, Node_104_ref_node_target_id_46_inverse_0);
*Node_104_retrieved_reference_46_inverse_0_target_id = 69;
ishidaopcua_node_set_target_id(Node_104_retrieved_reference_46_inverse_0_target_id, Node_104_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_104_retrieved_reference_46_inverse_0->references,"1",Node_104_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_104_retrieved_reference_40_0;
ishidaopcua_NODE* Node_104_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_104_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_104_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_104_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_104_retrieved_reference_40_0);
*Node_104_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_104_retrieved_reference_40_0_typed_id,Node_104_ref_node_target_id_40_0);
*Node_104_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_104_retrieved_reference_40_false_0_is_inverse, Node_104_ref_node_target_id_40_0);
*Node_104_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_104_retrieved_reference_40_0_target_id, Node_104_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_104_retrieved_reference_40_0->references,"1",Node_104_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_104_retrieved_reference_37_0;
ishidaopcua_NODE* Node_104_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_104_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_104_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_104_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_104_retrieved_reference_37_0);
*Node_104_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_104_retrieved_reference_37_0_typed_id,Node_104_ref_node_target_id_37_0);
*Node_104_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_104_retrieved_reference_37_false_0_is_inverse, Node_104_ref_node_target_id_37_0);
*Node_104_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_104_retrieved_reference_37_0_target_id, Node_104_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_104_retrieved_reference_37_0->references,"1",Node_104_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DictionaryFragment ********/


case 105 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 105;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DictionaryFragment", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DictionaryFragment", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A fragment of a data type dictionary that defines the data type.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 15;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 105 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_105_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_105_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_105_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_105_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_105_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_105_retrieved_reference_46_inverse_0);
*Node_105_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_105_retrieved_reference_46_inverse_0_typed_id,Node_105_ref_node_target_id_46_inverse_0);
*Node_105_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_105_retrieved_reference_46_inverse_true_0_is_inverse, Node_105_ref_node_target_id_46_inverse_0);
*Node_105_retrieved_reference_46_inverse_0_target_id = 69;
ishidaopcua_node_set_target_id(Node_105_retrieved_reference_46_inverse_0_target_id, Node_105_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_105_retrieved_reference_46_inverse_0->references,"1",Node_105_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_105_retrieved_reference_40_0;
ishidaopcua_NODE* Node_105_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_105_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_105_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_105_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_105_retrieved_reference_40_0);
*Node_105_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_105_retrieved_reference_40_0_typed_id,Node_105_ref_node_target_id_40_0);
*Node_105_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_105_retrieved_reference_40_false_0_is_inverse, Node_105_ref_node_target_id_40_0);
*Node_105_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_105_retrieved_reference_40_0_target_id, Node_105_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_105_retrieved_reference_40_0->references,"1",Node_105_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_105_retrieved_reference_37_0;
ishidaopcua_NODE* Node_105_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_105_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_105_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_105_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_105_retrieved_reference_37_0);
*Node_105_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_105_retrieved_reference_37_0_typed_id,Node_105_ref_node_target_id_37_0);
*Node_105_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_105_retrieved_reference_37_false_0_is_inverse, Node_105_ref_node_target_id_37_0);
*Node_105_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_105_retrieved_reference_37_0_target_id, Node_105_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_105_retrieved_reference_37_0->references,"1",Node_105_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DataTypeVersion ********/


case 106 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 106;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DataTypeVersion", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DataTypeVersion", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The version number for the data type dictionary.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 106 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_106_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_106_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_106_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_106_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_106_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_106_retrieved_reference_46_inverse_0);
*Node_106_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_106_retrieved_reference_46_inverse_0_typed_id,Node_106_ref_node_target_id_46_inverse_0);
*Node_106_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_106_retrieved_reference_46_inverse_true_0_is_inverse, Node_106_ref_node_target_id_46_inverse_0);
*Node_106_retrieved_reference_46_inverse_0_target_id = 72;
ishidaopcua_node_set_target_id(Node_106_retrieved_reference_46_inverse_0_target_id, Node_106_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_106_retrieved_reference_46_inverse_0->references,"1",Node_106_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_106_retrieved_reference_40_0;
ishidaopcua_NODE* Node_106_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_106_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_106_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_106_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_106_retrieved_reference_40_0);
*Node_106_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_106_retrieved_reference_40_0_typed_id,Node_106_ref_node_target_id_40_0);
*Node_106_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_106_retrieved_reference_40_false_0_is_inverse, Node_106_ref_node_target_id_40_0);
*Node_106_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_106_retrieved_reference_40_0_target_id, Node_106_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_106_retrieved_reference_40_0->references,"1",Node_106_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_106_retrieved_reference_37_0;
ishidaopcua_NODE* Node_106_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_106_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_106_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_106_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_106_retrieved_reference_37_0);
*Node_106_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_106_retrieved_reference_37_0_typed_id,Node_106_ref_node_target_id_37_0);
*Node_106_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_106_retrieved_reference_37_false_0_is_inverse, Node_106_ref_node_target_id_37_0);
*Node_106_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_106_retrieved_reference_37_0_target_id, Node_106_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_106_retrieved_reference_37_0->references,"1",Node_106_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* NamespaceUri ********/


case 107 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 107;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("NamespaceUri", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("NamespaceUri", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A URI that uniquely identifies the dictionary.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 107 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_107_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_107_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_107_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_107_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_107_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_107_retrieved_reference_46_inverse_0);
*Node_107_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_107_retrieved_reference_46_inverse_0_typed_id,Node_107_ref_node_target_id_46_inverse_0);
*Node_107_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_107_retrieved_reference_46_inverse_true_0_is_inverse, Node_107_ref_node_target_id_46_inverse_0);
*Node_107_retrieved_reference_46_inverse_0_target_id = 72;
ishidaopcua_node_set_target_id(Node_107_retrieved_reference_46_inverse_0_target_id, Node_107_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_107_retrieved_reference_46_inverse_0->references,"1",Node_107_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_107_retrieved_reference_40_0;
ishidaopcua_NODE* Node_107_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_107_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_107_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_107_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_107_retrieved_reference_40_0);
*Node_107_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_107_retrieved_reference_40_0_typed_id,Node_107_ref_node_target_id_40_0);
*Node_107_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_107_retrieved_reference_40_false_0_is_inverse, Node_107_ref_node_target_id_40_0);
*Node_107_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_107_retrieved_reference_40_0_target_id, Node_107_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_107_retrieved_reference_40_0->references,"1",Node_107_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_107_retrieved_reference_37_0;
ishidaopcua_NODE* Node_107_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_107_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_107_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_107_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_107_retrieved_reference_37_0);
*Node_107_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_107_retrieved_reference_37_0_typed_id,Node_107_ref_node_target_id_37_0);
*Node_107_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_107_retrieved_reference_37_false_0_is_inverse, Node_107_ref_node_target_id_37_0);
*Node_107_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_107_retrieved_reference_37_0_target_id, Node_107_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_107_retrieved_reference_37_0->references,"1",Node_107_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerStatusDataType ********/


case 862 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 862;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerStatusDataType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerStatusDataType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 862 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_862_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_862_retrieved_reference_45_inverse_0);
*Node_862_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_45_inverse_0_typed_id,Node_862_ref_node_target_id_45_inverse_0);
*Node_862_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_45_inverse_true_0_is_inverse, Node_862_ref_node_target_id_45_inverse_0);
*Node_862_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_45_inverse_0_target_id, Node_862_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_45_inverse_0->references,"1",Node_862_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_38_0;
ishidaopcua_NODE* Node_862_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_862_retrieved_reference_38_0);
*Node_862_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_38_0_typed_id,Node_862_ref_node_target_id_38_0);
*Node_862_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_38_false_0_is_inverse, Node_862_ref_node_target_id_38_0);
*Node_862_retrieved_reference_38_0_target_id = 864;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_38_0_target_id, Node_862_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_38_0->references,"1",Node_862_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_38_1;
ishidaopcua_NODE* Node_862_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_862_retrieved_reference_38_1);
*Node_862_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_38_1_typed_id,Node_862_ref_node_target_id_38_1);
*Node_862_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_38_false_1_is_inverse, Node_862_ref_node_target_id_38_1);
*Node_862_retrieved_reference_38_1_target_id = 863;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_38_1_target_id, Node_862_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_38_1->references,"2",Node_862_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_38_2;
ishidaopcua_NODE* Node_862_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_862_retrieved_reference_38_2);
*Node_862_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_38_2_typed_id,Node_862_ref_node_target_id_38_2);
*Node_862_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_38_false_2_is_inverse, Node_862_ref_node_target_id_38_2);
*Node_862_retrieved_reference_38_2_target_id = 15367;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_38_2_target_id, Node_862_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_38_2->references,"3",Node_862_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Opc.Ua ********/


case 7617 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7617;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Opc.Ua", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Opc.Ua", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 15;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 7617 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_7617_retrieved_reference_40_0;
ishidaopcua_NODE* Node_7617_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_7617_retrieved_reference_40_0);
*Node_7617_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_40_0_typed_id,Node_7617_ref_node_target_id_40_0);
*Node_7617_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_40_false_0_is_inverse, Node_7617_ref_node_target_id_40_0);
*Node_7617_retrieved_reference_40_0_target_id = 72;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_40_0_target_id, Node_7617_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_40_0->references,"1",Node_7617_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_7617_retrieved_reference_47_inverse_0);
*Node_7617_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_inverse_0_typed_id,Node_7617_ref_node_target_id_47_inverse_0);
*Node_7617_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_inverse_true_0_is_inverse, Node_7617_ref_node_target_id_47_inverse_0);
*Node_7617_retrieved_reference_47_inverse_0_target_id = 93;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_inverse_0_target_id, Node_7617_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_inverse_0->references,"1",Node_7617_ref_node_target_id_47_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_46_0;
ishidaopcua_NODE* Node_7617_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_7617_retrieved_reference_46_0);
*Node_7617_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_46_0_typed_id,Node_7617_ref_node_target_id_46_0);
*Node_7617_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_46_false_0_is_inverse, Node_7617_ref_node_target_id_46_0);
*Node_7617_retrieved_reference_46_0_target_id = 7619;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_46_0_target_id, Node_7617_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_46_0->references,"1",Node_7617_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_46_1;
ishidaopcua_NODE* Node_7617_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_7617_retrieved_reference_46_1);
*Node_7617_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_46_1_typed_id,Node_7617_ref_node_target_id_46_1);
*Node_7617_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_46_false_1_is_inverse, Node_7617_ref_node_target_id_46_1);
*Node_7617_retrieved_reference_46_1_target_id = 15037;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_46_1_target_id, Node_7617_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_46_1->references,"2",Node_7617_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_0;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_0);
*Node_7617_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_0_typed_id,Node_7617_ref_node_target_id_47_0);
*Node_7617_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_0_is_inverse, Node_7617_ref_node_target_id_47_0);
*Node_7617_retrieved_reference_47_0_target_id = 14873;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_0_target_id, Node_7617_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_0->references,"1",Node_7617_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_1;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_1);
*Node_7617_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_1_typed_id,Node_7617_ref_node_target_id_47_1);
*Node_7617_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_1_is_inverse, Node_7617_ref_node_target_id_47_1);
*Node_7617_retrieved_reference_47_1_target_id = 15734;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_1_target_id, Node_7617_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_1->references,"2",Node_7617_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_2;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_2);
*Node_7617_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_2_typed_id,Node_7617_ref_node_target_id_47_2);
*Node_7617_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_2_is_inverse, Node_7617_ref_node_target_id_47_2);
*Node_7617_retrieved_reference_47_2_target_id = 15738;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_2_target_id, Node_7617_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_2->references,"3",Node_7617_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_3;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_3);
*Node_7617_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_3_typed_id,Node_7617_ref_node_target_id_47_3);
*Node_7617_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_3_is_inverse, Node_7617_ref_node_target_id_47_3);
*Node_7617_retrieved_reference_47_3_target_id = 12681;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_3_target_id, Node_7617_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_3->references,"4",Node_7617_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_4;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_4);
*Node_7617_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_4_typed_id,Node_7617_ref_node_target_id_47_4);
*Node_7617_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_4_is_inverse, Node_7617_ref_node_target_id_47_4);
*Node_7617_retrieved_reference_47_4_target_id = 15741;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_4_target_id, Node_7617_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_4->references,"5",Node_7617_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_5;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_5);
*Node_7617_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_5_typed_id,Node_7617_ref_node_target_id_47_5);
*Node_7617_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_5_is_inverse, Node_7617_ref_node_target_id_47_5);
*Node_7617_retrieved_reference_47_5_target_id = 14855;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_5_target_id, Node_7617_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_5->references,"6",Node_7617_ref_node_target_id_47_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_6;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_6);
*Node_7617_retrieved_reference_47_6_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_6_typed_id,Node_7617_ref_node_target_id_47_6);
*Node_7617_retrieved_reference_47_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_6_is_inverse, Node_7617_ref_node_target_id_47_6);
*Node_7617_retrieved_reference_47_6_target_id = 15599;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_6_target_id, Node_7617_ref_node_target_id_47_6);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_6->references,"7",Node_7617_ref_node_target_id_47_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_7;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_7);
*Node_7617_retrieved_reference_47_7_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_7_typed_id,Node_7617_ref_node_target_id_47_7);
*Node_7617_retrieved_reference_47_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_7_is_inverse, Node_7617_ref_node_target_id_47_7);
*Node_7617_retrieved_reference_47_7_target_id = 15602;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_7_target_id, Node_7617_ref_node_target_id_47_7);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_7->references,"8",Node_7617_ref_node_target_id_47_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_8;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_8);
*Node_7617_retrieved_reference_47_8_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_8_typed_id,Node_7617_ref_node_target_id_47_8);
*Node_7617_retrieved_reference_47_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_8_is_inverse, Node_7617_ref_node_target_id_47_8);
*Node_7617_retrieved_reference_47_8_target_id = 15501;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_8_target_id, Node_7617_ref_node_target_id_47_8);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_8->references,"9",Node_7617_ref_node_target_id_47_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_9;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_9);
*Node_7617_retrieved_reference_47_9_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_9_typed_id,Node_7617_ref_node_target_id_47_9);
*Node_7617_retrieved_reference_47_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_9_is_inverse, Node_7617_ref_node_target_id_47_9);
*Node_7617_retrieved_reference_47_9_target_id = 15521;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_9_target_id, Node_7617_ref_node_target_id_47_9);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_9->references,"10",Node_7617_ref_node_target_id_47_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_10;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_10);
*Node_7617_retrieved_reference_47_10_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_10_typed_id,Node_7617_ref_node_target_id_47_10);
*Node_7617_retrieved_reference_47_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_10_is_inverse, Node_7617_ref_node_target_id_47_10);
*Node_7617_retrieved_reference_47_10_target_id = 14849;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_10_target_id, Node_7617_ref_node_target_id_47_10);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_10->references,"11",Node_7617_ref_node_target_id_47_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_11;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_11);
*Node_7617_retrieved_reference_47_11_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_11_typed_id,Node_7617_ref_node_target_id_47_11);
*Node_7617_retrieved_reference_47_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_11_is_inverse, Node_7617_ref_node_target_id_47_11);
*Node_7617_retrieved_reference_47_11_target_id = 14852;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_11_target_id, Node_7617_ref_node_target_id_47_11);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_11->references,"12",Node_7617_ref_node_target_id_47_11); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_12;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_12 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_12_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_12_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_12_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_12);
*Node_7617_retrieved_reference_47_12_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_12_typed_id,Node_7617_ref_node_target_id_47_12);
*Node_7617_retrieved_reference_47_false_12_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_12_is_inverse, Node_7617_ref_node_target_id_47_12);
*Node_7617_retrieved_reference_47_12_target_id = 14876;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_12_target_id, Node_7617_ref_node_target_id_47_12);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_12->references,"13",Node_7617_ref_node_target_id_47_12); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_13;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_13 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_13_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_13_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_13_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_13);
*Node_7617_retrieved_reference_47_13_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_13_typed_id,Node_7617_ref_node_target_id_47_13);
*Node_7617_retrieved_reference_47_false_13_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_13_is_inverse, Node_7617_ref_node_target_id_47_13);
*Node_7617_retrieved_reference_47_13_target_id = 15766;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_13_target_id, Node_7617_ref_node_target_id_47_13);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_13->references,"14",Node_7617_ref_node_target_id_47_13); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_14;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_14 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_14_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_14_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_14_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_14);
*Node_7617_retrieved_reference_47_14_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_14_typed_id,Node_7617_ref_node_target_id_47_14);
*Node_7617_retrieved_reference_47_false_14_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_14_is_inverse, Node_7617_ref_node_target_id_47_14);
*Node_7617_retrieved_reference_47_14_target_id = 15769;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_14_target_id, Node_7617_ref_node_target_id_47_14);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_14->references,"15",Node_7617_ref_node_target_id_47_14); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_15;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_15 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_15_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_15_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_15_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_15);
*Node_7617_retrieved_reference_47_15_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_15_typed_id,Node_7617_ref_node_target_id_47_15);
*Node_7617_retrieved_reference_47_false_15_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_15_is_inverse, Node_7617_ref_node_target_id_47_15);
*Node_7617_retrieved_reference_47_15_target_id = 14324;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_15_target_id, Node_7617_ref_node_target_id_47_15);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_15->references,"16",Node_7617_ref_node_target_id_47_15); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_16;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_16 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_16_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_16_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_16_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_16);
*Node_7617_retrieved_reference_47_16_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_16_typed_id,Node_7617_ref_node_target_id_47_16);
*Node_7617_retrieved_reference_47_false_16_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_16_is_inverse, Node_7617_ref_node_target_id_47_16);
*Node_7617_retrieved_reference_47_16_target_id = 15772;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_16_target_id, Node_7617_ref_node_target_id_47_16);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_16->references,"17",Node_7617_ref_node_target_id_47_16); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_17;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_17 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_17_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_17_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_17_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_17);
*Node_7617_retrieved_reference_47_17_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_17_typed_id,Node_7617_ref_node_target_id_47_17);
*Node_7617_retrieved_reference_47_false_17_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_17_is_inverse, Node_7617_ref_node_target_id_47_17);
*Node_7617_retrieved_reference_47_17_target_id = 15775;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_17_target_id, Node_7617_ref_node_target_id_47_17);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_17->references,"18",Node_7617_ref_node_target_id_47_17); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_18;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_18 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_18_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_18_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_18_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_18);
*Node_7617_retrieved_reference_47_18_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_18_typed_id,Node_7617_ref_node_target_id_47_18);
*Node_7617_retrieved_reference_47_false_18_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_18_is_inverse, Node_7617_ref_node_target_id_47_18);
*Node_7617_retrieved_reference_47_18_target_id = 15778;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_18_target_id, Node_7617_ref_node_target_id_47_18);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_18->references,"19",Node_7617_ref_node_target_id_47_18); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_19;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_19 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_19_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_19_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_19_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_19);
*Node_7617_retrieved_reference_47_19_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_19_typed_id,Node_7617_ref_node_target_id_47_19);
*Node_7617_retrieved_reference_47_false_19_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_19_is_inverse, Node_7617_ref_node_target_id_47_19);
*Node_7617_retrieved_reference_47_19_target_id = 15781;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_19_target_id, Node_7617_ref_node_target_id_47_19);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_19->references,"20",Node_7617_ref_node_target_id_47_19); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_20;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_20 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_20_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_20_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_20_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_20);
*Node_7617_retrieved_reference_47_20_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_20_typed_id,Node_7617_ref_node_target_id_47_20);
*Node_7617_retrieved_reference_47_false_20_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_20_is_inverse, Node_7617_ref_node_target_id_47_20);
*Node_7617_retrieved_reference_47_20_target_id = 15784;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_20_target_id, Node_7617_ref_node_target_id_47_20);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_20->references,"21",Node_7617_ref_node_target_id_47_20); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_21;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_21 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_21_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_21_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_21_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_21);
*Node_7617_retrieved_reference_47_21_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_21_typed_id,Node_7617_ref_node_target_id_47_21);
*Node_7617_retrieved_reference_47_false_21_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_21_is_inverse, Node_7617_ref_node_target_id_47_21);
*Node_7617_retrieved_reference_47_21_target_id = 15787;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_21_target_id, Node_7617_ref_node_target_id_47_21);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_21->references,"22",Node_7617_ref_node_target_id_47_21); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_22;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_22 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_22_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_22_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_22_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_22);
*Node_7617_retrieved_reference_47_22_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_22_typed_id,Node_7617_ref_node_target_id_47_22);
*Node_7617_retrieved_reference_47_false_22_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_22_is_inverse, Node_7617_ref_node_target_id_47_22);
*Node_7617_retrieved_reference_47_22_target_id = 21156;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_22_target_id, Node_7617_ref_node_target_id_47_22);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_22->references,"23",Node_7617_ref_node_target_id_47_22); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_23;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_23 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_23_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_23_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_23_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_23);
*Node_7617_retrieved_reference_47_23_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_23_typed_id,Node_7617_ref_node_target_id_47_23);
*Node_7617_retrieved_reference_47_false_23_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_23_is_inverse, Node_7617_ref_node_target_id_47_23);
*Node_7617_retrieved_reference_47_23_target_id = 15793;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_23_target_id, Node_7617_ref_node_target_id_47_23);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_23->references,"24",Node_7617_ref_node_target_id_47_23); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_24;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_24 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_24_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_24_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_24_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_24);
*Node_7617_retrieved_reference_47_24_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_24_typed_id,Node_7617_ref_node_target_id_47_24);
*Node_7617_retrieved_reference_47_false_24_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_24_is_inverse, Node_7617_ref_node_target_id_47_24);
*Node_7617_retrieved_reference_47_24_target_id = 15854;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_24_target_id, Node_7617_ref_node_target_id_47_24);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_24->references,"25",Node_7617_ref_node_target_id_47_24); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_25;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_25 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_25_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_25_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_25_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_25);
*Node_7617_retrieved_reference_47_25_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_25_typed_id,Node_7617_ref_node_target_id_47_25);
*Node_7617_retrieved_reference_47_false_25_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_25_is_inverse, Node_7617_ref_node_target_id_47_25);
*Node_7617_retrieved_reference_47_25_target_id = 15857;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_25_target_id, Node_7617_ref_node_target_id_47_25);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_25->references,"26",Node_7617_ref_node_target_id_47_25); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_26;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_26 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_26_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_26_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_26_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_26);
*Node_7617_retrieved_reference_47_26_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_26_typed_id,Node_7617_ref_node_target_id_47_26);
*Node_7617_retrieved_reference_47_false_26_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_26_is_inverse, Node_7617_ref_node_target_id_47_26);
*Node_7617_retrieved_reference_47_26_target_id = 15860;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_26_target_id, Node_7617_ref_node_target_id_47_26);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_26->references,"27",Node_7617_ref_node_target_id_47_26); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_27;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_27 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_27_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_27_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_27_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_27);
*Node_7617_retrieved_reference_47_27_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_27_typed_id,Node_7617_ref_node_target_id_47_27);
*Node_7617_retrieved_reference_47_false_27_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_27_is_inverse, Node_7617_ref_node_target_id_47_27);
*Node_7617_retrieved_reference_47_27_target_id = 21159;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_27_target_id, Node_7617_ref_node_target_id_47_27);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_27->references,"28",Node_7617_ref_node_target_id_47_27); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_28;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_28 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_28_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_28_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_28_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_28);
*Node_7617_retrieved_reference_47_28_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_28_typed_id,Node_7617_ref_node_target_id_47_28);
*Node_7617_retrieved_reference_47_false_28_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_28_is_inverse, Node_7617_ref_node_target_id_47_28);
*Node_7617_retrieved_reference_47_28_target_id = 21162;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_28_target_id, Node_7617_ref_node_target_id_47_28);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_28->references,"29",Node_7617_ref_node_target_id_47_28); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_29;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_29 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_29_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_29_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_29_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_29);
*Node_7617_retrieved_reference_47_29_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_29_typed_id,Node_7617_ref_node_target_id_47_29);
*Node_7617_retrieved_reference_47_false_29_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_29_is_inverse, Node_7617_ref_node_target_id_47_29);
*Node_7617_retrieved_reference_47_29_target_id = 21165;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_29_target_id, Node_7617_ref_node_target_id_47_29);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_29->references,"30",Node_7617_ref_node_target_id_47_29); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_30;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_30 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_30_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_30_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_30_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_30);
*Node_7617_retrieved_reference_47_30_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_30_typed_id,Node_7617_ref_node_target_id_47_30);
*Node_7617_retrieved_reference_47_false_30_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_30_is_inverse, Node_7617_ref_node_target_id_47_30);
*Node_7617_retrieved_reference_47_30_target_id = 15866;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_30_target_id, Node_7617_ref_node_target_id_47_30);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_30->references,"31",Node_7617_ref_node_target_id_47_30); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_31;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_31 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_31_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_31_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_31_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_31);
*Node_7617_retrieved_reference_47_31_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_31_typed_id,Node_7617_ref_node_target_id_47_31);
*Node_7617_retrieved_reference_47_false_31_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_31_is_inverse, Node_7617_ref_node_target_id_47_31);
*Node_7617_retrieved_reference_47_31_target_id = 15869;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_31_target_id, Node_7617_ref_node_target_id_47_31);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_31->references,"32",Node_7617_ref_node_target_id_47_31); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_32;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_32 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_32_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_32_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_32_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_32);
*Node_7617_retrieved_reference_47_32_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_32_typed_id,Node_7617_ref_node_target_id_47_32);
*Node_7617_retrieved_reference_47_false_32_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_32_is_inverse, Node_7617_ref_node_target_id_47_32);
*Node_7617_retrieved_reference_47_32_target_id = 15872;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_32_target_id, Node_7617_ref_node_target_id_47_32);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_32->references,"33",Node_7617_ref_node_target_id_47_32); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_33;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_33 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_33_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_33_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_33_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_33);
*Node_7617_retrieved_reference_47_33_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_33_typed_id,Node_7617_ref_node_target_id_47_33);
*Node_7617_retrieved_reference_47_false_33_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_33_is_inverse, Node_7617_ref_node_target_id_47_33);
*Node_7617_retrieved_reference_47_33_target_id = 15877;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_33_target_id, Node_7617_ref_node_target_id_47_33);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_33->references,"34",Node_7617_ref_node_target_id_47_33); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_34;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_34 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_34_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_34_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_34_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_34);
*Node_7617_retrieved_reference_47_34_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_34_typed_id,Node_7617_ref_node_target_id_47_34);
*Node_7617_retrieved_reference_47_false_34_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_34_is_inverse, Node_7617_ref_node_target_id_47_34);
*Node_7617_retrieved_reference_47_34_target_id = 15880;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_34_target_id, Node_7617_ref_node_target_id_47_34);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_34->references,"35",Node_7617_ref_node_target_id_47_34); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_35;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_35 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_35_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_35_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_35_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_35);
*Node_7617_retrieved_reference_47_35_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_35_typed_id,Node_7617_ref_node_target_id_47_35);
*Node_7617_retrieved_reference_47_false_35_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_35_is_inverse, Node_7617_ref_node_target_id_47_35);
*Node_7617_retrieved_reference_47_35_target_id = 15883;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_35_target_id, Node_7617_ref_node_target_id_47_35);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_35->references,"36",Node_7617_ref_node_target_id_47_35); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_36;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_36 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_36_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_36_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_36_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_36);
*Node_7617_retrieved_reference_47_36_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_36_typed_id,Node_7617_ref_node_target_id_47_36);
*Node_7617_retrieved_reference_47_false_36_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_36_is_inverse, Node_7617_ref_node_target_id_47_36);
*Node_7617_retrieved_reference_47_36_target_id = 15886;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_36_target_id, Node_7617_ref_node_target_id_47_36);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_36->references,"37",Node_7617_ref_node_target_id_47_36); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_37;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_37 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_37_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_37_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_37_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_37);
*Node_7617_retrieved_reference_47_37_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_37_typed_id,Node_7617_ref_node_target_id_47_37);
*Node_7617_retrieved_reference_47_false_37_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_37_is_inverse, Node_7617_ref_node_target_id_47_37);
*Node_7617_retrieved_reference_47_37_target_id = 21002;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_37_target_id, Node_7617_ref_node_target_id_47_37);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_37->references,"38",Node_7617_ref_node_target_id_47_37); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_38;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_38 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_38_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_38_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_38_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_38);
*Node_7617_retrieved_reference_47_38_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_38_typed_id,Node_7617_ref_node_target_id_47_38);
*Node_7617_retrieved_reference_47_false_38_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_38_is_inverse, Node_7617_ref_node_target_id_47_38);
*Node_7617_retrieved_reference_47_38_target_id = 15889;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_38_target_id, Node_7617_ref_node_target_id_47_38);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_38->references,"39",Node_7617_ref_node_target_id_47_38); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_39;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_39 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_39_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_39_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_39_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_39);
*Node_7617_retrieved_reference_47_39_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_39_typed_id,Node_7617_ref_node_target_id_47_39);
*Node_7617_retrieved_reference_47_false_39_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_39_is_inverse, Node_7617_ref_node_target_id_47_39);
*Node_7617_retrieved_reference_47_39_target_id = 21168;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_39_target_id, Node_7617_ref_node_target_id_47_39);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_39->references,"40",Node_7617_ref_node_target_id_47_39); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_40;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_40 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_40_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_40_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_40_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_40);
*Node_7617_retrieved_reference_47_40_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_40_typed_id,Node_7617_ref_node_target_id_47_40);
*Node_7617_retrieved_reference_47_false_40_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_40_is_inverse, Node_7617_ref_node_target_id_47_40);
*Node_7617_retrieved_reference_47_40_target_id = 15895;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_40_target_id, Node_7617_ref_node_target_id_47_40);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_40->references,"41",Node_7617_ref_node_target_id_47_40); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_41;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_41 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_41_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_41_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_41_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_41);
*Node_7617_retrieved_reference_47_41_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_41_typed_id,Node_7617_ref_node_target_id_47_41);
*Node_7617_retrieved_reference_47_false_41_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_41_is_inverse, Node_7617_ref_node_target_id_47_41);
*Node_7617_retrieved_reference_47_41_target_id = 15898;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_41_target_id, Node_7617_ref_node_target_id_47_41);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_41->references,"42",Node_7617_ref_node_target_id_47_41); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_42;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_42 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_42_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_42_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_42_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_42);
*Node_7617_retrieved_reference_47_42_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_42_typed_id,Node_7617_ref_node_target_id_47_42);
*Node_7617_retrieved_reference_47_false_42_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_42_is_inverse, Node_7617_ref_node_target_id_47_42);
*Node_7617_retrieved_reference_47_42_target_id = 15919;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_42_target_id, Node_7617_ref_node_target_id_47_42);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_42->references,"43",Node_7617_ref_node_target_id_47_42); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_43;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_43 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_43_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_43_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_43_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_43);
*Node_7617_retrieved_reference_47_43_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_43_typed_id,Node_7617_ref_node_target_id_47_43);
*Node_7617_retrieved_reference_47_false_43_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_43_is_inverse, Node_7617_ref_node_target_id_47_43);
*Node_7617_retrieved_reference_47_43_target_id = 15922;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_43_target_id, Node_7617_ref_node_target_id_47_43);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_43->references,"44",Node_7617_ref_node_target_id_47_43); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_44;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_44 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_44_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_44_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_44_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_44);
*Node_7617_retrieved_reference_47_44_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_44_typed_id,Node_7617_ref_node_target_id_47_44);
*Node_7617_retrieved_reference_47_false_44_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_44_is_inverse, Node_7617_ref_node_target_id_47_44);
*Node_7617_retrieved_reference_47_44_target_id = 15925;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_44_target_id, Node_7617_ref_node_target_id_47_44);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_44->references,"45",Node_7617_ref_node_target_id_47_44); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_45;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_45 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_45_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_45_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_45_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_45);
*Node_7617_retrieved_reference_47_45_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_45_typed_id,Node_7617_ref_node_target_id_47_45);
*Node_7617_retrieved_reference_47_false_45_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_45_is_inverse, Node_7617_ref_node_target_id_47_45);
*Node_7617_retrieved_reference_47_45_target_id = 15931;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_45_target_id, Node_7617_ref_node_target_id_47_45);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_45->references,"46",Node_7617_ref_node_target_id_47_45); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_46;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_46 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_46_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_46_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_46_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_46);
*Node_7617_retrieved_reference_47_46_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_46_typed_id,Node_7617_ref_node_target_id_47_46);
*Node_7617_retrieved_reference_47_false_46_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_46_is_inverse, Node_7617_ref_node_target_id_47_46);
*Node_7617_retrieved_reference_47_46_target_id = 17469;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_46_target_id, Node_7617_ref_node_target_id_47_46);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_46->references,"47",Node_7617_ref_node_target_id_47_46); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_47;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_47 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_47_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_47_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_47_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_47);
*Node_7617_retrieved_reference_47_47_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_47_typed_id,Node_7617_ref_node_target_id_47_47);
*Node_7617_retrieved_reference_47_false_47_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_47_is_inverse, Node_7617_ref_node_target_id_47_47);
*Node_7617_retrieved_reference_47_47_target_id = 21171;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_47_target_id, Node_7617_ref_node_target_id_47_47);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_47->references,"48",Node_7617_ref_node_target_id_47_47); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_48;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_48 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_48_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_48_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_48_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_48);
*Node_7617_retrieved_reference_47_48_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_48_typed_id,Node_7617_ref_node_target_id_47_48);
*Node_7617_retrieved_reference_47_false_48_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_48_is_inverse, Node_7617_ref_node_target_id_47_48);
*Node_7617_retrieved_reference_47_48_target_id = 15524;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_48_target_id, Node_7617_ref_node_target_id_47_48);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_48->references,"49",Node_7617_ref_node_target_id_47_48); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_49;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_49 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_49_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_49_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_49_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_49);
*Node_7617_retrieved_reference_47_49_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_49_typed_id,Node_7617_ref_node_target_id_47_49);
*Node_7617_retrieved_reference_47_false_49_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_49_is_inverse, Node_7617_ref_node_target_id_47_49);
*Node_7617_retrieved_reference_47_49_target_id = 15940;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_49_target_id, Node_7617_ref_node_target_id_47_49);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_49->references,"50",Node_7617_ref_node_target_id_47_49); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_50;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_50 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_50_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_50_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_50_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_50);
*Node_7617_retrieved_reference_47_50_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_50_typed_id,Node_7617_ref_node_target_id_47_50);
*Node_7617_retrieved_reference_47_false_50_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_50_is_inverse, Node_7617_ref_node_target_id_47_50);
*Node_7617_retrieved_reference_47_50_target_id = 15943;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_50_target_id, Node_7617_ref_node_target_id_47_50);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_50->references,"51",Node_7617_ref_node_target_id_47_50); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_51;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_51 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_51_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_51_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_51_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_51);
*Node_7617_retrieved_reference_47_51_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_51_typed_id,Node_7617_ref_node_target_id_47_51);
*Node_7617_retrieved_reference_47_false_51_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_51_is_inverse, Node_7617_ref_node_target_id_47_51);
*Node_7617_retrieved_reference_47_51_target_id = 15946;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_51_target_id, Node_7617_ref_node_target_id_47_51);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_51->references,"52",Node_7617_ref_node_target_id_47_51); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_52;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_52 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_52_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_52_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_52_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_52);
*Node_7617_retrieved_reference_47_52_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_52_typed_id,Node_7617_ref_node_target_id_47_52);
*Node_7617_retrieved_reference_47_false_52_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_52_is_inverse, Node_7617_ref_node_target_id_47_52);
*Node_7617_retrieved_reference_47_52_target_id = 16131;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_52_target_id, Node_7617_ref_node_target_id_47_52);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_52->references,"53",Node_7617_ref_node_target_id_47_52); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_53;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_53 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_53_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_53_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_53_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_53);
*Node_7617_retrieved_reference_47_53_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_53_typed_id,Node_7617_ref_node_target_id_47_53);
*Node_7617_retrieved_reference_47_false_53_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_53_is_inverse, Node_7617_ref_node_target_id_47_53);
*Node_7617_retrieved_reference_47_53_target_id = 18178;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_53_target_id, Node_7617_ref_node_target_id_47_53);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_53->references,"54",Node_7617_ref_node_target_id_47_53); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_54;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_54 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_54_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_54_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_54_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_54);
*Node_7617_retrieved_reference_47_54_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_54_typed_id,Node_7617_ref_node_target_id_47_54);
*Node_7617_retrieved_reference_47_false_54_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_54_is_inverse, Node_7617_ref_node_target_id_47_54);
*Node_7617_retrieved_reference_47_54_target_id = 18181;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_54_target_id, Node_7617_ref_node_target_id_47_54);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_54->references,"55",Node_7617_ref_node_target_id_47_54); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_55;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_55 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_55_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_55_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_55_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_55);
*Node_7617_retrieved_reference_47_55_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_55_typed_id,Node_7617_ref_node_target_id_47_55);
*Node_7617_retrieved_reference_47_false_55_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_55_is_inverse, Node_7617_ref_node_target_id_47_55);
*Node_7617_retrieved_reference_47_55_target_id = 18184;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_55_target_id, Node_7617_ref_node_target_id_47_55);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_55->references,"56",Node_7617_ref_node_target_id_47_55); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_56;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_56 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_56_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_56_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_56_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_56);
*Node_7617_retrieved_reference_47_56_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_56_typed_id,Node_7617_ref_node_target_id_47_56);
*Node_7617_retrieved_reference_47_false_56_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_56_is_inverse, Node_7617_ref_node_target_id_47_56);
*Node_7617_retrieved_reference_47_56_target_id = 18187;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_56_target_id, Node_7617_ref_node_target_id_47_56);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_56->references,"57",Node_7617_ref_node_target_id_47_56); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_57;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_57 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_57_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_57_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_57_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_57);
*Node_7617_retrieved_reference_47_57_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_57_typed_id,Node_7617_ref_node_target_id_47_57);
*Node_7617_retrieved_reference_47_false_57_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_57_is_inverse, Node_7617_ref_node_target_id_47_57);
*Node_7617_retrieved_reference_47_57_target_id = 7650;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_57_target_id, Node_7617_ref_node_target_id_47_57);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_57->references,"58",Node_7617_ref_node_target_id_47_57); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_58;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_58 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_58_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_58_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_58_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_58);
*Node_7617_retrieved_reference_47_58_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_58_typed_id,Node_7617_ref_node_target_id_47_58);
*Node_7617_retrieved_reference_47_false_58_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_58_is_inverse, Node_7617_ref_node_target_id_47_58);
*Node_7617_retrieved_reference_47_58_target_id = 7656;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_58_target_id, Node_7617_ref_node_target_id_47_58);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_58->references,"59",Node_7617_ref_node_target_id_47_58); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_59;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_59 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_59_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_59_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_59_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_59);
*Node_7617_retrieved_reference_47_59_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_59_typed_id,Node_7617_ref_node_target_id_47_59);
*Node_7617_retrieved_reference_47_false_59_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_59_is_inverse, Node_7617_ref_node_target_id_47_59);
*Node_7617_retrieved_reference_47_59_target_id = 14870;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_59_target_id, Node_7617_ref_node_target_id_47_59);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_59->references,"60",Node_7617_ref_node_target_id_47_59); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_60;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_60 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_60_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_60_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_60_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_60);
*Node_7617_retrieved_reference_47_60_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_60_typed_id,Node_7617_ref_node_target_id_47_60);
*Node_7617_retrieved_reference_47_false_60_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_60_is_inverse, Node_7617_ref_node_target_id_47_60);
*Node_7617_retrieved_reference_47_60_target_id = 12767;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_60_target_id, Node_7617_ref_node_target_id_47_60);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_60->references,"61",Node_7617_ref_node_target_id_47_60); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_61;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_61 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_61_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_61_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_61_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_61);
*Node_7617_retrieved_reference_47_61_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_61_typed_id,Node_7617_ref_node_target_id_47_61);
*Node_7617_retrieved_reference_47_false_61_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_61_is_inverse, Node_7617_ref_node_target_id_47_61);
*Node_7617_retrieved_reference_47_61_target_id = 12770;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_61_target_id, Node_7617_ref_node_target_id_47_61);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_61->references,"62",Node_7617_ref_node_target_id_47_61); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_62;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_62 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_62_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_62_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_62_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_62);
*Node_7617_retrieved_reference_47_62_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_62_typed_id,Node_7617_ref_node_target_id_47_62);
*Node_7617_retrieved_reference_47_false_62_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_62_is_inverse, Node_7617_ref_node_target_id_47_62);
*Node_7617_retrieved_reference_47_62_target_id = 8914;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_62_target_id, Node_7617_ref_node_target_id_47_62);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_62->references,"63",Node_7617_ref_node_target_id_47_62); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_63;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_63 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_63_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_63_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_63_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_63);
*Node_7617_retrieved_reference_47_63_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_63_typed_id,Node_7617_ref_node_target_id_47_63);
*Node_7617_retrieved_reference_47_false_63_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_63_is_inverse, Node_7617_ref_node_target_id_47_63);
*Node_7617_retrieved_reference_47_63_target_id = 7665;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_63_target_id, Node_7617_ref_node_target_id_47_63);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_63->references,"64",Node_7617_ref_node_target_id_47_63); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_64;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_64 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_64_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_64_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_64_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_64);
*Node_7617_retrieved_reference_47_64_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_64_typed_id,Node_7617_ref_node_target_id_47_64);
*Node_7617_retrieved_reference_47_false_64_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_64_is_inverse, Node_7617_ref_node_target_id_47_64);
*Node_7617_retrieved_reference_47_64_target_id = 12213;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_64_target_id, Node_7617_ref_node_target_id_47_64);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_64->references,"65",Node_7617_ref_node_target_id_47_64); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_65;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_65 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_65_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_65_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_65_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_65);
*Node_7617_retrieved_reference_47_65_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_65_typed_id,Node_7617_ref_node_target_id_47_65);
*Node_7617_retrieved_reference_47_false_65_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_65_is_inverse, Node_7617_ref_node_target_id_47_65);
*Node_7617_retrieved_reference_47_65_target_id = 7662;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_65_target_id, Node_7617_ref_node_target_id_47_65);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_65->references,"66",Node_7617_ref_node_target_id_47_65); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_66;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_66 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_66_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_66_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_66_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_66);
*Node_7617_retrieved_reference_47_66_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_66_typed_id,Node_7617_ref_node_target_id_47_66);
*Node_7617_retrieved_reference_47_false_66_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_66_is_inverse, Node_7617_ref_node_target_id_47_66);
*Node_7617_retrieved_reference_47_66_target_id = 7668;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_66_target_id, Node_7617_ref_node_target_id_47_66);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_66->references,"67",Node_7617_ref_node_target_id_47_66); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_67;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_67 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_67_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_67_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_67_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_67);
*Node_7617_retrieved_reference_47_67_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_67_typed_id,Node_7617_ref_node_target_id_47_67);
*Node_7617_retrieved_reference_47_false_67_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_67_is_inverse, Node_7617_ref_node_target_id_47_67);
*Node_7617_retrieved_reference_47_67_target_id = 7782;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_67_target_id, Node_7617_ref_node_target_id_47_67);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_67->references,"68",Node_7617_ref_node_target_id_47_67); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_68;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_68 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_68_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_68_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_68_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_68);
*Node_7617_retrieved_reference_47_68_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_68_typed_id,Node_7617_ref_node_target_id_47_68);
*Node_7617_retrieved_reference_47_false_68_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_68_is_inverse, Node_7617_ref_node_target_id_47_68);
*Node_7617_retrieved_reference_47_68_target_id = 12902;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_68_target_id, Node_7617_ref_node_target_id_47_68);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_68->references,"69",Node_7617_ref_node_target_id_47_68); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_69;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_69 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_69_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_69_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_69_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_69);
*Node_7617_retrieved_reference_47_69_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_69_typed_id,Node_7617_ref_node_target_id_47_69);
*Node_7617_retrieved_reference_47_false_69_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_69_is_inverse, Node_7617_ref_node_target_id_47_69);
*Node_7617_retrieved_reference_47_69_target_id = 12905;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_69_target_id, Node_7617_ref_node_target_id_47_69);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_69->references,"70",Node_7617_ref_node_target_id_47_69); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_70;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_70 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_70_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_70_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_70_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_70);
*Node_7617_retrieved_reference_47_70_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_70_typed_id,Node_7617_ref_node_target_id_47_70);
*Node_7617_retrieved_reference_47_false_70_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_70_is_inverse, Node_7617_ref_node_target_id_47_70);
*Node_7617_retrieved_reference_47_70_target_id = 7698;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_70_target_id, Node_7617_ref_node_target_id_47_70);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_70->references,"71",Node_7617_ref_node_target_id_47_70); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_71;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_71 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_71_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_71_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_71_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_71);
*Node_7617_retrieved_reference_47_71_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_71_typed_id,Node_7617_ref_node_target_id_47_71);
*Node_7617_retrieved_reference_47_false_71_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_71_is_inverse, Node_7617_ref_node_target_id_47_71);
*Node_7617_retrieved_reference_47_71_target_id = 7671;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_71_target_id, Node_7617_ref_node_target_id_47_71);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_71->references,"72",Node_7617_ref_node_target_id_47_71); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_72;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_72 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_72_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_72_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_72_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_72);
*Node_7617_retrieved_reference_47_72_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_72_typed_id,Node_7617_ref_node_target_id_47_72);
*Node_7617_retrieved_reference_47_false_72_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_72_is_inverse, Node_7617_ref_node_target_id_47_72);
*Node_7617_retrieved_reference_47_72_target_id = 7674;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_72_target_id, Node_7617_ref_node_target_id_47_72);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_72->references,"73",Node_7617_ref_node_target_id_47_72); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_73;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_73 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_73_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_73_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_73_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_73);
*Node_7617_retrieved_reference_47_73_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_73_typed_id,Node_7617_ref_node_target_id_47_73);
*Node_7617_retrieved_reference_47_false_73_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_73_is_inverse, Node_7617_ref_node_target_id_47_73);
*Node_7617_retrieved_reference_47_73_target_id = 7677;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_73_target_id, Node_7617_ref_node_target_id_47_73);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_73->references,"74",Node_7617_ref_node_target_id_47_73); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_74;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_74 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_74_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_74_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_74_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_74);
*Node_7617_retrieved_reference_47_74_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_74_typed_id,Node_7617_ref_node_target_id_47_74);
*Node_7617_retrieved_reference_47_false_74_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_74_is_inverse, Node_7617_ref_node_target_id_47_74);
*Node_7617_retrieved_reference_47_74_target_id = 7680;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_74_target_id, Node_7617_ref_node_target_id_47_74);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_74->references,"75",Node_7617_ref_node_target_id_47_74); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_75;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_75 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_75_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_75_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_75_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_75);
*Node_7617_retrieved_reference_47_75_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_75_typed_id,Node_7617_ref_node_target_id_47_75);
*Node_7617_retrieved_reference_47_false_75_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_75_is_inverse, Node_7617_ref_node_target_id_47_75);
*Node_7617_retrieved_reference_47_75_target_id = 7683;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_75_target_id, Node_7617_ref_node_target_id_47_75);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_75->references,"76",Node_7617_ref_node_target_id_47_75); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_76;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_76 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_76_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_76_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_76_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_76);
*Node_7617_retrieved_reference_47_76_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_76_typed_id,Node_7617_ref_node_target_id_47_76);
*Node_7617_retrieved_reference_47_false_76_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_76_is_inverse, Node_7617_ref_node_target_id_47_76);
*Node_7617_retrieved_reference_47_76_target_id = 7728;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_76_target_id, Node_7617_ref_node_target_id_47_76);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_76->references,"77",Node_7617_ref_node_target_id_47_76); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_77;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_77 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_77_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_77_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_77_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_77);
*Node_7617_retrieved_reference_47_77_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_77_typed_id,Node_7617_ref_node_target_id_47_77);
*Node_7617_retrieved_reference_47_false_77_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_77_is_inverse, Node_7617_ref_node_target_id_47_77);
*Node_7617_retrieved_reference_47_77_target_id = 7731;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_77_target_id, Node_7617_ref_node_target_id_47_77);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_77->references,"78",Node_7617_ref_node_target_id_47_77); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_78;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_78 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_78_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_78_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_78_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_78);
*Node_7617_retrieved_reference_47_78_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_78_typed_id,Node_7617_ref_node_target_id_47_78);
*Node_7617_retrieved_reference_47_false_78_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_78_is_inverse, Node_7617_ref_node_target_id_47_78);
*Node_7617_retrieved_reference_47_78_target_id = 7734;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_78_target_id, Node_7617_ref_node_target_id_47_78);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_78->references,"79",Node_7617_ref_node_target_id_47_78); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_79;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_79 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_79_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_79_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_79_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_79);
*Node_7617_retrieved_reference_47_79_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_79_typed_id,Node_7617_ref_node_target_id_47_79);
*Node_7617_retrieved_reference_47_false_79_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_79_is_inverse, Node_7617_ref_node_target_id_47_79);
*Node_7617_retrieved_reference_47_79_target_id = 7737;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_79_target_id, Node_7617_ref_node_target_id_47_79);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_79->references,"80",Node_7617_ref_node_target_id_47_79); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_80;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_80 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_80_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_80_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_80_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_80);
*Node_7617_retrieved_reference_47_80_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_80_typed_id,Node_7617_ref_node_target_id_47_80);
*Node_7617_retrieved_reference_47_false_80_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_80_is_inverse, Node_7617_ref_node_target_id_47_80);
*Node_7617_retrieved_reference_47_80_target_id = 12718;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_80_target_id, Node_7617_ref_node_target_id_47_80);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_80->references,"81",Node_7617_ref_node_target_id_47_80); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_81;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_81 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_81_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_81_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_81_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_81);
*Node_7617_retrieved_reference_47_81_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_81_typed_id,Node_7617_ref_node_target_id_47_81);
*Node_7617_retrieved_reference_47_false_81_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_81_is_inverse, Node_7617_ref_node_target_id_47_81);
*Node_7617_retrieved_reference_47_81_target_id = 12721;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_81_target_id, Node_7617_ref_node_target_id_47_81);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_81->references,"82",Node_7617_ref_node_target_id_47_81); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_82;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_82 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_82_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_82_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_82_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_82);
*Node_7617_retrieved_reference_47_82_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_82_typed_id,Node_7617_ref_node_target_id_47_82);
*Node_7617_retrieved_reference_47_false_82_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_82_is_inverse, Node_7617_ref_node_target_id_47_82);
*Node_7617_retrieved_reference_47_82_target_id = 7686;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_82_target_id, Node_7617_ref_node_target_id_47_82);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_82->references,"83",Node_7617_ref_node_target_id_47_82); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_83;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_83 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_83_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_83_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_83_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_83);
*Node_7617_retrieved_reference_47_83_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_83_typed_id,Node_7617_ref_node_target_id_47_83);
*Node_7617_retrieved_reference_47_false_83_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_83_is_inverse, Node_7617_ref_node_target_id_47_83);
*Node_7617_retrieved_reference_47_83_target_id = 7929;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_83_target_id, Node_7617_ref_node_target_id_47_83);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_83->references,"84",Node_7617_ref_node_target_id_47_83); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_84;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_84 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_84_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_84_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_84_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_84);
*Node_7617_retrieved_reference_47_84_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_84_typed_id,Node_7617_ref_node_target_id_47_84);
*Node_7617_retrieved_reference_47_false_84_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_84_is_inverse, Node_7617_ref_node_target_id_47_84);
*Node_7617_retrieved_reference_47_84_target_id = 7932;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_84_target_id, Node_7617_ref_node_target_id_47_84);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_84->references,"85",Node_7617_ref_node_target_id_47_84); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_85;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_85 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_85_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_85_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_85_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_85);
*Node_7617_retrieved_reference_47_85_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_85_typed_id,Node_7617_ref_node_target_id_47_85);
*Node_7617_retrieved_reference_47_false_85_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_85_is_inverse, Node_7617_ref_node_target_id_47_85);
*Node_7617_retrieved_reference_47_85_target_id = 7935;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_85_target_id, Node_7617_ref_node_target_id_47_85);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_85->references,"86",Node_7617_ref_node_target_id_47_85); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_86;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_86 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_86_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_86_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_86_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_86);
*Node_7617_retrieved_reference_47_86_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_86_typed_id,Node_7617_ref_node_target_id_47_86);
*Node_7617_retrieved_reference_47_false_86_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_86_is_inverse, Node_7617_ref_node_target_id_47_86);
*Node_7617_retrieved_reference_47_86_target_id = 7938;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_86_target_id, Node_7617_ref_node_target_id_47_86);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_86->references,"87",Node_7617_ref_node_target_id_47_86); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_87;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_87 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_87_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_87_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_87_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_87);
*Node_7617_retrieved_reference_47_87_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_87_typed_id,Node_7617_ref_node_target_id_47_87);
*Node_7617_retrieved_reference_47_false_87_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_87_is_inverse, Node_7617_ref_node_target_id_47_87);
*Node_7617_retrieved_reference_47_87_target_id = 7941;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_87_target_id, Node_7617_ref_node_target_id_47_87);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_87->references,"88",Node_7617_ref_node_target_id_47_87); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_88;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_88 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_88_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_88_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_88_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_88);
*Node_7617_retrieved_reference_47_88_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_88_typed_id,Node_7617_ref_node_target_id_47_88);
*Node_7617_retrieved_reference_47_false_88_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_88_is_inverse, Node_7617_ref_node_target_id_47_88);
*Node_7617_retrieved_reference_47_88_target_id = 7944;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_88_target_id, Node_7617_ref_node_target_id_47_88);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_88->references,"89",Node_7617_ref_node_target_id_47_88); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_89;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_89 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_89_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_89_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_89_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_89);
*Node_7617_retrieved_reference_47_89_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_89_typed_id,Node_7617_ref_node_target_id_47_89);
*Node_7617_retrieved_reference_47_false_89_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_89_is_inverse, Node_7617_ref_node_target_id_47_89);
*Node_7617_retrieved_reference_47_89_target_id = 7947;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_89_target_id, Node_7617_ref_node_target_id_47_89);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_89->references,"90",Node_7617_ref_node_target_id_47_89); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_90;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_90 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_90_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_90_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_90_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_90);
*Node_7617_retrieved_reference_47_90_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_90_typed_id,Node_7617_ref_node_target_id_47_90);
*Node_7617_retrieved_reference_47_false_90_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_90_is_inverse, Node_7617_ref_node_target_id_47_90);
*Node_7617_retrieved_reference_47_90_target_id = 8004;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_90_target_id, Node_7617_ref_node_target_id_47_90);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_90->references,"91",Node_7617_ref_node_target_id_47_90); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_91;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_91 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_91_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_91_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_91_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_91);
*Node_7617_retrieved_reference_47_91_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_91_typed_id,Node_7617_ref_node_target_id_47_91);
*Node_7617_retrieved_reference_47_false_91_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_91_is_inverse, Node_7617_ref_node_target_id_47_91);
*Node_7617_retrieved_reference_47_91_target_id = 8067;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_91_target_id, Node_7617_ref_node_target_id_47_91);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_91->references,"92",Node_7617_ref_node_target_id_47_91); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_92;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_92 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_92_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_92_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_92_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_92);
*Node_7617_retrieved_reference_47_92_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_92_typed_id,Node_7617_ref_node_target_id_47_92);
*Node_7617_retrieved_reference_47_false_92_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_92_is_inverse, Node_7617_ref_node_target_id_47_92);
*Node_7617_retrieved_reference_47_92_target_id = 8073;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_92_target_id, Node_7617_ref_node_target_id_47_92);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_92->references,"93",Node_7617_ref_node_target_id_47_92); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_93;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_93 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_93_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_93_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_93_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_93);
*Node_7617_retrieved_reference_47_93_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_93_typed_id,Node_7617_ref_node_target_id_47_93);
*Node_7617_retrieved_reference_47_false_93_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_93_is_inverse, Node_7617_ref_node_target_id_47_93);
*Node_7617_retrieved_reference_47_93_target_id = 8076;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_93_target_id, Node_7617_ref_node_target_id_47_93);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_93->references,"94",Node_7617_ref_node_target_id_47_93); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_94;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_94 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_94_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_94_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_94_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_94);
*Node_7617_retrieved_reference_47_94_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_94_typed_id,Node_7617_ref_node_target_id_47_94);
*Node_7617_retrieved_reference_47_false_94_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_94_is_inverse, Node_7617_ref_node_target_id_47_94);
*Node_7617_retrieved_reference_47_94_target_id = 8172;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_94_target_id, Node_7617_ref_node_target_id_47_94);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_94->references,"95",Node_7617_ref_node_target_id_47_94); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_95;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_95 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_95_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_95_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_95_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_95);
*Node_7617_retrieved_reference_47_95_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_95_typed_id,Node_7617_ref_node_target_id_47_95);
*Node_7617_retrieved_reference_47_false_95_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_95_is_inverse, Node_7617_ref_node_target_id_47_95);
*Node_7617_retrieved_reference_47_95_target_id = 7692;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_95_target_id, Node_7617_ref_node_target_id_47_95);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_95->references,"96",Node_7617_ref_node_target_id_47_95); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_96;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_96 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_96_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_96_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_96_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_96);
*Node_7617_retrieved_reference_47_96_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_96_typed_id,Node_7617_ref_node_target_id_47_96);
*Node_7617_retrieved_reference_47_false_96_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_96_is_inverse, Node_7617_ref_node_target_id_47_96);
*Node_7617_retrieved_reference_47_96_target_id = 8208;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_96_target_id, Node_7617_ref_node_target_id_47_96);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_96->references,"97",Node_7617_ref_node_target_id_47_96); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_97;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_97 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_97_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_97_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_97_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_97);
*Node_7617_retrieved_reference_47_97_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_97_typed_id,Node_7617_ref_node_target_id_47_97);
*Node_7617_retrieved_reference_47_false_97_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_97_is_inverse, Node_7617_ref_node_target_id_47_97);
*Node_7617_retrieved_reference_47_97_target_id = 11959;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_97_target_id, Node_7617_ref_node_target_id_47_97);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_97->references,"98",Node_7617_ref_node_target_id_47_97); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_98;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_98 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_98_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_98_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_98_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_98);
*Node_7617_retrieved_reference_47_98_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_98_typed_id,Node_7617_ref_node_target_id_47_98);
*Node_7617_retrieved_reference_47_false_98_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_98_is_inverse, Node_7617_ref_node_target_id_47_98);
*Node_7617_retrieved_reference_47_98_target_id = 11962;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_98_target_id, Node_7617_ref_node_target_id_47_98);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_98->references,"99",Node_7617_ref_node_target_id_47_98); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_99;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_99 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_99_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_99_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_99_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_99);
*Node_7617_retrieved_reference_47_99_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_99_typed_id,Node_7617_ref_node_target_id_47_99);
*Node_7617_retrieved_reference_47_false_99_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_99_is_inverse, Node_7617_ref_node_target_id_47_99);
*Node_7617_retrieved_reference_47_99_target_id = 8211;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_99_target_id, Node_7617_ref_node_target_id_47_99);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_99->references,"100",Node_7617_ref_node_target_id_47_99); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_100;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_100 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_100_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_100_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_100_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_100);
*Node_7617_retrieved_reference_47_100_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_100_typed_id,Node_7617_ref_node_target_id_47_100);
*Node_7617_retrieved_reference_47_false_100_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_100_is_inverse, Node_7617_ref_node_target_id_47_100);
*Node_7617_retrieved_reference_47_100_target_id = 8214;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_100_target_id, Node_7617_ref_node_target_id_47_100);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_100->references,"101",Node_7617_ref_node_target_id_47_100); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_101;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_101 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_101_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_101_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_101_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_101);
*Node_7617_retrieved_reference_47_101_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_101_typed_id,Node_7617_ref_node_target_id_47_101);
*Node_7617_retrieved_reference_47_false_101_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_101_is_inverse, Node_7617_ref_node_target_id_47_101);
*Node_7617_retrieved_reference_47_101_target_id = 8217;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_101_target_id, Node_7617_ref_node_target_id_47_101);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_101->references,"102",Node_7617_ref_node_target_id_47_101); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_102;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_102 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_102_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_102_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_102_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_102);
*Node_7617_retrieved_reference_47_102_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_102_typed_id,Node_7617_ref_node_target_id_47_102);
*Node_7617_retrieved_reference_47_false_102_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_102_is_inverse, Node_7617_ref_node_target_id_47_102);
*Node_7617_retrieved_reference_47_102_target_id = 8220;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_102_target_id, Node_7617_ref_node_target_id_47_102);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_102->references,"103",Node_7617_ref_node_target_id_47_102); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_103;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_103 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_103_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_103_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_103_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_103);
*Node_7617_retrieved_reference_47_103_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_103_typed_id,Node_7617_ref_node_target_id_47_103);
*Node_7617_retrieved_reference_47_false_103_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_103_is_inverse, Node_7617_ref_node_target_id_47_103);
*Node_7617_retrieved_reference_47_103_target_id = 8223;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_103_target_id, Node_7617_ref_node_target_id_47_103);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_103->references,"104",Node_7617_ref_node_target_id_47_103); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_104;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_104 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_104_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_104_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_104_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_104);
*Node_7617_retrieved_reference_47_104_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_104_typed_id,Node_7617_ref_node_target_id_47_104);
*Node_7617_retrieved_reference_47_false_104_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_104_is_inverse, Node_7617_ref_node_target_id_47_104);
*Node_7617_retrieved_reference_47_104_target_id = 8226;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_104_target_id, Node_7617_ref_node_target_id_47_104);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_104->references,"105",Node_7617_ref_node_target_id_47_104); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_105;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_105 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_105_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_105_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_105_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_105);
*Node_7617_retrieved_reference_47_105_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_105_typed_id,Node_7617_ref_node_target_id_47_105);
*Node_7617_retrieved_reference_47_false_105_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_105_is_inverse, Node_7617_ref_node_target_id_47_105);
*Node_7617_retrieved_reference_47_105_target_id = 7659;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_105_target_id, Node_7617_ref_node_target_id_47_105);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_105->references,"106",Node_7617_ref_node_target_id_47_105); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_106;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_106 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_106_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_106_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_106_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_106);
*Node_7617_retrieved_reference_47_106_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_106_typed_id,Node_7617_ref_node_target_id_47_106);
*Node_7617_retrieved_reference_47_false_106_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_106_is_inverse, Node_7617_ref_node_target_id_47_106);
*Node_7617_retrieved_reference_47_106_target_id = 8229;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_106_target_id, Node_7617_ref_node_target_id_47_106);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_106->references,"107",Node_7617_ref_node_target_id_47_106); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_107;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_107 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_107_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_107_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_107_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_107);
*Node_7617_retrieved_reference_47_107_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_107_typed_id,Node_7617_ref_node_target_id_47_107);
*Node_7617_retrieved_reference_47_false_107_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_107_is_inverse, Node_7617_ref_node_target_id_47_107);
*Node_7617_retrieved_reference_47_107_target_id = 8232;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_107_target_id, Node_7617_ref_node_target_id_47_107);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_107->references,"108",Node_7617_ref_node_target_id_47_107); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_108;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_108 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_108_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_108_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_108_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_108);
*Node_7617_retrieved_reference_47_108_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_108_typed_id,Node_7617_ref_node_target_id_47_108);
*Node_7617_retrieved_reference_47_false_108_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_108_is_inverse, Node_7617_ref_node_target_id_47_108);
*Node_7617_retrieved_reference_47_108_target_id = 8235;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_108_target_id, Node_7617_ref_node_target_id_47_108);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_108->references,"109",Node_7617_ref_node_target_id_47_108); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_109;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_109 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_109_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_109_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_109_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_109);
*Node_7617_retrieved_reference_47_109_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_109_typed_id,Node_7617_ref_node_target_id_47_109);
*Node_7617_retrieved_reference_47_false_109_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_109_is_inverse, Node_7617_ref_node_target_id_47_109);
*Node_7617_retrieved_reference_47_109_target_id = 8238;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_109_target_id, Node_7617_ref_node_target_id_47_109);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_109->references,"110",Node_7617_ref_node_target_id_47_109); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_110;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_110 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_110_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_110_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_110_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_110);
*Node_7617_retrieved_reference_47_110_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_110_typed_id,Node_7617_ref_node_target_id_47_110);
*Node_7617_retrieved_reference_47_false_110_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_110_is_inverse, Node_7617_ref_node_target_id_47_110);
*Node_7617_retrieved_reference_47_110_target_id = 8241;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_110_target_id, Node_7617_ref_node_target_id_47_110);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_110->references,"111",Node_7617_ref_node_target_id_47_110); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_111;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_111 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_111_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_111_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_111_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_111);
*Node_7617_retrieved_reference_47_111_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_111_typed_id,Node_7617_ref_node_target_id_47_111);
*Node_7617_retrieved_reference_47_false_111_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_111_is_inverse, Node_7617_ref_node_target_id_47_111);
*Node_7617_retrieved_reference_47_111_target_id = 12183;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_111_target_id, Node_7617_ref_node_target_id_47_111);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_111->references,"112",Node_7617_ref_node_target_id_47_111); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_112;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_112 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_112_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_112_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_112_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_112);
*Node_7617_retrieved_reference_47_112_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_112_typed_id,Node_7617_ref_node_target_id_47_112);
*Node_7617_retrieved_reference_47_false_112_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_112_is_inverse, Node_7617_ref_node_target_id_47_112);
*Node_7617_retrieved_reference_47_112_target_id = 12186;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_112_target_id, Node_7617_ref_node_target_id_47_112);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_112->references,"113",Node_7617_ref_node_target_id_47_112); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_113;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_113 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_113_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_113_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_113_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_113);
*Node_7617_retrieved_reference_47_113_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_113_typed_id,Node_7617_ref_node_target_id_47_113);
*Node_7617_retrieved_reference_47_false_113_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_113_is_inverse, Node_7617_ref_node_target_id_47_113);
*Node_7617_retrieved_reference_47_113_target_id = 12091;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_113_target_id, Node_7617_ref_node_target_id_47_113);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_113->references,"114",Node_7617_ref_node_target_id_47_113); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_114;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_114 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_114_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_114_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_114_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_114);
*Node_7617_retrieved_reference_47_114_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_114_typed_id,Node_7617_ref_node_target_id_47_114);
*Node_7617_retrieved_reference_47_false_114_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_114_is_inverse, Node_7617_ref_node_target_id_47_114);
*Node_7617_retrieved_reference_47_114_target_id = 12094;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_114_target_id, Node_7617_ref_node_target_id_47_114);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_114->references,"115",Node_7617_ref_node_target_id_47_114); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_115;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_115 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_115_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_115_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_115_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_115);
*Node_7617_retrieved_reference_47_115_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_115_typed_id,Node_7617_ref_node_target_id_47_115);
*Node_7617_retrieved_reference_47_false_115_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_115_is_inverse, Node_7617_ref_node_target_id_47_115);
*Node_7617_retrieved_reference_47_115_target_id = 8247;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_115_target_id, Node_7617_ref_node_target_id_47_115);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_115->references,"116",Node_7617_ref_node_target_id_47_115); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_116;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_116 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_116_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_116_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_116_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_116);
*Node_7617_retrieved_reference_47_116_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_116_typed_id,Node_7617_ref_node_target_id_47_116);
*Node_7617_retrieved_reference_47_false_116_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_116_is_inverse, Node_7617_ref_node_target_id_47_116);
*Node_7617_retrieved_reference_47_116_target_id = 15398;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_116_target_id, Node_7617_ref_node_target_id_47_116);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_116->references,"117",Node_7617_ref_node_target_id_47_116); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7617_retrieved_reference_47_117;
ishidaopcua_NODE* Node_7617_ref_node_target_id_47_117 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_117_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_false_117_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7617_retrieved_reference_47_117_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_7617_retrieved_reference_47_117);
*Node_7617_retrieved_reference_47_117_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7617_retrieved_reference_47_117_typed_id,Node_7617_ref_node_target_id_47_117);
*Node_7617_retrieved_reference_47_false_117_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7617_retrieved_reference_47_false_117_is_inverse, Node_7617_ref_node_target_id_47_117);
*Node_7617_retrieved_reference_47_117_target_id = 8244;
ishidaopcua_node_set_target_id(Node_7617_retrieved_reference_47_117_target_id, Node_7617_ref_node_target_id_47_117);
ishidaeutz_put_hashmap(Node_7617_retrieved_reference_47_117->references,"118",Node_7617_ref_node_target_id_47_117); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Argument ********/


case 7650 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7650;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Argument", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Argument", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 7650 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_7650_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_7650_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7650_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7650_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7650_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_7650_retrieved_reference_47_inverse_0);
*Node_7650_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7650_retrieved_reference_47_inverse_0_typed_id,Node_7650_ref_node_target_id_47_inverse_0);
*Node_7650_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7650_retrieved_reference_47_inverse_true_0_is_inverse, Node_7650_ref_node_target_id_47_inverse_0);
*Node_7650_retrieved_reference_47_inverse_0_target_id = 7617;
ishidaopcua_node_set_target_id(Node_7650_retrieved_reference_47_inverse_0_target_id, Node_7650_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_7650_retrieved_reference_47_inverse_0->references,"1",Node_7650_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_7650_retrieved_reference_40_0;
ishidaopcua_NODE* Node_7650_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7650_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7650_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7650_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_7650_retrieved_reference_40_0);
*Node_7650_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_7650_retrieved_reference_40_0_typed_id,Node_7650_ref_node_target_id_40_0);
*Node_7650_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7650_retrieved_reference_40_false_0_is_inverse, Node_7650_ref_node_target_id_40_0);
*Node_7650_retrieved_reference_40_0_target_id = 69;
ishidaopcua_node_set_target_id(Node_7650_retrieved_reference_40_0_target_id, Node_7650_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_7650_retrieved_reference_40_0->references,"1",Node_7650_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* EnumValueType ********/


case 7656 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7656;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("EnumValueType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("EnumValueType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 7656 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_7656_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_7656_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7656_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7656_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7656_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_7656_retrieved_reference_47_inverse_0);
*Node_7656_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_7656_retrieved_reference_47_inverse_0_typed_id,Node_7656_ref_node_target_id_47_inverse_0);
*Node_7656_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7656_retrieved_reference_47_inverse_true_0_is_inverse, Node_7656_ref_node_target_id_47_inverse_0);
*Node_7656_retrieved_reference_47_inverse_0_target_id = 7617;
ishidaopcua_node_set_target_id(Node_7656_retrieved_reference_47_inverse_0_target_id, Node_7656_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_7656_retrieved_reference_47_inverse_0->references,"1",Node_7656_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_7656_retrieved_reference_40_0;
ishidaopcua_NODE* Node_7656_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7656_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7656_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7656_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_7656_retrieved_reference_40_0);
*Node_7656_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_7656_retrieved_reference_40_0_typed_id,Node_7656_ref_node_target_id_40_0);
*Node_7656_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7656_retrieved_reference_40_false_0_is_inverse, Node_7656_ref_node_target_id_40_0);
*Node_7656_retrieved_reference_40_0_target_id = 69;
ishidaopcua_node_set_target_id(Node_7656_retrieved_reference_40_0_target_id, Node_7656_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_7656_retrieved_reference_40_0->references,"1",Node_7656_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/