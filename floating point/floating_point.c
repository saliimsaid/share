/*
 ============================================================================
 Name        : floating_point.c
 Author      : salim
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>


/**
 * How to run the test
 *
 * Call the 2 assert methods inside main() or opcua_entry() functions
 * 	assert_double();
	assert_float();
 */

/* assert double **/

void ishidaeutz_convert_double_to_byte_array(double idouble, unsigned char *idata_buffer, unsigned int iwrite_size)
{
	memcpy((idata_buffer + iwrite_size),&idouble, (int)sizeof(double));
}

double ishidaeutz_convert_byte_array_to_double(unsigned char *idata_buffer, unsigned int iparsing_offset)
{
	double result;
	memcpy(&result, (idata_buffer + iparsing_offset), sizeof(double));

	return result;
}

void assert_double()
{
	double ddouble = 99.253;
	char doublebytes[sizeof(double)];

	double recovered;

	ishidaeutz_convert_double_to_byte_array(ddouble,doublebytes,0);

	recovered = ishidaeutz_convert_byte_array_to_double(doublebytes,0);

	if(recovered == ddouble)
	{
		puts("double assertion passed");
		printf("double value is %lf \n", recovered);
	}else
	{
		puts("double assertion failed");
	}

}

/** end assert double */

/** assert float ***/

void ishidaopcua_decode_FLOAT(unsigned char *ipdata_buffer, float* ipfloat)
{
	memcpy(ipfloat, ipdata_buffer, sizeof(float));
}

void ishidaopcua_encode_FLOAT(unsigned char *ipdata_buffer, float* ipfloat)
{
	memcpy(ipdata_buffer, ipfloat, (int)sizeof(float));
}

void assert_float()
{
	float ffloat = 3.334054;
	char floatbytes[sizeof(float)];
	float recovered;

	ishidaopcua_encode_FLOAT(floatbytes,&ffloat);

	ishidaopcua_decode_FLOAT(floatbytes,&recovered);

	if(recovered == ffloat)
	{
		puts("float assertion passed");
		printf("float value is %lf \n", recovered);
	}
	else
	{
		puts("float assertion failed");
	}

}

/** end assert float **/


/** assert long long **/

void long_to_byte_array(long long* ilong, char* byte_array)
{
	memcpy(byte_array,ilong,sizeof(long long));
}

void byte_array_to_long_long(char *byte_array, long long* ilong)
{
	memcpy(ilong,byte_array,sizeof(long long));
}

void assert_long()
{
	long long llong = 8.88;
	char long_bytes[sizeof(long long)];

	long_to_byte_array(&llong,long_bytes);

	long long recovered;
	byte_array_to_long_long(long_bytes,&recovered);

	if(recovered == llong)
	{
		puts("long long assertion passed");
		printf("long long is %llu \n", llong);
	}
	else
	{
		puts("long long assertion failed");
	}

}




int main(void) {
	printf("long long size is %d \n", sizeof(long long));

	assert_long();
	assert_double();
	assert_float();

	return EXIT_SUCCESS;
}
