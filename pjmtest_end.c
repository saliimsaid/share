#include "pjmtest.h"

ishidaopcua_UINT32 hierarchichal_references[9] = {(ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_EVENT_SOURCE_REFERENCE , (ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_NOTIFIER_REFERENCE , (ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_CHILD , (ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_SUB_TYPE_REFERENCE ,
		(ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_ORGANIZES_REFERENCE , (ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_AGGREGATES_REFERENCE , (ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_PROPERTY_REFERENCE , (ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_COMPONENT_REFERENCE , (ishidaopcua_UINT32) ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_ORDERED_COMPONENT_REFERENCE};

ishidaeutz_HASHMAP* ishidaopcua_client_session_hmap;

/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag){
	ishidaopcua_NODE* fetched_node = NULL;

	if((fetched_node = fetch_node_1(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_2(nodeid, references_flag)) != NULL){
		return fetched_node;
	}
	if((fetched_node = fetch_node_3(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_4(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_5(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_6(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_7(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_8(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_9(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_10(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_11(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_12(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_13(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_14(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_15(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_16(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_17(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_18(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_19(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	if((fetched_node = fetch_node_20(nodeid, references_flag)) != NULL){
		return fetched_node;
	}

	return fetched_node;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/

/*************************************** ISHIDAOPCUA_MAIN START ***************************************/

ishidaeutz_HASHMAP* ptransient_node_memory = NULL;

ishidaopcua_BOOLEAN qualifies_node_class_mask(ishidaopcua_UINT32* pinode_class_mask,ishidaopcua_UINT32* pinode_class)
{
	ishidaopcua_BOOLEAN qualifies;

	memset(&qualifies, 0, (size_t) sizeof(ishidaopcua_BOOLEAN));

	qualifies =ishidaopcua_BOOLEAN_FALSE;

	switch(*pinode_class)
	{
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_OBJECT):
		{
			ishidaopcua_UINT32 result;

			memset(&result, 0, (size_t) sizeof(ishidaopcua_UINT32));

			result = (*pinode_class_mask & 1) == 1U ? 1U : 0U ;

			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_VARIABLE):
		{
			ishidaopcua_UINT32 result;

			memset(&result, 0, (size_t) sizeof(ishidaopcua_UINT32));

			result = (*pinode_class_mask & 2U) == 2U ? 1U : 0U ;

			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_METHOD):
		{
			ishidaopcua_UINT32 result = (*pinode_class_mask & 4U) == 4U ? 1U : 0U ;
			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_OBJECT_TYPE):
		{
			ishidaopcua_UINT32 result = (*pinode_class_mask & 8U) == 8U ? 1U : 0U ;
			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_VARIABLE_TYPE):
		{
			ishidaopcua_UINT32 result = (*pinode_class_mask & 16U) == 16U ? 1U : 0U ;
			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_REFERENCE_TYPE):
		{
			ishidaopcua_UINT32 result = (*pinode_class_mask & 32U) == 32U ? 1U : 0U ;
			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_DATA_TYPE):
		{
			/*printf("case 64 node class mask %d\n", *pinode_class);*/
			ishidaopcua_UINT32 result = (*pinode_class_mask & 64U) == 64U ? 1U : 0U ;
			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		case ((ishidaopcua_UINT32) ishidaopcua_NODE_CLASS_VIEW):
		{
			ishidaopcua_UINT32 result = (*pinode_class_mask & 128U) == 128U ? 1U : 0U ;
			if(result)
				qualifies =ishidaopcua_BOOLEAN_TRUE;
			break;
		}
		default:
		{
			break;
		}
	}

	if(*pinode_class_mask == 0U)
	{
		qualifies =ishidaopcua_BOOLEAN_TRUE;
	}

	return qualifies;
}

void record_node(ishidaopcua_UINT32 inode_id, ishidaopcua_NODE* ipnode){
	if(ptransient_node_memory != NULL && ipnode != NULL){
		printf("Recording node with id %d and address %p\n", inode_id, ipnode);
		ishidaeutz_put_hashmap(ptransient_node_memory, (const char*) ishidaeutz_unsigned_int_to_string(inode_id), (void*) ipnode);
	}
}

ishidaopcua_NODE* search_node(ishidaopcua_UINT32 inode_id, ishidaopcua_BOOLEAN icache_node){
	ishidaopcua_NODE* pnode = NULL;
	/*puts("Searching node.");*/
	if(ptransient_node_memory != NULL){
		/*puts("Searching node in transient memory.");*/
		ishidaeutz_get_hashmap(ptransient_node_memory, ishidaeutz_unsigned_int_to_string(inode_id), ((const void**) &pnode));

		if(pnode == NULL){
			/*puts("Node is not in cache.");*/
			if(icache_node == ishidaopcua_BOOLEAN_TRUE){
				/*puts("Caching node.");*/
				ishidaeutz_mtrack_pause();
				pnode =  fetch_node(inode_id, 0U);

				if(pnode != NULL){
					/*puts("Cached node.");*/
					record_node(inode_id, pnode);
				}

				ishidaeutz_mtrack_resume();
			}
			else{
				pnode =  fetch_node(inode_id, 0U);
			}
		}
	}
	else{
		pnode = fetch_node(inode_id, 0U);
	}

	return pnode;
}

void ishidaopcua_prepare_response(ishidaopcua_BYTE* ioutput_buffer, ishidaopcua_BYTE* imessage_type, ishidaopcua_BINARY_SERVER_CLIENT_ARGS* iclient_args, ishidaopcua_UINT32* iwrite_size)
{
	if(memcmp("ACK",imessage_type,3) == 0){
		ishidaopcua_UINT32 server_protocol_version = (ishidaopcua_UINT32) ISHIDAOPCUA_SERVER_PROTOCOL_VERSION;

		ishidaopcua_UINT32 server_receive_buffer_size = (ishidaopcua_UINT32) ISHIDAOPCUA_SERVER_SEND_RECEIVE_BUFFER_SIZE;

		ishidaopcua_UINT32 server_send_buffer_size = (ishidaopcua_UINT32) ISHIDAOPCUA_SERVER_SEND_RECEIVE_BUFFER_SIZE;

		ishidaopcua_UINT32 server_max_message_buffer_size = (ishidaopcua_UINT32) ISHIDAOPCUA_MSG_MAXBUFFERSIZE;

		ishidaopcua_UINT32 server_max_chunk_count = (ishidaopcua_UINT32) ISHIDAOPCUA_MAX_CHUNK_COUNT;

		printf("ENCODER: Receive buffer size: %u\n",server_receive_buffer_size);
		printf("ENCODER: Write size: %u\n",*iwrite_size);

		ishidaopcua_encode_UINT32(ioutput_buffer, &server_protocol_version, iwrite_size);
		ishidaopcua_encode_UINT32(ioutput_buffer, &server_receive_buffer_size,iwrite_size);
		ishidaopcua_encode_UINT32(ioutput_buffer, &server_send_buffer_size,iwrite_size);
		ishidaopcua_encode_UINT32(ioutput_buffer, &server_max_message_buffer_size,iwrite_size);
		ishidaopcua_encode_UINT32(ioutput_buffer, &server_max_chunk_count,iwrite_size);
	}
	else if(memcmp("OPN",imessage_type,3) == 0 || memcmp("MSG",imessage_type,3) == 0 || memcmp("CLO",imessage_type,3) == 0){
		printf("ENCODER: Secure channel ID: %u\n",iclient_args->security_token->secure_channel_id);
		ishidaopcua_encode_UINT32(ioutput_buffer, &(iclient_args->security_token->secure_channel_id),iwrite_size);

		if(memcmp("OPN",imessage_type,3) == 0){
			ishidaopcua_BYTESTRING* psecurity_policy = ishidaopcua_init_string();

			ishidaopcua_BYTESTRING* psender_certificate = ishidaopcua_init_string();

			ishidaopcua_BYTESTRING* preceiver_certificate = ishidaopcua_init_string();

			printf("ENCODER: Sender certificate : %s with length: %u\n",psender_certificate->text, psender_certificate->length);

			printf("ENCODER: Security policy URI: %s with length: %u\n",psecurity_policy->text, psecurity_policy->length);

			ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_SECURITY_POLICY_URI_NONE), psecurity_policy);
			ishidaopcua_encode_BYTESTRING(ioutput_buffer, psecurity_policy,iwrite_size);
			ishidaopcua_set_string(0, psender_certificate);
			ishidaopcua_encode_BYTESTRING(ioutput_buffer, psender_certificate,iwrite_size);
			ishidaopcua_set_string(0, preceiver_certificate);
			ishidaopcua_encode_BYTESTRING(ioutput_buffer, preceiver_certificate,iwrite_size);
			printf("ENCODER: Receiver certificate : %s with length: %u\n",preceiver_certificate->text, preceiver_certificate->length);
		}
		else if(memcmp("MSG",imessage_type,3) == 0 || memcmp("CLO",imessage_type,3) == 0){
			ishidaopcua_encode_UINT32(ioutput_buffer, &(iclient_args->security_token->token_id),iwrite_size);
			printf("ENCODER: Token ID: %u\n",iclient_args->security_token->token_id);
		}

		ishidaopcua_encode_UINT32(ioutput_buffer, &(iclient_args->sequence_number),iwrite_size);
		printf("ENCODER: Sequence number: %u\n",iclient_args->sequence_number);

		ishidaopcua_encode_UINT32(ioutput_buffer, &(iclient_args->request_id),iwrite_size);
		printf("ENCODER: Request id: %u\n",iclient_args->request_id );
	}
}

ishidaopcua_VARIANT* ishidaopcua_get_attribute(ishidaopcua_READ_VALUE_ID iread_value_id, ishidaeutz_HASHMAP* ipcache){
	ishidaopcua_NODE* node = NULL;
	ishidaopcua_VARIANT* attribute;

	/*printf("Node id %s\n",ishidaeutz_unsigned_int_to_string(iread_value_id.node_id->identifier.numeric));*/

	ishidaeutz_get_hashmap(ipcache, ishidaeutz_unsigned_int_to_string(iread_value_id.node_id->identifier.numeric), ((const void**) &node));

	if(node == NULL){
		/*puts("Node to be read not in cache");*/
		node = search_node(iread_value_id.node_id->identifier.numeric, ishidaopcua_BOOLEAN_FALSE);
		if(node != NULL){
			ishidaeutz_put_hashmap(ipcache,ishidaeutz_unsigned_int_to_string(iread_value_id.node_id->identifier.numeric), node);
		}
	}
	else{
		/*printf("Node to be read already in cache with address %p\n", node);*/
	}

	attribute = ishidaopcua_init_variant();

	if(node != NULL){
		switch(iread_value_id.attribute_id){
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_NODE_ID_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_node_id(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_NODE_ID, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_NODE_ID, attribute);
					attribute->value = ishidaopcua_init_node_id();
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_NODE_CLASS_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_node_class(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_INT32, attribute);

				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_INT32, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_BROWSE_NAME_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_browse_name(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_QUALIFIED_NAME, attribute);
				}
				else
				{
					ishidaopcua_QUALIFIED_NAME* pqualified_name ;

					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_QUALIFIED_NAME, attribute);
					pqualified_name = ishidaopcua_init_qualified_name();
					pqualified_name->name = ishidaopcua_init_string();
					attribute->value = pqualified_name;
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_DISPLAY_NAME_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_display_name(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_LOCALIZED_TEXT, attribute);
				}
				else{
					ishidaopcua_LOCALIZED_TEXT* plocalized_text;

					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_LOCALIZED_TEXT, attribute);
					plocalized_text = ishidaopcua_init_localized_text();
					plocalized_text->locale = ishidaopcua_init_string();
					plocalized_text->text = ishidaopcua_init_string();
					attribute->value = plocalized_text;
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_DESCRIPTION_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_description(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_LOCALIZED_TEXT, attribute);
				}
				else{
					ishidaopcua_LOCALIZED_TEXT* plocalized_text;

					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_LOCALIZED_TEXT, attribute);
					plocalized_text = ishidaopcua_init_localized_text();
					plocalized_text->locale = ishidaopcua_init_string();
					plocalized_text->text = ishidaopcua_init_string();
					attribute->value = plocalized_text;
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_WRITE_MASK_ATTRIBUTE_KEY):{
				/*puts("IGT 4");*/
				attribute->value = ishidaopcua_node_get_write_mask(node);
				/*puts("IGT 5");*/
				if(attribute->value != NULL){
					/*puts("IGT 6");*/
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_UINT32, attribute);
				}
				else{
					/*puts("IGT 7");*/
					ishidaopcua_UINT32* puint32;

					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_UINT32, attribute);
					puint32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
					*puint32 = 100U;
					attribute->value  = puint32;
					/*puts("IGT 8");*/
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_USER_WRITE_MASK_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_user_write_mask(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_UINT32, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_UINT32, attribute);
					attribute->value  = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_IS_ABSTRACT_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_is_abstract(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
					attribute->value  = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_SYMMETRIC_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_symmetric(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
					attribute->value  = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_INVERSE_NAME_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_inverse_name(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_LOCALIZED_TEXT, attribute);
				}
				else{
					ishidaopcua_LOCALIZED_TEXT* plocalized_text;

					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_LOCALIZED_TEXT, attribute);
					plocalized_text = ishidaopcua_init_localized_text();
					plocalized_text->locale = ishidaopcua_init_string();
					plocalized_text->text = ishidaopcua_init_string();
					attribute->value = plocalized_text;
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_CONTAINS_NO_LOOPS_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_contains_no_loops(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
					attribute->value  = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_EVENT_NOTIFIER_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_event_notifier(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BYTE, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BYTE, attribute);
					attribute->value  = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_VALUE_ATTRIBUTE_KEY):{
				attribute = ishidaopcua_node_get_value(node);

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_DATA_TYPE_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_data_type(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_NODE_ID, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_NODE_ID, attribute);
					attribute->value = ishidaopcua_init_node_id();
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_VALUE_RANK_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_value_rank(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_INT32, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_INT32, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_ARRAY_DIMENSIONS_ATTRIBUTE_KEY):{
				ishidaopcua_INT32* pvalue_rank = ishidaopcua_node_get_value_rank(node);

				if(pvalue_rank != NULL && (*pvalue_rank) > 0U){
					ishidaopcua_ARRAY* parray_dimensions = ishidaopcua_node_get_array_dimensions(node);

					if(parray_dimensions != NULL && parray_dimensions->elements != 0U && parray_dimensions->size > 0U)
					{
						ishidaopcua_set_variant_value(parray_dimensions->type, parray_dimensions->elements, attribute);

						if(parray_dimensions->size > 0U)
						{
							ishidaopcua_set_variant_array_length(parray_dimensions->size, attribute);
						}
					}
				}
				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_ACCESS_LEVEL_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_access_level(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BYTE, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BYTE, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_USER_ACCESS_LEVEL_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_user_access_level(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BYTE, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BYTE, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_MINIMUM_SAMPLING_INTERVAL_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_minimum_sampling_interval(node);

				/*printf("Reading minimum sampling interval in attributes hashmap is %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(attribute->value));*/

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_DOUBLE, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_DOUBLE, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_DOUBLE, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_HISTORIZING_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_historizing(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_EXECUTABLE_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_executable(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN, 1);
				}

				break;
			}
			case ((ishidaopcua_UINT32) ISHIDAOPCUA_NODE_USER_EXECUTABLE_ATTRIBUTE_KEY):{
				attribute->value = ishidaopcua_node_get_user_executable(node);

				if(attribute->value != NULL){
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
				}
				else{
					ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID_BOOLEAN, attribute);
					attribute->value = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN, 1);
				}

				break;
			}
			default:{}
		}
	}

	return attribute;
}

ishidaopcua_BOOLEAN is_hierarchichal_reference(ishidaopcua_UINT32 reference_type)
{
	int i;
	for(i = 0; i < 9; i++){
		if(hierarchichal_references[i] == reference_type){
			return ishidaopcua_BOOLEAN_TRUE;
		}
	}

	return ishidaopcua_BOOLEAN_FALSE;
}

char* ishidaopcua_convert_reference_description(ishidaopcua_UINT32 reference_type_id, ishidaopcua_UINT32 direction)
{
	if(direction == 1)
	{
		return ((char*) strcat(ishidaeutz_unsigned_int_to_string(reference_type_id),"_inverse"));
	}
	else
	{
		return  ((char*) ishidaeutz_unsigned_int_to_string(reference_type_id));
	}
}

ishidaopcua_UINT32 is_forward_browse_direction(char* key_name)
{
	if(strlen(key_name) > 8 && strcmp(key_name+(strlen(key_name)-8),"_inverse") == 0)
	{
		return 0;
	}

	return 1;
}

static void* process_hiearchical_references_reallocate_reference_description(ishidaopcua_REFERENCE_DESCRIPTION *ipreference_description, unsigned int inum)
{
	ishidaopcua_REFERENCE_DESCRIPTION* temp = ishidaeutz_malloc(inum, sizeof(ishidaopcua_REFERENCE_DESCRIPTION));

	if(temp == NULL){
		/*puts("ERROR: Reference description elements reallocation failed\n");*/
		ishidaeu_exit(233011);
	}
	else{
		unsigned int i;
		unsigned int copy_limit = inum -1;
		for(i = 0; i < copy_limit; i++){
			temp[i].is_forward = ipreference_description[i].is_forward;
			temp[i].node_class = ipreference_description[i].node_class;
			temp[i].pbrowse_name = ipreference_description[i].pbrowse_name;
			temp[i].pdisplay_name = ipreference_description[i].pdisplay_name;
			temp[i].pnode_id = ipreference_description[i].pnode_id;
			temp[i].preference_type_id = ipreference_description[i].preference_type_id;
			temp[i].ptype_definition = ipreference_description[i].ptype_definition;
		}
	}
	return temp;
}

void process_hiearchical_references(ishidaopcua_NODE* pnode_from_address_space, ishidaopcua_UINT32* pstatus_code, ishidaopcua_ARRAY* preference_description_array,ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_BOOLEAN hierarchy_filter, ishidaeutz_HASHMAP* ipcache)
{
	int node_from_address_space_total_references = ishidaopcua_get_hashmap_size(pnode_from_address_space->references);
	int array_size = 1;

	/*printf("process hreferences total nodes %d , node id %u\n", node_from_address_space_total_references, pnode_from_address_space_node_id->identifier.numeric);*/

	ishidaopcua_NODE* pretrieved_reference;
	ishidaopcua_NODE* preferenced_node_target_node ;
	ishidaopcua_NODE* preferenced_node_type_definition_references;
	ishidaopcua_NODE* preferenced_node_type_definition;
	ishidaopcua_NODE_ID* preferenced_node_id_type_definition;
	ishidaopcua_REFERENCE_DESCRIPTION* pout_reference_description;
	ishidaopcua_UINT32* preference_type_id_2;
	ishidaopcua_UINT32 reference_type_id_2;
	ishidaopcua_UINT32* ptarget_id_2;
	ishidaopcua_UINT32 target_id_2;
	ishidaopcua_UINT32* pis_inverse_2;
	ishidaopcua_UINT32 is_inverse_2;
	ishidaopcua_UINT32* ptarget_reference_type_id;
	ishidaopcua_UINT32 target_reference_type_id;
	ishidaopcua_UINT32* ptarget_reference;
	ishidaopcua_UINT32 target_reference;
	ishidaopcua_NODE_CLASS* preferenced_node_node_class;
	ishidaopcua_UINT32* preference_type_id;
	char* pkey_name;
	ishidaopcua_NODE* upper_level_node;
	ishidaopcua_UINT32 reference_type_keyy;
	ishidaopcua_BOOLEAN flag;
	ishidaopcua_UINT32 browse_direction;
	ishidaopcua_UINT32 direction;
	ishidaopcua_UINT32 browse_reference_direction;
	int reference_type_id;
	char* preference_type_id_string;
	char* pkey_offset;
	int map_size_type_def;
	int referenced_node_is_forward;
	int upper_level_node_total_references;

	int i;

	*pstatus_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
	for(i = 0; i < node_from_address_space_total_references; i++)
	{
		pkey_name = (char*) pnode_from_address_space->references->keys[i].name;


		ishidaeutz_get_hashmap(pnode_from_address_space->references, pkey_name,((const void**) &upper_level_node));

		upper_level_node_total_references = ishidaopcua_get_hashmap_size(upper_level_node->references);

		reference_type_keyy = atoi(pkey_name);
		flag = is_hierarchichal_reference(reference_type_keyy);
		browse_direction = is_forward_browse_direction(pkey_name);
		direction = ipbrowse_description->browse_direction;
		browse_reference_direction = direction == 0 ? 1 : direction == 1 ? 0 : browse_direction;

		reference_type_id = ipbrowse_description->preference_type_id->identifier.numeric;
		preference_type_id_string =  ishidaopcua_convert_reference_description(reference_type_id,direction);

		switch(reference_type_id)
		{
			case  0 :

			{
				/*puts("Get reference description trigger ALL hierarchical references zero");*/
				hierarchy_filter = flag;
				preference_type_id_string = pkey_name;
				reference_type_id = ISHIDAOPCUA_REFERENCES;
				break;
			}
			case  ISHIDAOPCUA_HIERARCHICAL_REFERENCES :
			{
				/*puts("Get reference description trigger hierarchical references");*/
				preference_type_id_string = pkey_name;
				break;
			}
			case  ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES :
			{
				/*puts("Get reference description trigger NON hierarchical references");*/
				preference_type_id_string = pkey_name;
				break;
			}
			case  ISHIDAOPCUA_REFERENCES :
			{
				/*puts("Get reference description trigger ALL hierarchical references");*/
				hierarchy_filter = flag;
				preference_type_id_string = pkey_name;
				break;
			}
			default:
			{
				/*puts("Get reference description trigger SPECIFIC reference type");*/
				hierarchy_filter = flag;
				break;
			}
		}

		if(upper_level_node_total_references > 0 && flag == hierarchy_filter && browse_direction == browse_reference_direction && pkey_name == preference_type_id_string)
		{
			int t;
			for(t = 1; t <= upper_level_node_total_references; t++)
			{
				ishidaopcua_UINT32* node_class_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
				pkey_offset = ishidaeutz_unsigned_int_to_string(t);

				ishidaeutz_get_hashmap(upper_level_node->references, pkey_offset, ((const void**) &pretrieved_reference));

				preference_type_id_2 = ishidaopcua_node_get_reference_type_id(pretrieved_reference);
				reference_type_id_2 = *preference_type_id_2;
				ptarget_id_2 = ishidaopcua_node_get_target_id(pretrieved_reference);
				target_id_2 = *ptarget_id_2;
				pis_inverse_2 = ishidaopcua_node_get_is_inverse(pretrieved_reference);
				is_inverse_2 = *pis_inverse_2;

				*node_class_mask = ipbrowse_description->node_class_mask;

				if(pretrieved_reference != 0)
				{
					ishidaeutz_get_hashmap(ipcache, ishidaeutz_unsigned_int_to_string(target_id_2), ((const void**) &preferenced_node_target_node));

					if(preferenced_node_target_node == NULL){
						preferenced_node_target_node = fetch_node(target_id_2, 2);
						ishidaeutz_put_hashmap(ipcache, ishidaeutz_unsigned_int_to_string(target_id_2), pnode_from_address_space);
					}

					if(preferenced_node_target_node != 0)
					{
						ishidaopcua_BOOLEAN is_subset_of_node_class_mask;
						ishidaopcua_UINT32* node_class_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
						ishidaopcua_UINT32* node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);

						preference_description_array->size = array_size;

						preference_description_array->elements = process_hiearchical_references_reallocate_reference_description(preference_description_array->elements, preference_description_array->size);

						ishidaeutz_get_hashmap(preferenced_node_target_node->references,"40", ((const void**) &preferenced_node_type_definition_references));
						map_size_type_def = ishidaopcua_get_hashmap_size(preferenced_node_type_definition_references->references);

						preferenced_node_type_definition = 0;

						preferenced_node_id_type_definition = ishidaopcua_init_node_id();
						preferenced_node_id_type_definition->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;

						if((preferenced_node_type_definition_references != 0) && (map_size_type_def > 0))
						{

							ishidaeutz_get_hashmap(preferenced_node_type_definition_references->references,"1", ((const void**) &preferenced_node_type_definition));

							if(preferenced_node_type_definition != 0)
							{
								ptarget_reference_type_id = ishidaopcua_node_get_reference_type_id(preferenced_node_type_definition);
								target_reference_type_id = *ptarget_reference_type_id;
								ptarget_reference = ishidaopcua_node_get_target_id(preferenced_node_type_definition);
								target_reference = *ptarget_reference;

								if(target_reference_type_id == ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES_HAS_TYPE_DEFINITION_REFERENCE)
								{
									preferenced_node_id_type_definition->identifier.numeric = target_reference;
								}
							}
						}

						referenced_node_is_forward = (is_inverse_2 == 0) ? 1 : 0;

						pout_reference_description = ishidaopcua_init_reference_description();
						pout_reference_description->is_forward = referenced_node_is_forward;
						preferenced_node_node_class = ishidaopcua_node_get_node_class(preferenced_node_target_node);
						pout_reference_description->node_class = *preferenced_node_node_class;
						pout_reference_description->pbrowse_name = ishidaopcua_node_get_browse_name(preferenced_node_target_node);
						pout_reference_description->pdisplay_name = ishidaopcua_node_get_display_name(preferenced_node_target_node);
						pout_reference_description->pnode_id = ishidaopcua_init_expanded_node_id();
						pout_reference_description->pnode_id->node_id = ishidaopcua_node_get_node_id(preferenced_node_target_node);
						pout_reference_description->preference_type_id = ishidaopcua_init_node_id();
						preference_type_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
						*preference_type_id = reference_type_id_2;
						ishidaopcua_set_node_id_identifier_numeric(*preference_type_id, pout_reference_description->preference_type_id);
						pout_reference_description->ptype_definition = ishidaopcua_init_expanded_node_id();
						pout_reference_description->ptype_definition->node_id = preferenced_node_id_type_definition;

						*node_class_mask = ipbrowse_description->node_class_mask;

						*node_class = *preferenced_node_node_class;

						is_subset_of_node_class_mask = qualifies_node_class_mask(node_class_mask,node_class);

						if(is_subset_of_node_class_mask)
						{
							((ishidaopcua_REFERENCE_DESCRIPTION*)(preference_description_array->elements))[array_size-1] = *pout_reference_description;
							++array_size;
							/*puts("node class mask valid");*/
						}
					}
				}
			}
		}
	}
}

ishidaopcua_ARRAY* ishidaopcua_get_reference_description(ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_STATUS_CODE* ipstatus_code, ishidaeutz_HASHMAP* ipcache)
{
	ishidaopcua_NODE* pnode_from_address_space;
	ishidaopcua_ARRAY* preference_description_array = ishidaopcua_init_array();

	ishidaeutz_get_hashmap(ipcache, ishidaeutz_unsigned_int_to_string(ipbrowse_description->pnode_id->identifier.numeric), ((const void**) &pnode_from_address_space));

	if(pnode_from_address_space == NULL || (pnode_from_address_space != NULL && (pnode_from_address_space->references == NULL || (pnode_from_address_space->references != NULL && pnode_from_address_space->references->expected_size == 0)))){
		pnode_from_address_space = fetch_node(ipbrowse_description->pnode_id->identifier.numeric, 1);
		ishidaeutz_put_hashmap(ipcache, ishidaeutz_unsigned_int_to_string(ipbrowse_description->pnode_id->identifier.numeric), pnode_from_address_space);
	}

	/*printf("ishidaopcua_get_reference_description 1; Used up predef memory %d\n", predef_mem_used);*/

	preference_description_array->type = ishidaopcua_TYPE_ID_REFERENCE_DESCRIPTION;

	if(pnode_from_address_space == 0)
	{
		*ipstatus_code = ishidaopcua_STATUS_CODE_BAD_NODE_ID_UNKNOWN;
	}
	else
	{
		switch(ipbrowse_description->preference_type_id->identifier.numeric)
		{
			case ISHIDAOPCUA_REFERENCES :
			{
				process_hiearchical_references(pnode_from_address_space,ipstatus_code,preference_description_array,ipbrowse_description,ishidaopcua_BOOLEAN_TRUE, ipcache);
				/*printf("ishidaopcua_get_reference_description 2; Used up predef memory %d\n", predef_mem_used);*/
				break;
			}
			case ISHIDAOPCUA_HIERARCHICAL_REFERENCES :
			{
				process_hiearchical_references(pnode_from_address_space,ipstatus_code,preference_description_array,ipbrowse_description,ishidaopcua_BOOLEAN_TRUE, ipcache);
				/*printf("ishidaopcua_get_reference_description 3; Used up predef memory %d\n", predef_mem_used);*/
				break;
			}
			case ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES :
			{
				process_hiearchical_references(pnode_from_address_space,ipstatus_code,preference_description_array,ipbrowse_description,ishidaopcua_BOOLEAN_FALSE, ipcache);
				/*printf("ishidaopcua_get_reference_description 4; Used up predef memory %d\n", predef_mem_used);*/
				break;
			}
			default:
			{
				process_hiearchical_references(pnode_from_address_space,ipstatus_code,preference_description_array,ipbrowse_description,ishidaopcua_BOOLEAN_TRUE, ipcache);
				/*printf("ishidaopcua_get_reference_description 5; Used up predef memory %d\n", predef_mem_used);*/
				break;
			}
		}

	}
	/*printf("array size is %d\n", preference_description_array->size);*/

	return preference_description_array;
}

ishidaopcua_STATUS_CODE ishidaopcua_set_attribute(ishidaopcua_WRITE_VALUE iwrite_value){
	ishidaopcua_NODE* node = 0;
	ishidaopcua_STATUS_CODE status_code = ishidaopcua_STATUS_CODE_SEVERITY_UNCERTAIN;

	/*printf("Node id %s\n",ishidaeutz_unsigned_int_to_string(iwrite_value.node_id->identifier.numeric));*/
	node = search_node(iwrite_value.node_id->identifier.numeric, ishidaopcua_BOOLEAN_TRUE);

	/*printf("Attribute id %u\n",iwrite_value.attribute_id);*/
	/*printf("Node %p\n",node);*/

	if(node != 0){
		switch(iwrite_value.attribute_id){
			case ISHIDAOPCUA_NODE_NODE_ID_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_NODE_ID){
					ishidaopcua_NODE_ID* node_id_value;
					ishidaeutz_mtrack_pause();
					node_id_value = ishidaopcua_node_get_node_id(node);
					if(node_id_value == NULL){
						node_id_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE_ID));
					}
					*node_id_value = *((ishidaopcua_NODE_ID*)iwrite_value.value->value->value);
					ishidaopcua_node_set_node_id(node_id_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_NODE_CLASS_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_UINT32){
					ishidaopcua_NODE_CLASS* node_class_value;
					ishidaeutz_mtrack_pause();
					node_class_value = ishidaopcua_node_get_node_class(node);
					if(node_class_value == NULL){
						node_class_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE_CLASS));
					}
					*node_class_value = *((ishidaopcua_NODE_CLASS*)iwrite_value.value->value->value);
					ishidaopcua_node_set_node_class(node_class_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_BROWSE_NAME_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_QUALIFIED_NAME){
					ishidaopcua_QUALIFIED_NAME* browse_name_value;
					ishidaeutz_mtrack_pause();
					browse_name_value = ishidaopcua_node_get_browse_name(node);
					if(browse_name_value == NULL){
						browse_name_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_QUALIFIED_NAME));
					}
					*browse_name_value = *((ishidaopcua_QUALIFIED_NAME*)iwrite_value.value->value->value);
					ishidaopcua_node_set_browse_name(browse_name_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_DISPLAY_NAME_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_LOCALIZED_TEXT){
					ishidaopcua_LOCALIZED_TEXT* display_name_value;
					ishidaeutz_mtrack_pause();
					display_name_value = ishidaopcua_node_get_display_name(node);
					if(display_name_value == NULL){
						display_name_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_LOCALIZED_TEXT));
					}
					*display_name_value = *((ishidaopcua_LOCALIZED_TEXT*)iwrite_value.value->value->value);
					ishidaopcua_node_set_display_name(display_name_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_DESCRIPTION_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_LOCALIZED_TEXT){
					ishidaopcua_LOCALIZED_TEXT* description_value;
					ishidaeutz_mtrack_pause();
					description_value = ishidaopcua_node_get_description(node);
					if(description_value == NULL){
						description_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_LOCALIZED_TEXT));
					}
					*description_value = *((ishidaopcua_LOCALIZED_TEXT*)iwrite_value.value->value->value);
					ishidaopcua_node_set_description(description_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_WRITE_MASK_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_UINT32){
					ishidaopcua_UINT32* write_mask_value;
					ishidaeutz_mtrack_pause();
					write_mask_value = ishidaopcua_node_get_write_mask(node);
					if(write_mask_value == NULL){
						write_mask_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_UINT32));
					}
					*write_mask_value = *((ishidaopcua_UINT32*)iwrite_value.value->value->value);
					ishidaopcua_node_set_write_mask(write_mask_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_USER_WRITE_MASK_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_UINT32){
					ishidaopcua_UINT32* user_write_mask_value;
					ishidaeutz_mtrack_pause();
					user_write_mask_value = ishidaopcua_node_get_user_write_mask(node);
					if(user_write_mask_value == NULL){
						user_write_mask_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_UINT32));
					}
					*user_write_mask_value = *((ishidaopcua_UINT32*)iwrite_value.value->value->value);
					ishidaopcua_node_set_user_write_mask(user_write_mask_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_IS_ABSTRACT_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value)== ishidaopcua_TYPE_ID_BOOLEAN){
					ishidaopcua_BOOLEAN* is_abstract_value;
					ishidaeutz_mtrack_pause();
					is_abstract_value = ishidaopcua_node_get_is_abstract(node);
					if(is_abstract_value == NULL){
						is_abstract_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BOOLEAN));
					}
					*is_abstract_value = *((ishidaopcua_BOOLEAN*)iwrite_value.value->value->value);
					ishidaopcua_node_set_is_abstract(is_abstract_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_SYMMETRIC_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value)== ishidaopcua_TYPE_ID_BOOLEAN){
					ishidaopcua_BOOLEAN* symmetric_value;
					ishidaeutz_mtrack_pause();
					symmetric_value = ishidaopcua_node_get_symmetric(node);
					if(symmetric_value == NULL){
						symmetric_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BOOLEAN));
					}
					*symmetric_value = *((ishidaopcua_BOOLEAN*)iwrite_value.value->value->value);
					ishidaopcua_node_set_symmetric(symmetric_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_INVERSE_NAME_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_LOCALIZED_TEXT){
					ishidaopcua_LOCALIZED_TEXT* inverse_name_value;
					ishidaeutz_mtrack_pause();
					inverse_name_value = ishidaopcua_node_get_inverse_name(node);
					if(inverse_name_value == NULL){
						inverse_name_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_LOCALIZED_TEXT));
					}
					*inverse_name_value = *((ishidaopcua_LOCALIZED_TEXT*)iwrite_value.value->value->value);
					ishidaopcua_node_set_inverse_name(inverse_name_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_CONTAINS_NO_LOOPS_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_BOOLEAN){
					ishidaopcua_BOOLEAN* contains_no_loops_value;
					ishidaeutz_mtrack_pause();
					contains_no_loops_value = ishidaopcua_node_get_contains_no_loops(node);
					if(contains_no_loops_value == NULL){
						contains_no_loops_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BOOLEAN));
					}
					*contains_no_loops_value = *((ishidaopcua_BOOLEAN*)iwrite_value.value->value->value);
					ishidaopcua_node_set_contains_no_loops(contains_no_loops_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_EVENT_NOTIFIER_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_BYTE){
					ishidaopcua_BOOLEAN* event_notifier_value;
					ishidaeutz_mtrack_pause();
					event_notifier_value = ishidaopcua_node_get_event_notifier(node);
					if(event_notifier_value == NULL){
						event_notifier_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BOOLEAN));
					}
					*event_notifier_value = *((ishidaopcua_BOOLEAN*)iwrite_value.value->value->value);
					ishidaopcua_node_set_event_notifier(event_notifier_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_VALUE_ATTRIBUTE_KEY:{
				ishidaopcua_VARIANT* value;
				ishidaeutz_mtrack_pause();
				value = ishidaopcua_node_get_value(node);
				if(value == NULL){
					value = ishidaeutz_malloc(1, sizeof(ishidaopcua_VARIANT));
				}

				value->array_dimensions = (*((ishidaopcua_VARIANT*)iwrite_value.value->value->value)).array_dimensions;
				value->array_length = (*((ishidaopcua_VARIANT*)iwrite_value.value->value->value)).array_length;
				value->encoding_mask = (*((ishidaopcua_VARIANT*)iwrite_value.value->value->value)).encoding_mask;
				value->value = (*((ishidaopcua_VARIANT*)iwrite_value.value->value->value)).value;

				ishidaopcua_node_set_value(value,node);
				status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
				ishidaeutz_mtrack_resume();

				break;
			}
			case ISHIDAOPCUA_NODE_DATA_TYPE_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_NODE_ID){
					ishidaopcua_NODE_ID* data_type_value;
					ishidaeutz_mtrack_pause();
					data_type_value = ishidaopcua_node_get_data_type(node);
					if(data_type_value == NULL){
						data_type_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE_ID));
					}
					*data_type_value = *((ishidaopcua_NODE_ID*)iwrite_value.value->value->value);
					ishidaopcua_node_set_data_type(data_type_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_VALUE_RANK_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_INT32){
					ishidaopcua_INT32* value_rank_value;
					ishidaeutz_mtrack_pause();
					value_rank_value = ishidaopcua_node_get_value_rank(node);
					if(value_rank_value == NULL){
						value_rank_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_INT32));
					}
					*value_rank_value = *((ishidaopcua_INT32*)iwrite_value.value->value->value);
					ishidaopcua_node_set_value_rank(value_rank_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_ARRAY_DIMENSIONS_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_UINT32 && iwrite_value.value->value->array_length > 0){
					ishidaopcua_ARRAY* array_dimensions;
					ishidaeutz_mtrack_pause();
					array_dimensions = ishidaopcua_node_get_array_dimensions(node);
					if(array_dimensions == NULL){
						array_dimensions = ishidaeutz_malloc(1, sizeof(ishidaopcua_ARRAY));
					}
					array_dimensions->elements = iwrite_value.value->value->value;
					array_dimensions->size = iwrite_value.value->value->array_length;
					array_dimensions->type = ishidaopcua_get_variant_type_id(iwrite_value.value->value);
					ishidaopcua_node_set_array_dimensions(array_dimensions,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_ACCESS_LEVEL_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_BYTE){
					ishidaopcua_BYTE* access_level_value;
					ishidaeutz_mtrack_pause();
					access_level_value = ishidaopcua_node_get_access_level(node);
					if(access_level_value == NULL){
						access_level_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BYTE));
					}
					*access_level_value = *((ishidaopcua_BYTE*)iwrite_value.value->value->value);
					ishidaopcua_node_set_access_level(access_level_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_USER_ACCESS_LEVEL_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_BYTE){
					ishidaopcua_BYTE* user_access_level_value;
					ishidaeutz_mtrack_pause();
					user_access_level_value = ishidaopcua_node_get_user_access_level(node);
					if(user_access_level_value == NULL){
						user_access_level_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BYTE));
					}
					*user_access_level_value = *((ishidaopcua_BYTE*)iwrite_value.value->value->value);
					ishidaopcua_node_set_user_access_level(user_access_level_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_MINIMUM_SAMPLING_INTERVAL_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_DOUBLE){
					ishidaopcua_DURATION* minimum_sampling_interval_value;
					/*ishidaopcua_DURATION* minimum_sampling_interval_value_retrieved;*/
					ishidaeutz_mtrack_pause();
					minimum_sampling_interval_value = ishidaopcua_node_get_minimum_sampling_interval(node);
					/*printf("Minimum sampling written is %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(((ishidaopcua_DURATION*)(iwrite_value.value->value->value))));*/
					/*printf("Minimum sampling searched is %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(minimum_sampling_interval_value));*/
					if(minimum_sampling_interval_value == NULL){
						minimum_sampling_interval_value = deep_copy_ishidaeutz_LARGE_INTs(((ishidaopcua_DURATION*)(iwrite_value.value->value->value)));
					}
					/*printf("Minimum sampling saved is %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(minimum_sampling_interval_value));*/
					ishidaopcua_node_set_minimum_sampling_interval(minimum_sampling_interval_value,node);
					/*minimum_sampling_interval_value_retrieved = ishidaopcua_node_get_minimum_sampling_interval(node);*/
					/*printf("Retrieved sampling written is %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(minimum_sampling_interval_value_retrieved));*/
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_HISTORIZING_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_BOOLEAN){
					ishidaopcua_BOOLEAN* historizing_value;
					ishidaeutz_mtrack_pause();
					historizing_value = ishidaopcua_node_get_historizing(node);
					if(historizing_value == NULL){
						historizing_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BOOLEAN));
					}
					*historizing_value = *((ishidaopcua_BOOLEAN*)iwrite_value.value->value->value);
					ishidaopcua_node_set_historizing(historizing_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			case ISHIDAOPCUA_NODE_EXECUTABLE_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_BOOLEAN){
					ishidaopcua_BOOLEAN* executable_value;
					ishidaeutz_mtrack_pause();
					executable_value = ishidaopcua_node_get_executable(node);
					if(executable_value == NULL){
						executable_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BOOLEAN));
					}
					*executable_value = *((ishidaopcua_BOOLEAN*)iwrite_value.value->value->value);
					ishidaopcua_node_set_executable(executable_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}
				break;
			}
			case ISHIDAOPCUA_NODE_USER_EXECUTABLE_ATTRIBUTE_KEY:{
				if(ishidaopcua_get_variant_type_id(iwrite_value.value->value) == ishidaopcua_TYPE_ID_BOOLEAN){
					ishidaopcua_BOOLEAN* user_executable_value;
					ishidaeutz_mtrack_pause();
					user_executable_value = ishidaopcua_node_get_user_executable(node);
					if(user_executable_value == NULL){
						user_executable_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_BOOLEAN));
					}
					*user_executable_value = *((ishidaopcua_BOOLEAN*)iwrite_value.value->value->value);
					ishidaopcua_node_set_user_executable(user_executable_value,node);
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					ishidaeutz_mtrack_resume();
				}
				else{
					status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE;
				}

				break;
			}
			default:{
				status_code = ishidaopcua_STATUS_CODE_SEVERITY_BAD_INVALID_ATTRIBUTE;
				break;
			}
		}
	}

	return status_code;
}

void initialize_reference_nodes(ishidaopcua_NODE* node)
{
	ishidaopcua_NODE* node_AGGREGATES_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_AGGREGATES_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_encoding_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_encoding_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_ordered_component_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_ordered_component_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_type_definition_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_type_definition_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_organizes_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_organizes_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_sub_type_definition_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_sub_type_definition_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_property_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_property_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_component_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_component_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_modelling_rule_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_modelling_rule_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_event_source_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_event_source_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_notifier_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_notifier_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_child_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_child_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_description_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_description_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_generates_events_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_generates_events_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_always_generates_events_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_always_generates_events_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_from_state_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_from_state_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_to_state_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_to_state_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_cause_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_cause_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_effect_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_effect_reference_inverse = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_historical_configuration_reference = ishidaopcua_node_init(0,2,0);
	ishidaopcua_NODE* node_has_historical_configuration_reference_inverse = ishidaopcua_node_init(0,2,0);

	ishidaeutz_put_hashmap(node->references,"44",node_AGGREGATES_reference);
	ishidaeutz_put_hashmap(node->references,"44_inverse",node_AGGREGATES_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"38",node_has_encoding_reference);
	ishidaeutz_put_hashmap(node->references,"38_inverse",node_has_encoding_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"49",node_has_ordered_component_reference);
	ishidaeutz_put_hashmap(node->references,"49_inverse",node_has_ordered_component_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"40",node_type_definition_reference);
	ishidaeutz_put_hashmap(node->references,"40_inverse",node_type_definition_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"35",node_organizes_reference);
	ishidaeutz_put_hashmap(node->references,"35_inverse",node_organizes_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"45",node_has_sub_type_definition_reference);
	ishidaeutz_put_hashmap(node->references,"45_inverse",node_has_sub_type_definition_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"46",node_has_property_reference);
	ishidaeutz_put_hashmap(node->references,"46_inverse",node_has_property_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"47",node_has_component_reference);
	ishidaeutz_put_hashmap(node->references,"47_inverse",node_has_component_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"37",node_has_modelling_rule_reference);
	ishidaeutz_put_hashmap(node->references,"37_inverse",node_has_modelling_rule_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"36",node_has_event_source_reference);
	ishidaeutz_put_hashmap(node->references,"36_inverse",node_has_event_source_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"48",node_has_notifier_reference);
	ishidaeutz_put_hashmap(node->references,"48_inverse",node_has_notifier_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"34",node_has_child_reference);
	ishidaeutz_put_hashmap(node->references,"34_inverse",node_has_child_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"39",node_has_description_reference);
	ishidaeutz_put_hashmap(node->references,"39_inverse",node_has_description_reference_inverse);


	ishidaeutz_put_hashmap(node->references,"41",node_generates_events_reference);
	ishidaeutz_put_hashmap(node->references,"41_inverse",node_generates_events_reference_inverse);


	ishidaeutz_put_hashmap(node->references,"3065",node_always_generates_events_reference);
	ishidaeutz_put_hashmap(node->references,"3065_inverse",node_always_generates_events_reference_inverse);

	/* new references */
	ishidaeutz_put_hashmap(node->references,"51",node_from_state_reference);
	ishidaeutz_put_hashmap(node->references,"51_inverse",node_from_state_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"52",node_to_state_reference);
	ishidaeutz_put_hashmap(node->references,"52_inverse",node_to_state_reference_inverse);


	ishidaeutz_put_hashmap(node->references,"53",node_has_cause_reference);
	ishidaeutz_put_hashmap(node->references,"53_inverse",node_has_cause_reference_inverse);


	ishidaeutz_put_hashmap(node->references,"54",node_has_effect_reference);
	ishidaeutz_put_hashmap(node->references,"54_inverse",node_has_effect_reference_inverse);

	ishidaeutz_put_hashmap(node->references,"56",node_has_historical_configuration_reference);
	ishidaeutz_put_hashmap(node->references,"56_inverse",node_has_historical_configuration_reference_inverse);
}

void ishidaopcua_decode_msg(ishidaopcua_BYTE* ipinput_buffer, ishidaopcua_UINT32 iread_size,ishidaopcua_BYTE* ipoutput_buffer, ishidaopcua_UINT32* ipwrite_size,ishidaopcua_BINARY_SERVER_CLIENT_ARGS* iclient_args, ishidaopcua_BYTE* ipclose_socket)
{
	ishidaopcua_UINT32 *pparsing_offset = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
	ishidaopcua_REQUEST_HEADER* prequest_header = ishidaopcua_init_request_header();
	ishidaopcua_TCP_MESSAGE_HEADER* ptcp_message_header = ishidaopcua_init_tcp_message_header();
	*pparsing_offset = 0;

	ishidaopcua_decode_TCP_MESSAGE_HEADER((ishidaopcua_BYTE*)ipinput_buffer, ptcp_message_header, pparsing_offset);
	printf("DECODER: Message type: %s\n",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header));
	printf("DECODER: Chunk type: %c\n",ishidaopcua_get_tcp_message_header_reserved(ptcp_message_header));
	printf("DECODER: Message size : %u\n",ishidaopcua_get_tcp_message_header_message_size(ptcp_message_header));
	printf("Parsing offset %u\n",*pparsing_offset);

	if(memcmp("HEL",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header),3) == 0)
	{
		ishidaopcua_STRING* pendpoint_url = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING, 1);
		ishidaopcua_UINT32* pprotocol_version = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
		ishidaopcua_UINT32* preceive_buffer_size = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
		ishidaopcua_UINT32* psend_buffer_size = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
		ishidaopcua_UINT32* pmax_msg_size = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
		ishidaopcua_UINT32* pmax_chunk_count = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
		ishidaopcua_BINARY_SERVER_CLIENT_ARGS* pclient_args;

		ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, pprotocol_version, pparsing_offset);
		/*printf("DECODER: Protocol version: %u\n",*pprotocol_version);*/

		ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, preceive_buffer_size, pparsing_offset);
		/*printf("DECODER: Receive buffer size: %u\n",*preceive_buffer_size);*/

		ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, psend_buffer_size, pparsing_offset);
		/*printf("DECODER: Send buffer size: %u\n",*psend_buffer_size);*/

		ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, pmax_msg_size, pparsing_offset);
		/*printf("DECODER: Max message size: %u\n",*pmax_msg_size);*/

		ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, pmax_chunk_count, pparsing_offset);
		/*printf("DECODER: Max chunk count: %u\n",*pmax_chunk_count);*/

		/*printf("DECODER: Client IP address is %s\n",inet_ntoa(iclient_args->client_details->sin_addr));*/

		ishidaeutz_get_hashmap(ishidaopcua_client_session_hmap,inet_ntoa(iclient_args->client_details->sin_addr), ((const void**) &pclient_args));

		if(pclient_args != NULL){
			if(pclient_args->security_token != NULL){
				memblock_free(pclient_args->security_token);
				pclient_args->security_token = NULL;
			}
			memblock_free(pclient_args);
		}
		ishidaeutz_put_hashmap(ishidaopcua_client_session_hmap,inet_ntoa(iclient_args->client_details->sin_addr),NULL);

		ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, pendpoint_url, pparsing_offset);
		/*printf("DECODER: Endpoint URL: %s of length: %u\n",pendpoint_url->text,pendpoint_url->length);*/

		/*printf("ENCODER: Write size: %u\n",*ipwrite_size);*/
		ishidaopcua_prepare_response(ipoutput_buffer, ((ishidaopcua_BYTE*) "ACK"), iclient_args, ipwrite_size);
		/*printf("ENCODER: Write size: %u\n",*ipwrite_size);*/

		ishidaopcua_set_tcp_message_header_message_type(((ishidaopcua_BYTE*) "ACK"), ptcp_message_header);
		/*printf("ENCODER: Message type: %s\n",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header));*/
		memblock_free(iclient_args);
	}
	else if(memcmp("OPN",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header),3) == 0 ||
			memcmp("CLO",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header),3) == 0 ||
			memcmp("MSG",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header),3) == 0)
	{
		ishidaopcua_UINT64* pcurrent_timestamp_ms = ishidaeutz_get_current_unix_timestamp_in_milliseconds_uint64();
		ishidaopcua_UINT32* psecure_channel_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
		ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, psecure_channel_id, pparsing_offset);
		printf("DECODER: Secure channel ID: %u\n",*psecure_channel_id);
		printf("Parsing offset %u\n",*pparsing_offset);

		if(memcmp("OPN",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header),3) == 0)
		{
			ishidaopcua_STRING* psecurity_policy_uri = ishidaopcua_init_string();
			ishidaopcua_EXPANDED_NODE_ID* puath_token_nodeid = ishidaopcua_init_expanded_node_id();
			ishidaopcua_STRING* psender_certificate = ishidaopcua_init_string();
			ishidaopcua_STRING* preceiver_certificate_thumbprint = ishidaopcua_init_string();
			ishidaopcua_UINT32* psequence_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);

			ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, psecurity_policy_uri, pparsing_offset);
			printf("DECODER: Security policy URI length: %u\n",psecurity_policy_uri->length);
			printf("DECODER: Security policy URI: %s\n",psecurity_policy_uri->text);
			printf("Parsing offset %u\n",*pparsing_offset);

			ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, psender_certificate, pparsing_offset);
			printf("DECODER: Sender certificate length: %u\n",psender_certificate->length);
			printf("DECODER: Sender certificate: %s\n",psender_certificate->text);
			printf("Parsing offset %u\n",*pparsing_offset);

			ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, preceiver_certificate_thumbprint, pparsing_offset);
			printf("DECODER: Receiver certificate thumbprint length: %u\n",preceiver_certificate_thumbprint->length);
			printf("DECODER: Receiver certificate thumbprint: %s\n",preceiver_certificate_thumbprint->text);
			printf("Parsing offset %u\n",*pparsing_offset);


			ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, psequence_id, pparsing_offset);
			printf("DECODER: Sequence ID: %u\n",*psequence_id);
			printf("Parsing offset %u\n",*pparsing_offset);

			ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, &iclient_args->request_id, pparsing_offset);
			printf("DECODER: Request ID: %u\n",iclient_args->request_id);
			printf("Parsing offset %u\n",*pparsing_offset);


			ishidaopcua_decode_EXPANDED_NODE_ID((ishidaopcua_BYTE*)ipinput_buffer,puath_token_nodeid, pparsing_offset);
			printf("DECODER: Node ID encoding mask: %x\n",puath_token_nodeid->node_id->encoding_mask);
			printf("DECODER: Node ID namespace index: %hu\n",puath_token_nodeid->node_id->namespace_index);
			printf("DECODER: Node ID identifier type: %u\n",puath_token_nodeid->node_id->identifier_type);
			printf("DECODER: Node ID identifier: %u\n",puath_token_nodeid->node_id->identifier.numeric);
			printf("Parsing offset %u\n",*pparsing_offset);

			if(puath_token_nodeid->node_id->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->node_id->identifier.numeric == 446)
			{
				ishidaopcua_UINT32* client_protocol_version = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
				ishidaopcua_UINT32* request_type = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
				ishidaopcua_UINT32* message_security_mode = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
				ishidaopcua_BYTESTRING* client_nonce = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTESTRING, 1);
				ishidaopcua_INT32* requested_lifetime = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_BYTESTRING* pserver_nonce = ishidaopcua_init_string();
				ishidaopcua_UINT32 server_protocol_version = ISHIDAOPCUA_SERVER_PROTOCOL_VERSION;
				ishidaopcua_SECURITY_TOKEN* psecurity_token;
				ishidaopcua_UINT32 test_security_token;

				ishidaeutz_mtrack_pause();
				psecurity_token = ishidaeutz_malloc(1, sizeof(ishidaopcua_SECURITY_TOKEN));
				ishidaeutz_mtrack_resume();

				puts("DECODER: Open secure channel request confirmed.");

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				printf("Request handle %u\n",prequest_header->request_handle);

				ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, client_protocol_version, pparsing_offset);
				printf("DECODER: Client protocol version: %u\n",*client_protocol_version);


				ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, request_type, pparsing_offset);
				printf("DECODER: Request type: %u\n",*request_type);


				ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, message_security_mode, pparsing_offset);
				printf("DECODER: Message security mode: %u\n",*message_security_mode);


				ishidaopcua_decode_BYTESTRING((ishidaopcua_BYTE*)ipinput_buffer, client_nonce, pparsing_offset);
				printf("DECODER: Client nonce: %s at length: %u\n",client_nonce->text, client_nonce->length);

				ishidaopcua_decode_INT32((ishidaopcua_BYTE*)ipinput_buffer, requested_lifetime, pparsing_offset);
				printf("DECODER: Requested lifetime: %d\n",*requested_lifetime);

				/*ishidaopcua_SECURITY_TOKEN* psecurity_token = ishidaopcua_malloc(ishidaopcua_TYPE_ID_SECURITY_TOKEN, 1);*/
				test_security_token = ishidaeutz_get_current_unix_timestamp_in_seconds_uint32() * iclient_args->client_details->sin_port * iclient_args->client_details->sin_addr.s_addr;
				printf("time %u , port %u, addr %u ,sec token %u\n", ishidaeutz_get_current_unix_timestamp_in_seconds_uint32(), iclient_args->client_details->sin_port, iclient_args->client_details->sin_addr.s_addr, test_security_token);
				psecurity_token->token_id = iclient_args->client_details->sin_port;

				psecurity_token->secure_channel_id = iclient_args->client_details->sin_port;
				printf("DECODER: Security token secure channel id: %d\n",psecurity_token->secure_channel_id);
				psecurity_token->pcreated_at = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				psecurity_token->revised_lifetime = 3600000;

				iclient_args->sequence_number = 1;
				iclient_args->security_token = psecurity_token;

				printf("DECODER: Client IP address is %s\n",inet_ntoa(iclient_args->client_details->sin_addr));
				ishidaeutz_put_hashmap(ishidaopcua_client_session_hmap,inet_ntoa(iclient_args->client_details->sin_addr),(void *)iclient_args);

				printf("Write size %u\n",*ipwrite_size);
				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), iclient_args, ipwrite_size);

				printf("Write size %u\n",*ipwrite_size);
				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 449;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				printf("Write size %u\n",*ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				printf("RESPONSE HEADER: Timestamp %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(presponse_header->ptimestamp));
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);
				printf("Write size %u\n",*ipwrite_size);

				ishidaopcua_encode_UINT32((ishidaopcua_BYTE*)ipoutput_buffer, &server_protocol_version, ipwrite_size);
				printf("ENCODER: Protocol version: %d\n",server_protocol_version);

				ishidaopcua_encode_SECURITY_TOKEN((ishidaopcua_BYTE*)ipoutput_buffer, psecurity_token, ipwrite_size);
				printf("Write size %u\n",*ipwrite_size);

				ishidaopcua_set_string(0, pserver_nonce);
				ishidaopcua_encode_BYTESTRING(ipoutput_buffer, pserver_nonce,ipwrite_size);
				printf("F::Write size %u\n",*ipwrite_size);
			}
			else{
				memblock_free(iclient_args);
			}
		}
		else if(memcmp("MSG",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header),3) == 0)
		{
			ishidaopcua_INT32* ptoken_id =  ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
			ishidaopcua_NODE_ID* puath_token_nodeid = ishidaopcua_init_node_id();
			ishidaopcua_UINT32* psequence_id =  ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
			ishidaopcua_BINARY_SERVER_CLIENT_ARGS* isaved_client_args;

			ishidaopcua_decode_INT32((ishidaopcua_BYTE*)ipinput_buffer, ptoken_id, pparsing_offset);
			/*printf("DECODER: Token ID: %u\n",*ptoken_id);*/

			ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, psequence_id, pparsing_offset);
			/*printf("DECODER: Sequence ID: %u\n",*psequence_id);*/

			ishidaeutz_get_hashmap(ishidaopcua_client_session_hmap,inet_ntoa(iclient_args->client_details->sin_addr), ((const void**) &isaved_client_args));
			isaved_client_args->sequence_number++;
			/*printf("After sequence number increment %d\n",isaved_client_args->sequence_number);*/

			/*printf("DECODER: Security token secure channel id %d\n",isaved_client_args->security_token->secure_channel_id);*/

			ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, &(isaved_client_args->request_id), pparsing_offset);
			/*printf("DECODER: Request ID: %u\n",isaved_client_args->request_id);*/

			ishidaopcua_decode_NODE_ID(ipinput_buffer,puath_token_nodeid, pparsing_offset);
			/*printf("DECODER: Identifier type: %u\n",puath_token_nodeid->identifier_type);*/

			if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 428)
			{
				ishidaopcua_STRING* pendpoint_url = ishidaopcua_init_string();
				ishidaopcua_ARRAY* plocale_ids = ishidaopcua_init_array();
				ishidaopcua_ARRAY* pprofile_uris = ishidaopcua_init_array();
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_STRING *pendpoint_description_server_application_name_locale = ishidaopcua_init_string();
				ishidaopcua_STRING *pendpoint_description_server_application_name_text = ishidaopcua_init_string();
				ishidaopcua_STRING* pdiscovery_urls = ishidaopcua_init_string();
				ishidaopcua_ARRAY* user_token_policy_array = ishidaopcua_malloc(ishidaopcua_TYPE_ID_ARRAY,1);
				ishidaopcua_ARRAY* pendpoint_description_array = ishidaopcua_init_array();
				ishidaopcua_ENDPOINT_DESCRIPTION* pendpoint_description = ishidaopcua_init_endpoint_description();
				ishidaopcua_USER_TOKEN_POLICY* puser_token_policy = ishidaopcua_malloc(ishidaopcua_TYPE_ID_USER_TOKEN_POLICY,1);
				/*puts("DECODER: Get endpoints request confirmed.");*/

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				/*printf("Parsing offset %u\n",*pparsing_offset);*/
				/*printf("Request handle %u\n",prequest_header->request_handle);*/

				ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, pendpoint_url, pparsing_offset);
				/*printf("DECODER: Endpoint url: %s of length: %lu\n",pendpoint_url->text,pendpoint_url->length);*/

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, plocale_ids, ishidaopcua_TYPE_ID_STRING, 0, pparsing_offset);
				/*printf("DECODER: Locale IDs array: size: %u\n",plocale_ids->size);*/

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, pprofile_uris, ishidaopcua_TYPE_ID_STRING, 0, pparsing_offset);
				/*printf("DECODER: Profile URIs array: size: %u\n",pprofile_uris->size);*/

				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 431;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				pendpoint_description->endpoint_url = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING, 1);
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), pendpoint_description->endpoint_url);

				pendpoint_description->server = ishidaopcua_init_application_description();

				pendpoint_description->server->application_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), pendpoint_description->server->application_uri);

				pendpoint_description->server->product_uri = ishidaopcua_init_string();


				ishidaopcua_set_string(((ishidaopcua_BYTE*) "ISHIDA-OPCUA-SERVER"), pendpoint_description_server_application_name_text);

				pendpoint_description->server->application_name = ishidaopcua_init_localized_text();
				ishidaopcua_set_localized_text(pendpoint_description_server_application_name_locale, pendpoint_description_server_application_name_text, pendpoint_description->server->application_name);

				ishidaopcua_set_application_description_application_type(ishidaopcua_APPLICATION_DESCRIPTION_APPLICATION_TYPE_SERVER, pendpoint_description->server);

				pendpoint_description->server->gateway_server_uri = ishidaopcua_init_string();

				pendpoint_description->server->discovery_profile_uri = ishidaopcua_init_string();

				pendpoint_description->server->discovery_urls = ishidaopcua_init_array();

				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), pdiscovery_urls);
				ishidaopcua_set_array(1, ishidaopcua_TYPE_ID_STRING, pdiscovery_urls, pendpoint_description->server->discovery_urls);

				pendpoint_description->server_certificate = ishidaopcua_init_string();

				ishidaopcua_set_endpoint_description_security_mode(ishidaopcua_MESSAGE_SECURITY_MODE_NONE, pendpoint_description);

				pendpoint_description->security_policy_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_SECURITY_POLICY_URI_NONE), pendpoint_description->security_policy_uri);


				puser_token_policy[0].policy_id = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) "ishida-anonymous-user-token-policy-1"), puser_token_policy[0].policy_id);
				puser_token_policy[0].token_type = 0;
				puser_token_policy[0].issued_token_type = ishidaopcua_init_string();
				puser_token_policy[0].issuer_endpoint_url = ishidaopcua_init_string();
				puser_token_policy[0].security_policy_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_SECURITY_POLICY_URI_NONE), puser_token_policy[0].security_policy_uri);

				ishidaopcua_set_array(1,ishidaopcua_TYPE_ID_USER_TOKEN_POLICY,puser_token_policy,user_token_policy_array);

				pendpoint_description->user_identity_tokens = user_token_policy_array;

				pendpoint_description->transport_profile_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_OPCUA_BINARY_TRANSPORT_PROFILE), pendpoint_description->transport_profile_uri);

				ishidaopcua_set_endpoint_description_security_level((ishidaopcua_BYTE)0x00, pendpoint_description);

				ishidaopcua_set_array(1, ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION, pendpoint_description, pendpoint_description_array);

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pendpoint_description_array, ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION, 0,ipwrite_size);

				memblock_free(iclient_args);
			}
			else if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 461){
				ishidaopcua_APPLICATION_DESCRIPTION* pclient_description = ishidaopcua_init_application_description();
				ishidaopcua_STRING* pserver_uri = ishidaopcua_init_string();
				ishidaopcua_STRING* pendpoint_url = ishidaopcua_init_string();
				ishidaopcua_STRING* psession_name = ishidaopcua_init_string();
				ishidaopcua_BYTESTRING* pclient_nonce = ishidaopcua_init_string();
				ishidaopcua_STRING* pclient_certificate = ishidaopcua_init_string();

				ishidaopcua_DURATION* prequested_session_timeout = ishidaopcua_malloc(ishidaopcua_TYPE_ID_DURATION, 1);

				ishidaopcua_UINT32* pmax_response_msg_size = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_NODE_ID* psession_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_NODE_ID* psession_authentication_token = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_BYTESTRING* pserver_nonce = ishidaopcua_init_string();
				ishidaopcua_USER_TOKEN_POLICY* puser_token_policy = ishidaopcua_malloc(ishidaopcua_TYPE_ID_USER_TOKEN_POLICY,1);
				ishidaopcua_UTC_TIME* pcurrent_time_ms = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				ishidaopcua_BYTESTRING* pserver_certificate = ishidaopcua_init_string();
				ishidaopcua_STRING *pendpoint_description_server_application_name_locale = ishidaopcua_init_string();
				ishidaopcua_STRING *pendpoint_description_server_application_name_text = ishidaopcua_init_string();
				ishidaopcua_ARRAY* pendpoint_description_array = ishidaopcua_init_array();
				ishidaopcua_STRING* pdiscovery_urls = ishidaopcua_init_string();
				ishidaopcua_ARRAY* user_token_policy_array = ishidaopcua_malloc(ishidaopcua_TYPE_ID_ARRAY,1);
				ishidaopcua_ARRAY* pserver_certificate_array = ishidaopcua_init_array();
				ishidaopcua_SIGNATURE* psignature = ishidaopcua_init_signature();
				ishidaopcua_UINT32 server_max_message_buffer_size = ISHIDAOPCUA_MSG_MAXBUFFERSIZE;
				ishidaopcua_ENDPOINT_DESCRIPTION* pendpoint_description = ishidaopcua_init_endpoint_description();
				ishidaeutz_LARGE_INT* ptimestamp;
				puts("DECODER: Create session request confirmed.");

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				printf("Parsing offset %u\n",*pparsing_offset);
				printf("Request handle %u\n",prequest_header->request_handle);

				ishidaopcua_decode_APPLICATION_DESCRIPTION((ishidaopcua_BYTE*)ipinput_buffer,pclient_description,pparsing_offset);
				printf("DECODER: Application URI: %s at length: %u\n", pclient_description->application_uri->text, pclient_description->application_uri->length);

				ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, pserver_uri, pparsing_offset);
				printf("DECODER: Server URI: %s at length: %u\n", pserver_uri->text, pserver_uri->length);

				ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, pendpoint_url, pparsing_offset);
				printf("DECODER: Endpoint URL: %s at length: %u\n", pendpoint_url->text, pendpoint_url->length);

				ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, psession_name, pparsing_offset);
				printf("DECODER: Session name: %s at length: %u\n", psession_name->text, psession_name->length);

				ishidaopcua_decode_BYTESTRING((ishidaopcua_BYTE*)ipinput_buffer, pclient_nonce, pparsing_offset);
				printf("DECODER: Client nonce: %s at length: %u\n", pclient_nonce->text, pclient_nonce->length);

				ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer, pclient_certificate, pparsing_offset);
				printf("DECODER: Client certificate: %s at length: %u\n", pclient_certificate->text, pclient_certificate->length);

				ishidaopcua_decode_DURATION((ishidaopcua_BYTE*)ipinput_buffer, prequested_session_timeout, pparsing_offset);
				printf("DECODER: Requested session timeout: %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(prequested_session_timeout));

				ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, pmax_response_msg_size, pparsing_offset);

				printf("DECODER: Max response message size: %u\n", *pmax_response_msg_size);

				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 464;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();

				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();

				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();

				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();

				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();

				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);

				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);

				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				psession_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_BYTESTRING;
				isaved_client_args->session_id = ishidaeutz_get_current_unix_timestamp_in_seconds_uint32();

				psession_id->identifier.bytestring = ishidaopcua_init_string();
				psession_id->identifier.bytestring->length = sizeof(isaved_client_args->session_id);
				psession_id->identifier.bytestring->text = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, psession_id->identifier.bytestring->length);

				ishidaeutz_copy_byte_array(((char*)&(isaved_client_args->session_id)), ((char*) psession_id->identifier.bytestring->text),0,0, psession_id->identifier.bytestring->length);

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, psession_id, ipwrite_size);

				psession_authentication_token->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_BYTESTRING;
				ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);

				ishidaeutz_mtrack_pause();
				isaved_client_args->session_authentication_token = deep_copy_ishidaeutz_LARGE_INTs(ptimestamp);
				ishidaeutz_mtrack_resume();

				psession_authentication_token->identifier.bytestring = ishidaopcua_init_string();

				yield_ishidaeutz_LARGE_INT_int_byte_array(isaved_client_args->session_authentication_token, ishidaopcua_SERVER_LARGE_INT_ENDIANNESS, 8);

				psession_authentication_token->identifier.bytestring->length = isaved_client_args->session_authentication_token->int_bytearray_size;
				psession_authentication_token->identifier.bytestring->text = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, psession_authentication_token->identifier.bytestring->length);
				ishidaeutz_copy_byte_array(((char*) &(isaved_client_args->session_authentication_token->int_bytearray[0])), ((char*) psession_authentication_token->identifier.bytestring->text),0,0, psession_authentication_token->identifier.bytestring->length);

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, psession_authentication_token, ipwrite_size);

				ishidaopcua_encode_DURATION((ishidaopcua_BYTE*) ipoutput_buffer, prequested_session_timeout, ipwrite_size);

				pserver_nonce->length = 32;
				pserver_nonce->text = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, pserver_nonce->length);

				ishidaeutz_copy_byte_array(((char*) &(isaved_client_args->session_id)),((char*) pserver_nonce->text),0,0, 4);
				ishidaeutz_copy_byte_array(((char*) &(isaved_client_args->session_authentication_token->int_bytearray[0])),((char*) pserver_nonce->text),0,4, isaved_client_args->session_authentication_token->int_bytearray_size);
				ishidaeutz_copy_byte_array(((char*) &(iclient_args->sequence_number)),((char*) pserver_nonce->text),0,12, 4);
				ishidaeutz_copy_byte_array(((char*) &(iclient_args->client_details->sin_port)),((char*) pserver_nonce->text),0,16, 4);
				stretch_ishidaeutz_LARGE_INT(pcurrent_time_ms, 8);
				ishidaeutz_copy_byte_array(((char*) &(pcurrent_time_ms->number_bytestring[0])),((char*) pserver_nonce->text),0,20, 8);
				ishidaeutz_copy_byte_array(((char*) &(iclient_args->client_details->sin_addr.s_addr)),((char*) pserver_nonce->text),0,28, 4);
				ishidaopcua_encode_BYTESTRING((ishidaopcua_BYTE*) ipoutput_buffer, pserver_nonce, ipwrite_size);

				ishidaopcua_encode_BYTESTRING((ishidaopcua_BYTE*) ipoutput_buffer, pserver_certificate, ipwrite_size);

				pendpoint_description->endpoint_url = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING, 1);
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), pendpoint_description->endpoint_url);

				pendpoint_description->server = ishidaopcua_init_application_description();

				pendpoint_description->server->application_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), pendpoint_description->server->application_uri);

				pendpoint_description->server->product_uri = ishidaopcua_init_string();


				ishidaopcua_set_string(((ishidaopcua_BYTE*) "ISHIDA-OPCUA-SERVER"), pendpoint_description_server_application_name_text);

				pendpoint_description->server->application_name = ishidaopcua_init_localized_text();
				ishidaopcua_set_localized_text(pendpoint_description_server_application_name_locale, pendpoint_description_server_application_name_text, pendpoint_description->server->application_name);

				pendpoint_description->server->gateway_server_uri = ishidaopcua_init_string();

				pendpoint_description->server->discovery_profile_uri = ishidaopcua_init_string();

				pendpoint_description->server->discovery_urls = ishidaopcua_init_array();

				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), pdiscovery_urls);
				ishidaopcua_set_array(1, ishidaopcua_TYPE_ID_STRING, pdiscovery_urls, pendpoint_description->server->discovery_urls);

				pendpoint_description->server_certificate = ishidaopcua_init_string();

				ishidaopcua_set_endpoint_description_security_mode(ishidaopcua_MESSAGE_SECURITY_MODE_NONE, pendpoint_description);

				pendpoint_description->security_policy_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_SECURITY_POLICY_URI_NONE), pendpoint_description->security_policy_uri);

				puser_token_policy[0].policy_id = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) "ishida-anonymous-user-token-policy-1"), puser_token_policy[0].policy_id);
				puser_token_policy[0].token_type = 0;
				puser_token_policy[0].issued_token_type = ishidaopcua_init_string();
				puser_token_policy[0].issuer_endpoint_url = ishidaopcua_init_string();
				puser_token_policy[0].security_policy_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_SECURITY_POLICY_URI_NONE), puser_token_policy[0].security_policy_uri);

				ishidaopcua_set_array(1,ishidaopcua_TYPE_ID_USER_TOKEN_POLICY,puser_token_policy,user_token_policy_array);

				pendpoint_description->user_identity_tokens = user_token_policy_array;

				pendpoint_description->transport_profile_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_OPCUA_BINARY_TRANSPORT_PROFILE), pendpoint_description->transport_profile_uri);

				ishidaopcua_set_endpoint_description_security_level((ishidaopcua_BYTE)0x00, pendpoint_description);

				ishidaopcua_set_array(1, ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION, pendpoint_description, pendpoint_description_array);

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pendpoint_description_array, ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION, 0,ipwrite_size);

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pserver_certificate_array, ishidaopcua_TYPE_ID_SIGNED_SOFTWARE_CERTIFICATE, 0,ipwrite_size);

				ishidaopcua_encode_SIGNATURE((ishidaopcua_BYTE*) ipoutput_buffer, psignature, ipwrite_size);

				ishidaopcua_encode_UINT32(ipoutput_buffer, &server_max_message_buffer_size,ipwrite_size);
			}
			else if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 467){
				ishidaopcua_SIGNATURE* pclient_signature = ishidaopcua_init_signature();
				ishidaopcua_ARRAY* pclient_software_certificates = ishidaopcua_init_array();
				ishidaopcua_ARRAY* plocale_ids = ishidaopcua_init_array();
				ishidaopcua_EXTENSION_OBJECT* puser_identity_token = ishidaopcua_init_extension_object();
				ishidaopcua_SIGNATURE* puser_token_signature = ishidaopcua_init_signature();
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaeutz_LARGE_INT* pcurrent_time_ms = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				ishidaopcua_BYTESTRING* pserver_nonce = ishidaopcua_init_string();
				ishidaopcua_ARRAY* presults = ishidaopcua_init_array();
				ishidaopcua_ARRAY* pdiagnostic_infos = ishidaopcua_init_array();
				ishidaopcua_STATUS_CODE status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
				/*puts("DECODER: Activate session request confirmed.");*/

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				/*printf("Parsing offset %u\n",*pparsing_offset);*/
				/*printf("Request handle %u\n",prequest_header->request_handle);*/

				ishidaopcua_decode_SIGNATURE((ishidaopcua_BYTE*)ipinput_buffer, pclient_signature, pparsing_offset);

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, pclient_software_certificates, ishidaopcua_TYPE_ID_SIGNED_SOFTWARE_CERTIFICATE, 0, pparsing_offset);
				/*printf("DECODER: Client software certs array: size: %u\n",pclient_software_certificates->size);*/

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, plocale_ids, ishidaopcua_TYPE_ID_STRING, 0, pparsing_offset);
				/*printf("DECODER: Locale IDs array: size: %u\n",plocale_ids->size);*/

				ishidaopcua_decode_EXTENSION_OBJECT((ishidaopcua_BYTE*)ipinput_buffer, puser_identity_token, pparsing_offset);

				ishidaopcua_decode_SIGNATURE((ishidaopcua_BYTE*)ipinput_buffer, puser_token_signature, pparsing_offset);

				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 470;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				pserver_nonce->length = 32;
				pserver_nonce->text = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, pserver_nonce->length);
				ishidaeutz_copy_byte_array(((char*) &(isaved_client_args->session_id)),((char*) pserver_nonce->text),0,0, 4);
				if(isaved_client_args != NULL && isaved_client_args->session_authentication_token != NULL && isaved_client_args->session_authentication_token->int_bytearray_size <= 0){
					ishidaeutz_LARGE_INT* pserver_nonce_text_part = deep_copy_ishidaeutz_LARGE_INTs(isaved_client_args->session_authentication_token);

					yield_ishidaeutz_LARGE_INT_int_byte_array(pserver_nonce_text_part, ishidaopcua_SERVER_LARGE_INT_ENDIANNESS, 8);

					ishidaeutz_copy_byte_array(((char*) &(pserver_nonce_text_part->int_bytearray[0])),((char*) pserver_nonce->text),0,4, ((isaved_client_args->session_authentication_token->int_bytearray_size <= 8)?isaved_client_args->session_authentication_token->int_bytearray_size:8));
				}

				ishidaeutz_copy_byte_array(((char*) &(iclient_args->sequence_number)),((char*) pserver_nonce->text),0,12, 4);
				ishidaeutz_copy_byte_array(((char*) &(iclient_args->client_details->sin_port)),((char*) pserver_nonce->text),0,16, 4);
				stretch_ishidaeutz_LARGE_INT(pcurrent_time_ms, 8);
				ishidaeutz_copy_byte_array(((char*) &(pcurrent_time_ms->number_bytestring[0])),((char*) pserver_nonce->text),0,20, 8);
				ishidaeutz_copy_byte_array(((char*) &(iclient_args->client_details->sin_addr.s_addr)),((char*) pserver_nonce->text),0,28, 4);
				ishidaopcua_encode_BYTESTRING((ishidaopcua_BYTE*) ipoutput_buffer, pserver_nonce, ipwrite_size);

				presults->size = 1;
				presults->type = ishidaopcua_TYPE_ID_STATUS_CODE;
				presults->elements = &status_code;

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, presults, ishidaopcua_TYPE_ID_STATUS_CODE, 0,ipwrite_size);

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pdiagnostic_infos, ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO, 0,ipwrite_size);
			}
			else if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 631){
				ishidaopcua_DURATION* pmax_age = ishidaopcua_malloc(ishidaopcua_TYPE_ID_DURATION, 1);
				ishidaopcua_UINT32* ptimestamps_to_return = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
				ishidaopcua_ARRAY* pnodes_to_read = ishidaopcua_init_array();
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_DATA_VALUE *presults;
				int i;
				ishidaopcua_READ_VALUE_ID* pread_value_id;
				ishidaeutz_HASHMAP* pcache;
				ishidaopcua_ARRAY* presults_array = ishidaopcua_init_array();
				ishidaopcua_ARRAY* pdiagnostic_infos = ishidaopcua_init_array();
				puts("DECODER: Read request confirmed.");

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				printf("Parsing offset %u\n",*pparsing_offset);
				printf("Request handle %u\n",prequest_header->request_handle);

				ishidaopcua_decode_DURATION((ishidaopcua_BYTE*)ipinput_buffer, pmax_age, pparsing_offset);
				/*printf("DECODER: Max age: %llf\n", *pmax_age);*/

				ishidaopcua_decode_UINT32((ishidaopcua_BYTE*)ipinput_buffer, ptimestamps_to_return, pparsing_offset);
				printf("DECODER: Timestamps to return: %u\n", *ptimestamps_to_return);

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, pnodes_to_read, ishidaopcua_TYPE_ID_READ_VALUE_ID, 0, pparsing_offset);
				printf("DECODER: Nodes to read array: size: %u\n",pnodes_to_read->size);

				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 634;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				presults = ishidaopcua_malloc(ishidaopcua_TYPE_ID_DATA_VALUE, pnodes_to_read->size);

				pcache = ishidaeutz_init_hashmap((pnodes_to_read->size > 4)? (ishidaopcua_UINT32)(pnodes_to_read->size / 2 ): pnodes_to_read->size);
				puts("Engaging for loop.");
				for(i = 0; i < pnodes_to_read->size; i++){

					pread_value_id = (ishidaopcua_READ_VALUE_ID*)(((unsigned int)(pnodes_to_read->elements))+(i*sizeof(ishidaopcua_READ_VALUE_ID)));

					presults[i].value = ishidaopcua_get_attribute(*pread_value_id, pcache);

					if(presults[i].value == 0 || presults[i].value->encoding_mask == 0){
						presults[i].status_code =ishidaopcua_STATUS_CODE_SEVERITY_BAD_INVALID_ATTRIBUTE;
					}
					else{
						presults[i].status_code =ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
					}

					presults[i].psource_timestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
					presults[i].source_pico_seconds = 0;
					presults[i].pserver_timestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
					presults[i].server_pico_seconds = 0;
				}
				puts("End of for loop");
				presults_array->size = pnodes_to_read->size;
				presults_array->type = ishidaopcua_TYPE_ID_READ_VALUE_ID;
				presults_array->elements = presults;
				puts("End of loop 2");
				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, presults_array, ishidaopcua_TYPE_ID_DATA_VALUE, 0,ipwrite_size);
				puts("Following encode 1");
				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pdiagnostic_infos, ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO, 0,ipwrite_size);
				puts("Following encode 2");
			}
			else if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 422){
				ishidaopcua_STRING* pendpoint_url = ishidaopcua_init_string();
				ishidaopcua_ARRAY* plocale_ids = ishidaopcua_init_array();
				ishidaopcua_ARRAY* pserver_uris = ishidaopcua_init_array();
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_STRING *papplication_description_application_name_locale = ishidaopcua_init_string();
				ishidaopcua_STRING *papplication_description_application_name_text = ishidaopcua_init_string();
				ishidaopcua_APPLICATION_DESCRIPTION* application_description = ishidaopcua_init_application_description();
				ishidaopcua_ARRAY* application_descriptions_array = ishidaopcua_init_array();
				/*puts("DECODER: Find servers request confirmed.");*/

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				/*printf("Parsing offset %u\n",*pparsing_offset);*/
				/*printf("Request handle %u\n",prequest_header->request_handle);*/

				ishidaopcua_decode_STRING((ishidaopcua_BYTE*)ipinput_buffer,pendpoint_url,pparsing_offset);
				/*printf("DECODER: Endpoint URL: %s at length: %u\n", pendpoint_url->text, pendpoint_url->length);*/

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, plocale_ids, ishidaopcua_TYPE_ID_STRING, 0, pparsing_offset);
				/*printf("DECODER: Locale IDs array: size: %u\n",plocale_ids->size);*/

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, pserver_uris, ishidaopcua_TYPE_ID_STRING, 0, pparsing_offset);
				/*printf("DECODER: Locale IDs array: size: %u\n",pserver_uris->size);*/

				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 425;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				application_description->application_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), application_description->application_uri);

				application_description->product_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(((ishidaopcua_BYTE*) ISHIDAOPCUA_MAIN_URL), application_description->product_uri);

				ishidaopcua_set_string(((ishidaopcua_BYTE*) "ISHIDA-OPCUA-SERVER"), papplication_description_application_name_text);

				application_description->application_name = ishidaopcua_init_localized_text();
				ishidaopcua_set_localized_text(papplication_description_application_name_locale, papplication_description_application_name_text, application_description->application_name);

				ishidaopcua_set_application_description_application_type(ishidaopcua_APPLICATION_DESCRIPTION_APPLICATION_TYPE_SERVER, application_description);

				application_description->gateway_server_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(0, application_description->gateway_server_uri);

				application_description->discovery_profile_uri = ishidaopcua_init_string();
				ishidaopcua_set_string(0, application_description->discovery_profile_uri);

				application_description->discovery_urls = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, application_description->discovery_urls);

				application_descriptions_array->size = 1;
				application_descriptions_array->type = ishidaopcua_TYPE_ID_APPLICATION_DESCRIPTION;
				application_descriptions_array->elements = application_description;

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, application_descriptions_array, ishidaopcua_TYPE_ID_APPLICATION_DESCRIPTION, 0,ipwrite_size);
			}
			else if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 527){
				ishidaopcua_ARRAY* pnodes_to_browse = ishidaopcua_init_array();
				ishidaopcua_VIEW_DESCRIPTION* pview_description =  ishidaopcua_init_view_description();
				ishidaopcua_COUNTER* requested_max_references_per_node =  ishidaopcua_malloc(ishidaopcua_TYPE_ID_COUNTER,1);
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_BROWSE_RESULT *presults;
				ishidaopcua_ARRAY* pbrowse_results = ishidaopcua_init_array();
				int i;
				ishidaeutz_HASHMAP* pcache;
				ishidaopcua_ARRAY* preference_description;
				ishidaopcua_ARRAY* pdiagnostic_infos = ishidaopcua_init_array();
				ishidaopcua_STATUS_CODE status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;
				ishidaopcua_BROWSE_DESCRIPTION* pbrowse_description;
				/*puts("DECODER: Browse request confirmed.");*/

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				/*printf("Parsing offset %u\n",*pparsing_offset);*/
				/*printf("Request handle %u\n",prequest_header->request_handle);*/

				ishidaopcua_decode_VIEW_DESCRIPTION((ishidaopcua_BYTE*)ipinput_buffer, pview_description, pparsing_offset);

				/*printf("DECODER: View ID is %d\n", pview_description->pview_id->identifier.numeric);*/

				ishidaopcua_decode_COUNTER((ishidaopcua_BYTE*)ipinput_buffer, requested_max_references_per_node, pparsing_offset);

				/*printf("DECODER: Counter is %d\n", *requested_max_references_per_node);*/

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, pnodes_to_browse, ishidaopcua_TYPE_ID_BROWSE_DESCRIPTION, 0, pparsing_offset);
				/*printf("DECODER: Nodes to read array: size: %u\n",pnodes_to_browse->size);*/

				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 530;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				presults = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BROWSE_RESULT,pnodes_to_browse->size);

				pcache = ishidaeutz_init_hashmap((pnodes_to_browse->size > 4)? (ishidaopcua_UINT32)(pnodes_to_browse->size / 2 ): pnodes_to_browse->size);
				/*puts("BROWSE SERVICE: Going into for loop");*/
				for(i = 0; i < pnodes_to_browse->size; i++)
				{
					/*printf("In loop; Used up predef memory %d\n", predef_mem_used);*/
					pbrowse_description = (ishidaopcua_BROWSE_DESCRIPTION*)(((unsigned int)(pnodes_to_browse->elements))+(i*sizeof(ishidaopcua_BROWSE_DESCRIPTION)));

					preference_description = (ishidaopcua_ARRAY*) ishidaopcua_get_reference_description(pbrowse_description, &status_code, pcache);

					presults[i].status_code = status_code;
					presults[i].pcontinuation_point = ishidaopcua_init_string();

					if(status_code == ishidaopcua_STATUS_CODE_SEVERITY_GOOD)
					{
						presults[i].preference_descriptions = preference_description;
					}
					else
					{
						presults[i].preference_descriptions = ishidaopcua_init_array();
					}
				}
				/*puts("BROWSE SERVICE: Exiting out of loop");*/

				pbrowse_results->type = ishidaopcua_TYPE_ID_BROWSE_RESULT;
				pbrowse_results->size = pnodes_to_browse->size;
				pbrowse_results->elements = presults;

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pbrowse_results, ishidaopcua_TYPE_ID_BROWSE_RESULT, 0, ipwrite_size);

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pdiagnostic_infos, ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO, 0,ipwrite_size);
			}
			else if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 673){
				ishidaopcua_ARRAY* pdiagnostic_infos = ishidaopcua_init_array();
				ishidaopcua_ARRAY* pnodes_to_write = ishidaopcua_init_array();
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_STATUS_CODE *presults;
				ishidaopcua_ARRAY* presults_array = ishidaopcua_init_array();
				int i;
				ishidaopcua_WRITE_VALUE* pwrite_value;
				/*puts("DECODER: Write request confirmed.");*/

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				/*printf("Parsing offset %u\n",*pparsing_offset);*/
				/*printf("Request handle %u\n",prequest_header->request_handle);*/

				ishidaopcua_decode_ARRAY((ishidaopcua_BYTE*)ipinput_buffer, pnodes_to_write, ishidaopcua_TYPE_ID_WRITE_VALUE, 0, pparsing_offset);
				/*printf("DECODER: Nodes to write array: size: %u\n",pnodes_to_write->size);*/

				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 676;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				presults = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STATUS_CODE, pnodes_to_write->size);

				for(i = 0; i < pnodes_to_write->size; i++){
					pwrite_value = (ishidaopcua_WRITE_VALUE*)(((unsigned int)(pnodes_to_write->elements))+(i*sizeof(ishidaopcua_WRITE_VALUE)));
					presults[i] = ishidaopcua_set_attribute(*pwrite_value);
				}

				presults_array->size = pnodes_to_write->size;
				presults_array->type = ishidaopcua_TYPE_ID_READ_VALUE_ID;
				presults_array->elements = presults;

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, presults_array, ishidaopcua_TYPE_ID_STATUS_CODE, 0,ipwrite_size);

				ishidaopcua_encode_ARRAY((ishidaopcua_BYTE*) ipoutput_buffer, pdiagnostic_infos, ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO, 0,ipwrite_size);
			}
			else if(puath_token_nodeid->identifier_type == ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC && puath_token_nodeid->identifier.numeric == 473){
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				/*puts("DECODER: Close session request confirmed.");*/

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				/*printf("Parsing offset %u\n",*pparsing_offset);*/
				/*printf("Request handle %u\n",prequest_header->request_handle);*/


				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 476;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);

				if(isaved_client_args != NULL){
					if(isaved_client_args->security_token != NULL){
						memblock_free(isaved_client_args->security_token);
						isaved_client_args->security_token = NULL;
					}


					if(isaved_client_args->session_authentication_token != NULL){
						if(isaved_client_args->session_authentication_token->number_bytestring != NULL && isaved_client_args->session_authentication_token->number_bytestring_size > 0){
							memblock_free(isaved_client_args->session_authentication_token->number_bytestring);
						}

						memblock_free(isaved_client_args->session_authentication_token);
						isaved_client_args->session_authentication_token = NULL;
					}

					memblock_free(isaved_client_args);
				}
				ishidaeutz_put_hashmap(ishidaopcua_client_session_hmap,inet_ntoa(iclient_args->client_details->sin_addr),NULL);
			}
			else {
				ishidaopcua_RESPONSE_HEADER* presponse_header = ishidaopcua_malloc(ishidaopcua_TYPE_ID_RESPONSE_HEADER, 1);
				ishidaopcua_NODE_ID* ptype_id_node_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_NODE_ID, 1);
				ishidaopcua_BOOLEAN* pdelete_all_subscriptions = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN, 1);

				puts("DECODER: Service unsupported.");
				ishidaopcua_prepare_response(ipoutput_buffer, ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header), isaved_client_args, ipwrite_size);

				ishidaopcua_decode_REQUEST_HEADER((ishidaopcua_BYTE*)ipinput_buffer,prequest_header,pparsing_offset);
				/*printf("Parsing offset %u\n",*pparsing_offset);*/
				/*printf("Request handle %u\n",prequest_header->request_handle);*/

				ishidaopcua_decode_BOOLEAN((ishidaopcua_BYTE*)ipinput_buffer,pdelete_all_subscriptions,pparsing_offset);

				/*printf("Delete all subscriptions %x\n",*pdelete_all_subscriptions);*/

				ptype_id_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				ptype_id_node_id->identifier.numeric = 0;

				ishidaopcua_encode_NODE_ID((ishidaopcua_BYTE*) ipoutput_buffer, ptype_id_node_id, ipwrite_size);

				presponse_header->additional_header = ishidaopcua_init_extension_object();
				presponse_header->additional_header->typed_id = ishidaopcua_init_expanded_node_id();
				presponse_header->additional_header->typed_id->node_id = ishidaopcua_init_node_id();
				presponse_header->additional_header->typed_id->node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
				presponse_header->additional_header->typed_id->node_id->identifier.numeric = 0;
				presponse_header->additional_header->encoding = ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
				presponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
				presponse_header->service_diagnostics->additional_info = 0;
				presponse_header->service_diagnostics->inner_diagnostic_info = 0;
				presponse_header->string_table = ishidaopcua_init_array();
				ishidaopcua_set_array(0, ishidaopcua_TYPE_ID_STRING, 0, presponse_header->string_table);
				presponse_header->ptimestamp = deep_copy_ishidaeutz_LARGE_INTs(pcurrent_timestamp_ms);
				presponse_header->request_handle = prequest_header->request_handle;
				presponse_header->service_result = ishidaopcua_STATUS_CODE_BAD_SERVICE_UNSUPPORTED;

				ishidaopcua_encode_RESPONSE_HEADER((ishidaopcua_BYTE*) ipoutput_buffer, presponse_header,ipwrite_size);
			}

			memblock_free(iclient_args);
		}
		else if(memcmp("CLO",ishidaopcua_get_tcp_message_header_message_type(ptcp_message_header),3) == 0)
		{
			/*puts("DECODER: Close secure channel request confirmed.");*/

			*ipclose_socket = 1;
			memblock_free(iclient_args);
		}
	}

	ishidaopcua_set_tcp_message_header_message_size(((*ipwrite_size) + 8), ptcp_message_header);
	/*printf("ENCODER: Message size: %u\n",ishidaopcua_get_tcp_message_header_message_size(ptcp_message_header));*/

	/*printf("ENCODER: Chunk type: %c\n",ishidaopcua_get_tcp_message_header_reserved(ptcp_message_header));*/

	ishidaopcua_encode_TCP_MESSAGE_HEADER((ishidaopcua_BYTE*)ipoutput_buffer, ptcp_message_header, (ishidaopcua_UINT32*) ipwrite_size);
	/*printf("ENCODER: Write size: %u\n",*ipwrite_size);*/

	/*printf("ENCODER: Message size: %d\n",*ipwrite_size);*/
}

ishidaeutz_LARGE_INT* pishidaopcua_UINT64_MIN;
ishidaeutz_LARGE_INT* pishidaopcua_UINT64_MAX;
ishidaeutz_LARGE_INT* pishidaopcua_DATE_TIME_MIN;
ishidaeutz_LARGE_INT* pishidaopcua_DATE_TIME_MAX;
ishidaeutz_LARGE_INT* pishidaopcua_INT64_MIN;
ishidaeutz_LARGE_INT* pishidaopcua_INT64_MAX;
ishidaeutz_LARGE_INT* pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS;
ishidaeutz_LARGE_INT* pishidaopcua_MILLI_SECONDS_TO_100_NANO_SECONDS;
ishidaeutz_LARGE_INT* pishidaopcua_SECONDS_TO_100_NANO_SECONDS;
ishidaeutz_LARGE_INT* pishidaopcua_SECONDS_TO_MILLI_SECONDS;

static ishidaopcua_BINARY_SERVER_ARGS server_args;

int main(int argc, char *argv[])
{

	ishidaeutz_mtrack_init();
	ishidaeutz_mtrack_pause();

	ishidaopcua_SERVER_LARGE_INT_ENDIANNESS = ((ishidaeutz_system_is_little_endian() == 1)?ishidaeutz_LARGE_INT_ENDIANNESS_LITTLE_ENDIAN: ishidaeutz_LARGE_INT_ENDIANNESS_BIG_ENDIAN);

	ishidaopcua_client_session_hmap = ishidaeutz_init_hashmap(ISHIDAOPCUA_MAX_CLIENT_CONNECTIONS);
	ptransient_node_memory = ishidaeutz_init_hashmap(380);
	puts("*****START ISHIDA OPC UA SERVER*****");

	pishidaopcua_UINT64_MIN = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("0", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_UINT64_MAX = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("18446744073709551615", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_DATE_TIME_MIN = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("0", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_DATE_TIME_MAX = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("18446744073709551615", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_INT64_MIN = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("9223372036854775808", ishidaeutz_LARGE_INT_SIGN_NEGATIVE);
	pishidaopcua_INT64_MAX = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("9223372036854775807", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("116444736000000000", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_MILLI_SECONDS_TO_100_NANO_SECONDS = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("10000", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_SECONDS_TO_100_NANO_SECONDS = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("10000000", ishidaeutz_LARGE_INT_SIGN_POSITIVE);
	pishidaopcua_SECONDS_TO_MILLI_SECONDS = create_ishidaeutz_LARGE_INT_from_ASCII_STRING("1000", ishidaeutz_LARGE_INT_SIGN_POSITIVE);

	memset(&server_args, 0, (size_t) sizeof(ishidaopcua_BINARY_SERVER_ARGS));
	server_args.server_ip_addr = ISHIDAOPCUA_SERVER_IP;
	server_args.server_port = ISHIDAOPCUA_SERVER_PORT;
	ishidaopcua_binary_server_init(server_args);

	
	

	/*
	unsigned int i = 1;
	char*c = (char*)&i;
	puts("*****END ISHIDA OPC UA SERVER*****");
   
   if (*c)    
       puts("System is Little endian");
   else
       puts("Big endian");
	
	puts("Endian check done, now checking if alloca pases");
	
	for(;;);

	*/

	return 1;
}
/*************************************** ISHIDAOPCUA_MAIN END ***************************************/
