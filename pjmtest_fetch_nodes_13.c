#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_13(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* BaseObjectType ********/


case 58 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 58;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BaseObjectType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BaseObjectType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The base type for all object nodes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

puts("finished adding node >> 58 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_0;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_0);
*Node_58_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_0_typed_id,Node_58_ref_node_target_id_45_0);
*Node_58_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_0_is_inverse, Node_58_ref_node_target_id_45_0);
*Node_58_retrieved_reference_45_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_0_target_id, Node_58_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_0->references,"1",Node_58_ref_node_target_id_45_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_1;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_1);
*Node_58_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_1_typed_id,Node_58_ref_node_target_id_45_1);
*Node_58_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_1_is_inverse, Node_58_ref_node_target_id_45_1);
*Node_58_retrieved_reference_45_1_target_id = 75;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_1_target_id, Node_58_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_1->references,"2",Node_58_ref_node_target_id_45_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_2;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_2);
*Node_58_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_2_typed_id,Node_58_ref_node_target_id_45_2);
*Node_58_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_2_is_inverse, Node_58_ref_node_target_id_45_2);
*Node_58_retrieved_reference_45_2_target_id = 76;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_2_target_id, Node_58_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_2->references,"3",Node_58_ref_node_target_id_45_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_3;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_3);
*Node_58_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_3_typed_id,Node_58_ref_node_target_id_45_3);
*Node_58_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_3_is_inverse, Node_58_ref_node_target_id_45_3);
*Node_58_retrieved_reference_45_3_target_id = 77;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_3_target_id, Node_58_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_3->references,"4",Node_58_ref_node_target_id_45_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_58_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_58_retrieved_reference_35_inverse_0);
*Node_58_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_35_inverse_0_typed_id,Node_58_ref_node_target_id_35_inverse_0);
*Node_58_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_35_inverse_true_0_is_inverse, Node_58_ref_node_target_id_35_inverse_0);
*Node_58_retrieved_reference_35_inverse_0_target_id = 88;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_35_inverse_0_target_id, Node_58_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_35_inverse_0->references,"1",Node_58_ref_node_target_id_35_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_4;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_4);
*Node_58_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_4_typed_id,Node_58_ref_node_target_id_45_4);
*Node_58_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_4_is_inverse, Node_58_ref_node_target_id_45_4);
*Node_58_retrieved_reference_45_4_target_id = 2004;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_4_target_id, Node_58_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_4->references,"5",Node_58_ref_node_target_id_45_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_5;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_5);
*Node_58_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_5_typed_id,Node_58_ref_node_target_id_45_5);
*Node_58_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_5_is_inverse, Node_58_ref_node_target_id_45_5);
*Node_58_retrieved_reference_45_5_target_id = 2013;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_5_target_id, Node_58_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_5->references,"6",Node_58_ref_node_target_id_45_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_6;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_6);
*Node_58_retrieved_reference_45_6_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_6_typed_id,Node_58_ref_node_target_id_45_6);
*Node_58_retrieved_reference_45_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_6_is_inverse, Node_58_ref_node_target_id_45_6);
*Node_58_retrieved_reference_45_6_target_id = 2020;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_6_target_id, Node_58_ref_node_target_id_45_6);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_6->references,"7",Node_58_ref_node_target_id_45_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_7;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_7);
*Node_58_retrieved_reference_45_7_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_7_typed_id,Node_58_ref_node_target_id_45_7);
*Node_58_retrieved_reference_45_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_7_is_inverse, Node_58_ref_node_target_id_45_7);
*Node_58_retrieved_reference_45_7_target_id = 2026;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_7_target_id, Node_58_ref_node_target_id_45_7);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_7->references,"8",Node_58_ref_node_target_id_45_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_8;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_8);
*Node_58_retrieved_reference_45_8_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_8_typed_id,Node_58_ref_node_target_id_45_8);
*Node_58_retrieved_reference_45_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_8_is_inverse, Node_58_ref_node_target_id_45_8);
*Node_58_retrieved_reference_45_8_target_id = 2029;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_8_target_id, Node_58_ref_node_target_id_45_8);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_8->references,"9",Node_58_ref_node_target_id_45_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_9;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_9);
*Node_58_retrieved_reference_45_9_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_9_typed_id,Node_58_ref_node_target_id_45_9);
*Node_58_retrieved_reference_45_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_9_is_inverse, Node_58_ref_node_target_id_45_9);
*Node_58_retrieved_reference_45_9_target_id = 2033;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_9_target_id, Node_58_ref_node_target_id_45_9);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_9->references,"10",Node_58_ref_node_target_id_45_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_10;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_10);
*Node_58_retrieved_reference_45_10_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_10_typed_id,Node_58_ref_node_target_id_45_10);
*Node_58_retrieved_reference_45_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_10_is_inverse, Node_58_ref_node_target_id_45_10);
*Node_58_retrieved_reference_45_10_target_id = 2034;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_10_target_id, Node_58_ref_node_target_id_45_10);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_10->references,"11",Node_58_ref_node_target_id_45_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_11;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_11);
*Node_58_retrieved_reference_45_11_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_11_typed_id,Node_58_ref_node_target_id_45_11);
*Node_58_retrieved_reference_45_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_11_is_inverse, Node_58_ref_node_target_id_45_11);
*Node_58_retrieved_reference_45_11_target_id = 11575;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_11_target_id, Node_58_ref_node_target_id_45_11);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_11->references,"12",Node_58_ref_node_target_id_45_11); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_12;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_12 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_12_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_12_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_12_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_12);
*Node_58_retrieved_reference_45_12_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_12_typed_id,Node_58_ref_node_target_id_45_12);
*Node_58_retrieved_reference_45_false_12_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_12_is_inverse, Node_58_ref_node_target_id_45_12);
*Node_58_retrieved_reference_45_12_target_id = 11616;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_12_target_id, Node_58_ref_node_target_id_45_12);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_12->references,"13",Node_58_ref_node_target_id_45_12); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_13;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_13 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_13_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_13_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_13_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_13);
*Node_58_retrieved_reference_45_13_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_13_typed_id,Node_58_ref_node_target_id_45_13);
*Node_58_retrieved_reference_45_false_13_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_13_is_inverse, Node_58_ref_node_target_id_45_13);
*Node_58_retrieved_reference_45_13_target_id = 11645;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_13_target_id, Node_58_ref_node_target_id_45_13);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_13->references,"14",Node_58_ref_node_target_id_45_13); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_14;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_14 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_14_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_14_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_14_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_14);
*Node_58_retrieved_reference_45_14_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_14_typed_id,Node_58_ref_node_target_id_45_14);
*Node_58_retrieved_reference_45_false_14_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_14_is_inverse, Node_58_ref_node_target_id_45_14);
*Node_58_retrieved_reference_45_14_target_id = 2041;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_14_target_id, Node_58_ref_node_target_id_45_14);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_14->references,"15",Node_58_ref_node_target_id_45_14); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_15;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_15 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_15_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_15_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_15_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_15);
*Node_58_retrieved_reference_45_15_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_15_typed_id,Node_58_ref_node_target_id_45_15);
*Node_58_retrieved_reference_45_false_15_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_15_is_inverse, Node_58_ref_node_target_id_45_15);
*Node_58_retrieved_reference_45_15_target_id = 2340;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_15_target_id, Node_58_ref_node_target_id_45_15);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_15->references,"16",Node_58_ref_node_target_id_45_15); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_16;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_16 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_16_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_16_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_16_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_16);
*Node_58_retrieved_reference_45_16_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_16_typed_id,Node_58_ref_node_target_id_45_16);
*Node_58_retrieved_reference_45_false_16_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_16_is_inverse, Node_58_ref_node_target_id_45_16);
*Node_58_retrieved_reference_45_16_target_id = 2299;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_16_target_id, Node_58_ref_node_target_id_45_16);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_16->references,"17",Node_58_ref_node_target_id_45_16); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_17;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_17 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_17_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_17_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_17_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_17);
*Node_58_retrieved_reference_45_17_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_17_typed_id,Node_58_ref_node_target_id_45_17);
*Node_58_retrieved_reference_45_false_17_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_17_is_inverse, Node_58_ref_node_target_id_45_17);
*Node_58_retrieved_reference_45_17_target_id = 2307;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_17_target_id, Node_58_ref_node_target_id_45_17);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_17->references,"18",Node_58_ref_node_target_id_45_17); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_18;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_18 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_18_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_18_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_18_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_18);
*Node_58_retrieved_reference_45_18_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_18_typed_id,Node_58_ref_node_target_id_45_18);
*Node_58_retrieved_reference_45_false_18_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_18_is_inverse, Node_58_ref_node_target_id_45_18);
*Node_58_retrieved_reference_45_18_target_id = 2310;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_18_target_id, Node_58_ref_node_target_id_45_18);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_18->references,"19",Node_58_ref_node_target_id_45_18); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_19;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_19 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_19_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_19_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_19_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_19);
*Node_58_retrieved_reference_45_19_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_19_typed_id,Node_58_ref_node_target_id_45_19);
*Node_58_retrieved_reference_45_false_19_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_19_is_inverse, Node_58_ref_node_target_id_45_19);
*Node_58_retrieved_reference_45_19_target_id = 15744;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_19_target_id, Node_58_ref_node_target_id_45_19);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_19->references,"20",Node_58_ref_node_target_id_45_19); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_20;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_20 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_20_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_20_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_20_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_20);
*Node_58_retrieved_reference_45_20_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_20_typed_id,Node_58_ref_node_target_id_45_20);
*Node_58_retrieved_reference_45_false_20_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_20_is_inverse, Node_58_ref_node_target_id_45_20);
*Node_58_retrieved_reference_45_20_target_id = 15607;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_20_target_id, Node_58_ref_node_target_id_45_20);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_20->references,"21",Node_58_ref_node_target_id_45_20); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_21;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_21 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_21_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_21_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_21_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_21);
*Node_58_retrieved_reference_45_21_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_21_typed_id,Node_58_ref_node_target_id_45_21);
*Node_58_retrieved_reference_45_false_21_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_21_is_inverse, Node_58_ref_node_target_id_45_21);
*Node_58_retrieved_reference_45_21_target_id = 15620;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_21_target_id, Node_58_ref_node_target_id_45_21);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_21->references,"22",Node_58_ref_node_target_id_45_21); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_22;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_22 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_22_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_22_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_22_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_22);
*Node_58_retrieved_reference_45_22_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_22_typed_id,Node_58_ref_node_target_id_45_22);
*Node_58_retrieved_reference_45_false_22_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_22_is_inverse, Node_58_ref_node_target_id_45_22);
*Node_58_retrieved_reference_45_22_target_id = 11163;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_22_target_id, Node_58_ref_node_target_id_45_22);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_22->references,"23",Node_58_ref_node_target_id_45_22); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_23;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_23 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_23_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_23_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_23_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_23);
*Node_58_retrieved_reference_45_23_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_23_typed_id,Node_58_ref_node_target_id_45_23);
*Node_58_retrieved_reference_45_false_23_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_23_is_inverse, Node_58_ref_node_target_id_45_23);
*Node_58_retrieved_reference_45_23_target_id = 17279;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_23_target_id, Node_58_ref_node_target_id_45_23);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_23->references,"24",Node_58_ref_node_target_id_45_23); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_24;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_24 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_24_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_24_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_24_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_24);
*Node_58_retrieved_reference_45_24_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_24_typed_id,Node_58_ref_node_target_id_45_24);
*Node_58_retrieved_reference_45_false_24_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_24_is_inverse, Node_58_ref_node_target_id_45_24);
*Node_58_retrieved_reference_45_24_target_id = 2318;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_24_target_id, Node_58_ref_node_target_id_45_24);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_24->references,"25",Node_58_ref_node_target_id_45_24); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_25;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_25 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_25_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_25_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_25_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_25);
*Node_58_retrieved_reference_45_25_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_25_typed_id,Node_58_ref_node_target_id_45_25);
*Node_58_retrieved_reference_45_false_25_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_25_is_inverse, Node_58_ref_node_target_id_45_25);
*Node_58_retrieved_reference_45_25_target_id = 2330;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_25_target_id, Node_58_ref_node_target_id_45_25);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_25->references,"26",Node_58_ref_node_target_id_45_25); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_26;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_26 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_26_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_26_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_26_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_26);
*Node_58_retrieved_reference_45_26_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_26_typed_id,Node_58_ref_node_target_id_45_26);
*Node_58_retrieved_reference_45_false_26_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_26_is_inverse, Node_58_ref_node_target_id_45_26);
*Node_58_retrieved_reference_45_26_target_id = 12555;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_26_target_id, Node_58_ref_node_target_id_45_26);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_26->references,"27",Node_58_ref_node_target_id_45_26); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_27;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_27 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_27_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_27_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_27_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_27);
*Node_58_retrieved_reference_45_27_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_27_typed_id,Node_58_ref_node_target_id_45_27);
*Node_58_retrieved_reference_45_false_27_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_27_is_inverse, Node_58_ref_node_target_id_45_27);
*Node_58_retrieved_reference_45_27_target_id = 12556;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_27_target_id, Node_58_ref_node_target_id_45_27);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_27->references,"28",Node_58_ref_node_target_id_45_27); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_28;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_28 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_28_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_28_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_28_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_28);
*Node_58_retrieved_reference_45_28_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_28_typed_id,Node_58_ref_node_target_id_45_28);
*Node_58_retrieved_reference_45_false_28_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_28_is_inverse, Node_58_ref_node_target_id_45_28);
*Node_58_retrieved_reference_45_28_target_id = 12581;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_28_target_id, Node_58_ref_node_target_id_45_28);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_28->references,"29",Node_58_ref_node_target_id_45_28); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_29;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_29 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_29_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_29_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_29_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_29);
*Node_58_retrieved_reference_45_29_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_29_typed_id,Node_58_ref_node_target_id_45_29);
*Node_58_retrieved_reference_45_false_29_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_29_is_inverse, Node_58_ref_node_target_id_45_29);
*Node_58_retrieved_reference_45_29_target_id = 18001;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_29_target_id, Node_58_ref_node_target_id_45_29);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_29->references,"30",Node_58_ref_node_target_id_45_29); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_30;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_30 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_30_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_30_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_30_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_30);
*Node_58_retrieved_reference_45_30_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_30_typed_id,Node_58_ref_node_target_id_45_30);
*Node_58_retrieved_reference_45_false_30_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_30_is_inverse, Node_58_ref_node_target_id_45_30);
*Node_58_retrieved_reference_45_30_target_id = 17852;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_30_target_id, Node_58_ref_node_target_id_45_30);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_30->references,"31",Node_58_ref_node_target_id_45_30); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_31;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_31 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_31_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_31_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_31_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_31);
*Node_58_retrieved_reference_45_31_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_31_typed_id,Node_58_ref_node_target_id_45_31);
*Node_58_retrieved_reference_45_false_31_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_31_is_inverse, Node_58_ref_node_target_id_45_31);
*Node_58_retrieved_reference_45_31_target_id = 11187;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_31_target_id, Node_58_ref_node_target_id_45_31);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_31->references,"32",Node_58_ref_node_target_id_45_31); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_32;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_32 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_32_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_32_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_32_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_32);
*Node_58_retrieved_reference_45_32_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_32_typed_id,Node_58_ref_node_target_id_45_32);
*Node_58_retrieved_reference_45_false_32_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_32_is_inverse, Node_58_ref_node_target_id_45_32);
*Node_58_retrieved_reference_45_32_target_id = 15906;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_32_target_id, Node_58_ref_node_target_id_45_32);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_32->references,"33",Node_58_ref_node_target_id_45_32); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_33;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_33 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_33_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_33_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_33_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_33);
*Node_58_retrieved_reference_45_33_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_33_typed_id,Node_58_ref_node_target_id_45_33);
*Node_58_retrieved_reference_45_false_33_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_33_is_inverse, Node_58_ref_node_target_id_45_33);
*Node_58_retrieved_reference_45_33_target_id = 15471;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_33_target_id, Node_58_ref_node_target_id_45_33);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_33->references,"34",Node_58_ref_node_target_id_45_33); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_34;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_34 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_34_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_34_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_34_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_34);
*Node_58_retrieved_reference_45_34_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_34_typed_id,Node_58_ref_node_target_id_45_34);
*Node_58_retrieved_reference_45_false_34_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_34_is_inverse, Node_58_ref_node_target_id_45_34);
*Node_58_retrieved_reference_45_34_target_id = 14509;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_34_target_id, Node_58_ref_node_target_id_45_34);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_34->references,"35",Node_58_ref_node_target_id_45_34); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_35;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_35 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_35_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_35_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_35_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_35);
*Node_58_retrieved_reference_45_35_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_35_typed_id,Node_58_ref_node_target_id_45_35);
*Node_58_retrieved_reference_45_false_35_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_35_is_inverse, Node_58_ref_node_target_id_45_35);
*Node_58_retrieved_reference_45_35_target_id = 15489;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_35_target_id, Node_58_ref_node_target_id_45_35);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_35->references,"36",Node_58_ref_node_target_id_45_35); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_36;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_36 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_36_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_36_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_36_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_36);
*Node_58_retrieved_reference_45_36_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_36_typed_id,Node_58_ref_node_target_id_45_36);
*Node_58_retrieved_reference_45_false_36_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_36_is_inverse, Node_58_ref_node_target_id_45_36);
*Node_58_retrieved_reference_45_36_target_id = 14209;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_36_target_id, Node_58_ref_node_target_id_45_36);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_36->references,"37",Node_58_ref_node_target_id_45_36); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_37;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_37 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_37_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_37_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_37_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_37);
*Node_58_retrieved_reference_45_37_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_37_typed_id,Node_58_ref_node_target_id_45_37);
*Node_58_retrieved_reference_45_false_37_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_37_is_inverse, Node_58_ref_node_target_id_45_37);
*Node_58_retrieved_reference_45_37_target_id = 17721;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_37_target_id, Node_58_ref_node_target_id_45_37);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_37->references,"38",Node_58_ref_node_target_id_45_37); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_38;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_38 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_38_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_38_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_38_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_38);
*Node_58_retrieved_reference_45_38_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_38_typed_id,Node_58_ref_node_target_id_45_38);
*Node_58_retrieved_reference_45_false_38_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_38_is_inverse, Node_58_ref_node_target_id_45_38);
*Node_58_retrieved_reference_45_38_target_id = 14232;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_38_target_id, Node_58_ref_node_target_id_45_38);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_38->references,"39",Node_58_ref_node_target_id_45_38); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_39;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_39 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_39_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_39_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_39_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_39);
*Node_58_retrieved_reference_45_39_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_39_typed_id,Node_58_ref_node_target_id_45_39);
*Node_58_retrieved_reference_45_false_39_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_39_is_inverse, Node_58_ref_node_target_id_45_39);
*Node_58_retrieved_reference_45_39_target_id = 17997;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_39_target_id, Node_58_ref_node_target_id_45_39);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_39->references,"40",Node_58_ref_node_target_id_45_39); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_40;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_40 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_40_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_40_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_40_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_40);
*Node_58_retrieved_reference_45_40_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_40_typed_id,Node_58_ref_node_target_id_45_40);
*Node_58_retrieved_reference_45_false_40_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_40_is_inverse, Node_58_ref_node_target_id_45_40);
*Node_58_retrieved_reference_45_40_target_id = 17998;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_40_target_id, Node_58_ref_node_target_id_45_40);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_40->references,"41",Node_58_ref_node_target_id_45_40); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_41;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_41 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_41_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_41_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_41_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_41);
*Node_58_retrieved_reference_45_41_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_41_typed_id,Node_58_ref_node_target_id_45_41);
*Node_58_retrieved_reference_45_false_41_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_41_is_inverse, Node_58_ref_node_target_id_45_41);
*Node_58_retrieved_reference_45_41_target_id = 21090;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_41_target_id, Node_58_ref_node_target_id_45_41);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_41->references,"42",Node_58_ref_node_target_id_45_41); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_42;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_42 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_42_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_42_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_42_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_42);
*Node_58_retrieved_reference_45_42_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_42_typed_id,Node_58_ref_node_target_id_45_42);
*Node_58_retrieved_reference_45_false_42_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_42_is_inverse, Node_58_ref_node_target_id_45_42);
*Node_58_retrieved_reference_45_42_target_id = 21091;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_42_target_id, Node_58_ref_node_target_id_45_42);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_42->references,"43",Node_58_ref_node_target_id_45_42); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_43;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_43 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_43_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_43_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_43_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_43);
*Node_58_retrieved_reference_45_43_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_43_typed_id,Node_58_ref_node_target_id_45_43);
*Node_58_retrieved_reference_45_false_43_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_43_is_inverse, Node_58_ref_node_target_id_45_43);
*Node_58_retrieved_reference_45_43_target_id = 15298;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_43_target_id, Node_58_ref_node_target_id_45_43);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_43->references,"44",Node_58_ref_node_target_id_45_43); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_44;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_44 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_44_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_44_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_44_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_44);
*Node_58_retrieved_reference_45_44_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_44_typed_id,Node_58_ref_node_target_id_45_44);
*Node_58_retrieved_reference_45_false_44_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_44_is_inverse, Node_58_ref_node_target_id_45_44);
*Node_58_retrieved_reference_45_44_target_id = 15305;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_44_target_id, Node_58_ref_node_target_id_45_44);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_44->references,"45",Node_58_ref_node_target_id_45_44); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_45;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_45 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_45_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_45_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_45_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_45);
*Node_58_retrieved_reference_45_45_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_45_typed_id,Node_58_ref_node_target_id_45_45);
*Node_58_retrieved_reference_45_false_45_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_45_is_inverse, Node_58_ref_node_target_id_45_45);
*Node_58_retrieved_reference_45_45_target_id = 21096;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_45_target_id, Node_58_ref_node_target_id_45_45);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_45->references,"46",Node_58_ref_node_target_id_45_45); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_46;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_46 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_46_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_46_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_46_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_46);
*Node_58_retrieved_reference_45_46_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_46_typed_id,Node_58_ref_node_target_id_45_46);
*Node_58_retrieved_reference_45_false_46_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_46_is_inverse, Node_58_ref_node_target_id_45_46);
*Node_58_retrieved_reference_45_46_target_id = 15306;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_46_target_id, Node_58_ref_node_target_id_45_46);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_46->references,"47",Node_58_ref_node_target_id_45_46); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_47;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_47 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_47_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_47_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_47_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_47);
*Node_58_retrieved_reference_45_47_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_47_typed_id,Node_58_ref_node_target_id_45_47);
*Node_58_retrieved_reference_45_false_47_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_47_is_inverse, Node_58_ref_node_target_id_45_47);
*Node_58_retrieved_reference_45_47_target_id = 15319;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_47_target_id, Node_58_ref_node_target_id_45_47);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_47->references,"48",Node_58_ref_node_target_id_45_47); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_48;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_48 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_48_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_48_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_48_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_48);
*Node_58_retrieved_reference_45_48_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_48_typed_id,Node_58_ref_node_target_id_45_48);
*Node_58_retrieved_reference_45_false_48_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_48_is_inverse, Node_58_ref_node_target_id_45_48);
*Node_58_retrieved_reference_45_48_target_id = 21104;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_48_target_id, Node_58_ref_node_target_id_45_48);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_48->references,"49",Node_58_ref_node_target_id_45_48); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_49;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_49 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_49_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_49_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_49_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_49);
*Node_58_retrieved_reference_45_49_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_49_typed_id,Node_58_ref_node_target_id_45_49);
*Node_58_retrieved_reference_45_false_49_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_49_is_inverse, Node_58_ref_node_target_id_45_49);
*Node_58_retrieved_reference_45_49_target_id = 15108;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_49_target_id, Node_58_ref_node_target_id_45_49);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_49->references,"50",Node_58_ref_node_target_id_45_49); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_50;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_50 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_50_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_50_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_50_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_50);
*Node_58_retrieved_reference_45_50_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_50_typed_id,Node_58_ref_node_target_id_45_50);
*Node_58_retrieved_reference_45_false_50_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_50_is_inverse, Node_58_ref_node_target_id_45_50);
*Node_58_retrieved_reference_45_50_target_id = 14643;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_50_target_id, Node_58_ref_node_target_id_45_50);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_50->references,"51",Node_58_ref_node_target_id_45_50); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_51;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_51 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_51_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_51_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_51_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_51);
*Node_58_retrieved_reference_45_51_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_51_typed_id,Node_58_ref_node_target_id_45_51);
*Node_58_retrieved_reference_45_false_51_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_51_is_inverse, Node_58_ref_node_target_id_45_51);
*Node_58_retrieved_reference_45_51_target_id = 19677;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_51_target_id, Node_58_ref_node_target_id_45_51);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_51->references,"52",Node_58_ref_node_target_id_45_51); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_58_retrieved_reference_45_52;
ishidaopcua_NODE* Node_58_ref_node_target_id_45_52 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_58_retrieved_reference_45_52_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_false_52_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_58_retrieved_reference_45_52_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_58_retrieved_reference_45_52);
*Node_58_retrieved_reference_45_52_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_58_retrieved_reference_45_52_typed_id,Node_58_ref_node_target_id_45_52);
*Node_58_retrieved_reference_45_false_52_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_58_retrieved_reference_45_false_52_is_inverse, Node_58_ref_node_target_id_45_52);
*Node_58_retrieved_reference_45_52_target_id = 21145;
ishidaopcua_node_set_target_id(Node_58_retrieved_reference_45_52_target_id, Node_58_ref_node_target_id_45_52);
ishidaeutz_put_hashmap(Node_58_retrieved_reference_45_52->references,"53",Node_58_ref_node_target_id_45_52); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* FolderType ********/


case 61 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 61;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("FolderType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("FolderType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for objects that organize other nodes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

puts("finished adding node >> 61 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_61_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_61_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_61_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_61_retrieved_reference_45_inverse_0);
*Node_61_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_61_retrieved_reference_45_inverse_0_typed_id,Node_61_ref_node_target_id_45_inverse_0);
*Node_61_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_61_retrieved_reference_45_inverse_true_0_is_inverse, Node_61_ref_node_target_id_45_inverse_0);
*Node_61_retrieved_reference_45_inverse_0_target_id = 58;
ishidaopcua_node_set_target_id(Node_61_retrieved_reference_45_inverse_0_target_id, Node_61_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_61_retrieved_reference_45_inverse_0->references,"1",Node_61_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_61_retrieved_reference_45_0;
ishidaopcua_NODE* Node_61_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_61_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_61_retrieved_reference_45_0);
*Node_61_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_61_retrieved_reference_45_0_typed_id,Node_61_ref_node_target_id_45_0);
*Node_61_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_61_retrieved_reference_45_false_0_is_inverse, Node_61_ref_node_target_id_45_0);
*Node_61_retrieved_reference_45_0_target_id = 11564;
ishidaopcua_node_set_target_id(Node_61_retrieved_reference_45_0_target_id, Node_61_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_61_retrieved_reference_45_0->references,"1",Node_61_ref_node_target_id_45_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_61_retrieved_reference_45_1;
ishidaopcua_NODE* Node_61_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_61_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_61_retrieved_reference_45_1);
*Node_61_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_61_retrieved_reference_45_1_typed_id,Node_61_ref_node_target_id_45_1);
*Node_61_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_61_retrieved_reference_45_false_1_is_inverse, Node_61_ref_node_target_id_45_1);
*Node_61_retrieved_reference_45_1_target_id = 13353;
ishidaopcua_node_set_target_id(Node_61_retrieved_reference_45_1_target_id, Node_61_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_61_retrieved_reference_45_1->references,"2",Node_61_ref_node_target_id_45_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_61_retrieved_reference_45_2;
ishidaopcua_NODE* Node_61_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_61_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_61_retrieved_reference_45_2);
*Node_61_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_61_retrieved_reference_45_2_typed_id,Node_61_ref_node_target_id_45_2);
*Node_61_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_61_retrieved_reference_45_false_2_is_inverse, Node_61_ref_node_target_id_45_2);
*Node_61_retrieved_reference_45_2_target_id = 16405;
ishidaopcua_node_set_target_id(Node_61_retrieved_reference_45_2_target_id, Node_61_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_61_retrieved_reference_45_2->references,"3",Node_61_ref_node_target_id_45_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_61_retrieved_reference_45_3;
ishidaopcua_NODE* Node_61_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_61_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_61_retrieved_reference_45_3);
*Node_61_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_61_retrieved_reference_45_3_typed_id,Node_61_ref_node_target_id_45_3);
*Node_61_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_61_retrieved_reference_45_false_3_is_inverse, Node_61_ref_node_target_id_45_3);
*Node_61_retrieved_reference_45_3_target_id = 13813;
ishidaopcua_node_set_target_id(Node_61_retrieved_reference_45_3_target_id, Node_61_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_61_retrieved_reference_45_3->references,"4",Node_61_ref_node_target_id_45_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_61_retrieved_reference_45_4;
ishidaopcua_NODE* Node_61_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_61_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_61_retrieved_reference_45_4);
*Node_61_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_61_retrieved_reference_45_4_typed_id,Node_61_ref_node_target_id_45_4);
*Node_61_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_61_retrieved_reference_45_false_4_is_inverse, Node_61_ref_node_target_id_45_4);
*Node_61_retrieved_reference_45_4_target_id = 15452;
ishidaopcua_node_set_target_id(Node_61_retrieved_reference_45_4_target_id, Node_61_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_61_retrieved_reference_45_4->references,"5",Node_61_ref_node_target_id_45_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_61_retrieved_reference_45_5;
ishidaopcua_NODE* Node_61_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_61_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_61_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_61_retrieved_reference_45_5);
*Node_61_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_61_retrieved_reference_45_5_typed_id,Node_61_ref_node_target_id_45_5);
*Node_61_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_61_retrieved_reference_45_false_5_is_inverse, Node_61_ref_node_target_id_45_5);
*Node_61_retrieved_reference_45_5_target_id = 14477;
ishidaopcua_node_set_target_id(Node_61_retrieved_reference_45_5_target_id, Node_61_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_61_retrieved_reference_45_5->references,"6",Node_61_ref_node_target_id_45_5); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BaseVariableType ********/


case 62 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 62;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BaseVariableType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BaseVariableType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The abstract base type for all variable nodes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 24;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -2;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 62 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_62_retrieved_reference_45_0;
ishidaopcua_NODE* Node_62_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_62_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_62_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_62_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_62_retrieved_reference_45_0);
*Node_62_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_62_retrieved_reference_45_0_typed_id,Node_62_ref_node_target_id_45_0);
*Node_62_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_62_retrieved_reference_45_false_0_is_inverse, Node_62_ref_node_target_id_45_0);
*Node_62_retrieved_reference_45_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_62_retrieved_reference_45_0_target_id, Node_62_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_62_retrieved_reference_45_0->references,"1",Node_62_ref_node_target_id_45_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_62_retrieved_reference_45_1;
ishidaopcua_NODE* Node_62_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_62_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_62_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_62_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_62_retrieved_reference_45_1);
*Node_62_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_62_retrieved_reference_45_1_typed_id,Node_62_ref_node_target_id_45_1);
*Node_62_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_62_retrieved_reference_45_false_1_is_inverse, Node_62_ref_node_target_id_45_1);
*Node_62_retrieved_reference_45_1_target_id = 68;
ishidaopcua_node_set_target_id(Node_62_retrieved_reference_45_1_target_id, Node_62_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_62_retrieved_reference_45_1->references,"2",Node_62_ref_node_target_id_45_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_62_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_62_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_62_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_62_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_62_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_62_retrieved_reference_35_inverse_0);
*Node_62_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_62_retrieved_reference_35_inverse_0_typed_id,Node_62_ref_node_target_id_35_inverse_0);
*Node_62_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_62_retrieved_reference_35_inverse_true_0_is_inverse, Node_62_ref_node_target_id_35_inverse_0);
*Node_62_retrieved_reference_35_inverse_0_target_id = 89;
ishidaopcua_node_set_target_id(Node_62_retrieved_reference_35_inverse_0_target_id, Node_62_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_62_retrieved_reference_35_inverse_0->references,"1",Node_62_ref_node_target_id_35_inverse_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BaseDataVariableType ********/


case 63 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 63;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BaseDataVariableType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BaseDataVariableType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for variable that represents a process value.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 24;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -2;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 63 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_63_retrieved_reference_45_inverse_0);
*Node_63_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_inverse_0_typed_id,Node_63_ref_node_target_id_45_inverse_0);
*Node_63_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_inverse_true_0_is_inverse, Node_63_ref_node_target_id_45_inverse_0);
*Node_63_retrieved_reference_45_inverse_0_target_id = 62;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_inverse_0_target_id, Node_63_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_inverse_0->references,"1",Node_63_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_0;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_0);
*Node_63_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_0_typed_id,Node_63_ref_node_target_id_45_0);
*Node_63_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_0_is_inverse, Node_63_ref_node_target_id_45_0);
*Node_63_retrieved_reference_45_0_target_id = 69;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_0_target_id, Node_63_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_0->references,"1",Node_63_ref_node_target_id_45_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_1;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_1);
*Node_63_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_1_typed_id,Node_63_ref_node_target_id_45_1);
*Node_63_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_1_is_inverse, Node_63_ref_node_target_id_45_1);
*Node_63_retrieved_reference_45_1_target_id = 72;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_1_target_id, Node_63_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_1->references,"2",Node_63_ref_node_target_id_45_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_2;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_2);
*Node_63_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_2_typed_id,Node_63_ref_node_target_id_45_2);
*Node_63_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_2_is_inverse, Node_63_ref_node_target_id_45_2);
*Node_63_retrieved_reference_45_2_target_id = 2137;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_2_target_id, Node_63_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_2->references,"3",Node_63_ref_node_target_id_45_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_3;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_3);
*Node_63_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_3_typed_id,Node_63_ref_node_target_id_45_3);
*Node_63_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_3_is_inverse, Node_63_ref_node_target_id_45_3);
*Node_63_retrieved_reference_45_3_target_id = 2138;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_3_target_id, Node_63_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_3->references,"4",Node_63_ref_node_target_id_45_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_4;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_4);
*Node_63_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_4_typed_id,Node_63_ref_node_target_id_45_4);
*Node_63_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_4_is_inverse, Node_63_ref_node_target_id_45_4);
*Node_63_retrieved_reference_45_4_target_id = 3051;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_4_target_id, Node_63_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_4->references,"5",Node_63_ref_node_target_id_45_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_5;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_5);
*Node_63_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_5_typed_id,Node_63_ref_node_target_id_45_5);
*Node_63_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_5_is_inverse, Node_63_ref_node_target_id_45_5);
*Node_63_retrieved_reference_45_5_target_id = 2150;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_5_target_id, Node_63_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_5->references,"6",Node_63_ref_node_target_id_45_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_6;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_6);
*Node_63_retrieved_reference_45_6_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_6_typed_id,Node_63_ref_node_target_id_45_6);
*Node_63_retrieved_reference_45_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_6_is_inverse, Node_63_ref_node_target_id_45_6);
*Node_63_retrieved_reference_45_6_target_id = 2164;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_6_target_id, Node_63_ref_node_target_id_45_6);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_6->references,"7",Node_63_ref_node_target_id_45_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_7;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_7);
*Node_63_retrieved_reference_45_7_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_7_typed_id,Node_63_ref_node_target_id_45_7);
*Node_63_retrieved_reference_45_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_7_is_inverse, Node_63_ref_node_target_id_45_7);
*Node_63_retrieved_reference_45_7_target_id = 2165;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_7_target_id, Node_63_ref_node_target_id_45_7);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_7->references,"8",Node_63_ref_node_target_id_45_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_8;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_8);
*Node_63_retrieved_reference_45_8_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_8_typed_id,Node_63_ref_node_target_id_45_8);
*Node_63_retrieved_reference_45_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_8_is_inverse, Node_63_ref_node_target_id_45_8);
*Node_63_retrieved_reference_45_8_target_id = 2171;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_8_target_id, Node_63_ref_node_target_id_45_8);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_8->references,"9",Node_63_ref_node_target_id_45_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_9;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_9);
*Node_63_retrieved_reference_45_9_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_9_typed_id,Node_63_ref_node_target_id_45_9);
*Node_63_retrieved_reference_45_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_9_is_inverse, Node_63_ref_node_target_id_45_9);
*Node_63_retrieved_reference_45_9_target_id = 2172;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_9_target_id, Node_63_ref_node_target_id_45_9);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_9->references,"10",Node_63_ref_node_target_id_45_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_10;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_10);
*Node_63_retrieved_reference_45_10_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_10_typed_id,Node_63_ref_node_target_id_45_10);
*Node_63_retrieved_reference_45_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_10_is_inverse, Node_63_ref_node_target_id_45_10);
*Node_63_retrieved_reference_45_10_target_id = 2196;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_10_target_id, Node_63_ref_node_target_id_45_10);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_10->references,"11",Node_63_ref_node_target_id_45_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_11;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_11);
*Node_63_retrieved_reference_45_11_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_11_typed_id,Node_63_ref_node_target_id_45_11);
*Node_63_retrieved_reference_45_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_11_is_inverse, Node_63_ref_node_target_id_45_11);
*Node_63_retrieved_reference_45_11_target_id = 2197;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_11_target_id, Node_63_ref_node_target_id_45_11);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_11->references,"12",Node_63_ref_node_target_id_45_11); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_12;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_12 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_12_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_12_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_12_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_12);
*Node_63_retrieved_reference_45_12_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_12_typed_id,Node_63_ref_node_target_id_45_12);
*Node_63_retrieved_reference_45_false_12_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_12_is_inverse, Node_63_ref_node_target_id_45_12);
*Node_63_retrieved_reference_45_12_target_id = 2243;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_12_target_id, Node_63_ref_node_target_id_45_12);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_12->references,"13",Node_63_ref_node_target_id_45_12); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_13;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_13 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_13_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_13_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_13_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_13);
*Node_63_retrieved_reference_45_13_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_13_typed_id,Node_63_ref_node_target_id_45_13);
*Node_63_retrieved_reference_45_false_13_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_13_is_inverse, Node_63_ref_node_target_id_45_13);
*Node_63_retrieved_reference_45_13_target_id = 2244;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_13_target_id, Node_63_ref_node_target_id_45_13);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_13->references,"14",Node_63_ref_node_target_id_45_13); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_14;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_14 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_14_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_14_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_14_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_14);
*Node_63_retrieved_reference_45_14_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_14_typed_id,Node_63_ref_node_target_id_45_14);
*Node_63_retrieved_reference_45_false_14_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_14_is_inverse, Node_63_ref_node_target_id_45_14);
*Node_63_retrieved_reference_45_14_target_id = 11487;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_14_target_id, Node_63_ref_node_target_id_45_14);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_14->references,"15",Node_63_ref_node_target_id_45_14); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_15;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_15 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_15_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_15_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_15_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_15);
*Node_63_retrieved_reference_45_15_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_15_typed_id,Node_63_ref_node_target_id_45_15);
*Node_63_retrieved_reference_45_false_15_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_15_is_inverse, Node_63_ref_node_target_id_45_15);
*Node_63_retrieved_reference_45_15_target_id = 16309;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_15_target_id, Node_63_ref_node_target_id_45_15);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_15->references,"16",Node_63_ref_node_target_id_45_15); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_16;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_16 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_16_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_16_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_16_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_16);
*Node_63_retrieved_reference_45_16_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_16_typed_id,Node_63_ref_node_target_id_45_16);
*Node_63_retrieved_reference_45_false_16_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_16_is_inverse, Node_63_ref_node_target_id_45_16);
*Node_63_retrieved_reference_45_16_target_id = 17986;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_16_target_id, Node_63_ref_node_target_id_45_16);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_16->references,"17",Node_63_ref_node_target_id_45_16); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_17;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_17 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_17_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_17_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_17_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_17);
*Node_63_retrieved_reference_45_17_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_17_typed_id,Node_63_ref_node_target_id_45_17);
*Node_63_retrieved_reference_45_false_17_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_17_is_inverse, Node_63_ref_node_target_id_45_17);
*Node_63_retrieved_reference_45_17_target_id = 2755;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_17_target_id, Node_63_ref_node_target_id_45_17);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_17->references,"18",Node_63_ref_node_target_id_45_17); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_18;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_18 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_18_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_18_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_18_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_18);
*Node_63_retrieved_reference_45_18_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_18_typed_id,Node_63_ref_node_target_id_45_18);
*Node_63_retrieved_reference_45_false_18_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_18_is_inverse, Node_63_ref_node_target_id_45_18);
*Node_63_retrieved_reference_45_18_target_id = 2762;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_18_target_id, Node_63_ref_node_target_id_45_18);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_18->references,"19",Node_63_ref_node_target_id_45_18); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_19;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_19 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_19_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_19_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_19_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_19);
*Node_63_retrieved_reference_45_19_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_19_typed_id,Node_63_ref_node_target_id_45_19);
*Node_63_retrieved_reference_45_false_19_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_19_is_inverse, Node_63_ref_node_target_id_45_19);
*Node_63_retrieved_reference_45_19_target_id = 2365;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_19_target_id, Node_63_ref_node_target_id_45_19);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_19->references,"20",Node_63_ref_node_target_id_45_19); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_20;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_20 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_20_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_20_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_20_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_20);
*Node_63_retrieved_reference_45_20_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_20_typed_id,Node_63_ref_node_target_id_45_20);
*Node_63_retrieved_reference_45_false_20_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_20_is_inverse, Node_63_ref_node_target_id_45_20);
*Node_63_retrieved_reference_45_20_target_id = 9002;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_20_target_id, Node_63_ref_node_target_id_45_20);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_20->references,"21",Node_63_ref_node_target_id_45_20); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_21;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_21 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_21_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_21_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_21_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_21);
*Node_63_retrieved_reference_45_21_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_21_typed_id,Node_63_ref_node_target_id_45_21);
*Node_63_retrieved_reference_45_false_21_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_21_is_inverse, Node_63_ref_node_target_id_45_21);
*Node_63_retrieved_reference_45_21_target_id = 17277;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_21_target_id, Node_63_ref_node_target_id_45_21);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_21->references,"22",Node_63_ref_node_target_id_45_21); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_22;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_22 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_22_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_22_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_22_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_22);
*Node_63_retrieved_reference_45_22_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_22_typed_id,Node_63_ref_node_target_id_45_22);
*Node_63_retrieved_reference_45_false_22_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_22_is_inverse, Node_63_ref_node_target_id_45_22);
*Node_63_retrieved_reference_45_22_target_id = 2380;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_22_target_id, Node_63_ref_node_target_id_45_22);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_22->references,"23",Node_63_ref_node_target_id_45_22); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_23;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_23 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_23_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_23_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_23_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_23);
*Node_63_retrieved_reference_45_23_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_23_typed_id,Node_63_ref_node_target_id_45_23);
*Node_63_retrieved_reference_45_false_23_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_23_is_inverse, Node_63_ref_node_target_id_45_23);
*Node_63_retrieved_reference_45_23_target_id = 15383;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_23_target_id, Node_63_ref_node_target_id_45_23);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_23->references,"24",Node_63_ref_node_target_id_45_23); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_63_retrieved_reference_45_24;
ishidaopcua_NODE* Node_63_ref_node_target_id_45_24 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_63_retrieved_reference_45_24_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_false_24_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_63_retrieved_reference_45_24_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_63_retrieved_reference_45_24);
*Node_63_retrieved_reference_45_24_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_63_retrieved_reference_45_24_typed_id,Node_63_ref_node_target_id_45_24);
*Node_63_retrieved_reference_45_false_24_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_63_retrieved_reference_45_false_24_is_inverse, Node_63_ref_node_target_id_45_24);
*Node_63_retrieved_reference_45_24_target_id = 19725;
ishidaopcua_node_set_target_id(Node_63_retrieved_reference_45_24_target_id, Node_63_ref_node_target_id_45_24);
ishidaeutz_put_hashmap(Node_63_retrieved_reference_45_24->references,"25",Node_63_ref_node_target_id_45_24); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Views ********/


case 87 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 87;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Views", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Views", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The browse entry point when looking for views in the server address space.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 87 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_87_retrieved_reference_40_0;
ishidaopcua_NODE* Node_87_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_87_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_87_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_87_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_87_retrieved_reference_40_0);
*Node_87_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_87_retrieved_reference_40_0_typed_id,Node_87_ref_node_target_id_40_0);
*Node_87_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_87_retrieved_reference_40_false_0_is_inverse, Node_87_ref_node_target_id_40_0);
*Node_87_retrieved_reference_40_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_87_retrieved_reference_40_0_target_id, Node_87_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_87_retrieved_reference_40_0->references,"1",Node_87_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_87_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_87_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_87_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_87_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_87_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_87_retrieved_reference_35_inverse_0);
*Node_87_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_87_retrieved_reference_35_inverse_0_typed_id,Node_87_ref_node_target_id_35_inverse_0);
*Node_87_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_87_retrieved_reference_35_inverse_true_0_is_inverse, Node_87_ref_node_target_id_35_inverse_0);
*Node_87_retrieved_reference_35_inverse_0_target_id = 84;
ishidaopcua_node_set_target_id(Node_87_retrieved_reference_35_inverse_0_target_id, Node_87_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_87_retrieved_reference_35_inverse_0->references,"1",Node_87_ref_node_target_id_35_inverse_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ObjectTypes ********/


case 88 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 88;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ObjectTypes", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ObjectTypes", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The browse entry point when looking for object types in the server address space.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 88 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_88_retrieved_reference_40_0;
ishidaopcua_NODE* Node_88_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_88_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_88_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_88_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_88_retrieved_reference_40_0);
*Node_88_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_88_retrieved_reference_40_0_typed_id,Node_88_ref_node_target_id_40_0);
*Node_88_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_88_retrieved_reference_40_false_0_is_inverse, Node_88_ref_node_target_id_40_0);
*Node_88_retrieved_reference_40_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_88_retrieved_reference_40_0_target_id, Node_88_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_88_retrieved_reference_40_0->references,"1",Node_88_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_88_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_88_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_88_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_88_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_88_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_88_retrieved_reference_35_inverse_0);
*Node_88_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_88_retrieved_reference_35_inverse_0_typed_id,Node_88_ref_node_target_id_35_inverse_0);
*Node_88_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_88_retrieved_reference_35_inverse_true_0_is_inverse, Node_88_ref_node_target_id_35_inverse_0);
*Node_88_retrieved_reference_35_inverse_0_target_id = 86;
ishidaopcua_node_set_target_id(Node_88_retrieved_reference_35_inverse_0_target_id, Node_88_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_88_retrieved_reference_35_inverse_0->references,"1",Node_88_ref_node_target_id_35_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_88_retrieved_reference_35_0;
ishidaopcua_NODE* Node_88_ref_node_target_id_35_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_88_retrieved_reference_35_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_88_retrieved_reference_35_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_88_retrieved_reference_35_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35",&Node_88_retrieved_reference_35_0);
*Node_88_retrieved_reference_35_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_88_retrieved_reference_35_0_typed_id,Node_88_ref_node_target_id_35_0);
*Node_88_retrieved_reference_35_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_88_retrieved_reference_35_false_0_is_inverse, Node_88_ref_node_target_id_35_0);
*Node_88_retrieved_reference_35_0_target_id = 58;
ishidaopcua_node_set_target_id(Node_88_retrieved_reference_35_0_target_id, Node_88_ref_node_target_id_35_0);
ishidaeutz_put_hashmap(Node_88_retrieved_reference_35_0->references,"1",Node_88_ref_node_target_id_35_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* VariableTypes ********/


case 89 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 89;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("VariableTypes", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("VariableTypes", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The browse entry point when looking for variable types in the server address space.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 89 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_89_retrieved_reference_40_0;
ishidaopcua_NODE* Node_89_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_89_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_89_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_89_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_89_retrieved_reference_40_0);
*Node_89_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_89_retrieved_reference_40_0_typed_id,Node_89_ref_node_target_id_40_0);
*Node_89_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_89_retrieved_reference_40_false_0_is_inverse, Node_89_ref_node_target_id_40_0);
*Node_89_retrieved_reference_40_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_89_retrieved_reference_40_0_target_id, Node_89_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_89_retrieved_reference_40_0->references,"1",Node_89_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_89_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_89_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_89_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_89_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_89_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_89_retrieved_reference_35_inverse_0);
*Node_89_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_89_retrieved_reference_35_inverse_0_typed_id,Node_89_ref_node_target_id_35_inverse_0);
*Node_89_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_89_retrieved_reference_35_inverse_true_0_is_inverse, Node_89_ref_node_target_id_35_inverse_0);
*Node_89_retrieved_reference_35_inverse_0_target_id = 86;
ishidaopcua_node_set_target_id(Node_89_retrieved_reference_35_inverse_0_target_id, Node_89_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_89_retrieved_reference_35_inverse_0->references,"1",Node_89_ref_node_target_id_35_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_89_retrieved_reference_35_0;
ishidaopcua_NODE* Node_89_ref_node_target_id_35_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_89_retrieved_reference_35_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_89_retrieved_reference_35_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_89_retrieved_reference_35_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35",&Node_89_retrieved_reference_35_0);
*Node_89_retrieved_reference_35_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_89_retrieved_reference_35_0_typed_id,Node_89_ref_node_target_id_35_0);
*Node_89_retrieved_reference_35_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_89_retrieved_reference_35_false_0_is_inverse, Node_89_ref_node_target_id_35_0);
*Node_89_retrieved_reference_35_0_target_id = 62;
ishidaopcua_node_set_target_id(Node_89_retrieved_reference_35_0_target_id, Node_89_ref_node_target_id_35_0);
ishidaeutz_put_hashmap(Node_89_retrieved_reference_35_0->references,"1",Node_89_ref_node_target_id_35_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DataTypes ********/


case 90 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 90;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DataTypes", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DataTypes", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The browse entry point when looking for data types in the server address space.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 90 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_90_retrieved_reference_40_0;
ishidaopcua_NODE* Node_90_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_90_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_90_retrieved_reference_40_0);
*Node_90_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_90_retrieved_reference_40_0_typed_id,Node_90_ref_node_target_id_40_0);
*Node_90_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_90_retrieved_reference_40_false_0_is_inverse, Node_90_ref_node_target_id_40_0);
*Node_90_retrieved_reference_40_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_90_retrieved_reference_40_0_target_id, Node_90_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_90_retrieved_reference_40_0->references,"1",Node_90_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_90_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_90_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_90_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_90_retrieved_reference_35_inverse_0);
*Node_90_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_90_retrieved_reference_35_inverse_0_typed_id,Node_90_ref_node_target_id_35_inverse_0);
*Node_90_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_90_retrieved_reference_35_inverse_true_0_is_inverse, Node_90_ref_node_target_id_35_inverse_0);
*Node_90_retrieved_reference_35_inverse_0_target_id = 86;
ishidaopcua_node_set_target_id(Node_90_retrieved_reference_35_inverse_0_target_id, Node_90_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_90_retrieved_reference_35_inverse_0->references,"1",Node_90_ref_node_target_id_35_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_90_retrieved_reference_35_0;
ishidaopcua_NODE* Node_90_ref_node_target_id_35_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_90_retrieved_reference_35_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35",&Node_90_retrieved_reference_35_0);
*Node_90_retrieved_reference_35_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_90_retrieved_reference_35_0_typed_id,Node_90_ref_node_target_id_35_0);
*Node_90_retrieved_reference_35_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_90_retrieved_reference_35_false_0_is_inverse, Node_90_ref_node_target_id_35_0);
*Node_90_retrieved_reference_35_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_90_retrieved_reference_35_0_target_id, Node_90_ref_node_target_id_35_0);
ishidaeutz_put_hashmap(Node_90_retrieved_reference_35_0->references,"1",Node_90_ref_node_target_id_35_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_90_retrieved_reference_35_1;
ishidaopcua_NODE* Node_90_ref_node_target_id_35_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_90_retrieved_reference_35_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35",&Node_90_retrieved_reference_35_1);
*Node_90_retrieved_reference_35_1_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_90_retrieved_reference_35_1_typed_id,Node_90_ref_node_target_id_35_1);
*Node_90_retrieved_reference_35_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_90_retrieved_reference_35_false_1_is_inverse, Node_90_ref_node_target_id_35_1);
*Node_90_retrieved_reference_35_1_target_id = 92;
ishidaopcua_node_set_target_id(Node_90_retrieved_reference_35_1_target_id, Node_90_ref_node_target_id_35_1);
ishidaeutz_put_hashmap(Node_90_retrieved_reference_35_1->references,"2",Node_90_ref_node_target_id_35_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_90_retrieved_reference_35_2;
ishidaopcua_NODE* Node_90_ref_node_target_id_35_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_90_retrieved_reference_35_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_90_retrieved_reference_35_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35",&Node_90_retrieved_reference_35_2);
*Node_90_retrieved_reference_35_2_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_90_retrieved_reference_35_2_typed_id,Node_90_ref_node_target_id_35_2);
*Node_90_retrieved_reference_35_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_90_retrieved_reference_35_false_2_is_inverse, Node_90_ref_node_target_id_35_2);
*Node_90_retrieved_reference_35_2_target_id = 93;
ishidaopcua_node_set_target_id(Node_90_retrieved_reference_35_2_target_id, Node_90_ref_node_target_id_35_2);
ishidaeutz_put_hashmap(Node_90_retrieved_reference_35_2->references,"3",Node_90_ref_node_target_id_35_2); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ReferenceTypes ********/


case 91 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 91;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ReferenceTypes", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ReferenceTypes", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The browse entry point when looking for reference types in the server address space.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 91 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_91_retrieved_reference_40_0;
ishidaopcua_NODE* Node_91_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_91_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_91_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_91_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_91_retrieved_reference_40_0);
*Node_91_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_91_retrieved_reference_40_0_typed_id,Node_91_ref_node_target_id_40_0);
*Node_91_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_91_retrieved_reference_40_false_0_is_inverse, Node_91_ref_node_target_id_40_0);
*Node_91_retrieved_reference_40_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_91_retrieved_reference_40_0_target_id, Node_91_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_91_retrieved_reference_40_0->references,"1",Node_91_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_91_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_91_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_91_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_91_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_91_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_91_retrieved_reference_35_inverse_0);
*Node_91_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_91_retrieved_reference_35_inverse_0_typed_id,Node_91_ref_node_target_id_35_inverse_0);
*Node_91_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_91_retrieved_reference_35_inverse_true_0_is_inverse, Node_91_ref_node_target_id_35_inverse_0);
*Node_91_retrieved_reference_35_inverse_0_target_id = 86;
ishidaopcua_node_set_target_id(Node_91_retrieved_reference_35_inverse_0_target_id, Node_91_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_91_retrieved_reference_35_inverse_0->references,"1",Node_91_ref_node_target_id_35_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_91_retrieved_reference_35_0;
ishidaopcua_NODE* Node_91_ref_node_target_id_35_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_91_retrieved_reference_35_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_91_retrieved_reference_35_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_91_retrieved_reference_35_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35",&Node_91_retrieved_reference_35_0);
*Node_91_retrieved_reference_35_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_91_retrieved_reference_35_0_typed_id,Node_91_ref_node_target_id_35_0);
*Node_91_retrieved_reference_35_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_91_retrieved_reference_35_false_0_is_inverse, Node_91_ref_node_target_id_35_0);
*Node_91_retrieved_reference_35_0_target_id = 31;
ishidaopcua_node_set_target_id(Node_91_retrieved_reference_35_0_target_id, Node_91_ref_node_target_id_35_0);
ishidaeutz_put_hashmap(Node_91_retrieved_reference_35_0->references,"1",Node_91_ref_node_target_id_35_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* EventTypes ********/


case 3048 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 3048;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("EventTypes", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("EventTypes", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 3048 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_3048_retrieved_reference_40_0;
ishidaopcua_NODE* Node_3048_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3048_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3048_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3048_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_3048_retrieved_reference_40_0);
*Node_3048_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_3048_retrieved_reference_40_0_typed_id,Node_3048_ref_node_target_id_40_0);
*Node_3048_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3048_retrieved_reference_40_false_0_is_inverse, Node_3048_ref_node_target_id_40_0);
*Node_3048_retrieved_reference_40_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_3048_retrieved_reference_40_0_target_id, Node_3048_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_3048_retrieved_reference_40_0->references,"1",Node_3048_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3048_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_3048_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3048_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3048_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3048_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_3048_retrieved_reference_35_inverse_0);
*Node_3048_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_3048_retrieved_reference_35_inverse_0_typed_id,Node_3048_ref_node_target_id_35_inverse_0);
*Node_3048_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_3048_retrieved_reference_35_inverse_true_0_is_inverse, Node_3048_ref_node_target_id_35_inverse_0);
*Node_3048_retrieved_reference_35_inverse_0_target_id = 86;
ishidaopcua_node_set_target_id(Node_3048_retrieved_reference_35_inverse_0_target_id, Node_3048_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_3048_retrieved_reference_35_inverse_0->references,"1",Node_3048_ref_node_target_id_35_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3048_retrieved_reference_35_0;
ishidaopcua_NODE* Node_3048_ref_node_target_id_35_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3048_retrieved_reference_35_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3048_retrieved_reference_35_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3048_retrieved_reference_35_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35",&Node_3048_retrieved_reference_35_0);
*Node_3048_retrieved_reference_35_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_3048_retrieved_reference_35_0_typed_id,Node_3048_ref_node_target_id_35_0);
*Node_3048_retrieved_reference_35_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3048_retrieved_reference_35_false_0_is_inverse, Node_3048_ref_node_target_id_35_0);
*Node_3048_retrieved_reference_35_0_target_id = 2041;
ishidaopcua_node_set_target_id(Node_3048_retrieved_reference_35_0_target_id, Node_3048_ref_node_target_id_35_0);
ishidaeutz_put_hashmap(Node_3048_retrieved_reference_35_0->references,"1",Node_3048_ref_node_target_id_35_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/