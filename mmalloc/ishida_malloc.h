/*
 * ishida_malloc.h
 *
 *  Created on: Oct 4, 2018
 *      Author: salim
 */

#ifndef ISHIDA_MALLOC_H_
#define ISHIDA_MALLOC_H_

#include <stdio.h>
#include <stdlib.h>

#include <stdio.h>

#define SIZE 50000 //size of the memory
#define INIT 0 //'i' if array is initialized, '\0' otherwise
#define I 1 //i to use in for loops, 4 bytes
#define START 5 //actual position that can use to allocate

/* memory initialization */
void init_block();
/* end memory initialization */


/******* memory statistics ***********/
void update_allocated_size(int size);
void increment_total_allocations();
void increment_total_frees();
void get_total_allocations();
void get_memory_stats();
void get_total_frees();
void get_total_allocated_size();
void init_memory_statistics();
/********* end memory stats ********/

/******** mmalloc functions **********/
void* mmalloc(int size);
void merge_left(int left_index, int current_index);
void merge_right(int right_index, int current_index);
int get_next_available_index(int size);
void* allocate(int index, int size);
/********  end malloc functions **********************/

/******** ffree functions ********/
void ffree(void* ipointer);
void clear_memory();
/******** end ffree functions ***/

/**** miscelleneous functions **/
void print(int size);

#endif /* ISHIDA_MALLOC_H_ */
