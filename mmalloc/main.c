/*
 * main.c
 *
 *  Created on: Oct 4, 2018
 *      Author: salim
 */

#include "ishida_malloc.h"

struct st{
	char a;
	char b;
	char c;
}st;

int main()
{
	puts("welcome");
	void* one_pointer = mmalloc(5);

	void* two_pointer = mmalloc(4);

	void* three = mmalloc(1);

	void* four = mmalloc(1);

	void* fifth_pointer = mmalloc(3);

	void* last_alloc = mmalloc(1);

	ffree(one_pointer);
	ffree(two_pointer);
	ffree(three);
	ffree(four);
	ffree(fifth_pointer);
	ffree(last_alloc);

	print(0);

	get_memory_stats();

	puts("finished");
	return 0;
}

