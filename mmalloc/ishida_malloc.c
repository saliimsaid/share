/*
 * ishida_malloc.c
 *
 *  Created on: Oct 4, 2018
 *      Author: salim
 */

#include "ishida_malloc.h"

char mem[SIZE] = {'\0'}; //Null all values in the array

void init_block()
{
	if(mem[0] == '\0')
	{
	puts("init");
	*(int*)&mem[0] = 1;
	int value = *(int*)&mem[0];
	printf("the value is %d \n", value);
	mem[1] = 'f';
	*((int*)(&mem[2])) = (SIZE - 6) - 12;


	init_memory_statistics();

	}
}

void clear_memory()
{
	mem[0] = '\0';
	init_block();

}



int get_next_available_index(int size)
{
	int free_index = 1;
	int loop = 1;
	int next_available_index = 0;

	if(size+5 >= (SIZE))
	{
		printf("size minus header is %d\n", (SIZE - 5));
		return next_available_index;
	}

	while(loop)
	{
		if(mem[free_index] == 'f' && *((int*)(&mem[free_index+1])) >= (size+5))
		{
			next_available_index = free_index;
			loop = 0;

		}else
		{
			free_index += *((int*)(&mem[free_index+1])) + 5;
			if(free_index+5 >= SIZE)
			{
				loop = 0;
			}
		}

	}

	if(free_index >= (SIZE - 5))
	{
		puts("out of memory , return NULL");
		return 0; /* we are out of memory */

	}



	return next_available_index;
}

void* allocate(int index, int size)
{
	void* result = NULL;
	int current_size = *(int*)&mem[index + 1];
	int available_size = current_size - (size+5);
	int next_free;
	mem[index] = 'a';
	*(int*)&mem[index + 1] = size;

	result = (void*)(&mem[index+5]);
	printf("memory allocated, write at this block %d\n", index+5);

	/* mark next free block */
	next_free = index + *(int*)&mem[index + 1] + 5;
	mem[next_free] = 'f';
	*(int*)&mem[next_free+1] = available_size;
	printf("next free index is %d and available size is %d\n",next_free+1,available_size );

	return result;
}


/*
 **********************************
 * Memory Statistics functions
 */
void update_allocated_size(int size)
{
	int total_alloc_index = (SIZE - (12)) + 1;

	int total_alloc_size = (*(int*)&mem[total_alloc_index]) ;

	*(int*)&mem[total_alloc_index] = total_alloc_size + size;
}

void increment_total_allocations()
{
	int total_allocations_index = (SIZE - (4)) + 1;

	int total_allocations = (*(int*)&mem[total_allocations_index]) ;

	*(int*)&mem[total_allocations_index] = total_allocations + 1;

}


void increment_total_frees()
{
	int total_free_index = (SIZE - (8)) + 1;

	int total_frees = (*(int*)&mem[total_free_index]) ;

	*(int*)&mem[total_free_index] = total_frees + 1;
}


void get_total_allocations()
{
	int total_allocations_index = (SIZE - (4)) + 1;

	int total_memory = *(int*)&mem[total_allocations_index];

	printf("total allocations memory is %d \n", total_memory);
}

void get_memory_stats()
{
	get_total_allocations();
	get_total_frees();
	get_total_allocated_size();
}

void get_total_frees()
{
	int total_free_index = (SIZE - (8)) + 1;


	int total_freed = *(int*)&mem[total_free_index];

	printf("total free count  is %d \n", total_freed);
}

void get_total_allocated_size()
{
	int total_alloc_index = (SIZE - (12)) + 1;

	int total_alloc_size = (*(int*)&mem[total_alloc_index]) ;

	printf("total allocated size is %d bytes \n", total_alloc_size);
}



void init_memory_statistics()
{
	/*
	 * Total allocations
	 * Total frees
	 * Total alloc size
	 * Total free size
	 */
	puts("in init stats");
	int total_allocations_index = (SIZE - (4)) + 1;
	printf("toatal alloc index is %d\n", total_allocations_index);
	int total_frees_index = (SIZE - (8)) + 1;
	int total_alloc_size = (SIZE - (12)) + 1;
	int total_free_size = (SIZE - (16)) + 1;
	puts("a");
	*(int*)&mem[total_allocations_index] = 0;
	puts("b");
	*(int*)&mem[total_frees_index] = 0;
	puts("c");
	*(int*)&mem[total_alloc_size] = 0;
	*(int*)&mem[total_free_size] = 0;
	puts("d");
}


/*
 * End memory statistics functions
 * ********************************
 */


void* mmalloc(int size)
{
	void* memory_pointer = NULL;
	int available_index = 0;
	init_block();
	available_index = get_next_available_index(size);
	if(available_index == 0)
	{
		puts("mmalloc out of memory, NULL");
		return memory_pointer;
	}

	printf("next available index pointer is %p\n", &available_index);
	printf("next available index is %d\n", available_index);

	memory_pointer = allocate(available_index, size);
	if(memory_pointer != NULL)
	{
		puts("incrementing...");
		increment_total_allocations();
		update_allocated_size(size);
	}else
	{
		puts("not incrementing, pointer is null");
	}
	printf("available index is %d \n", available_index);

	printf("memory pointer is %p\n", memory_pointer);

	return memory_pointer;

}


/*
 * Free implementation
 */

void merge_left(int left_index, int current_index)
{
	if(mem[left_index] == 'f')
	{

		*(int*)(&mem[left_index + 1]) = *(int*)(&mem[left_index + 1]) + 5 + *(int*)(&mem[current_index + 1]);
	}
}

void merge_right(int right_index, int current_index)
{
	printf("ccccurrent index is %d and right index is %d \n" , current_index,right_index);
	int right_size = *(int*)&mem[right_index + 1];
	int current_size = *(int*)&mem[current_index + 1];
	printf("current size is %d and right size is %d \n", current_size,right_size);

	if(mem[right_index] == 'f')
	{
		*(int*)&mem[current_index + 1] = right_size + current_size+5;
	}
}

void ffree(void* ipointer)
{
	int index = 1;
	int size = *(int*)(&mem[index+1]);
	printf("look up size is %d \n", size);
	int loop = 1;
	int found = 0;

	int before = 0;
	int counter = 0;

	int before_size = 0;
	int right_index = 0;


	while(loop)
	{
		counter++;

		/*printf("pointer is %p \n", (void*)&mem[index+4+1]);*/

		if(ipointer == (void*)&mem[index+4+1])
		{
			puts("found pointer");
			mem[index]= 'f';
			loop = 0;

			right_index = *(int*)&mem[index+1] + 5 + index;
			printf("right index is %d \n", right_index);
			printf("current index is %d \n", index);
			merge_right(right_index,index);

			merge_left(before,index);
			/*decrement_total_allocations();*/
			increment_total_frees();

		}
		else
		{
			before = index;
			before_size =  *(int*)(&mem[before+1]);
			index += before_size + 5;
			/*puts("nonee");*/
			if(index+5 >= SIZE)
			{
				printf("index  is %d \n", index);
				puts("free, didn't find the pointer");
				loop = 0;

			}
		}
	}
	printf("counter is %d \n", counter);

	if(found)
	{
		puts("found pointer");
	}else
	{
		puts("none none none");
	}



}

/*
 * End free implementation
 */



void print(int size)
{
	int loop = 1;
	int i = 1;
	int j;
	int length;
	if(mem[0] != '\0')
	{
	printf("%d|",(int)mem[0]);

	if(i+5 >= SIZE)
	{
		loop = 0;
	}

	while(loop)
	{


	if(i+5 <= SIZE){
		if((mem[i] == 'a' || mem[i] == 'f') && ((i+5) <= SIZE))
		{
			length = *(int*)&mem[i+1];

			printf("%c|",mem[i]);
			printf("%x|%x|%x|%x|", (int)mem[i+1],(int)mem[i+2],(int)mem[i+3],(int)mem[i+4]);

			if(mem[i] == 'a')
			{
				for(j = 0; j < length; j++)
				{
					printf("+|");
				}
			}else
			{
				for(j = 0; j < length; j++)
				{
					printf("-|");
				}
			}

			i+= (5+length);

		}else
		{

			loop = 0;
			puts("end of file");
		}

	}else
	{
		break;
		loop = 0;
	}
	}


	puts("");
	}else
	{
		puts("mem empty");
	}



}




