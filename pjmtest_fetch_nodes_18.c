#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_18(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* SByte ********/


case 2 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SByte", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SByte", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between -128 and 127.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 2 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_2_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_2_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_2_retrieved_reference_45_inverse_0);
*Node_2_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2_retrieved_reference_45_inverse_0_typed_id,Node_2_ref_node_target_id_45_inverse_0);
*Node_2_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2_retrieved_reference_45_inverse_true_0_is_inverse, Node_2_ref_node_target_id_45_inverse_0);
*Node_2_retrieved_reference_45_inverse_0_target_id = 27;
ishidaopcua_node_set_target_id(Node_2_retrieved_reference_45_inverse_0_target_id, Node_2_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_2_retrieved_reference_45_inverse_0->references,"1",Node_2_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Byte ********/


case 3 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 3;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Byte", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Byte", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between 0 and 255.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 3 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_3_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_3_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_3_retrieved_reference_45_inverse_0);
*Node_3_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_3_retrieved_reference_45_inverse_0_typed_id,Node_3_ref_node_target_id_45_inverse_0);
*Node_3_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_3_retrieved_reference_45_inverse_true_0_is_inverse, Node_3_ref_node_target_id_45_inverse_0);
*Node_3_retrieved_reference_45_inverse_0_target_id = 28;
ishidaopcua_node_set_target_id(Node_3_retrieved_reference_45_inverse_0_target_id, Node_3_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_3_retrieved_reference_45_inverse_0->references,"1",Node_3_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_3_retrieved_reference_45_0;
ishidaopcua_NODE* Node_3_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_3_retrieved_reference_45_0);
*Node_3_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_3_retrieved_reference_45_0_typed_id,Node_3_ref_node_target_id_45_0);
*Node_3_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3_retrieved_reference_45_false_0_is_inverse, Node_3_ref_node_target_id_45_0);
*Node_3_retrieved_reference_45_0_target_id = 15031;
ishidaopcua_node_set_target_id(Node_3_retrieved_reference_45_0_target_id, Node_3_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_3_retrieved_reference_45_0->references,"1",Node_3_ref_node_target_id_45_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Int16 ********/


case 4 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 4;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Int16", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Int16", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between −32,768 and 32,767.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 4 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_4_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_4_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_4_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_4_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_4_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_4_retrieved_reference_45_inverse_0);
*Node_4_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_4_retrieved_reference_45_inverse_0_typed_id,Node_4_ref_node_target_id_45_inverse_0);
*Node_4_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_4_retrieved_reference_45_inverse_true_0_is_inverse, Node_4_ref_node_target_id_45_inverse_0);
*Node_4_retrieved_reference_45_inverse_0_target_id = 27;
ishidaopcua_node_set_target_id(Node_4_retrieved_reference_45_inverse_0_target_id, Node_4_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_4_retrieved_reference_45_inverse_0->references,"1",Node_4_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Int32 ********/


case 6 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 6;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Int32", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Int32", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between −2,147,483,648 and 2,147,483,647.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 6 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_6_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_6_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_6_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_6_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_6_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_6_retrieved_reference_45_inverse_0);
*Node_6_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_6_retrieved_reference_45_inverse_0_typed_id,Node_6_ref_node_target_id_45_inverse_0);
*Node_6_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_6_retrieved_reference_45_inverse_true_0_is_inverse, Node_6_ref_node_target_id_45_inverse_0);
*Node_6_retrieved_reference_45_inverse_0_target_id = 27;
ishidaopcua_node_set_target_id(Node_6_retrieved_reference_45_inverse_0_target_id, Node_6_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_6_retrieved_reference_45_inverse_0->references,"1",Node_6_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Int64 ********/


case 8 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 8;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Int64", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Int64", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between −9,223,372,036,854,775,808 and 9,223,372,036,854,775,807.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 8 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_8_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_8_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_8_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_8_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_8_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_8_retrieved_reference_45_inverse_0);
*Node_8_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_8_retrieved_reference_45_inverse_0_typed_id,Node_8_ref_node_target_id_45_inverse_0);
*Node_8_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_8_retrieved_reference_45_inverse_true_0_is_inverse, Node_8_ref_node_target_id_45_inverse_0);
*Node_8_retrieved_reference_45_inverse_0_target_id = 27;
ishidaopcua_node_set_target_id(Node_8_retrieved_reference_45_inverse_0_target_id, Node_8_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_8_retrieved_reference_45_inverse_0->references,"1",Node_8_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Float ********/


case 10 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 10;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Float", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Float", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an IEEE 754-1985 single precision floating point number.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 10 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_10_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_10_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_10_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_10_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_10_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_10_retrieved_reference_45_inverse_0);
*Node_10_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_10_retrieved_reference_45_inverse_0_typed_id,Node_10_ref_node_target_id_45_inverse_0);
*Node_10_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_10_retrieved_reference_45_inverse_true_0_is_inverse, Node_10_ref_node_target_id_45_inverse_0);
*Node_10_retrieved_reference_45_inverse_0_target_id = 26;
ishidaopcua_node_set_target_id(Node_10_retrieved_reference_45_inverse_0_target_id, Node_10_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_10_retrieved_reference_45_inverse_0->references,"1",Node_10_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Double ********/


case 11 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Double", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Double", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an IEEE 754-1985 double precision floating point number.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 11 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_11_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_11_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_11_retrieved_reference_45_inverse_0);
*Node_11_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_11_retrieved_reference_45_inverse_0_typed_id,Node_11_ref_node_target_id_45_inverse_0);
*Node_11_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11_retrieved_reference_45_inverse_true_0_is_inverse, Node_11_ref_node_target_id_45_inverse_0);
*Node_11_retrieved_reference_45_inverse_0_target_id = 26;
ishidaopcua_node_set_target_id(Node_11_retrieved_reference_45_inverse_0_target_id, Node_11_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_11_retrieved_reference_45_inverse_0->references,"1",Node_11_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_11_retrieved_reference_45_0;
ishidaopcua_NODE* Node_11_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_11_retrieved_reference_45_0);
*Node_11_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_11_retrieved_reference_45_0_typed_id,Node_11_ref_node_target_id_45_0);
*Node_11_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11_retrieved_reference_45_false_0_is_inverse, Node_11_ref_node_target_id_45_0);
*Node_11_retrieved_reference_45_0_target_id = 290;
ishidaopcua_node_set_target_id(Node_11_retrieved_reference_45_0_target_id, Node_11_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_11_retrieved_reference_45_0->references,"1",Node_11_ref_node_target_id_45_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Integer ********/


case 27 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 27;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Integer", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Integer", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that can have any integer DataType.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 27 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_27_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_27_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_27_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_27_retrieved_reference_45_inverse_0);
*Node_27_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_27_retrieved_reference_45_inverse_0_typed_id,Node_27_ref_node_target_id_45_inverse_0);
*Node_27_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_27_retrieved_reference_45_inverse_true_0_is_inverse, Node_27_ref_node_target_id_45_inverse_0);
*Node_27_retrieved_reference_45_inverse_0_target_id = 26;
ishidaopcua_node_set_target_id(Node_27_retrieved_reference_45_inverse_0_target_id, Node_27_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_27_retrieved_reference_45_inverse_0->references,"1",Node_27_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_27_retrieved_reference_45_0;
ishidaopcua_NODE* Node_27_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_27_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_27_retrieved_reference_45_0);
*Node_27_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_27_retrieved_reference_45_0_typed_id,Node_27_ref_node_target_id_45_0);
*Node_27_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_27_retrieved_reference_45_false_0_is_inverse, Node_27_ref_node_target_id_45_0);
*Node_27_retrieved_reference_45_0_target_id = 2;
ishidaopcua_node_set_target_id(Node_27_retrieved_reference_45_0_target_id, Node_27_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_27_retrieved_reference_45_0->references,"1",Node_27_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_27_retrieved_reference_45_1;
ishidaopcua_NODE* Node_27_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_27_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_27_retrieved_reference_45_1);
*Node_27_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_27_retrieved_reference_45_1_typed_id,Node_27_ref_node_target_id_45_1);
*Node_27_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_27_retrieved_reference_45_false_1_is_inverse, Node_27_ref_node_target_id_45_1);
*Node_27_retrieved_reference_45_1_target_id = 4;
ishidaopcua_node_set_target_id(Node_27_retrieved_reference_45_1_target_id, Node_27_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_27_retrieved_reference_45_1->references,"2",Node_27_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_27_retrieved_reference_45_2;
ishidaopcua_NODE* Node_27_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_27_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_27_retrieved_reference_45_2);
*Node_27_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_27_retrieved_reference_45_2_typed_id,Node_27_ref_node_target_id_45_2);
*Node_27_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_27_retrieved_reference_45_false_2_is_inverse, Node_27_ref_node_target_id_45_2);
*Node_27_retrieved_reference_45_2_target_id = 6;
ishidaopcua_node_set_target_id(Node_27_retrieved_reference_45_2_target_id, Node_27_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_27_retrieved_reference_45_2->references,"3",Node_27_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_27_retrieved_reference_45_3;
ishidaopcua_NODE* Node_27_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_27_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_27_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_27_retrieved_reference_45_3);
*Node_27_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_27_retrieved_reference_45_3_typed_id,Node_27_ref_node_target_id_45_3);
*Node_27_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_27_retrieved_reference_45_false_3_is_inverse, Node_27_ref_node_target_id_45_3);
*Node_27_retrieved_reference_45_3_target_id = 8;
ishidaopcua_node_set_target_id(Node_27_retrieved_reference_45_3_target_id, Node_27_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_27_retrieved_reference_45_3->references,"4",Node_27_ref_node_target_id_45_3);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* UInteger ********/


case 28 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 28;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("UInteger", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("UInteger", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that can have any unsigned integer DataType.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 28 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_28_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_28_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_28_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_28_retrieved_reference_45_inverse_0);
*Node_28_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_28_retrieved_reference_45_inverse_0_typed_id,Node_28_ref_node_target_id_45_inverse_0);
*Node_28_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_28_retrieved_reference_45_inverse_true_0_is_inverse, Node_28_ref_node_target_id_45_inverse_0);
*Node_28_retrieved_reference_45_inverse_0_target_id = 26;
ishidaopcua_node_set_target_id(Node_28_retrieved_reference_45_inverse_0_target_id, Node_28_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_28_retrieved_reference_45_inverse_0->references,"1",Node_28_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_28_retrieved_reference_45_0;
ishidaopcua_NODE* Node_28_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_28_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_28_retrieved_reference_45_0);
*Node_28_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_28_retrieved_reference_45_0_typed_id,Node_28_ref_node_target_id_45_0);
*Node_28_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_28_retrieved_reference_45_false_0_is_inverse, Node_28_ref_node_target_id_45_0);
*Node_28_retrieved_reference_45_0_target_id = 3;
ishidaopcua_node_set_target_id(Node_28_retrieved_reference_45_0_target_id, Node_28_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_28_retrieved_reference_45_0->references,"1",Node_28_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_28_retrieved_reference_45_1;
ishidaopcua_NODE* Node_28_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_28_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_28_retrieved_reference_45_1);
*Node_28_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_28_retrieved_reference_45_1_typed_id,Node_28_ref_node_target_id_45_1);
*Node_28_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_28_retrieved_reference_45_false_1_is_inverse, Node_28_ref_node_target_id_45_1);
*Node_28_retrieved_reference_45_1_target_id = 5;
ishidaopcua_node_set_target_id(Node_28_retrieved_reference_45_1_target_id, Node_28_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_28_retrieved_reference_45_1->references,"2",Node_28_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_28_retrieved_reference_45_2;
ishidaopcua_NODE* Node_28_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_28_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_28_retrieved_reference_45_2);
*Node_28_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_28_retrieved_reference_45_2_typed_id,Node_28_ref_node_target_id_45_2);
*Node_28_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_28_retrieved_reference_45_false_2_is_inverse, Node_28_ref_node_target_id_45_2);
*Node_28_retrieved_reference_45_2_target_id = 7;
ishidaopcua_node_set_target_id(Node_28_retrieved_reference_45_2_target_id, Node_28_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_28_retrieved_reference_45_2->references,"3",Node_28_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_28_retrieved_reference_45_3;
ishidaopcua_NODE* Node_28_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_28_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_28_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_28_retrieved_reference_45_3);
*Node_28_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_28_retrieved_reference_45_3_typed_id,Node_28_ref_node_target_id_45_3);
*Node_28_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_28_retrieved_reference_45_false_3_is_inverse, Node_28_ref_node_target_id_45_3);
*Node_28_retrieved_reference_45_3_target_id = 9;
ishidaopcua_node_set_target_id(Node_28_retrieved_reference_45_3_target_id, Node_28_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_28_retrieved_reference_45_3->references,"4",Node_28_ref_node_target_id_45_3);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HierarchicalReferences ********/


case 33 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 33;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HierarchicalReferences", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HierarchicalReferences", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The abstract base type for all hierarchical references.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 1;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("HierarchicalReferences", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 33 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_33_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_33_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_33_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_33_retrieved_reference_45_inverse_0);
*Node_33_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_33_retrieved_reference_45_inverse_0_typed_id,Node_33_ref_node_target_id_45_inverse_0);
*Node_33_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_33_retrieved_reference_45_inverse_true_0_is_inverse, Node_33_ref_node_target_id_45_inverse_0);
*Node_33_retrieved_reference_45_inverse_0_target_id = 31;
ishidaopcua_node_set_target_id(Node_33_retrieved_reference_45_inverse_0_target_id, Node_33_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_33_retrieved_reference_45_inverse_0->references,"1",Node_33_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_33_retrieved_reference_45_0;
ishidaopcua_NODE* Node_33_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_33_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_33_retrieved_reference_45_0);
*Node_33_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_33_retrieved_reference_45_0_typed_id,Node_33_ref_node_target_id_45_0);
*Node_33_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_33_retrieved_reference_45_false_0_is_inverse, Node_33_ref_node_target_id_45_0);
*Node_33_retrieved_reference_45_0_target_id = 34;
ishidaopcua_node_set_target_id(Node_33_retrieved_reference_45_0_target_id, Node_33_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_33_retrieved_reference_45_0->references,"1",Node_33_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_33_retrieved_reference_45_1;
ishidaopcua_NODE* Node_33_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_33_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_33_retrieved_reference_45_1);
*Node_33_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_33_retrieved_reference_45_1_typed_id,Node_33_ref_node_target_id_45_1);
*Node_33_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_33_retrieved_reference_45_false_1_is_inverse, Node_33_ref_node_target_id_45_1);
*Node_33_retrieved_reference_45_1_target_id = 35;
ishidaopcua_node_set_target_id(Node_33_retrieved_reference_45_1_target_id, Node_33_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_33_retrieved_reference_45_1->references,"2",Node_33_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_33_retrieved_reference_45_2;
ishidaopcua_NODE* Node_33_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_33_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_33_retrieved_reference_45_2);
*Node_33_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_33_retrieved_reference_45_2_typed_id,Node_33_ref_node_target_id_45_2);
*Node_33_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_33_retrieved_reference_45_false_2_is_inverse, Node_33_ref_node_target_id_45_2);
*Node_33_retrieved_reference_45_2_target_id = 36;
ishidaopcua_node_set_target_id(Node_33_retrieved_reference_45_2_target_id, Node_33_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_33_retrieved_reference_45_2->references,"3",Node_33_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_33_retrieved_reference_45_3;
ishidaopcua_NODE* Node_33_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_33_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_33_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_33_retrieved_reference_45_3);
*Node_33_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_33_retrieved_reference_45_3_typed_id,Node_33_ref_node_target_id_45_3);
*Node_33_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_33_retrieved_reference_45_false_3_is_inverse, Node_33_ref_node_target_id_45_3);
*Node_33_retrieved_reference_45_3_target_id = 14936;
ishidaopcua_node_set_target_id(Node_33_retrieved_reference_45_3_target_id, Node_33_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_33_retrieved_reference_45_3->references,"4",Node_33_ref_node_target_id_45_3);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Duration ********/


case 290 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 290;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Duration", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Duration", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A period of time measured in milliseconds.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 290 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_290_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_290_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_290_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_290_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_290_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_290_retrieved_reference_45_inverse_0);
*Node_290_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_290_retrieved_reference_45_inverse_0_typed_id,Node_290_ref_node_target_id_45_inverse_0);
*Node_290_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_290_retrieved_reference_45_inverse_true_0_is_inverse, Node_290_ref_node_target_id_45_inverse_0);
*Node_290_retrieved_reference_45_inverse_0_target_id = 11;
ishidaopcua_node_set_target_id(Node_290_retrieved_reference_45_inverse_0_target_id, Node_290_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_290_retrieved_reference_45_inverse_0->references,"1",Node_290_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/