#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_19(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* References ********/


case 31 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 31;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("References", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("References", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The abstract base type for all references.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 1;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 1;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("References", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 31 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_31_retrieved_reference_45_0;
ishidaopcua_NODE* Node_31_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_31_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_31_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_31_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_31_retrieved_reference_45_0);
*Node_31_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_31_retrieved_reference_45_0_typed_id,Node_31_ref_node_target_id_45_0);
*Node_31_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_31_retrieved_reference_45_false_0_is_inverse, Node_31_ref_node_target_id_45_0);
*Node_31_retrieved_reference_45_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_31_retrieved_reference_45_0_target_id, Node_31_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_31_retrieved_reference_45_0->references,"1",Node_31_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_31_retrieved_reference_45_1;
ishidaopcua_NODE* Node_31_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_31_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_31_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_31_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_31_retrieved_reference_45_1);
*Node_31_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_31_retrieved_reference_45_1_typed_id,Node_31_ref_node_target_id_45_1);
*Node_31_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_31_retrieved_reference_45_false_1_is_inverse, Node_31_ref_node_target_id_45_1);
*Node_31_retrieved_reference_45_1_target_id = 33;
ishidaopcua_node_set_target_id(Node_31_retrieved_reference_45_1_target_id, Node_31_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_31_retrieved_reference_45_1->references,"2",Node_31_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_31_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_31_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_31_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_31_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_31_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_31_retrieved_reference_35_inverse_0);
*Node_31_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_31_retrieved_reference_35_inverse_0_typed_id,Node_31_ref_node_target_id_35_inverse_0);
*Node_31_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_31_retrieved_reference_35_inverse_true_0_is_inverse, Node_31_ref_node_target_id_35_inverse_0);
*Node_31_retrieved_reference_35_inverse_0_target_id = 91;
ishidaopcua_node_set_target_id(Node_31_retrieved_reference_35_inverse_0_target_id, Node_31_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_31_retrieved_reference_35_inverse_0->references,"1",Node_31_ref_node_target_id_35_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Organizes ********/


case 35 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 35;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Organizes", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Organizes", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for hierarchical references that are used to organize nodes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("OrganizedBy", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 35 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_35_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_35_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_35_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_35_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_35_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_35_retrieved_reference_45_inverse_0);
*Node_35_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_35_retrieved_reference_45_inverse_0_typed_id,Node_35_ref_node_target_id_45_inverse_0);
*Node_35_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_35_retrieved_reference_45_inverse_true_0_is_inverse, Node_35_ref_node_target_id_45_inverse_0);
*Node_35_retrieved_reference_45_inverse_0_target_id = 33;
ishidaopcua_node_set_target_id(Node_35_retrieved_reference_45_inverse_0_target_id, Node_35_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_35_retrieved_reference_45_inverse_0->references,"1",Node_35_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_35_retrieved_reference_45_0;
ishidaopcua_NODE* Node_35_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_35_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_35_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_35_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_35_retrieved_reference_45_0);
*Node_35_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_35_retrieved_reference_45_0_typed_id,Node_35_ref_node_target_id_45_0);
*Node_35_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_35_retrieved_reference_45_false_0_is_inverse, Node_35_ref_node_target_id_45_0);
*Node_35_retrieved_reference_45_0_target_id = 16362;
ishidaopcua_node_set_target_id(Node_35_retrieved_reference_45_0_target_id, Node_35_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_35_retrieved_reference_45_0->references,"1",Node_35_ref_node_target_id_45_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasEventSource ********/


case 36 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 36;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasEventSource", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasEventSource", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for non-looping hierarchical references that are used to organize event sources.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("EventSourceOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 36 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_36_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_36_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_36_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_36_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_36_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_36_retrieved_reference_45_inverse_0);
*Node_36_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_36_retrieved_reference_45_inverse_0_typed_id,Node_36_ref_node_target_id_45_inverse_0);
*Node_36_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_36_retrieved_reference_45_inverse_true_0_is_inverse, Node_36_ref_node_target_id_45_inverse_0);
*Node_36_retrieved_reference_45_inverse_0_target_id = 33;
ishidaopcua_node_set_target_id(Node_36_retrieved_reference_45_inverse_0_target_id, Node_36_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_36_retrieved_reference_45_inverse_0->references,"1",Node_36_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_36_retrieved_reference_45_0;
ishidaopcua_NODE* Node_36_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_36_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_36_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_36_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_36_retrieved_reference_45_0);
*Node_36_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_36_retrieved_reference_45_0_typed_id,Node_36_ref_node_target_id_45_0);
*Node_36_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_36_retrieved_reference_45_false_0_is_inverse, Node_36_ref_node_target_id_45_0);
*Node_36_retrieved_reference_45_0_target_id = 48;
ishidaopcua_node_set_target_id(Node_36_retrieved_reference_45_0_target_id, Node_36_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_36_retrieved_reference_45_0->references,"1",Node_36_ref_node_target_id_45_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasModellingRule ********/


case 37 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 37;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasModellingRule", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasModellingRule", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for references from instance declarations to modelling rule nodes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("ModellingRuleOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 37 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_37_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_37_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_37_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_37_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_37_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_37_retrieved_reference_45_inverse_0);
*Node_37_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_37_retrieved_reference_45_inverse_0_typed_id,Node_37_ref_node_target_id_45_inverse_0);
*Node_37_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_37_retrieved_reference_45_inverse_true_0_is_inverse, Node_37_ref_node_target_id_45_inverse_0);
*Node_37_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_37_retrieved_reference_45_inverse_0_target_id, Node_37_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_37_retrieved_reference_45_inverse_0->references,"1",Node_37_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasEncoding ********/


case 38 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 38;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasEncoding", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasEncoding", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for references from data type nodes to to data type encoding nodes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("EncodingOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 38 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_38_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_38_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_38_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_38_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_38_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_38_retrieved_reference_45_inverse_0);
*Node_38_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_38_retrieved_reference_45_inverse_0_typed_id,Node_38_ref_node_target_id_45_inverse_0);
*Node_38_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_38_retrieved_reference_45_inverse_true_0_is_inverse, Node_38_ref_node_target_id_45_inverse_0);
*Node_38_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_38_retrieved_reference_45_inverse_0_target_id, Node_38_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_38_retrieved_reference_45_inverse_0->references,"1",Node_38_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasDescription ********/


case 39 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 39;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasDescription", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasDescription", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for references from data type encoding nodes to data type description nodes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("DescriptionOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 39 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_39_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_39_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_39_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_39_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_39_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_39_retrieved_reference_45_inverse_0);
*Node_39_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_39_retrieved_reference_45_inverse_0_typed_id,Node_39_ref_node_target_id_45_inverse_0);
*Node_39_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_39_retrieved_reference_45_inverse_true_0_is_inverse, Node_39_ref_node_target_id_45_inverse_0);
*Node_39_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_39_retrieved_reference_45_inverse_0_target_id, Node_39_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_39_retrieved_reference_45_inverse_0->references,"1",Node_39_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasTypeDefinition ********/


case 40 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 40;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasTypeDefinition", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasTypeDefinition", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for references from a instance node its type defintion node.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("TypeDefinitionOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 40 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_40_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_40_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_40_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_40_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_40_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_40_retrieved_reference_45_inverse_0);
*Node_40_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_40_retrieved_reference_45_inverse_0_typed_id,Node_40_ref_node_target_id_45_inverse_0);
*Node_40_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_40_retrieved_reference_45_inverse_true_0_is_inverse, Node_40_ref_node_target_id_45_inverse_0);
*Node_40_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_40_retrieved_reference_45_inverse_0_target_id, Node_40_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_40_retrieved_reference_45_inverse_0->references,"1",Node_40_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* GeneratesEvent ********/


case 41 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 41;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("GeneratesEvent", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("GeneratesEvent", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for references from a node to an event type that is raised by node.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("GeneratedBy", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 41 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_41_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_41_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_41_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_41_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_41_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_41_retrieved_reference_45_inverse_0);
*Node_41_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_41_retrieved_reference_45_inverse_0_typed_id,Node_41_ref_node_target_id_45_inverse_0);
*Node_41_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_41_retrieved_reference_45_inverse_true_0_is_inverse, Node_41_ref_node_target_id_45_inverse_0);
*Node_41_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_41_retrieved_reference_45_inverse_0_target_id, Node_41_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_41_retrieved_reference_45_inverse_0->references,"1",Node_41_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_41_retrieved_reference_45_0;
ishidaopcua_NODE* Node_41_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_41_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_41_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_41_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_41_retrieved_reference_45_0);
*Node_41_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_41_retrieved_reference_45_0_typed_id,Node_41_ref_node_target_id_45_0);
*Node_41_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_41_retrieved_reference_45_false_0_is_inverse, Node_41_ref_node_target_id_45_0);
*Node_41_retrieved_reference_45_0_target_id = 3065;
ishidaopcua_node_set_target_id(Node_41_retrieved_reference_45_0_target_id, Node_41_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_41_retrieved_reference_45_0->references,"1",Node_41_ref_node_target_id_45_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasNotifier ********/


case 48 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 48;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasNotifier", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasNotifier", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for non-looping hierarchical references that are used to indicate how events propagate from node to node.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("NotifierOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 48 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_48_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_48_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_48_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_48_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_48_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_48_retrieved_reference_45_inverse_0);
*Node_48_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_48_retrieved_reference_45_inverse_0_typed_id,Node_48_ref_node_target_id_45_inverse_0);
*Node_48_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_48_retrieved_reference_45_inverse_true_0_is_inverse, Node_48_ref_node_target_id_45_inverse_0);
*Node_48_retrieved_reference_45_inverse_0_target_id = 36;
ishidaopcua_node_set_target_id(Node_48_retrieved_reference_45_inverse_0_target_id, Node_48_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_48_retrieved_reference_45_inverse_0->references,"1",Node_48_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* AlwaysGeneratesEvent ********/


case 3065 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 3065;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("AlwaysGeneratesEvent", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("AlwaysGeneratesEvent", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for references from a node to an event type that is always raised by node.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("AlwaysGeneratedBy", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 3065 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_3065_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_3065_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3065_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3065_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3065_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_3065_retrieved_reference_45_inverse_0);
*Node_3065_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_3065_retrieved_reference_45_inverse_0_typed_id,Node_3065_ref_node_target_id_45_inverse_0);
*Node_3065_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_3065_retrieved_reference_45_inverse_true_0_is_inverse, Node_3065_ref_node_target_id_45_inverse_0);
*Node_3065_retrieved_reference_45_inverse_0_target_id = 41;
ishidaopcua_node_set_target_id(Node_3065_retrieved_reference_45_inverse_0_target_id, Node_3065_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_3065_retrieved_reference_45_inverse_0->references,"1",Node_3065_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/