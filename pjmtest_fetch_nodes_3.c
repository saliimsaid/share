#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_3(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	
	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* DataValue ********/


case 23 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 23;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DataValue", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DataValue", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a structure containing a value, a status code and timestamps.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 23 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_23_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_23_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_23_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_23_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_23_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_23_retrieved_reference_45_inverse_0);
*Node_23_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_23_retrieved_reference_45_inverse_0_typed_id,Node_23_ref_node_target_id_45_inverse_0);
*Node_23_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_23_retrieved_reference_45_inverse_true_0_is_inverse, Node_23_ref_node_target_id_45_inverse_0);
*Node_23_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_23_retrieved_reference_45_inverse_0_target_id, Node_23_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_23_retrieved_reference_45_inverse_0->references,"1",Node_23_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DiagnosticInfo ********/


case 25 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 25;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DiagnosticInfo", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DiagnosticInfo", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a structure containing diagnostics associated with a StatusCode.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 25 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_25_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_25_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_25_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_25_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_25_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_25_retrieved_reference_45_inverse_0);
*Node_25_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_25_retrieved_reference_45_inverse_0_typed_id,Node_25_ref_node_target_id_45_inverse_0);
*Node_25_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_25_retrieved_reference_45_inverse_true_0_is_inverse, Node_25_ref_node_target_id_45_inverse_0);
*Node_25_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_25_retrieved_reference_45_inverse_0_target_id, Node_25_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_25_retrieved_reference_45_inverse_0->references,"1",Node_25_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Enumeration ********/


case 29 : 
{ 

	printf("29.1");

	if(references_flag == 1) {
		printf("29.2");
		
		universal_node = ishidaopcua_node_init(12,12,0);
	}
	else
	{ 
		printf("29.3");

		universal_node = ishidaopcua_node_init(12,1,0);
	} 

	printf("29.4");

	universal_node_id = ishidaopcua_init_node_id();
	universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
	universal_node_id->identifier.numeric = 29;

	printf("29.5");

	ishidaopcua_node_set_node_id(universal_node_id, universal_node);

	printf("29.6");

	universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);

	printf("29.7");

	*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
	ishidaopcua_node_set_node_class(universal_node_class, universal_node);

	printf("29.8");

	universal_browse_name = ishidaopcua_init_qualified_name();

	printf("29.9");

	universal_browse_name_string = ishidaopcua_init_string(); 

	printf("29.10");

	ishidaopcua_set_string("Enumeration", universal_browse_name_string);

	printf("29.11");

	ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);

	printf("29.12");

	ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

	printf("29.13");

	universal_display_name = ishidaopcua_init_localized_text();

	printf("29.14");

	universal_display_name_locale = ishidaopcua_init_string();

	printf("29.15");

	universal_display_name_text = ishidaopcua_init_string();

	printf("29.16");

	ishidaopcua_set_string("en-UK", (universal_display_name_locale));

	printf("29.17");

	ishidaopcua_set_string("Enumeration", (universal_display_name_text));

	printf("29.18");

	ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);

	printf("29.19");

	ishidaopcua_node_set_display_name(universal_display_name, universal_node);

	printf("29.20");

	universal_description = ishidaopcua_init_localized_text();

	printf("29.21");

	universal_description_locale = ishidaopcua_init_string();

	printf("29.22");

	ishidaopcua_set_string("en-UK", (universal_description_locale));

	printf("29.23");

	universal_description_text = ishidaopcua_init_string();

	printf("29.24");

	ishidaopcua_set_string("Describes a value that is an enumerated DataType.", (universal_description_text));

	printf("29.25");

	ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);

	printf("29.26");

	ishidaopcua_node_set_description(universal_description, universal_node);

	printf("29.27");

	universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);

	printf("29.28");

	*universal_write_mask = 0;
	ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

	printf("29.29");

	universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);

	printf("29.30");

	*universal_user_write_mask = 0;
	ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

	puts("finished adding node >> 29 \n");

	
	
	if(references_flag > 0)
	{
		printf("29.31");
		
		initialize_reference_nodes(universal_node);

		printf("29.32");
		
		if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_inverse_0;			
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.33");
			
			ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_29_retrieved_reference_45_inverse_0);
			*Node_29_retrieved_reference_45_inverse_0_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_inverse_0_typed_id,Node_29_ref_node_target_id_45_inverse_0);
			*Node_29_retrieved_reference_45_inverse_true_0_is_inverse = 1;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_inverse_true_0_is_inverse, Node_29_ref_node_target_id_45_inverse_0);
			*Node_29_retrieved_reference_45_inverse_0_target_id = 24;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_inverse_0_target_id, Node_29_ref_node_target_id_45_inverse_0);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_inverse_0->references,"1",Node_29_ref_node_target_id_45_inverse_0);
			
			printf("29.34");

		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_0;			
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.35");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_0);
			*Node_29_retrieved_reference_45_0_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_0_typed_id,Node_29_ref_node_target_id_45_0);
			*Node_29_retrieved_reference_45_false_0_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_0_is_inverse, Node_29_ref_node_target_id_45_0);
			*Node_29_retrieved_reference_45_0_target_id = 120;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_0_target_id, Node_29_ref_node_target_id_45_0);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_0->references,"1",Node_29_ref_node_target_id_45_0);

			printf("29.36");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_1;			
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.37");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_1);
			*Node_29_retrieved_reference_45_1_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_1_typed_id,Node_29_ref_node_target_id_45_1);
			*Node_29_retrieved_reference_45_false_1_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_1_is_inverse, Node_29_ref_node_target_id_45_1);
			*Node_29_retrieved_reference_45_1_target_id = 11939;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_1_target_id, Node_29_ref_node_target_id_45_1);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_1->references,"2",Node_29_ref_node_target_id_45_1);

			printf("29.38");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_2;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.39");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_2);
			*Node_29_retrieved_reference_45_2_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_2_typed_id,Node_29_ref_node_target_id_45_2);
			*Node_29_retrieved_reference_45_false_2_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_2_is_inverse, Node_29_ref_node_target_id_45_2);
			*Node_29_retrieved_reference_45_2_target_id = 15632;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_2_target_id, Node_29_ref_node_target_id_45_2);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_2->references,"3",Node_29_ref_node_target_id_45_2);

			printf("29.40");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_3;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.41");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_3);
			*Node_29_retrieved_reference_45_3_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_3_typed_id,Node_29_ref_node_target_id_45_3);
			*Node_29_retrieved_reference_45_false_3_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_3_is_inverse, Node_29_ref_node_target_id_45_3);
			*Node_29_retrieved_reference_45_3_target_id = 12552;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_3_target_id, Node_29_ref_node_target_id_45_3);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_3->references,"4",Node_29_ref_node_target_id_45_3);

			printf("29.42");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_4;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.43");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_4);
			*Node_29_retrieved_reference_45_4_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_4_typed_id,Node_29_ref_node_target_id_45_4);
			*Node_29_retrieved_reference_45_false_4_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_4_is_inverse, Node_29_ref_node_target_id_45_4);
			*Node_29_retrieved_reference_45_4_target_id = 14647;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_4_target_id, Node_29_ref_node_target_id_45_4);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_4->references,"5",Node_29_ref_node_target_id_45_4);

			printf("29.44");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_5;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.45");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_5);
			*Node_29_retrieved_reference_45_5_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_5_typed_id,Node_29_ref_node_target_id_45_5);
			*Node_29_retrieved_reference_45_false_5_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_5_is_inverse, Node_29_ref_node_target_id_45_5);
			*Node_29_retrieved_reference_45_5_target_id = 15874;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_5_target_id, Node_29_ref_node_target_id_45_5);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_5->references,"6",Node_29_ref_node_target_id_45_5);

			printf("29.46");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_6;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.47");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_6);
			*Node_29_retrieved_reference_45_6_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_6_typed_id,Node_29_ref_node_target_id_45_6);
			*Node_29_retrieved_reference_45_false_6_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_6_is_inverse, Node_29_ref_node_target_id_45_6);
			*Node_29_retrieved_reference_45_6_target_id = 20408;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_6_target_id, Node_29_ref_node_target_id_45_6);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_6->references,"7",Node_29_ref_node_target_id_45_6);

			printf("29.48");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_7;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.49");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_7);
			*Node_29_retrieved_reference_45_7_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_7_typed_id,Node_29_ref_node_target_id_45_7);
			*Node_29_retrieved_reference_45_false_7_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_7_is_inverse, Node_29_ref_node_target_id_45_7);
			*Node_29_retrieved_reference_45_7_target_id = 15008;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_7_target_id, Node_29_ref_node_target_id_45_7);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_7->references,"8",Node_29_ref_node_target_id_45_7);

			printf("29.50");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_8;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_8 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.51");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_8);
			*Node_29_retrieved_reference_45_8_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_8_typed_id,Node_29_ref_node_target_id_45_8);
			*Node_29_retrieved_reference_45_false_8_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_8_is_inverse, Node_29_ref_node_target_id_45_8);
			*Node_29_retrieved_reference_45_8_target_id = 19723;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_8_target_id, Node_29_ref_node_target_id_45_8);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_8->references,"9",Node_29_ref_node_target_id_45_8);

			printf("29.52");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_9;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_9 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.53");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_9);
			*Node_29_retrieved_reference_45_9_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_9_typed_id,Node_29_ref_node_target_id_45_9);
			*Node_29_retrieved_reference_45_false_9_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_9_is_inverse, Node_29_ref_node_target_id_45_9);
			*Node_29_retrieved_reference_45_9_target_id = 19730;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_9_target_id, Node_29_ref_node_target_id_45_9);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_9->references,"10",Node_29_ref_node_target_id_45_9);

			printf("29.54");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_10;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_10 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.55");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_10);
			*Node_29_retrieved_reference_45_10_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_10_typed_id,Node_29_ref_node_target_id_45_10);
			*Node_29_retrieved_reference_45_false_10_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_10_is_inverse, Node_29_ref_node_target_id_45_10);
			*Node_29_retrieved_reference_45_10_target_id = 256;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_10_target_id, Node_29_ref_node_target_id_45_10);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_10->references,"11",Node_29_ref_node_target_id_45_10);

			printf("29.56");
			
		}if(references_flag != 2){
			
			ishidaopcua_NODE* Node_29_retrieved_reference_45_11;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_11 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.57");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_11);
			*Node_29_retrieved_reference_45_11_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_11_typed_id,Node_29_ref_node_target_id_45_11);
			*Node_29_retrieved_reference_45_false_11_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_11_is_inverse, Node_29_ref_node_target_id_45_11);
			*Node_29_retrieved_reference_45_11_target_id = 257;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_11_target_id, Node_29_ref_node_target_id_45_11);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_11->references,"12",Node_29_ref_node_target_id_45_11);

			printf("29.58");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_12;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_12 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_12_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_12_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_12_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.59");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_12);
			*Node_29_retrieved_reference_45_12_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_12_typed_id,Node_29_ref_node_target_id_45_12);
			*Node_29_retrieved_reference_45_false_12_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_12_is_inverse, Node_29_ref_node_target_id_45_12);
			*Node_29_retrieved_reference_45_12_target_id = 98;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_12_target_id, Node_29_ref_node_target_id_45_12);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_12->references,"13",Node_29_ref_node_target_id_45_12);

			printf("29.60");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_13;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_13 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_13_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_13_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_13_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.61");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_13);
			*Node_29_retrieved_reference_45_13_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_13_typed_id,Node_29_ref_node_target_id_45_13);
			*Node_29_retrieved_reference_45_false_13_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_13_is_inverse, Node_29_ref_node_target_id_45_13);
			*Node_29_retrieved_reference_45_13_target_id = 307;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_13_target_id, Node_29_ref_node_target_id_45_13);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_13->references,"14",Node_29_ref_node_target_id_45_13);

			printf("29.62");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_14;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_14 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_14_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_14_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_14_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.63");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_14);
			*Node_29_retrieved_reference_45_14_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_14_typed_id,Node_29_ref_node_target_id_45_14);
			*Node_29_retrieved_reference_45_false_14_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_14_is_inverse, Node_29_ref_node_target_id_45_14);
			*Node_29_retrieved_reference_45_14_target_id = 302;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_14_target_id, Node_29_ref_node_target_id_45_14);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_14->references,"15",Node_29_ref_node_target_id_45_14);

			printf("29.64");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_15;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_15 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_15_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_15_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_15_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.65");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_15);
			*Node_29_retrieved_reference_45_15_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_15_typed_id,Node_29_ref_node_target_id_45_15);
			*Node_29_retrieved_reference_45_false_15_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_15_is_inverse, Node_29_ref_node_target_id_45_15);
			*Node_29_retrieved_reference_45_15_target_id = 303;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_15_target_id, Node_29_ref_node_target_id_45_15);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_15->references,"16",Node_29_ref_node_target_id_45_15);

			printf("29.66");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_16;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_16 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_16_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_16_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_16_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.67");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_16);
			*Node_29_retrieved_reference_45_16_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_16_typed_id,Node_29_ref_node_target_id_45_16);
			*Node_29_retrieved_reference_45_false_16_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_16_is_inverse, Node_29_ref_node_target_id_45_16);
			*Node_29_retrieved_reference_45_16_target_id = 315;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_16_target_id, Node_29_ref_node_target_id_45_16);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_16->references,"17",Node_29_ref_node_target_id_45_16);

			printf("29.68");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_17;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_17 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_17_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_17_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_17_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.69");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_17);
			*Node_29_retrieved_reference_45_17_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_17_typed_id,Node_29_ref_node_target_id_45_17);
			*Node_29_retrieved_reference_45_false_17_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_17_is_inverse, Node_29_ref_node_target_id_45_17);
			*Node_29_retrieved_reference_45_17_target_id = 348;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_17_target_id, Node_29_ref_node_target_id_45_17);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_17->references,"18",Node_29_ref_node_target_id_45_17);

			printf("29.70");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_18;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_18 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_18_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_18_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_18_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.71");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_18);
			*Node_29_retrieved_reference_45_18_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_18_typed_id,Node_29_ref_node_target_id_45_18);
			*Node_29_retrieved_reference_45_false_18_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_18_is_inverse, Node_29_ref_node_target_id_45_18);
			*Node_29_retrieved_reference_45_18_target_id = 576;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_18_target_id, Node_29_ref_node_target_id_45_18);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_18->references,"19",Node_29_ref_node_target_id_45_18);

			printf("29.72");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_19;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_19 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_19_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_19_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_19_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.73");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_19);
			*Node_29_retrieved_reference_45_19_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_19_typed_id,Node_29_ref_node_target_id_45_19);
			*Node_29_retrieved_reference_45_false_19_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_19_is_inverse, Node_29_ref_node_target_id_45_19);
			*Node_29_retrieved_reference_45_19_target_id = 11234;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_19_target_id, Node_29_ref_node_target_id_45_19);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_19->references,"20",Node_29_ref_node_target_id_45_19);

			printf("29.74");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_20;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_20 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_20_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_20_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_20_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.75");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_20);
			*Node_29_retrieved_reference_45_20_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_20_typed_id,Node_29_ref_node_target_id_45_20);
			*Node_29_retrieved_reference_45_false_20_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_20_is_inverse, Node_29_ref_node_target_id_45_20);
			*Node_29_retrieved_reference_45_20_target_id = 11293;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_20_target_id, Node_29_ref_node_target_id_45_20);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_20->references,"21",Node_29_ref_node_target_id_45_20);

			printf("29.76");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_21;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_21 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_21_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_21_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_21_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.77");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_21);
			*Node_29_retrieved_reference_45_21_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_21_typed_id,Node_29_ref_node_target_id_45_21);
			*Node_29_retrieved_reference_45_false_21_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_21_is_inverse, Node_29_ref_node_target_id_45_21);
			*Node_29_retrieved_reference_45_21_target_id = 851;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_21_target_id, Node_29_ref_node_target_id_45_21);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_21->references,"22",Node_29_ref_node_target_id_45_21);

			printf("29.78");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_22;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_22 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_22_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_22_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_22_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.79");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_22);
			*Node_29_retrieved_reference_45_22_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_22_typed_id,Node_29_ref_node_target_id_45_22);
			*Node_29_retrieved_reference_45_false_22_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_22_is_inverse, Node_29_ref_node_target_id_45_22);
			*Node_29_retrieved_reference_45_22_target_id = 852;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_22_target_id, Node_29_ref_node_target_id_45_22);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_22->references,"23",Node_29_ref_node_target_id_45_22);

			printf("29.80");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_23;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_23 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_23_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_23_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_23_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.81");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_23);
			*Node_29_retrieved_reference_45_23_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_23_typed_id,Node_29_ref_node_target_id_45_23);
			*Node_29_retrieved_reference_45_false_23_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_23_is_inverse, Node_29_ref_node_target_id_45_23);
			*Node_29_retrieved_reference_45_23_target_id = 12077;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_23_target_id, Node_29_ref_node_target_id_45_23);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_23->references,"24",Node_29_ref_node_target_id_45_23);

			printf("29.82");
			
		}if(references_flag != 2){
		
			ishidaopcua_NODE* Node_29_retrieved_reference_45_24;
			ishidaopcua_NODE* Node_29_ref_node_target_id_45_24 = ishidaopcua_node_init(3,0,0);
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_24_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_false_24_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			ishidaopcua_UINT32* Node_29_retrieved_reference_45_24_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
			
			printf("29.83");
			
			ishidaeutz_get_hashmap(universal_node->references,"45",&Node_29_retrieved_reference_45_24);
			*Node_29_retrieved_reference_45_24_typed_id =45;
			ishidaopcua_node_set_reference_type_id(Node_29_retrieved_reference_45_24_typed_id,Node_29_ref_node_target_id_45_24);
			*Node_29_retrieved_reference_45_false_24_is_inverse = 0;
			ishidaopcua_node_set_is_inverse(Node_29_retrieved_reference_45_false_24_is_inverse, Node_29_ref_node_target_id_45_24);
			*Node_29_retrieved_reference_45_24_target_id = 890;
			ishidaopcua_node_set_target_id(Node_29_retrieved_reference_45_24_target_id, Node_29_ref_node_target_id_45_24);
			ishidaeutz_put_hashmap(Node_29_retrieved_reference_45_24->references,"25",Node_29_ref_node_target_id_45_24);

			printf("29.84");
			
		} 
		
		printf("29.85");

	} /* ---- if match ---- */ 

	printf("29.86");

	return universal_node; 
	break;
} /* ---------- match case ---------------- */ 
/******* Decimal ********/


case 50 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 50;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Decimal", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Decimal", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes an arbitrary precision decimal value.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 50 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_50_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_50_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_50_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_50_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_50_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_50_retrieved_reference_45_inverse_0);
*Node_50_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_50_retrieved_reference_45_inverse_0_typed_id,Node_50_ref_node_target_id_45_inverse_0);
*Node_50_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_50_retrieved_reference_45_inverse_true_0_is_inverse, Node_50_ref_node_target_id_45_inverse_0);
*Node_50_retrieved_reference_45_inverse_0_target_id = 26;
ishidaopcua_node_set_target_id(Node_50_retrieved_reference_45_inverse_0_target_id, Node_50_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_50_retrieved_reference_45_inverse_0->references,"1",Node_50_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* PropertyType ********/


case 68 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 68;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("PropertyType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("PropertyType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for variable that represents a property of another node.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 24;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -2;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 68 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_68_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_68_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_68_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_68_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_68_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_68_retrieved_reference_45_inverse_0);
*Node_68_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_68_retrieved_reference_45_inverse_0_typed_id,Node_68_ref_node_target_id_45_inverse_0);
*Node_68_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_68_retrieved_reference_45_inverse_true_0_is_inverse, Node_68_ref_node_target_id_45_inverse_0);
*Node_68_retrieved_reference_45_inverse_0_target_id = 62;
ishidaopcua_node_set_target_id(Node_68_retrieved_reference_45_inverse_0_target_id, Node_68_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_68_retrieved_reference_45_inverse_0->references,"1",Node_68_ref_node_target_id_45_inverse_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* NamingRuleType ********/


case 120 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 120;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("NamingRuleType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("NamingRuleType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that specifies the significance of the BrowseName for an instance declaration.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 120 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_120_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_120_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_120_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_120_retrieved_reference_45_inverse_0);
*Node_120_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_120_retrieved_reference_45_inverse_0_typed_id,Node_120_ref_node_target_id_45_inverse_0);
*Node_120_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_120_retrieved_reference_45_inverse_true_0_is_inverse, Node_120_ref_node_target_id_45_inverse_0);
*Node_120_retrieved_reference_45_inverse_0_target_id = 29;
ishidaopcua_node_set_target_id(Node_120_retrieved_reference_45_inverse_0_target_id, Node_120_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_120_retrieved_reference_45_inverse_0->references,"1",Node_120_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_120_retrieved_reference_46_0;
ishidaopcua_NODE* Node_120_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_120_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_120_retrieved_reference_46_0);
*Node_120_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_120_retrieved_reference_46_0_typed_id,Node_120_ref_node_target_id_46_0);
*Node_120_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_120_retrieved_reference_46_false_0_is_inverse, Node_120_ref_node_target_id_46_0);
*Node_120_retrieved_reference_46_0_target_id = 12169;
ishidaopcua_node_set_target_id(Node_120_retrieved_reference_46_0_target_id, Node_120_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_120_retrieved_reference_46_0->references,"1",Node_120_ref_node_target_id_46_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* RedundancySupport ********/


case 851 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 851;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("RedundancySupport", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("RedundancySupport", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 851 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_851_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_851_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_851_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_851_retrieved_reference_45_inverse_0);
*Node_851_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_851_retrieved_reference_45_inverse_0_typed_id,Node_851_ref_node_target_id_45_inverse_0);
*Node_851_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_851_retrieved_reference_45_inverse_true_0_is_inverse, Node_851_ref_node_target_id_45_inverse_0);
*Node_851_retrieved_reference_45_inverse_0_target_id = 29;
ishidaopcua_node_set_target_id(Node_851_retrieved_reference_45_inverse_0_target_id, Node_851_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_851_retrieved_reference_45_inverse_0->references,"1",Node_851_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_851_retrieved_reference_46_0;
ishidaopcua_NODE* Node_851_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_851_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_851_retrieved_reference_46_0);
*Node_851_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_851_retrieved_reference_46_0_typed_id,Node_851_ref_node_target_id_46_0);
*Node_851_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_851_retrieved_reference_46_false_0_is_inverse, Node_851_ref_node_target_id_46_0);
*Node_851_retrieved_reference_46_0_target_id = 7611;
ishidaopcua_node_set_target_id(Node_851_retrieved_reference_46_0_target_id, Node_851_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_851_retrieved_reference_46_0->references,"1",Node_851_ref_node_target_id_46_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerState ********/


case 852 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 852;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerState", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerState", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 852 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_852_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_852_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_852_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_852_retrieved_reference_45_inverse_0);
*Node_852_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_852_retrieved_reference_45_inverse_0_typed_id,Node_852_ref_node_target_id_45_inverse_0);
*Node_852_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_852_retrieved_reference_45_inverse_true_0_is_inverse, Node_852_ref_node_target_id_45_inverse_0);
*Node_852_retrieved_reference_45_inverse_0_target_id = 29;
ishidaopcua_node_set_target_id(Node_852_retrieved_reference_45_inverse_0_target_id, Node_852_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_852_retrieved_reference_45_inverse_0->references,"1",Node_852_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_852_retrieved_reference_46_0;
ishidaopcua_NODE* Node_852_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_852_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_852_retrieved_reference_46_0);
*Node_852_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_852_retrieved_reference_46_0_typed_id,Node_852_ref_node_target_id_46_0);
*Node_852_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_852_retrieved_reference_46_false_0_is_inverse, Node_852_ref_node_target_id_46_0);
*Node_852_retrieved_reference_46_0_target_id = 7612;
ishidaopcua_node_set_target_id(Node_852_retrieved_reference_46_0_target_id, Node_852_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_852_retrieved_reference_46_0->references,"1",Node_852_ref_node_target_id_46_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerRedundancyType ********/


case 2034 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2034;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerRedundancyType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerRedundancyType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A base type for an object that describe how a server supports redundancy.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

puts("finished adding node >> 2034 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2034_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_2034_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_2034_retrieved_reference_45_inverse_0);
*Node_2034_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2034_retrieved_reference_45_inverse_0_typed_id,Node_2034_ref_node_target_id_45_inverse_0);
*Node_2034_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2034_retrieved_reference_45_inverse_true_0_is_inverse, Node_2034_ref_node_target_id_45_inverse_0);
*Node_2034_retrieved_reference_45_inverse_0_target_id = 58;
ishidaopcua_node_set_target_id(Node_2034_retrieved_reference_45_inverse_0_target_id, Node_2034_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_2034_retrieved_reference_45_inverse_0->references,"1",Node_2034_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2034_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2034_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2034_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2034_retrieved_reference_46_0);
*Node_2034_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2034_retrieved_reference_46_0_typed_id,Node_2034_ref_node_target_id_46_0);
*Node_2034_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2034_retrieved_reference_46_false_0_is_inverse, Node_2034_ref_node_target_id_46_0);
*Node_2034_retrieved_reference_46_0_target_id = 2035;
ishidaopcua_node_set_target_id(Node_2034_retrieved_reference_46_0_target_id, Node_2034_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2034_retrieved_reference_46_0->references,"1",Node_2034_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2034_retrieved_reference_45_0;
ishidaopcua_NODE* Node_2034_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_2034_retrieved_reference_45_0);
*Node_2034_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2034_retrieved_reference_45_0_typed_id,Node_2034_ref_node_target_id_45_0);
*Node_2034_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2034_retrieved_reference_45_false_0_is_inverse, Node_2034_ref_node_target_id_45_0);
*Node_2034_retrieved_reference_45_0_target_id = 2036;
ishidaopcua_node_set_target_id(Node_2034_retrieved_reference_45_0_target_id, Node_2034_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_2034_retrieved_reference_45_0->references,"1",Node_2034_ref_node_target_id_45_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2034_retrieved_reference_45_1;
ishidaopcua_NODE* Node_2034_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2034_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_2034_retrieved_reference_45_1);
*Node_2034_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2034_retrieved_reference_45_1_typed_id,Node_2034_ref_node_target_id_45_1);
*Node_2034_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2034_retrieved_reference_45_false_1_is_inverse, Node_2034_ref_node_target_id_45_1);
*Node_2034_retrieved_reference_45_1_target_id = 2039;
ishidaopcua_node_set_target_id(Node_2034_retrieved_reference_45_1_target_id, Node_2034_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_2034_retrieved_reference_45_1->references,"2",Node_2034_ref_node_target_id_45_1); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerRedundancy ********/


case 2296 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2296;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerRedundancy", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerRedundancy", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes the redundancy capabilities of the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 2296 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2296_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2296_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2296_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2296_retrieved_reference_47_inverse_0);
*Node_2296_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2296_retrieved_reference_47_inverse_0_typed_id,Node_2296_ref_node_target_id_47_inverse_0);
*Node_2296_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2296_retrieved_reference_47_inverse_true_0_is_inverse, Node_2296_ref_node_target_id_47_inverse_0);
*Node_2296_retrieved_reference_47_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_2296_retrieved_reference_47_inverse_0_target_id, Node_2296_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2296_retrieved_reference_47_inverse_0->references,"1",Node_2296_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2296_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2296_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2296_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2296_retrieved_reference_40_0);
*Node_2296_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2296_retrieved_reference_40_0_typed_id,Node_2296_ref_node_target_id_40_0);
*Node_2296_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2296_retrieved_reference_40_false_0_is_inverse, Node_2296_ref_node_target_id_40_0);
*Node_2296_retrieved_reference_40_0_target_id = 2034;
ishidaopcua_node_set_target_id(Node_2296_retrieved_reference_40_0_target_id, Node_2296_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2296_retrieved_reference_40_0->references,"1",Node_2296_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2296_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2296_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2296_retrieved_reference_46_0);
*Node_2296_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2296_retrieved_reference_46_0_typed_id,Node_2296_ref_node_target_id_46_0);
*Node_2296_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2296_retrieved_reference_46_false_0_is_inverse, Node_2296_ref_node_target_id_46_0);
*Node_2296_retrieved_reference_46_0_target_id = 3709;
ishidaopcua_node_set_target_id(Node_2296_retrieved_reference_46_0_target_id, Node_2296_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2296_retrieved_reference_46_0->references,"1",Node_2296_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2296_retrieved_reference_46_1;
ishidaopcua_NODE* Node_2296_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2296_retrieved_reference_46_1);
*Node_2296_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2296_retrieved_reference_46_1_typed_id,Node_2296_ref_node_target_id_46_1);
*Node_2296_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2296_retrieved_reference_46_false_1_is_inverse, Node_2296_ref_node_target_id_46_1);
*Node_2296_retrieved_reference_46_1_target_id = 11312;
ishidaopcua_node_set_target_id(Node_2296_retrieved_reference_46_1_target_id, Node_2296_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_2296_retrieved_reference_46_1->references,"2",Node_2296_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2296_retrieved_reference_46_2;
ishidaopcua_NODE* Node_2296_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2296_retrieved_reference_46_2);
*Node_2296_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2296_retrieved_reference_46_2_typed_id,Node_2296_ref_node_target_id_46_2);
*Node_2296_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2296_retrieved_reference_46_false_2_is_inverse, Node_2296_ref_node_target_id_46_2);
*Node_2296_retrieved_reference_46_2_target_id = 11313;
ishidaopcua_node_set_target_id(Node_2296_retrieved_reference_46_2_target_id, Node_2296_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_2296_retrieved_reference_46_2->references,"3",Node_2296_ref_node_target_id_46_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2296_retrieved_reference_46_3;
ishidaopcua_NODE* Node_2296_ref_node_target_id_46_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2296_retrieved_reference_46_3);
*Node_2296_retrieved_reference_46_3_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2296_retrieved_reference_46_3_typed_id,Node_2296_ref_node_target_id_46_3);
*Node_2296_retrieved_reference_46_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2296_retrieved_reference_46_false_3_is_inverse, Node_2296_ref_node_target_id_46_3);
*Node_2296_retrieved_reference_46_3_target_id = 11314;
ishidaopcua_node_set_target_id(Node_2296_retrieved_reference_46_3_target_id, Node_2296_ref_node_target_id_46_3);
ishidaeutz_put_hashmap(Node_2296_retrieved_reference_46_3->references,"4",Node_2296_ref_node_target_id_46_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2296_retrieved_reference_46_4;
ishidaopcua_NODE* Node_2296_ref_node_target_id_46_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2296_retrieved_reference_46_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2296_retrieved_reference_46_4);
*Node_2296_retrieved_reference_46_4_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2296_retrieved_reference_46_4_typed_id,Node_2296_ref_node_target_id_46_4);
*Node_2296_retrieved_reference_46_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2296_retrieved_reference_46_false_4_is_inverse, Node_2296_ref_node_target_id_46_4);
*Node_2296_retrieved_reference_46_4_target_id = 14415;
ishidaopcua_node_set_target_id(Node_2296_retrieved_reference_46_4_target_id, Node_2296_ref_node_target_id_46_4);
ishidaeutz_put_hashmap(Node_2296_retrieved_reference_46_4->references,"5",Node_2296_ref_node_target_id_46_4); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/