#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_20(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* XmlElement ********/


case 16 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 16;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("XmlElement", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("XmlElement", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an XML element.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 16 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_16_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_16_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_16_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_16_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_16_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_16_retrieved_reference_45_inverse_0);
*Node_16_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_16_retrieved_reference_45_inverse_0_typed_id,Node_16_ref_node_target_id_45_inverse_0);
*Node_16_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_16_retrieved_reference_45_inverse_true_0_is_inverse, Node_16_ref_node_target_id_45_inverse_0);
*Node_16_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_16_retrieved_reference_45_inverse_0_target_id, Node_16_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_16_retrieved_reference_45_inverse_0->references,"1",Node_16_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* NonHierarchicalReferences ********/


case 32 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 32;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("NonHierarchicalReferences", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("NonHierarchicalReferences", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The abstract base type for all non-hierarchical references.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 1;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("NonHierarchicalReferences", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 32 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_32_retrieved_reference_45_inverse_0);
*Node_32_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_inverse_0_typed_id,Node_32_ref_node_target_id_45_inverse_0);
*Node_32_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_inverse_true_0_is_inverse, Node_32_ref_node_target_id_45_inverse_0);
*Node_32_retrieved_reference_45_inverse_0_target_id = 31;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_inverse_0_target_id, Node_32_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_inverse_0->references,"1",Node_32_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_0;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_0);
*Node_32_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_0_typed_id,Node_32_ref_node_target_id_45_0);
*Node_32_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_0_is_inverse, Node_32_ref_node_target_id_45_0);
*Node_32_retrieved_reference_45_0_target_id = 37;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_0_target_id, Node_32_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_0->references,"1",Node_32_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_1;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_1);
*Node_32_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_1_typed_id,Node_32_ref_node_target_id_45_1);
*Node_32_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_1_is_inverse, Node_32_ref_node_target_id_45_1);
*Node_32_retrieved_reference_45_1_target_id = 38;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_1_target_id, Node_32_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_1->references,"2",Node_32_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_2;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_2);
*Node_32_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_2_typed_id,Node_32_ref_node_target_id_45_2);
*Node_32_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_2_is_inverse, Node_32_ref_node_target_id_45_2);
*Node_32_retrieved_reference_45_2_target_id = 39;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_2_target_id, Node_32_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_2->references,"3",Node_32_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_3;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_3);
*Node_32_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_3_typed_id,Node_32_ref_node_target_id_45_3);
*Node_32_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_3_is_inverse, Node_32_ref_node_target_id_45_3);
*Node_32_retrieved_reference_45_3_target_id = 40;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_3_target_id, Node_32_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_3->references,"4",Node_32_ref_node_target_id_45_3);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_4;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_4);
*Node_32_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_4_typed_id,Node_32_ref_node_target_id_45_4);
*Node_32_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_4_is_inverse, Node_32_ref_node_target_id_45_4);
*Node_32_retrieved_reference_45_4_target_id = 41;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_4_target_id, Node_32_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_4->references,"5",Node_32_ref_node_target_id_45_4);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_5;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_5);
*Node_32_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_5_typed_id,Node_32_ref_node_target_id_45_5);
*Node_32_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_5_is_inverse, Node_32_ref_node_target_id_45_5);
*Node_32_retrieved_reference_45_5_target_id = 51;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_5_target_id, Node_32_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_5->references,"6",Node_32_ref_node_target_id_45_5);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_6;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_6);
*Node_32_retrieved_reference_45_6_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_6_typed_id,Node_32_ref_node_target_id_45_6);
*Node_32_retrieved_reference_45_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_6_is_inverse, Node_32_ref_node_target_id_45_6);
*Node_32_retrieved_reference_45_6_target_id = 52;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_6_target_id, Node_32_ref_node_target_id_45_6);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_6->references,"7",Node_32_ref_node_target_id_45_6);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_7;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_7);
*Node_32_retrieved_reference_45_7_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_7_typed_id,Node_32_ref_node_target_id_45_7);
*Node_32_retrieved_reference_45_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_7_is_inverse, Node_32_ref_node_target_id_45_7);
*Node_32_retrieved_reference_45_7_target_id = 53;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_7_target_id, Node_32_ref_node_target_id_45_7);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_7->references,"8",Node_32_ref_node_target_id_45_7);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_8;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_8);
*Node_32_retrieved_reference_45_8_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_8_typed_id,Node_32_ref_node_target_id_45_8);
*Node_32_retrieved_reference_45_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_8_is_inverse, Node_32_ref_node_target_id_45_8);
*Node_32_retrieved_reference_45_8_target_id = 54;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_8_target_id, Node_32_ref_node_target_id_45_8);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_8->references,"9",Node_32_ref_node_target_id_45_8);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_9;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_9);
*Node_32_retrieved_reference_45_9_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_9_typed_id,Node_32_ref_node_target_id_45_9);
*Node_32_retrieved_reference_45_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_9_is_inverse, Node_32_ref_node_target_id_45_9);
*Node_32_retrieved_reference_45_9_target_id = 117;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_9_target_id, Node_32_ref_node_target_id_45_9);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_9->references,"10",Node_32_ref_node_target_id_45_9);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_10;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_10);
*Node_32_retrieved_reference_45_10_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_10_typed_id,Node_32_ref_node_target_id_45_10);
*Node_32_retrieved_reference_45_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_10_is_inverse, Node_32_ref_node_target_id_45_10);
*Node_32_retrieved_reference_45_10_target_id = 9004;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_10_target_id, Node_32_ref_node_target_id_45_10);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_10->references,"11",Node_32_ref_node_target_id_45_10);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_11;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_11);
*Node_32_retrieved_reference_45_11_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_11_typed_id,Node_32_ref_node_target_id_45_11);
*Node_32_retrieved_reference_45_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_11_is_inverse, Node_32_ref_node_target_id_45_11);
*Node_32_retrieved_reference_45_11_target_id = 9005;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_11_target_id, Node_32_ref_node_target_id_45_11);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_11->references,"12",Node_32_ref_node_target_id_45_11);

}if(references_flag != 2){

ishidaopcua_NODE* Node_32_retrieved_reference_45_12;
ishidaopcua_NODE* Node_32_ref_node_target_id_45_12 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_32_retrieved_reference_45_12_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_false_12_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_32_retrieved_reference_45_12_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_32_retrieved_reference_45_12);
*Node_32_retrieved_reference_45_12_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_32_retrieved_reference_45_12_typed_id,Node_32_ref_node_target_id_45_12);
*Node_32_retrieved_reference_45_false_12_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_32_retrieved_reference_45_false_12_is_inverse, Node_32_ref_node_target_id_45_12);
*Node_32_retrieved_reference_45_12_target_id = 9006;
ishidaopcua_node_set_target_id(Node_32_retrieved_reference_45_12_target_id, Node_32_ref_node_target_id_45_12);
ishidaeutz_put_hashmap(Node_32_retrieved_reference_45_12->references,"13",Node_32_ref_node_target_id_45_12);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasChild ********/


case 34 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 34;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasChild", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasChild", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The abstract base type for all non-looping hierarchical references.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 1;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("ChildOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 34 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_34_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_34_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_34_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_34_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_34_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_34_retrieved_reference_45_inverse_0);
*Node_34_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_34_retrieved_reference_45_inverse_0_typed_id,Node_34_ref_node_target_id_45_inverse_0);
*Node_34_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_34_retrieved_reference_45_inverse_true_0_is_inverse, Node_34_ref_node_target_id_45_inverse_0);
*Node_34_retrieved_reference_45_inverse_0_target_id = 33;
ishidaopcua_node_set_target_id(Node_34_retrieved_reference_45_inverse_0_target_id, Node_34_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_34_retrieved_reference_45_inverse_0->references,"1",Node_34_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_34_retrieved_reference_45_0;
ishidaopcua_NODE* Node_34_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_34_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_34_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_34_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_34_retrieved_reference_45_0);
*Node_34_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_34_retrieved_reference_45_0_typed_id,Node_34_ref_node_target_id_45_0);
*Node_34_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_34_retrieved_reference_45_false_0_is_inverse, Node_34_ref_node_target_id_45_0);
*Node_34_retrieved_reference_45_0_target_id = 44;
ishidaopcua_node_set_target_id(Node_34_retrieved_reference_45_0_target_id, Node_34_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_34_retrieved_reference_45_0->references,"1",Node_34_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_34_retrieved_reference_45_1;
ishidaopcua_NODE* Node_34_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_34_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_34_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_34_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_34_retrieved_reference_45_1);
*Node_34_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_34_retrieved_reference_45_1_typed_id,Node_34_ref_node_target_id_45_1);
*Node_34_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_34_retrieved_reference_45_false_1_is_inverse, Node_34_ref_node_target_id_45_1);
*Node_34_retrieved_reference_45_1_target_id = 45;
ishidaopcua_node_set_target_id(Node_34_retrieved_reference_45_1_target_id, Node_34_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_34_retrieved_reference_45_1->references,"2",Node_34_ref_node_target_id_45_1);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Aggregates ********/


case 44 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 44;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Aggregates", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Aggregates", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for non-looping hierarchical references that are used to aggregate nodes into complex types.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 1;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("AggregatedBy", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 44 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_44_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_44_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_44_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_44_retrieved_reference_45_inverse_0);
*Node_44_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_44_retrieved_reference_45_inverse_0_typed_id,Node_44_ref_node_target_id_45_inverse_0);
*Node_44_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_44_retrieved_reference_45_inverse_true_0_is_inverse, Node_44_ref_node_target_id_45_inverse_0);
*Node_44_retrieved_reference_45_inverse_0_target_id = 34;
ishidaopcua_node_set_target_id(Node_44_retrieved_reference_45_inverse_0_target_id, Node_44_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_44_retrieved_reference_45_inverse_0->references,"1",Node_44_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_44_retrieved_reference_45_0;
ishidaopcua_NODE* Node_44_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_44_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_44_retrieved_reference_45_0);
*Node_44_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_44_retrieved_reference_45_0_typed_id,Node_44_ref_node_target_id_45_0);
*Node_44_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_44_retrieved_reference_45_false_0_is_inverse, Node_44_ref_node_target_id_45_0);
*Node_44_retrieved_reference_45_0_target_id = 46;
ishidaopcua_node_set_target_id(Node_44_retrieved_reference_45_0_target_id, Node_44_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_44_retrieved_reference_45_0->references,"1",Node_44_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_44_retrieved_reference_45_1;
ishidaopcua_NODE* Node_44_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_44_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_44_retrieved_reference_45_1);
*Node_44_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_44_retrieved_reference_45_1_typed_id,Node_44_ref_node_target_id_45_1);
*Node_44_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_44_retrieved_reference_45_false_1_is_inverse, Node_44_ref_node_target_id_45_1);
*Node_44_retrieved_reference_45_1_target_id = 47;
ishidaopcua_node_set_target_id(Node_44_retrieved_reference_45_1_target_id, Node_44_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_44_retrieved_reference_45_1->references,"2",Node_44_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_44_retrieved_reference_45_2;
ishidaopcua_NODE* Node_44_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_44_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_44_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_44_retrieved_reference_45_2);
*Node_44_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_44_retrieved_reference_45_2_typed_id,Node_44_ref_node_target_id_45_2);
*Node_44_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_44_retrieved_reference_45_false_2_is_inverse, Node_44_ref_node_target_id_45_2);
*Node_44_retrieved_reference_45_2_target_id = 56;
ishidaopcua_node_set_target_id(Node_44_retrieved_reference_45_2_target_id, Node_44_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_44_retrieved_reference_45_2->references,"3",Node_44_ref_node_target_id_45_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasSubtype ********/


case 45 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 45;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasSubtype", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasSubtype", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for non-looping hierarchical references that are used to define sub types.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("SubtypeOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 45 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_45_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_45_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_45_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_45_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_45_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_45_retrieved_reference_45_inverse_0);
*Node_45_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_45_retrieved_reference_45_inverse_0_typed_id,Node_45_ref_node_target_id_45_inverse_0);
*Node_45_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_45_retrieved_reference_45_inverse_true_0_is_inverse, Node_45_ref_node_target_id_45_inverse_0);
*Node_45_retrieved_reference_45_inverse_0_target_id = 34;
ishidaopcua_node_set_target_id(Node_45_retrieved_reference_45_inverse_0_target_id, Node_45_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_45_retrieved_reference_45_inverse_0->references,"1",Node_45_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasProperty ********/


case 46 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 46;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasProperty", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasProperty", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for non-looping hierarchical reference from a node to its property.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("PropertyOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 46 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_46_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_46_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_46_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_46_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_46_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_46_retrieved_reference_45_inverse_0);
*Node_46_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_46_retrieved_reference_45_inverse_0_typed_id,Node_46_ref_node_target_id_45_inverse_0);
*Node_46_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_46_retrieved_reference_45_inverse_true_0_is_inverse, Node_46_ref_node_target_id_45_inverse_0);
*Node_46_retrieved_reference_45_inverse_0_target_id = 44;
ishidaopcua_node_set_target_id(Node_46_retrieved_reference_45_inverse_0_target_id, Node_46_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_46_retrieved_reference_45_inverse_0->references,"1",Node_46_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasComponent ********/


case 47 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 47;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasComponent", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasComponent", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for non-looping hierarchical reference from a node to its component.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("ComponentOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 47 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_47_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_47_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_47_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_47_retrieved_reference_45_inverse_0);
*Node_47_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_47_retrieved_reference_45_inverse_0_typed_id,Node_47_ref_node_target_id_45_inverse_0);
*Node_47_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_47_retrieved_reference_45_inverse_true_0_is_inverse, Node_47_ref_node_target_id_45_inverse_0);
*Node_47_retrieved_reference_45_inverse_0_target_id = 44;
ishidaopcua_node_set_target_id(Node_47_retrieved_reference_45_inverse_0_target_id, Node_47_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_47_retrieved_reference_45_inverse_0->references,"1",Node_47_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_47_retrieved_reference_45_0;
ishidaopcua_NODE* Node_47_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_47_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_47_retrieved_reference_45_0);
*Node_47_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_47_retrieved_reference_45_0_typed_id,Node_47_ref_node_target_id_45_0);
*Node_47_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_47_retrieved_reference_45_false_0_is_inverse, Node_47_ref_node_target_id_45_0);
*Node_47_retrieved_reference_45_0_target_id = 49;
ishidaopcua_node_set_target_id(Node_47_retrieved_reference_45_0_target_id, Node_47_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_47_retrieved_reference_45_0->references,"1",Node_47_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_47_retrieved_reference_45_1;
ishidaopcua_NODE* Node_47_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_47_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_47_retrieved_reference_45_1);
*Node_47_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_47_retrieved_reference_45_1_typed_id,Node_47_ref_node_target_id_45_1);
*Node_47_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_47_retrieved_reference_45_false_1_is_inverse, Node_47_ref_node_target_id_45_1);
*Node_47_retrieved_reference_45_1_target_id = 16361;
ishidaopcua_node_set_target_id(Node_47_retrieved_reference_45_1_target_id, Node_47_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_47_retrieved_reference_45_1->references,"2",Node_47_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_47_retrieved_reference_45_2;
ishidaopcua_NODE* Node_47_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_47_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_47_retrieved_reference_45_2);
*Node_47_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_47_retrieved_reference_45_2_typed_id,Node_47_ref_node_target_id_45_2);
*Node_47_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_47_retrieved_reference_45_false_2_is_inverse, Node_47_ref_node_target_id_45_2);
*Node_47_retrieved_reference_45_2_target_id = 14476;
ishidaopcua_node_set_target_id(Node_47_retrieved_reference_45_2_target_id, Node_47_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_47_retrieved_reference_45_2->references,"3",Node_47_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_47_retrieved_reference_45_3;
ishidaopcua_NODE* Node_47_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_47_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_47_retrieved_reference_45_3);
*Node_47_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_47_retrieved_reference_45_3_typed_id,Node_47_ref_node_target_id_45_3);
*Node_47_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_47_retrieved_reference_45_false_3_is_inverse, Node_47_ref_node_target_id_45_3);
*Node_47_retrieved_reference_45_3_target_id = 15296;
ishidaopcua_node_set_target_id(Node_47_retrieved_reference_45_3_target_id, Node_47_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_47_retrieved_reference_45_3->references,"4",Node_47_ref_node_target_id_45_3);

}if(references_flag != 2){

ishidaopcua_NODE* Node_47_retrieved_reference_45_4;
ishidaopcua_NODE* Node_47_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_47_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_47_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_47_retrieved_reference_45_4);
*Node_47_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_47_retrieved_reference_45_4_typed_id,Node_47_ref_node_target_id_45_4);
*Node_47_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_47_retrieved_reference_45_false_4_is_inverse, Node_47_ref_node_target_id_45_4);
*Node_47_retrieved_reference_45_4_target_id = 15297;
ishidaopcua_node_set_target_id(Node_47_retrieved_reference_45_4_target_id, Node_47_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_47_retrieved_reference_45_4->references,"5",Node_47_ref_node_target_id_45_4);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasOrderedComponent ********/


case 49 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 49;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasOrderedComponent", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasOrderedComponent", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for non-looping hierarchical reference from a node to its component when the order of references matters.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("OrderedComponentOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 49 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_49_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_49_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_49_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_49_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_49_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_49_retrieved_reference_45_inverse_0);
*Node_49_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_49_retrieved_reference_45_inverse_0_typed_id,Node_49_ref_node_target_id_45_inverse_0);
*Node_49_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_49_retrieved_reference_45_inverse_true_0_is_inverse, Node_49_ref_node_target_id_45_inverse_0);
*Node_49_retrieved_reference_45_inverse_0_target_id = 47;
ishidaopcua_node_set_target_id(Node_49_retrieved_reference_45_inverse_0_target_id, Node_49_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_49_retrieved_reference_45_inverse_0->references,"1",Node_49_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerDiagnosticsSummaryDataType ********/


case 859 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 859;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerDiagnosticsSummaryDataType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerDiagnosticsSummaryDataType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 859 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_859_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_859_retrieved_reference_45_inverse_0);
*Node_859_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_45_inverse_0_typed_id,Node_859_ref_node_target_id_45_inverse_0);
*Node_859_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_45_inverse_true_0_is_inverse, Node_859_ref_node_target_id_45_inverse_0);
*Node_859_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_45_inverse_0_target_id, Node_859_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_45_inverse_0->references,"1",Node_859_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_38_0;
ishidaopcua_NODE* Node_859_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_859_retrieved_reference_38_0);
*Node_859_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_38_0_typed_id,Node_859_ref_node_target_id_38_0);
*Node_859_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_38_false_0_is_inverse, Node_859_ref_node_target_id_38_0);
*Node_859_retrieved_reference_38_0_target_id = 861;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_38_0_target_id, Node_859_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_38_0->references,"1",Node_859_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_38_1;
ishidaopcua_NODE* Node_859_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_859_retrieved_reference_38_1);
*Node_859_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_38_1_typed_id,Node_859_ref_node_target_id_38_1);
*Node_859_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_38_false_1_is_inverse, Node_859_ref_node_target_id_38_1);
*Node_859_retrieved_reference_38_1_target_id = 860;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_38_1_target_id, Node_859_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_38_1->references,"2",Node_859_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_38_2;
ishidaopcua_NODE* Node_859_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_859_retrieved_reference_38_2);
*Node_859_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_38_2_typed_id,Node_859_ref_node_target_id_38_2);
*Node_859_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_38_false_2_is_inverse, Node_859_ref_node_target_id_38_2);
*Node_859_retrieved_reference_38_2_target_id = 15366;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_38_2_target_id, Node_859_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_38_2->references,"3",Node_859_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerStatusDataType ********/


case 862 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 862;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerStatusDataType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerStatusDataType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 862 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_862_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_862_retrieved_reference_45_inverse_0);
*Node_862_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_45_inverse_0_typed_id,Node_862_ref_node_target_id_45_inverse_0);
*Node_862_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_45_inverse_true_0_is_inverse, Node_862_ref_node_target_id_45_inverse_0);
*Node_862_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_45_inverse_0_target_id, Node_862_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_45_inverse_0->references,"1",Node_862_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_38_0;
ishidaopcua_NODE* Node_862_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_862_retrieved_reference_38_0);
*Node_862_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_38_0_typed_id,Node_862_ref_node_target_id_38_0);
*Node_862_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_38_false_0_is_inverse, Node_862_ref_node_target_id_38_0);
*Node_862_retrieved_reference_38_0_target_id = 864;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_38_0_target_id, Node_862_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_38_0->references,"1",Node_862_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_38_1;
ishidaopcua_NODE* Node_862_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_862_retrieved_reference_38_1);
*Node_862_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_38_1_typed_id,Node_862_ref_node_target_id_38_1);
*Node_862_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_38_false_1_is_inverse, Node_862_ref_node_target_id_38_1);
*Node_862_retrieved_reference_38_1_target_id = 863;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_38_1_target_id, Node_862_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_38_1->references,"2",Node_862_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_862_retrieved_reference_38_2;
ishidaopcua_NODE* Node_862_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_862_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_862_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_862_retrieved_reference_38_2);
*Node_862_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_862_retrieved_reference_38_2_typed_id,Node_862_ref_node_target_id_38_2);
*Node_862_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_862_retrieved_reference_38_false_2_is_inverse, Node_862_ref_node_target_id_38_2);
*Node_862_retrieved_reference_38_2_target_id = 15367;
ishidaopcua_node_set_target_id(Node_862_retrieved_reference_38_2_target_id, Node_862_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_862_retrieved_reference_38_2->references,"3",Node_862_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* EnumValueType ********/


case 7594 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7594;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("EnumValueType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("EnumValueType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A mapping between a value of an enumerated type and a name and description.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 7594 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_7594_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_7594_retrieved_reference_45_inverse_0);
*Node_7594_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_45_inverse_0_typed_id,Node_7594_ref_node_target_id_45_inverse_0);
*Node_7594_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_45_inverse_true_0_is_inverse, Node_7594_ref_node_target_id_45_inverse_0);
*Node_7594_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_45_inverse_0_target_id, Node_7594_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_45_inverse_0->references,"1",Node_7594_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_45_0;
ishidaopcua_NODE* Node_7594_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7594_retrieved_reference_45_0);
*Node_7594_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_45_0_typed_id,Node_7594_ref_node_target_id_45_0);
*Node_7594_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_45_false_0_is_inverse, Node_7594_ref_node_target_id_45_0);
*Node_7594_retrieved_reference_45_0_target_id = 102;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_45_0_target_id, Node_7594_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_45_0->references,"1",Node_7594_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_38_0;
ishidaopcua_NODE* Node_7594_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_7594_retrieved_reference_38_0);
*Node_7594_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_38_0_typed_id,Node_7594_ref_node_target_id_38_0);
*Node_7594_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_38_false_0_is_inverse, Node_7594_ref_node_target_id_38_0);
*Node_7594_retrieved_reference_38_0_target_id = 8251;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_38_0_target_id, Node_7594_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_38_0->references,"1",Node_7594_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_38_1;
ishidaopcua_NODE* Node_7594_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_7594_retrieved_reference_38_1);
*Node_7594_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_38_1_typed_id,Node_7594_ref_node_target_id_38_1);
*Node_7594_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_38_false_1_is_inverse, Node_7594_ref_node_target_id_38_1);
*Node_7594_retrieved_reference_38_1_target_id = 7616;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_38_1_target_id, Node_7594_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_38_1->references,"2",Node_7594_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_38_2;
ishidaopcua_NODE* Node_7594_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_7594_retrieved_reference_38_2);
*Node_7594_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_38_2_typed_id,Node_7594_ref_node_target_id_38_2);
*Node_7594_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_38_false_2_is_inverse, Node_7594_ref_node_target_id_38_2);
*Node_7594_retrieved_reference_38_2_target_id = 15082;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_38_2_target_id, Node_7594_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_38_2->references,"3",Node_7594_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/