#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_15(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* Guid ********/


case 14 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 14;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Guid", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Guid", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a 128-bit globally unique identifier.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 14 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_14_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_14_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_14_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_14_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_14_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_14_retrieved_reference_45_inverse_0);
*Node_14_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_14_retrieved_reference_45_inverse_0_typed_id,Node_14_ref_node_target_id_45_inverse_0);
*Node_14_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_14_retrieved_reference_45_inverse_true_0_is_inverse, Node_14_ref_node_target_id_45_inverse_0);
*Node_14_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_14_retrieved_reference_45_inverse_0_target_id, Node_14_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_14_retrieved_reference_45_inverse_0->references,"1",Node_14_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* NodeId ********/


case 17 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 17;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("NodeId", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("NodeId", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an identifier for a node within a Server address space.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 17 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_17_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_17_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_17_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_17_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_17_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_17_retrieved_reference_45_inverse_0);
*Node_17_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_17_retrieved_reference_45_inverse_0_typed_id,Node_17_ref_node_target_id_45_inverse_0);
*Node_17_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_17_retrieved_reference_45_inverse_true_0_is_inverse, Node_17_ref_node_target_id_45_inverse_0);
*Node_17_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_17_retrieved_reference_45_inverse_0_target_id, Node_17_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_17_retrieved_reference_45_inverse_0->references,"1",Node_17_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_17_retrieved_reference_45_0;
ishidaopcua_NODE* Node_17_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_17_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_17_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_17_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_17_retrieved_reference_45_0);
*Node_17_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_17_retrieved_reference_45_0_typed_id,Node_17_ref_node_target_id_45_0);
*Node_17_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_17_retrieved_reference_45_false_0_is_inverse, Node_17_ref_node_target_id_45_0);
*Node_17_retrieved_reference_45_0_target_id = 388;
ishidaopcua_node_set_target_id(Node_17_retrieved_reference_45_0_target_id, Node_17_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_17_retrieved_reference_45_0->references,"1",Node_17_ref_node_target_id_45_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ExpandedNodeId ********/


case 18 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 18;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ExpandedNodeId", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ExpandedNodeId", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an absolute identifier for a node.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 18 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_18_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_18_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_18_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_18_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_18_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_18_retrieved_reference_45_inverse_0);
*Node_18_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_18_retrieved_reference_45_inverse_0_typed_id,Node_18_ref_node_target_id_45_inverse_0);
*Node_18_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_18_retrieved_reference_45_inverse_true_0_is_inverse, Node_18_ref_node_target_id_45_inverse_0);
*Node_18_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_18_retrieved_reference_45_inverse_0_target_id, Node_18_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_18_retrieved_reference_45_inverse_0->references,"1",Node_18_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* LocalizedText ********/


case 21 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 21;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("LocalizedText", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("LocalizedText", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is human readable Unicode text with a locale identifier.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 21 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_21_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_21_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_21_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_21_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_21_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_21_retrieved_reference_45_inverse_0);
*Node_21_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_21_retrieved_reference_45_inverse_0_typed_id,Node_21_ref_node_target_id_45_inverse_0);
*Node_21_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_21_retrieved_reference_45_inverse_true_0_is_inverse, Node_21_ref_node_target_id_45_inverse_0);
*Node_21_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_21_retrieved_reference_45_inverse_0_target_id, Node_21_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_21_retrieved_reference_45_inverse_0->references,"1",Node_21_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Number ********/


case 26 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 26;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Number", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Number", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that can have any numeric DataType.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 26 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_26_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_26_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_26_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_26_retrieved_reference_45_inverse_0);
*Node_26_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_26_retrieved_reference_45_inverse_0_typed_id,Node_26_ref_node_target_id_45_inverse_0);
*Node_26_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_26_retrieved_reference_45_inverse_true_0_is_inverse, Node_26_ref_node_target_id_45_inverse_0);
*Node_26_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_26_retrieved_reference_45_inverse_0_target_id, Node_26_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_26_retrieved_reference_45_inverse_0->references,"1",Node_26_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_26_retrieved_reference_45_0;
ishidaopcua_NODE* Node_26_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_26_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_26_retrieved_reference_45_0);
*Node_26_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_26_retrieved_reference_45_0_typed_id,Node_26_ref_node_target_id_45_0);
*Node_26_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_26_retrieved_reference_45_false_0_is_inverse, Node_26_ref_node_target_id_45_0);
*Node_26_retrieved_reference_45_0_target_id = 27;
ishidaopcua_node_set_target_id(Node_26_retrieved_reference_45_0_target_id, Node_26_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_26_retrieved_reference_45_0->references,"1",Node_26_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_26_retrieved_reference_45_1;
ishidaopcua_NODE* Node_26_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_26_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_26_retrieved_reference_45_1);
*Node_26_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_26_retrieved_reference_45_1_typed_id,Node_26_ref_node_target_id_45_1);
*Node_26_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_26_retrieved_reference_45_false_1_is_inverse, Node_26_ref_node_target_id_45_1);
*Node_26_retrieved_reference_45_1_target_id = 28;
ishidaopcua_node_set_target_id(Node_26_retrieved_reference_45_1_target_id, Node_26_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_26_retrieved_reference_45_1->references,"2",Node_26_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_26_retrieved_reference_45_2;
ishidaopcua_NODE* Node_26_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_26_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_26_retrieved_reference_45_2);
*Node_26_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_26_retrieved_reference_45_2_typed_id,Node_26_ref_node_target_id_45_2);
*Node_26_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_26_retrieved_reference_45_false_2_is_inverse, Node_26_ref_node_target_id_45_2);
*Node_26_retrieved_reference_45_2_target_id = 10;
ishidaopcua_node_set_target_id(Node_26_retrieved_reference_45_2_target_id, Node_26_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_26_retrieved_reference_45_2->references,"3",Node_26_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_26_retrieved_reference_45_3;
ishidaopcua_NODE* Node_26_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_26_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_26_retrieved_reference_45_3);
*Node_26_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_26_retrieved_reference_45_3_typed_id,Node_26_ref_node_target_id_45_3);
*Node_26_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_26_retrieved_reference_45_false_3_is_inverse, Node_26_ref_node_target_id_45_3);
*Node_26_retrieved_reference_45_3_target_id = 11;
ishidaopcua_node_set_target_id(Node_26_retrieved_reference_45_3_target_id, Node_26_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_26_retrieved_reference_45_3->references,"4",Node_26_ref_node_target_id_45_3);

}if(references_flag != 2){

ishidaopcua_NODE* Node_26_retrieved_reference_45_4;
ishidaopcua_NODE* Node_26_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_26_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_26_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_26_retrieved_reference_45_4);
*Node_26_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_26_retrieved_reference_45_4_typed_id,Node_26_ref_node_target_id_45_4);
*Node_26_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_26_retrieved_reference_45_false_4_is_inverse, Node_26_ref_node_target_id_45_4);
*Node_26_retrieved_reference_45_4_target_id = 50;
ishidaopcua_node_set_target_id(Node_26_retrieved_reference_45_4_target_id, Node_26_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_26_retrieved_reference_45_4->references,"5",Node_26_ref_node_target_id_45_4);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Decimal ********/


case 50 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 50;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Decimal", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Decimal", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes an arbitrary precision decimal value.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 50 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_50_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_50_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_50_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_50_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_50_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_50_retrieved_reference_45_inverse_0);
*Node_50_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_50_retrieved_reference_45_inverse_0_typed_id,Node_50_ref_node_target_id_45_inverse_0);
*Node_50_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_50_retrieved_reference_45_inverse_true_0_is_inverse, Node_50_ref_node_target_id_45_inverse_0);
*Node_50_retrieved_reference_45_inverse_0_target_id = 26;
ishidaopcua_node_set_target_id(Node_50_retrieved_reference_45_inverse_0_target_id, Node_50_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_50_retrieved_reference_45_inverse_0->references,"1",Node_50_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DefaultBinary ********/


case 121 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 121;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DefaultBinary", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Default Binary", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 121 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_121_retrieved_reference_40_0;
ishidaopcua_NODE* Node_121_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_121_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_121_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_121_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_121_retrieved_reference_40_0);
*Node_121_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_121_retrieved_reference_40_0_typed_id,Node_121_ref_node_target_id_40_0);
*Node_121_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_121_retrieved_reference_40_false_0_is_inverse, Node_121_ref_node_target_id_40_0);
*Node_121_retrieved_reference_40_0_target_id = 76;
ishidaopcua_node_set_target_id(Node_121_retrieved_reference_40_0_target_id, Node_121_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_121_retrieved_reference_40_0->references,"1",Node_121_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_121_retrieved_reference_38_inverse_0;
ishidaopcua_NODE* Node_121_ref_node_target_id_38_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_121_retrieved_reference_38_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_121_retrieved_reference_38_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_121_retrieved_reference_38_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38_inverse",&Node_121_retrieved_reference_38_inverse_0);
*Node_121_retrieved_reference_38_inverse_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_121_retrieved_reference_38_inverse_0_typed_id,Node_121_ref_node_target_id_38_inverse_0);
*Node_121_retrieved_reference_38_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_121_retrieved_reference_38_inverse_true_0_is_inverse, Node_121_ref_node_target_id_38_inverse_0);
*Node_121_retrieved_reference_38_inverse_0_target_id = 97;
ishidaopcua_node_set_target_id(Node_121_retrieved_reference_38_inverse_0_target_id, Node_121_ref_node_target_id_38_inverse_0);
ishidaeutz_put_hashmap(Node_121_retrieved_reference_38_inverse_0->references,"1",Node_121_ref_node_target_id_38_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_121_retrieved_reference_39_0;
ishidaopcua_NODE* Node_121_ref_node_target_id_39_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_121_retrieved_reference_39_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_121_retrieved_reference_39_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_121_retrieved_reference_39_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"39",&Node_121_retrieved_reference_39_0);
*Node_121_retrieved_reference_39_0_typed_id =39;
ishidaopcua_node_set_reference_type_id(Node_121_retrieved_reference_39_0_typed_id,Node_121_ref_node_target_id_39_0);
*Node_121_retrieved_reference_39_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_121_retrieved_reference_39_false_0_is_inverse, Node_121_ref_node_target_id_39_0);
*Node_121_retrieved_reference_39_0_target_id = 18178;
ishidaopcua_node_set_target_id(Node_121_retrieved_reference_39_0_target_id, Node_121_ref_node_target_id_39_0);
ishidaeutz_put_hashmap(Node_121_retrieved_reference_39_0->references,"1",Node_121_ref_node_target_id_39_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* RedundancySupport ********/


case 851 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 851;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("RedundancySupport", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("RedundancySupport", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 851 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_851_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_851_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_851_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_851_retrieved_reference_45_inverse_0);
*Node_851_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_851_retrieved_reference_45_inverse_0_typed_id,Node_851_ref_node_target_id_45_inverse_0);
*Node_851_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_851_retrieved_reference_45_inverse_true_0_is_inverse, Node_851_ref_node_target_id_45_inverse_0);
*Node_851_retrieved_reference_45_inverse_0_target_id = 29;
ishidaopcua_node_set_target_id(Node_851_retrieved_reference_45_inverse_0_target_id, Node_851_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_851_retrieved_reference_45_inverse_0->references,"1",Node_851_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_851_retrieved_reference_46_0;
ishidaopcua_NODE* Node_851_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_851_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_851_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_851_retrieved_reference_46_0);
*Node_851_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_851_retrieved_reference_46_0_typed_id,Node_851_ref_node_target_id_46_0);
*Node_851_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_851_retrieved_reference_46_false_0_is_inverse, Node_851_ref_node_target_id_46_0);
*Node_851_retrieved_reference_46_0_target_id = 7611;
ishidaopcua_node_set_target_id(Node_851_retrieved_reference_46_0_target_id, Node_851_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_851_retrieved_reference_46_0->references,"1",Node_851_ref_node_target_id_46_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerState ********/


case 852 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 852;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerState", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerState", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 852 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_852_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_852_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_852_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_852_retrieved_reference_45_inverse_0);
*Node_852_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_852_retrieved_reference_45_inverse_0_typed_id,Node_852_ref_node_target_id_45_inverse_0);
*Node_852_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_852_retrieved_reference_45_inverse_true_0_is_inverse, Node_852_ref_node_target_id_45_inverse_0);
*Node_852_retrieved_reference_45_inverse_0_target_id = 29;
ishidaopcua_node_set_target_id(Node_852_retrieved_reference_45_inverse_0_target_id, Node_852_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_852_retrieved_reference_45_inverse_0->references,"1",Node_852_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_852_retrieved_reference_46_0;
ishidaopcua_NODE* Node_852_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_852_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_852_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_852_retrieved_reference_46_0);
*Node_852_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_852_retrieved_reference_46_0_typed_id,Node_852_ref_node_target_id_46_0);
*Node_852_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_852_retrieved_reference_46_false_0_is_inverse, Node_852_ref_node_target_id_46_0);
*Node_852_retrieved_reference_46_0_target_id = 7612;
ishidaopcua_node_set_target_id(Node_852_retrieved_reference_46_0_target_id, Node_852_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_852_retrieved_reference_46_0->references,"1",Node_852_ref_node_target_id_46_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* EnumStrings ********/


case 7611 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7611;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("EnumStrings", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("EnumStrings", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 21;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = 1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 7611 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_7611_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_7611_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7611_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_7611_retrieved_reference_46_inverse_0);
*Node_7611_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_7611_retrieved_reference_46_inverse_0_typed_id,Node_7611_ref_node_target_id_46_inverse_0);
*Node_7611_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7611_retrieved_reference_46_inverse_true_0_is_inverse, Node_7611_ref_node_target_id_46_inverse_0);
*Node_7611_retrieved_reference_46_inverse_0_target_id = 851;
ishidaopcua_node_set_target_id(Node_7611_retrieved_reference_46_inverse_0_target_id, Node_7611_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_7611_retrieved_reference_46_inverse_0->references,"1",Node_7611_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_7611_retrieved_reference_40_0;
ishidaopcua_NODE* Node_7611_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7611_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_7611_retrieved_reference_40_0);
*Node_7611_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_7611_retrieved_reference_40_0_typed_id,Node_7611_ref_node_target_id_40_0);
*Node_7611_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7611_retrieved_reference_40_false_0_is_inverse, Node_7611_ref_node_target_id_40_0);
*Node_7611_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_7611_retrieved_reference_40_0_target_id, Node_7611_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_7611_retrieved_reference_40_0->references,"1",Node_7611_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7611_retrieved_reference_37_0;
ishidaopcua_NODE* Node_7611_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7611_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_7611_retrieved_reference_37_0);
*Node_7611_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_7611_retrieved_reference_37_0_typed_id,Node_7611_ref_node_target_id_37_0);
*Node_7611_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7611_retrieved_reference_37_false_0_is_inverse, Node_7611_ref_node_target_id_37_0);
*Node_7611_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_7611_retrieved_reference_37_0_target_id, Node_7611_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_7611_retrieved_reference_37_0->references,"1",Node_7611_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/
