#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_9(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* ServerDiagnosticsSummaryDataType ********/


case 859 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 859;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerDiagnosticsSummaryDataType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerDiagnosticsSummaryDataType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 859 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_859_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_859_retrieved_reference_45_inverse_0);
*Node_859_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_45_inverse_0_typed_id,Node_859_ref_node_target_id_45_inverse_0);
*Node_859_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_45_inverse_true_0_is_inverse, Node_859_ref_node_target_id_45_inverse_0);
*Node_859_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_45_inverse_0_target_id, Node_859_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_45_inverse_0->references,"1",Node_859_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_38_0;
ishidaopcua_NODE* Node_859_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_859_retrieved_reference_38_0);
*Node_859_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_38_0_typed_id,Node_859_ref_node_target_id_38_0);
*Node_859_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_38_false_0_is_inverse, Node_859_ref_node_target_id_38_0);
*Node_859_retrieved_reference_38_0_target_id = 861;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_38_0_target_id, Node_859_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_38_0->references,"1",Node_859_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_38_1;
ishidaopcua_NODE* Node_859_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_859_retrieved_reference_38_1);
*Node_859_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_38_1_typed_id,Node_859_ref_node_target_id_38_1);
*Node_859_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_38_false_1_is_inverse, Node_859_ref_node_target_id_38_1);
*Node_859_retrieved_reference_38_1_target_id = 860;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_38_1_target_id, Node_859_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_38_1->references,"2",Node_859_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_859_retrieved_reference_38_2;
ishidaopcua_NODE* Node_859_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_859_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_859_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_859_retrieved_reference_38_2);
*Node_859_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_859_retrieved_reference_38_2_typed_id,Node_859_ref_node_target_id_38_2);
*Node_859_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_859_retrieved_reference_38_false_2_is_inverse, Node_859_ref_node_target_id_38_2);
*Node_859_retrieved_reference_38_2_target_id = 15366;
ishidaopcua_node_set_target_id(Node_859_retrieved_reference_38_2_target_id, Node_859_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_859_retrieved_reference_38_2->references,"3",Node_859_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerStatusType ********/


case 2138 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2138;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerStatusType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerStatusType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 862;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 2138 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2138_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_2138_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2138_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_2138_retrieved_reference_45_inverse_0);
*Node_2138_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2138_retrieved_reference_45_inverse_0_typed_id,Node_2138_ref_node_target_id_45_inverse_0);
*Node_2138_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2138_retrieved_reference_45_inverse_true_0_is_inverse, Node_2138_ref_node_target_id_45_inverse_0);
*Node_2138_retrieved_reference_45_inverse_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2138_retrieved_reference_45_inverse_0_target_id, Node_2138_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_2138_retrieved_reference_45_inverse_0->references,"1",Node_2138_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2138_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2138_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2138_retrieved_reference_47_0);
*Node_2138_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2138_retrieved_reference_47_0_typed_id,Node_2138_ref_node_target_id_47_0);
*Node_2138_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2138_retrieved_reference_47_false_0_is_inverse, Node_2138_ref_node_target_id_47_0);
*Node_2138_retrieved_reference_47_0_target_id = 2139;
ishidaopcua_node_set_target_id(Node_2138_retrieved_reference_47_0_target_id, Node_2138_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2138_retrieved_reference_47_0->references,"1",Node_2138_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2138_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2138_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2138_retrieved_reference_47_1);
*Node_2138_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2138_retrieved_reference_47_1_typed_id,Node_2138_ref_node_target_id_47_1);
*Node_2138_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2138_retrieved_reference_47_false_1_is_inverse, Node_2138_ref_node_target_id_47_1);
*Node_2138_retrieved_reference_47_1_target_id = 2140;
ishidaopcua_node_set_target_id(Node_2138_retrieved_reference_47_1_target_id, Node_2138_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2138_retrieved_reference_47_1->references,"2",Node_2138_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2138_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2138_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2138_retrieved_reference_47_2);
*Node_2138_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2138_retrieved_reference_47_2_typed_id,Node_2138_ref_node_target_id_47_2);
*Node_2138_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2138_retrieved_reference_47_false_2_is_inverse, Node_2138_ref_node_target_id_47_2);
*Node_2138_retrieved_reference_47_2_target_id = 2141;
ishidaopcua_node_set_target_id(Node_2138_retrieved_reference_47_2_target_id, Node_2138_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2138_retrieved_reference_47_2->references,"3",Node_2138_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2138_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2138_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2138_retrieved_reference_47_3);
*Node_2138_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2138_retrieved_reference_47_3_typed_id,Node_2138_ref_node_target_id_47_3);
*Node_2138_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2138_retrieved_reference_47_false_3_is_inverse, Node_2138_ref_node_target_id_47_3);
*Node_2138_retrieved_reference_47_3_target_id = 2142;
ishidaopcua_node_set_target_id(Node_2138_retrieved_reference_47_3_target_id, Node_2138_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2138_retrieved_reference_47_3->references,"4",Node_2138_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2138_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2138_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2138_retrieved_reference_47_4);
*Node_2138_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2138_retrieved_reference_47_4_typed_id,Node_2138_ref_node_target_id_47_4);
*Node_2138_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2138_retrieved_reference_47_false_4_is_inverse, Node_2138_ref_node_target_id_47_4);
*Node_2138_retrieved_reference_47_4_target_id = 2752;
ishidaopcua_node_set_target_id(Node_2138_retrieved_reference_47_4_target_id, Node_2138_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2138_retrieved_reference_47_4->references,"5",Node_2138_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2138_retrieved_reference_47_5;
ishidaopcua_NODE* Node_2138_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2138_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2138_retrieved_reference_47_5);
*Node_2138_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2138_retrieved_reference_47_5_typed_id,Node_2138_ref_node_target_id_47_5);
*Node_2138_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2138_retrieved_reference_47_false_5_is_inverse, Node_2138_ref_node_target_id_47_5);
*Node_2138_retrieved_reference_47_5_target_id = 2753;
ishidaopcua_node_set_target_id(Node_2138_retrieved_reference_47_5_target_id, Node_2138_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_2138_retrieved_reference_47_5->references,"6",Node_2138_ref_node_target_id_47_5); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerStatus ********/


case 2256 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2256;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerStatus", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerStatus", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The current status of the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 862;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 


universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(1000);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2256 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2256_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2256_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2256_retrieved_reference_47_inverse_0);
*Node_2256_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_47_inverse_0_typed_id,Node_2256_ref_node_target_id_47_inverse_0);
*Node_2256_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_47_inverse_true_0_is_inverse, Node_2256_ref_node_target_id_47_inverse_0);
*Node_2256_retrieved_reference_47_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_47_inverse_0_target_id, Node_2256_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_47_inverse_0->references,"1",Node_2256_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2256_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2256_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2256_retrieved_reference_40_0);
*Node_2256_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_40_0_typed_id,Node_2256_ref_node_target_id_40_0);
*Node_2256_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_40_false_0_is_inverse, Node_2256_ref_node_target_id_40_0);
*Node_2256_retrieved_reference_40_0_target_id = 2138;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_40_0_target_id, Node_2256_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_40_0->references,"1",Node_2256_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2256_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2256_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2256_retrieved_reference_47_0);
*Node_2256_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_47_0_typed_id,Node_2256_ref_node_target_id_47_0);
*Node_2256_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_47_false_0_is_inverse, Node_2256_ref_node_target_id_47_0);
*Node_2256_retrieved_reference_47_0_target_id = 2257;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_47_0_target_id, Node_2256_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_47_0->references,"1",Node_2256_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2256_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2256_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2256_retrieved_reference_47_1);
*Node_2256_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_47_1_typed_id,Node_2256_ref_node_target_id_47_1);
*Node_2256_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_47_false_1_is_inverse, Node_2256_ref_node_target_id_47_1);
*Node_2256_retrieved_reference_47_1_target_id = 2258;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_47_1_target_id, Node_2256_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_47_1->references,"2",Node_2256_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2256_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2256_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2256_retrieved_reference_47_2);
*Node_2256_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_47_2_typed_id,Node_2256_ref_node_target_id_47_2);
*Node_2256_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_47_false_2_is_inverse, Node_2256_ref_node_target_id_47_2);
*Node_2256_retrieved_reference_47_2_target_id = 2259;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_47_2_target_id, Node_2256_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_47_2->references,"3",Node_2256_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2256_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2256_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2256_retrieved_reference_47_3);
*Node_2256_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_47_3_typed_id,Node_2256_ref_node_target_id_47_3);
*Node_2256_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_47_false_3_is_inverse, Node_2256_ref_node_target_id_47_3);
*Node_2256_retrieved_reference_47_3_target_id = 2260;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_47_3_target_id, Node_2256_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_47_3->references,"4",Node_2256_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2256_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2256_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2256_retrieved_reference_47_4);
*Node_2256_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_47_4_typed_id,Node_2256_ref_node_target_id_47_4);
*Node_2256_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_47_false_4_is_inverse, Node_2256_ref_node_target_id_47_4);
*Node_2256_retrieved_reference_47_4_target_id = 2992;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_47_4_target_id, Node_2256_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_47_4->references,"5",Node_2256_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2256_retrieved_reference_47_5;
ishidaopcua_NODE* Node_2256_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2256_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2256_retrieved_reference_47_5);
*Node_2256_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2256_retrieved_reference_47_5_typed_id,Node_2256_ref_node_target_id_47_5);
*Node_2256_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2256_retrieved_reference_47_false_5_is_inverse, Node_2256_ref_node_target_id_47_5);
*Node_2256_retrieved_reference_47_5_target_id = 2993;
ishidaopcua_node_set_target_id(Node_2256_retrieved_reference_47_5_target_id, Node_2256_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_2256_retrieved_reference_47_5->references,"6",Node_2256_ref_node_target_id_47_5); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* StartTime ********/


case 2257 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2257;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("StartTime", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("StartTime", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 294;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2257 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2257_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2257_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2257_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2257_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2257_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2257_retrieved_reference_47_inverse_0);
*Node_2257_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2257_retrieved_reference_47_inverse_0_typed_id,Node_2257_ref_node_target_id_47_inverse_0);
*Node_2257_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2257_retrieved_reference_47_inverse_true_0_is_inverse, Node_2257_ref_node_target_id_47_inverse_0);
*Node_2257_retrieved_reference_47_inverse_0_target_id = 2256;
ishidaopcua_node_set_target_id(Node_2257_retrieved_reference_47_inverse_0_target_id, Node_2257_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2257_retrieved_reference_47_inverse_0->references,"1",Node_2257_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2257_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2257_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2257_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2257_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2257_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2257_retrieved_reference_40_0);
*Node_2257_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2257_retrieved_reference_40_0_typed_id,Node_2257_ref_node_target_id_40_0);
*Node_2257_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2257_retrieved_reference_40_false_0_is_inverse, Node_2257_ref_node_target_id_40_0);
*Node_2257_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2257_retrieved_reference_40_0_target_id, Node_2257_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2257_retrieved_reference_40_0->references,"1",Node_2257_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* CurrentTime ********/


case 2258 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2258;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("CurrentTime", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("CurrentTime", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 294;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2258 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2258_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2258_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2258_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2258_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2258_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2258_retrieved_reference_47_inverse_0);
*Node_2258_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2258_retrieved_reference_47_inverse_0_typed_id,Node_2258_ref_node_target_id_47_inverse_0);
*Node_2258_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2258_retrieved_reference_47_inverse_true_0_is_inverse, Node_2258_ref_node_target_id_47_inverse_0);
*Node_2258_retrieved_reference_47_inverse_0_target_id = 2256;
ishidaopcua_node_set_target_id(Node_2258_retrieved_reference_47_inverse_0_target_id, Node_2258_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2258_retrieved_reference_47_inverse_0->references,"1",Node_2258_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2258_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2258_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2258_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2258_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2258_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2258_retrieved_reference_40_0);
*Node_2258_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2258_retrieved_reference_40_0_typed_id,Node_2258_ref_node_target_id_40_0);
*Node_2258_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2258_retrieved_reference_40_false_0_is_inverse, Node_2258_ref_node_target_id_40_0);
*Node_2258_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2258_retrieved_reference_40_0_target_id, Node_2258_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2258_retrieved_reference_40_0->references,"1",Node_2258_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* State ********/


case 2259 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2259;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("State", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("State", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 852;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 6;
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2259 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2259_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2259_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2259_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2259_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2259_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2259_retrieved_reference_47_inverse_0);
*Node_2259_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2259_retrieved_reference_47_inverse_0_typed_id,Node_2259_ref_node_target_id_47_inverse_0);
*Node_2259_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2259_retrieved_reference_47_inverse_true_0_is_inverse, Node_2259_ref_node_target_id_47_inverse_0);
*Node_2259_retrieved_reference_47_inverse_0_target_id = 2256;
ishidaopcua_node_set_target_id(Node_2259_retrieved_reference_47_inverse_0_target_id, Node_2259_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2259_retrieved_reference_47_inverse_0->references,"1",Node_2259_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2259_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2259_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2259_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2259_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2259_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2259_retrieved_reference_40_0);
*Node_2259_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2259_retrieved_reference_40_0_typed_id,Node_2259_ref_node_target_id_40_0);
*Node_2259_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2259_retrieved_reference_40_false_0_is_inverse, Node_2259_ref_node_target_id_40_0);
*Node_2259_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2259_retrieved_reference_40_0_target_id, Node_2259_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2259_retrieved_reference_40_0->references,"1",Node_2259_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BuildInfo ********/


case 2260 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2260;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BuildInfo", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BuildInfo", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 338;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2260 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2260_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2260_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2260_retrieved_reference_47_inverse_0);
*Node_2260_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_47_inverse_0_typed_id,Node_2260_ref_node_target_id_47_inverse_0);
*Node_2260_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_47_inverse_true_0_is_inverse, Node_2260_ref_node_target_id_47_inverse_0);
*Node_2260_retrieved_reference_47_inverse_0_target_id = 2256;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_47_inverse_0_target_id, Node_2260_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_47_inverse_0->references,"1",Node_2260_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2260_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2260_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2260_retrieved_reference_40_0);
*Node_2260_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_40_0_typed_id,Node_2260_ref_node_target_id_40_0);
*Node_2260_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_40_false_0_is_inverse, Node_2260_ref_node_target_id_40_0);
*Node_2260_retrieved_reference_40_0_target_id = 3051;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_40_0_target_id, Node_2260_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_40_0->references,"1",Node_2260_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2260_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2260_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2260_retrieved_reference_47_0);
*Node_2260_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_47_0_typed_id,Node_2260_ref_node_target_id_47_0);
*Node_2260_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_47_false_0_is_inverse, Node_2260_ref_node_target_id_47_0);
*Node_2260_retrieved_reference_47_0_target_id = 2262;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_47_0_target_id, Node_2260_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_47_0->references,"1",Node_2260_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2260_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2260_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2260_retrieved_reference_47_1);
*Node_2260_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_47_1_typed_id,Node_2260_ref_node_target_id_47_1);
*Node_2260_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_47_false_1_is_inverse, Node_2260_ref_node_target_id_47_1);
*Node_2260_retrieved_reference_47_1_target_id = 2263;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_47_1_target_id, Node_2260_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_47_1->references,"2",Node_2260_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2260_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2260_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2260_retrieved_reference_47_2);
*Node_2260_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_47_2_typed_id,Node_2260_ref_node_target_id_47_2);
*Node_2260_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_47_false_2_is_inverse, Node_2260_ref_node_target_id_47_2);
*Node_2260_retrieved_reference_47_2_target_id = 2261;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_47_2_target_id, Node_2260_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_47_2->references,"3",Node_2260_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2260_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2260_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2260_retrieved_reference_47_3);
*Node_2260_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_47_3_typed_id,Node_2260_ref_node_target_id_47_3);
*Node_2260_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_47_false_3_is_inverse, Node_2260_ref_node_target_id_47_3);
*Node_2260_retrieved_reference_47_3_target_id = 2264;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_47_3_target_id, Node_2260_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_47_3->references,"4",Node_2260_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2260_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2260_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2260_retrieved_reference_47_4);
*Node_2260_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_47_4_typed_id,Node_2260_ref_node_target_id_47_4);
*Node_2260_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_47_false_4_is_inverse, Node_2260_ref_node_target_id_47_4);
*Node_2260_retrieved_reference_47_4_target_id = 2265;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_47_4_target_id, Node_2260_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_47_4->references,"5",Node_2260_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2260_retrieved_reference_47_5;
ishidaopcua_NODE* Node_2260_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2260_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2260_retrieved_reference_47_5);
*Node_2260_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2260_retrieved_reference_47_5_typed_id,Node_2260_ref_node_target_id_47_5);
*Node_2260_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2260_retrieved_reference_47_false_5_is_inverse, Node_2260_ref_node_target_id_47_5);
*Node_2260_retrieved_reference_47_5_target_id = 2266;
ishidaopcua_node_set_target_id(Node_2260_retrieved_reference_47_5_target_id, Node_2260_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_2260_retrieved_reference_47_5->references,"6",Node_2260_ref_node_target_id_47_5); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SoftwareVersion ********/


case 2264 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2264;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SoftwareVersion", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SoftwareVersion", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 


universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(1000);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2264 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2264_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2264_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2264_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2264_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2264_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2264_retrieved_reference_47_inverse_0);
*Node_2264_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2264_retrieved_reference_47_inverse_0_typed_id,Node_2264_ref_node_target_id_47_inverse_0);
*Node_2264_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2264_retrieved_reference_47_inverse_true_0_is_inverse, Node_2264_ref_node_target_id_47_inverse_0);
*Node_2264_retrieved_reference_47_inverse_0_target_id = 2260;
ishidaopcua_node_set_target_id(Node_2264_retrieved_reference_47_inverse_0_target_id, Node_2264_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2264_retrieved_reference_47_inverse_0->references,"1",Node_2264_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2264_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2264_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2264_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2264_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2264_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2264_retrieved_reference_40_0);
*Node_2264_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2264_retrieved_reference_40_0_typed_id,Node_2264_ref_node_target_id_40_0);
*Node_2264_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2264_retrieved_reference_40_false_0_is_inverse, Node_2264_ref_node_target_id_40_0);
*Node_2264_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2264_retrieved_reference_40_0_target_id, Node_2264_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2264_retrieved_reference_40_0->references,"1",Node_2264_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BuildNumber ********/


case 2265 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2265;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BuildNumber", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BuildNumber", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 


universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(1000);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2265 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2265_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2265_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2265_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2265_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2265_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2265_retrieved_reference_47_inverse_0);
*Node_2265_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2265_retrieved_reference_47_inverse_0_typed_id,Node_2265_ref_node_target_id_47_inverse_0);
*Node_2265_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2265_retrieved_reference_47_inverse_true_0_is_inverse, Node_2265_ref_node_target_id_47_inverse_0);
*Node_2265_retrieved_reference_47_inverse_0_target_id = 2260;
ishidaopcua_node_set_target_id(Node_2265_retrieved_reference_47_inverse_0_target_id, Node_2265_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2265_retrieved_reference_47_inverse_0->references,"1",Node_2265_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2265_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2265_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2265_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2265_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2265_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2265_retrieved_reference_40_0);
*Node_2265_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2265_retrieved_reference_40_0_typed_id,Node_2265_ref_node_target_id_40_0);
*Node_2265_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2265_retrieved_reference_40_false_0_is_inverse, Node_2265_ref_node_target_id_40_0);
*Node_2265_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2265_retrieved_reference_40_0_target_id, Node_2265_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2265_retrieved_reference_40_0->references,"1",Node_2265_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BuildDate ********/


case 2266 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2266;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BuildDate", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BuildDate", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 294;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 


universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(1000);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2266 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2266_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2266_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2266_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2266_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2266_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2266_retrieved_reference_47_inverse_0);
*Node_2266_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2266_retrieved_reference_47_inverse_0_typed_id,Node_2266_ref_node_target_id_47_inverse_0);
*Node_2266_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2266_retrieved_reference_47_inverse_true_0_is_inverse, Node_2266_ref_node_target_id_47_inverse_0);
*Node_2266_retrieved_reference_47_inverse_0_target_id = 2260;
ishidaopcua_node_set_target_id(Node_2266_retrieved_reference_47_inverse_0_target_id, Node_2266_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2266_retrieved_reference_47_inverse_0->references,"1",Node_2266_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2266_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2266_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2266_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2266_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2266_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2266_retrieved_reference_40_0);
*Node_2266_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2266_retrieved_reference_40_0_typed_id,Node_2266_ref_node_target_id_40_0);
*Node_2266_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2266_retrieved_reference_40_false_0_is_inverse, Node_2266_ref_node_target_id_40_0);
*Node_2266_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2266_retrieved_reference_40_0_target_id, Node_2266_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2266_retrieved_reference_40_0->references,"1",Node_2266_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/
