#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_8(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* ServiceLevel ********/


case 2267 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2267;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServiceLevel", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServiceLevel", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A value indicating the level of service the server can provide. 255 indicates the best.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 3;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 


universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(1000);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2267 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2267_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2267_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2267_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2267_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2267_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2267_retrieved_reference_46_inverse_0);
*Node_2267_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2267_retrieved_reference_46_inverse_0_typed_id,Node_2267_ref_node_target_id_46_inverse_0);
*Node_2267_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2267_retrieved_reference_46_inverse_true_0_is_inverse, Node_2267_ref_node_target_id_46_inverse_0);
*Node_2267_retrieved_reference_46_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_2267_retrieved_reference_46_inverse_0_target_id, Node_2267_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2267_retrieved_reference_46_inverse_0->references,"1",Node_2267_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2267_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2267_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2267_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2267_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2267_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2267_retrieved_reference_40_0);
*Node_2267_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2267_retrieved_reference_40_0_typed_id,Node_2267_ref_node_target_id_40_0);
*Node_2267_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2267_retrieved_reference_40_false_0_is_inverse, Node_2267_ref_node_target_id_40_0);
*Node_2267_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2267_retrieved_reference_40_0_target_id, Node_2267_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2267_retrieved_reference_40_0->references,"1",Node_2267_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerProfileArray ********/


case 2269 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2269;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerProfileArray", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerProfileArray", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A list of profiles supported by the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = 1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2269 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2269_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2269_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2269_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2269_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2269_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2269_retrieved_reference_46_inverse_0);
*Node_2269_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2269_retrieved_reference_46_inverse_0_typed_id,Node_2269_ref_node_target_id_46_inverse_0);
*Node_2269_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2269_retrieved_reference_46_inverse_true_0_is_inverse, Node_2269_ref_node_target_id_46_inverse_0);
*Node_2269_retrieved_reference_46_inverse_0_target_id = 2268;
ishidaopcua_node_set_target_id(Node_2269_retrieved_reference_46_inverse_0_target_id, Node_2269_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2269_retrieved_reference_46_inverse_0->references,"1",Node_2269_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2269_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2269_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2269_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2269_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2269_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2269_retrieved_reference_40_0);
*Node_2269_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2269_retrieved_reference_40_0_typed_id,Node_2269_ref_node_target_id_40_0);
*Node_2269_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2269_retrieved_reference_40_false_0_is_inverse, Node_2269_ref_node_target_id_40_0);
*Node_2269_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2269_retrieved_reference_40_0_target_id, Node_2269_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2269_retrieved_reference_40_0->references,"1",Node_2269_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxQueryContinuationPoints ********/


case 2736 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2736;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxQueryContinuationPoints", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxQueryContinuationPoints", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of continuation points for Query operations per session.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 5;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2736 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2736_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2736_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2736_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2736_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2736_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2736_retrieved_reference_46_inverse_0);
*Node_2736_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2736_retrieved_reference_46_inverse_0_typed_id,Node_2736_ref_node_target_id_46_inverse_0);
*Node_2736_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2736_retrieved_reference_46_inverse_true_0_is_inverse, Node_2736_ref_node_target_id_46_inverse_0);
*Node_2736_retrieved_reference_46_inverse_0_target_id = 2268;
ishidaopcua_node_set_target_id(Node_2736_retrieved_reference_46_inverse_0_target_id, Node_2736_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2736_retrieved_reference_46_inverse_0->references,"1",Node_2736_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2736_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2736_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2736_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2736_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2736_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2736_retrieved_reference_40_0);
*Node_2736_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2736_retrieved_reference_40_0_typed_id,Node_2736_ref_node_target_id_40_0);
*Node_2736_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2736_retrieved_reference_40_false_0_is_inverse, Node_2736_ref_node_target_id_40_0);
*Node_2736_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2736_retrieved_reference_40_0_target_id, Node_2736_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2736_retrieved_reference_40_0->references,"1",Node_2736_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxHistoryContinuationPoints ********/


case 2737 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2737;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxHistoryContinuationPoints", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxHistoryContinuationPoints", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of continuation points for ReadHistory operations per session.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 5;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2737 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2737_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2737_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2737_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2737_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2737_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2737_retrieved_reference_46_inverse_0);
*Node_2737_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2737_retrieved_reference_46_inverse_0_typed_id,Node_2737_ref_node_target_id_46_inverse_0);
*Node_2737_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2737_retrieved_reference_46_inverse_true_0_is_inverse, Node_2737_ref_node_target_id_46_inverse_0);
*Node_2737_retrieved_reference_46_inverse_0_target_id = 2268;
ishidaopcua_node_set_target_id(Node_2737_retrieved_reference_46_inverse_0_target_id, Node_2737_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2737_retrieved_reference_46_inverse_0->references,"1",Node_2737_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2737_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2737_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2737_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2737_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2737_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2737_retrieved_reference_40_0);
*Node_2737_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2737_retrieved_reference_40_0_typed_id,Node_2737_ref_node_target_id_40_0);
*Node_2737_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2737_retrieved_reference_40_false_0_is_inverse, Node_2737_ref_node_target_id_40_0);
*Node_2737_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2737_retrieved_reference_40_0_target_id, Node_2737_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2737_retrieved_reference_40_0->references,"1",Node_2737_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* RedundancySupport ********/


case 3709 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 3709;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("RedundancySupport", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("RedundancySupport", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Indicates what style of redundancy is supported by the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 851;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 3709 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_3709_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_3709_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3709_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3709_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3709_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_3709_retrieved_reference_46_inverse_0);
*Node_3709_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_3709_retrieved_reference_46_inverse_0_typed_id,Node_3709_ref_node_target_id_46_inverse_0);
*Node_3709_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_3709_retrieved_reference_46_inverse_true_0_is_inverse, Node_3709_ref_node_target_id_46_inverse_0);
*Node_3709_retrieved_reference_46_inverse_0_target_id = 2296;
ishidaopcua_node_set_target_id(Node_3709_retrieved_reference_46_inverse_0_target_id, Node_3709_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_3709_retrieved_reference_46_inverse_0->references,"1",Node_3709_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_3709_retrieved_reference_40_0;
ishidaopcua_NODE* Node_3709_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3709_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3709_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3709_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_3709_retrieved_reference_40_0);
*Node_3709_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_3709_retrieved_reference_40_0_typed_id,Node_3709_ref_node_target_id_40_0);
*Node_3709_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3709_retrieved_reference_40_false_0_is_inverse, Node_3709_ref_node_target_id_40_0);
*Node_3709_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_3709_retrieved_reference_40_0_target_id, Node_3709_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_3709_retrieved_reference_40_0->references,"1",Node_3709_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* EnumStrings ********/


case 7611 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7611;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("EnumStrings", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("EnumStrings", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 21;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = 1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 7611 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_7611_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_7611_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7611_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_7611_retrieved_reference_46_inverse_0);
*Node_7611_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_7611_retrieved_reference_46_inverse_0_typed_id,Node_7611_ref_node_target_id_46_inverse_0);
*Node_7611_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7611_retrieved_reference_46_inverse_true_0_is_inverse, Node_7611_ref_node_target_id_46_inverse_0);
*Node_7611_retrieved_reference_46_inverse_0_target_id = 851;
ishidaopcua_node_set_target_id(Node_7611_retrieved_reference_46_inverse_0_target_id, Node_7611_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_7611_retrieved_reference_46_inverse_0->references,"1",Node_7611_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_7611_retrieved_reference_40_0;
ishidaopcua_NODE* Node_7611_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7611_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_7611_retrieved_reference_40_0);
*Node_7611_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_7611_retrieved_reference_40_0_typed_id,Node_7611_ref_node_target_id_40_0);
*Node_7611_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7611_retrieved_reference_40_false_0_is_inverse, Node_7611_ref_node_target_id_40_0);
*Node_7611_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_7611_retrieved_reference_40_0_target_id, Node_7611_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_7611_retrieved_reference_40_0->references,"1",Node_7611_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_7611_retrieved_reference_37_0;
ishidaopcua_NODE* Node_7611_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7611_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7611_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_7611_retrieved_reference_37_0);
*Node_7611_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_7611_retrieved_reference_37_0_typed_id,Node_7611_ref_node_target_id_37_0);
*Node_7611_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7611_retrieved_reference_37_false_0_is_inverse, Node_7611_ref_node_target_id_37_0);
*Node_7611_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_7611_retrieved_reference_37_0_target_id, Node_7611_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_7611_retrieved_reference_37_0->references,"1",Node_7611_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerRead ********/


case 11705 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11705;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerRead", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerRead", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single Read request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11705 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11705_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11705_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11705_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11705_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11705_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11705_retrieved_reference_46_inverse_0);
*Node_11705_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11705_retrieved_reference_46_inverse_0_typed_id,Node_11705_ref_node_target_id_46_inverse_0);
*Node_11705_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11705_retrieved_reference_46_inverse_true_0_is_inverse, Node_11705_ref_node_target_id_46_inverse_0);
*Node_11705_retrieved_reference_46_inverse_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_11705_retrieved_reference_46_inverse_0_target_id, Node_11705_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11705_retrieved_reference_46_inverse_0->references,"1",Node_11705_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11705_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11705_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11705_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11705_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11705_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11705_retrieved_reference_40_0);
*Node_11705_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11705_retrieved_reference_40_0_typed_id,Node_11705_ref_node_target_id_40_0);
*Node_11705_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11705_retrieved_reference_40_false_0_is_inverse, Node_11705_ref_node_target_id_40_0);
*Node_11705_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11705_retrieved_reference_40_0_target_id, Node_11705_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11705_retrieved_reference_40_0->references,"1",Node_11705_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerRegisterNodes ********/


case 11711 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11711;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerRegisterNodes", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerRegisterNodes", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single RegisterNodes request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11711 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11711_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11711_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11711_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11711_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11711_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11711_retrieved_reference_46_inverse_0);
*Node_11711_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11711_retrieved_reference_46_inverse_0_typed_id,Node_11711_ref_node_target_id_46_inverse_0);
*Node_11711_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11711_retrieved_reference_46_inverse_true_0_is_inverse, Node_11711_ref_node_target_id_46_inverse_0);
*Node_11711_retrieved_reference_46_inverse_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_11711_retrieved_reference_46_inverse_0_target_id, Node_11711_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11711_retrieved_reference_46_inverse_0->references,"1",Node_11711_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11711_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11711_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11711_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11711_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11711_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11711_retrieved_reference_40_0);
*Node_11711_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11711_retrieved_reference_40_0_typed_id,Node_11711_ref_node_target_id_40_0);
*Node_11711_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11711_retrieved_reference_40_false_0_is_inverse, Node_11711_ref_node_target_id_40_0);
*Node_11711_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11711_retrieved_reference_40_0_target_id, Node_11711_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11711_retrieved_reference_40_0->references,"1",Node_11711_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerNodeManagement ********/


case 11713 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11713;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerNodeManagement", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerNodeManagement", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single AddNodes, AddReferences, DeleteNodes or DeleteReferences request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11713 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11713_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11713_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11713_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11713_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11713_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11713_retrieved_reference_46_inverse_0);
*Node_11713_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11713_retrieved_reference_46_inverse_0_typed_id,Node_11713_ref_node_target_id_46_inverse_0);
*Node_11713_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11713_retrieved_reference_46_inverse_true_0_is_inverse, Node_11713_ref_node_target_id_46_inverse_0);
*Node_11713_retrieved_reference_46_inverse_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_11713_retrieved_reference_46_inverse_0_target_id, Node_11713_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11713_retrieved_reference_46_inverse_0->references,"1",Node_11713_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11713_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11713_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11713_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11713_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11713_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11713_retrieved_reference_40_0);
*Node_11713_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11713_retrieved_reference_40_0_typed_id,Node_11713_ref_node_target_id_40_0);
*Node_11713_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11713_retrieved_reference_40_false_0_is_inverse, Node_11713_ref_node_target_id_40_0);
*Node_11713_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11713_retrieved_reference_40_0_target_id, Node_11713_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11713_retrieved_reference_40_0->references,"1",Node_11713_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxMonitoredItemsPerCall ********/


case 11714 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11714;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxMonitoredItemsPerCall", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxMonitoredItemsPerCall", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single MonitoredItem related request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11714 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11714_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11714_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11714_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11714_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11714_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11714_retrieved_reference_46_inverse_0);
*Node_11714_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11714_retrieved_reference_46_inverse_0_typed_id,Node_11714_ref_node_target_id_46_inverse_0);
*Node_11714_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11714_retrieved_reference_46_inverse_true_0_is_inverse, Node_11714_ref_node_target_id_46_inverse_0);
*Node_11714_retrieved_reference_46_inverse_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_11714_retrieved_reference_46_inverse_0_target_id, Node_11714_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11714_retrieved_reference_46_inverse_0->references,"1",Node_11714_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11714_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11714_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11714_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11714_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11714_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11714_retrieved_reference_40_0);
*Node_11714_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11714_retrieved_reference_40_0_typed_id,Node_11714_ref_node_target_id_40_0);
*Node_11714_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11714_retrieved_reference_40_false_0_is_inverse, Node_11714_ref_node_target_id_40_0);
*Node_11714_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11714_retrieved_reference_40_0_target_id, Node_11714_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11714_retrieved_reference_40_0->references,"1",Node_11714_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/