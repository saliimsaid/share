#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_4(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* Image ********/


case 30 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 30;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Image", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Image", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an image encoded as a string of bytes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 30 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_30_retrieved_reference_45_inverse_0);
*Node_30_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_inverse_0_typed_id,Node_30_ref_node_target_id_45_inverse_0);
*Node_30_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_inverse_true_0_is_inverse, Node_30_ref_node_target_id_45_inverse_0);
*Node_30_retrieved_reference_45_inverse_0_target_id = 15;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_inverse_0_target_id, Node_30_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_inverse_0->references,"1",Node_30_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_0;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_0);
*Node_30_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_0_typed_id,Node_30_ref_node_target_id_45_0);
*Node_30_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_0_is_inverse, Node_30_ref_node_target_id_45_0);
*Node_30_retrieved_reference_45_0_target_id = 2000;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_0_target_id, Node_30_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_0->references,"1",Node_30_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_1;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_1);
*Node_30_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_1_typed_id,Node_30_ref_node_target_id_45_1);
*Node_30_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_1_is_inverse, Node_30_ref_node_target_id_45_1);
*Node_30_retrieved_reference_45_1_target_id = 2001;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_1_target_id, Node_30_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_1->references,"2",Node_30_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_2;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_2);
*Node_30_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_2_typed_id,Node_30_ref_node_target_id_45_2);
*Node_30_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_2_is_inverse, Node_30_ref_node_target_id_45_2);
*Node_30_retrieved_reference_45_2_target_id = 2002;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_2_target_id, Node_30_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_2->references,"3",Node_30_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_3;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_3);
*Node_30_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_3_typed_id,Node_30_ref_node_target_id_45_3);
*Node_30_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_3_is_inverse, Node_30_ref_node_target_id_45_3);
*Node_30_retrieved_reference_45_3_target_id = 2003;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_3_target_id, Node_30_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_3->references,"4",Node_30_ref_node_target_id_45_3);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerViewCount ********/


case 2151 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2151;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerViewCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerViewCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2151 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2151_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2151_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2151_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2151_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2151_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2151_retrieved_reference_47_inverse_0);
*Node_2151_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2151_retrieved_reference_47_inverse_0_typed_id,Node_2151_ref_node_target_id_47_inverse_0);
*Node_2151_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2151_retrieved_reference_47_inverse_true_0_is_inverse, Node_2151_ref_node_target_id_47_inverse_0);
*Node_2151_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2151_retrieved_reference_47_inverse_0_target_id, Node_2151_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2151_retrieved_reference_47_inverse_0->references,"1",Node_2151_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2151_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2151_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2151_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2151_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2151_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2151_retrieved_reference_40_0);
*Node_2151_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2151_retrieved_reference_40_0_typed_id,Node_2151_ref_node_target_id_40_0);
*Node_2151_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2151_retrieved_reference_40_false_0_is_inverse, Node_2151_ref_node_target_id_40_0);
*Node_2151_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2151_retrieved_reference_40_0_target_id, Node_2151_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2151_retrieved_reference_40_0->references,"1",Node_2151_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2151_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2151_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2151_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2151_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2151_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2151_retrieved_reference_37_0);
*Node_2151_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2151_retrieved_reference_37_0_typed_id,Node_2151_ref_node_target_id_37_0);
*Node_2151_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2151_retrieved_reference_37_false_0_is_inverse, Node_2151_ref_node_target_id_37_0);
*Node_2151_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2151_retrieved_reference_37_0_target_id, Node_2151_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2151_retrieved_reference_37_0->references,"1",Node_2151_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* CurrentSessionCount ********/


case 2152 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2152;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("CurrentSessionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("CurrentSessionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2152 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2152_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2152_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2152_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2152_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2152_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2152_retrieved_reference_47_inverse_0);
*Node_2152_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2152_retrieved_reference_47_inverse_0_typed_id,Node_2152_ref_node_target_id_47_inverse_0);
*Node_2152_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2152_retrieved_reference_47_inverse_true_0_is_inverse, Node_2152_ref_node_target_id_47_inverse_0);
*Node_2152_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2152_retrieved_reference_47_inverse_0_target_id, Node_2152_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2152_retrieved_reference_47_inverse_0->references,"1",Node_2152_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2152_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2152_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2152_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2152_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2152_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2152_retrieved_reference_40_0);
*Node_2152_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2152_retrieved_reference_40_0_typed_id,Node_2152_ref_node_target_id_40_0);
*Node_2152_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2152_retrieved_reference_40_false_0_is_inverse, Node_2152_ref_node_target_id_40_0);
*Node_2152_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2152_retrieved_reference_40_0_target_id, Node_2152_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2152_retrieved_reference_40_0->references,"1",Node_2152_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2152_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2152_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2152_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2152_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2152_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2152_retrieved_reference_37_0);
*Node_2152_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2152_retrieved_reference_37_0_typed_id,Node_2152_ref_node_target_id_37_0);
*Node_2152_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2152_retrieved_reference_37_false_0_is_inverse, Node_2152_ref_node_target_id_37_0);
*Node_2152_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2152_retrieved_reference_37_0_target_id, Node_2152_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2152_retrieved_reference_37_0->references,"1",Node_2152_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* CumulatedSessionCount ********/


case 2153 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2153;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("CumulatedSessionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("CumulatedSessionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2153 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2153_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2153_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2153_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2153_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2153_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2153_retrieved_reference_47_inverse_0);
*Node_2153_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2153_retrieved_reference_47_inverse_0_typed_id,Node_2153_ref_node_target_id_47_inverse_0);
*Node_2153_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2153_retrieved_reference_47_inverse_true_0_is_inverse, Node_2153_ref_node_target_id_47_inverse_0);
*Node_2153_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2153_retrieved_reference_47_inverse_0_target_id, Node_2153_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2153_retrieved_reference_47_inverse_0->references,"1",Node_2153_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2153_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2153_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2153_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2153_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2153_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2153_retrieved_reference_40_0);
*Node_2153_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2153_retrieved_reference_40_0_typed_id,Node_2153_ref_node_target_id_40_0);
*Node_2153_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2153_retrieved_reference_40_false_0_is_inverse, Node_2153_ref_node_target_id_40_0);
*Node_2153_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2153_retrieved_reference_40_0_target_id, Node_2153_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2153_retrieved_reference_40_0->references,"1",Node_2153_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2153_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2153_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2153_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2153_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2153_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2153_retrieved_reference_37_0);
*Node_2153_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2153_retrieved_reference_37_0_typed_id,Node_2153_ref_node_target_id_37_0);
*Node_2153_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2153_retrieved_reference_37_false_0_is_inverse, Node_2153_ref_node_target_id_37_0);
*Node_2153_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2153_retrieved_reference_37_0_target_id, Node_2153_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2153_retrieved_reference_37_0->references,"1",Node_2153_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* RejectedSessionCount ********/


case 2155 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2155;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("RejectedSessionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("RejectedSessionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2155 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2155_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2155_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2155_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2155_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2155_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2155_retrieved_reference_47_inverse_0);
*Node_2155_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2155_retrieved_reference_47_inverse_0_typed_id,Node_2155_ref_node_target_id_47_inverse_0);
*Node_2155_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2155_retrieved_reference_47_inverse_true_0_is_inverse, Node_2155_ref_node_target_id_47_inverse_0);
*Node_2155_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2155_retrieved_reference_47_inverse_0_target_id, Node_2155_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2155_retrieved_reference_47_inverse_0->references,"1",Node_2155_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2155_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2155_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2155_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2155_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2155_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2155_retrieved_reference_40_0);
*Node_2155_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2155_retrieved_reference_40_0_typed_id,Node_2155_ref_node_target_id_40_0);
*Node_2155_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2155_retrieved_reference_40_false_0_is_inverse, Node_2155_ref_node_target_id_40_0);
*Node_2155_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2155_retrieved_reference_40_0_target_id, Node_2155_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2155_retrieved_reference_40_0->references,"1",Node_2155_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2155_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2155_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2155_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2155_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2155_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2155_retrieved_reference_37_0);
*Node_2155_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2155_retrieved_reference_37_0_typed_id,Node_2155_ref_node_target_id_37_0);
*Node_2155_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2155_retrieved_reference_37_false_0_is_inverse, Node_2155_ref_node_target_id_37_0);
*Node_2155_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2155_retrieved_reference_37_0_target_id, Node_2155_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2155_retrieved_reference_37_0->references,"1",Node_2155_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SessionTimeoutCount ********/


case 2156 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2156;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SessionTimeoutCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SessionTimeoutCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2156 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2156_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2156_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2156_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2156_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2156_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2156_retrieved_reference_47_inverse_0);
*Node_2156_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2156_retrieved_reference_47_inverse_0_typed_id,Node_2156_ref_node_target_id_47_inverse_0);
*Node_2156_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2156_retrieved_reference_47_inverse_true_0_is_inverse, Node_2156_ref_node_target_id_47_inverse_0);
*Node_2156_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2156_retrieved_reference_47_inverse_0_target_id, Node_2156_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2156_retrieved_reference_47_inverse_0->references,"1",Node_2156_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2156_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2156_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2156_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2156_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2156_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2156_retrieved_reference_40_0);
*Node_2156_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2156_retrieved_reference_40_0_typed_id,Node_2156_ref_node_target_id_40_0);
*Node_2156_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2156_retrieved_reference_40_false_0_is_inverse, Node_2156_ref_node_target_id_40_0);
*Node_2156_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2156_retrieved_reference_40_0_target_id, Node_2156_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2156_retrieved_reference_40_0->references,"1",Node_2156_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2156_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2156_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2156_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2156_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2156_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2156_retrieved_reference_37_0);
*Node_2156_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2156_retrieved_reference_37_0_typed_id,Node_2156_ref_node_target_id_37_0);
*Node_2156_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2156_retrieved_reference_37_false_0_is_inverse, Node_2156_ref_node_target_id_37_0);
*Node_2156_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2156_retrieved_reference_37_0_target_id, Node_2156_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2156_retrieved_reference_37_0->references,"1",Node_2156_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SessionAbortCount ********/


case 2157 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2157;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SessionAbortCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SessionAbortCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2157 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2157_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2157_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2157_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2157_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2157_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2157_retrieved_reference_47_inverse_0);
*Node_2157_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2157_retrieved_reference_47_inverse_0_typed_id,Node_2157_ref_node_target_id_47_inverse_0);
*Node_2157_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2157_retrieved_reference_47_inverse_true_0_is_inverse, Node_2157_ref_node_target_id_47_inverse_0);
*Node_2157_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2157_retrieved_reference_47_inverse_0_target_id, Node_2157_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2157_retrieved_reference_47_inverse_0->references,"1",Node_2157_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2157_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2157_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2157_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2157_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2157_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2157_retrieved_reference_40_0);
*Node_2157_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2157_retrieved_reference_40_0_typed_id,Node_2157_ref_node_target_id_40_0);
*Node_2157_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2157_retrieved_reference_40_false_0_is_inverse, Node_2157_ref_node_target_id_40_0);
*Node_2157_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2157_retrieved_reference_40_0_target_id, Node_2157_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2157_retrieved_reference_40_0->references,"1",Node_2157_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2157_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2157_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2157_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2157_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2157_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2157_retrieved_reference_37_0);
*Node_2157_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2157_retrieved_reference_37_0_typed_id,Node_2157_ref_node_target_id_37_0);
*Node_2157_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2157_retrieved_reference_37_false_0_is_inverse, Node_2157_ref_node_target_id_37_0);
*Node_2157_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2157_retrieved_reference_37_0_target_id, Node_2157_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2157_retrieved_reference_37_0->references,"1",Node_2157_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* CurrentSubscriptionCount ********/


case 2160 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2160;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("CurrentSubscriptionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("CurrentSubscriptionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2160 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2160_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2160_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2160_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2160_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2160_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2160_retrieved_reference_47_inverse_0);
*Node_2160_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2160_retrieved_reference_47_inverse_0_typed_id,Node_2160_ref_node_target_id_47_inverse_0);
*Node_2160_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2160_retrieved_reference_47_inverse_true_0_is_inverse, Node_2160_ref_node_target_id_47_inverse_0);
*Node_2160_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2160_retrieved_reference_47_inverse_0_target_id, Node_2160_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2160_retrieved_reference_47_inverse_0->references,"1",Node_2160_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2160_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2160_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2160_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2160_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2160_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2160_retrieved_reference_40_0);
*Node_2160_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2160_retrieved_reference_40_0_typed_id,Node_2160_ref_node_target_id_40_0);
*Node_2160_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2160_retrieved_reference_40_false_0_is_inverse, Node_2160_ref_node_target_id_40_0);
*Node_2160_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2160_retrieved_reference_40_0_target_id, Node_2160_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2160_retrieved_reference_40_0->references,"1",Node_2160_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2160_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2160_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2160_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2160_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2160_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2160_retrieved_reference_37_0);
*Node_2160_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2160_retrieved_reference_37_0_typed_id,Node_2160_ref_node_target_id_37_0);
*Node_2160_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2160_retrieved_reference_37_false_0_is_inverse, Node_2160_ref_node_target_id_37_0);
*Node_2160_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2160_retrieved_reference_37_0_target_id, Node_2160_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2160_retrieved_reference_37_0->references,"1",Node_2160_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* CumulatedSubscriptionCount ********/


case 2161 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2161;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("CumulatedSubscriptionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("CumulatedSubscriptionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2161 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2161_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2161_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2161_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2161_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2161_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2161_retrieved_reference_47_inverse_0);
*Node_2161_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2161_retrieved_reference_47_inverse_0_typed_id,Node_2161_ref_node_target_id_47_inverse_0);
*Node_2161_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2161_retrieved_reference_47_inverse_true_0_is_inverse, Node_2161_ref_node_target_id_47_inverse_0);
*Node_2161_retrieved_reference_47_inverse_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2161_retrieved_reference_47_inverse_0_target_id, Node_2161_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2161_retrieved_reference_47_inverse_0->references,"1",Node_2161_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2161_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2161_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2161_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2161_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2161_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2161_retrieved_reference_40_0);
*Node_2161_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2161_retrieved_reference_40_0_typed_id,Node_2161_ref_node_target_id_40_0);
*Node_2161_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2161_retrieved_reference_40_false_0_is_inverse, Node_2161_ref_node_target_id_40_0);
*Node_2161_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2161_retrieved_reference_40_0_target_id, Node_2161_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2161_retrieved_reference_40_0->references,"1",Node_2161_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2161_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2161_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2161_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2161_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2161_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2161_retrieved_reference_37_0);
*Node_2161_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2161_retrieved_reference_37_0_typed_id,Node_2161_ref_node_target_id_37_0);
*Node_2161_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2161_retrieved_reference_37_false_0_is_inverse, Node_2161_ref_node_target_id_37_0);
*Node_2161_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2161_retrieved_reference_37_0_target_id, Node_2161_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2161_retrieved_reference_37_0->references,"1",Node_2161_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BuildInfoType ********/


case 3051 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 3051;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BuildInfoType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BuildInfoType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

ishidaopcua_node_set_value(variant, universal_node);
data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 338;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

puts("finished adding node >> 3051 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_3051_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_3051_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3051_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_3051_retrieved_reference_45_inverse_0);
*Node_3051_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_3051_retrieved_reference_45_inverse_0_typed_id,Node_3051_ref_node_target_id_45_inverse_0);
*Node_3051_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_3051_retrieved_reference_45_inverse_true_0_is_inverse, Node_3051_ref_node_target_id_45_inverse_0);
*Node_3051_retrieved_reference_45_inverse_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_3051_retrieved_reference_45_inverse_0_target_id, Node_3051_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_3051_retrieved_reference_45_inverse_0->references,"1",Node_3051_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3051_retrieved_reference_47_0;
ishidaopcua_NODE* Node_3051_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_3051_retrieved_reference_47_0);
*Node_3051_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_3051_retrieved_reference_47_0_typed_id,Node_3051_ref_node_target_id_47_0);
*Node_3051_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3051_retrieved_reference_47_false_0_is_inverse, Node_3051_ref_node_target_id_47_0);
*Node_3051_retrieved_reference_47_0_target_id = 3052;
ishidaopcua_node_set_target_id(Node_3051_retrieved_reference_47_0_target_id, Node_3051_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_3051_retrieved_reference_47_0->references,"1",Node_3051_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3051_retrieved_reference_47_1;
ishidaopcua_NODE* Node_3051_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_3051_retrieved_reference_47_1);
*Node_3051_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_3051_retrieved_reference_47_1_typed_id,Node_3051_ref_node_target_id_47_1);
*Node_3051_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3051_retrieved_reference_47_false_1_is_inverse, Node_3051_ref_node_target_id_47_1);
*Node_3051_retrieved_reference_47_1_target_id = 3053;
ishidaopcua_node_set_target_id(Node_3051_retrieved_reference_47_1_target_id, Node_3051_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_3051_retrieved_reference_47_1->references,"2",Node_3051_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3051_retrieved_reference_47_2;
ishidaopcua_NODE* Node_3051_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_3051_retrieved_reference_47_2);
*Node_3051_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_3051_retrieved_reference_47_2_typed_id,Node_3051_ref_node_target_id_47_2);
*Node_3051_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3051_retrieved_reference_47_false_2_is_inverse, Node_3051_ref_node_target_id_47_2);
*Node_3051_retrieved_reference_47_2_target_id = 3054;
ishidaopcua_node_set_target_id(Node_3051_retrieved_reference_47_2_target_id, Node_3051_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_3051_retrieved_reference_47_2->references,"3",Node_3051_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3051_retrieved_reference_47_3;
ishidaopcua_NODE* Node_3051_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_3051_retrieved_reference_47_3);
*Node_3051_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_3051_retrieved_reference_47_3_typed_id,Node_3051_ref_node_target_id_47_3);
*Node_3051_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3051_retrieved_reference_47_false_3_is_inverse, Node_3051_ref_node_target_id_47_3);
*Node_3051_retrieved_reference_47_3_target_id = 3055;
ishidaopcua_node_set_target_id(Node_3051_retrieved_reference_47_3_target_id, Node_3051_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_3051_retrieved_reference_47_3->references,"4",Node_3051_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3051_retrieved_reference_47_4;
ishidaopcua_NODE* Node_3051_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_3051_retrieved_reference_47_4);
*Node_3051_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_3051_retrieved_reference_47_4_typed_id,Node_3051_ref_node_target_id_47_4);
*Node_3051_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3051_retrieved_reference_47_false_4_is_inverse, Node_3051_ref_node_target_id_47_4);
*Node_3051_retrieved_reference_47_4_target_id = 3056;
ishidaopcua_node_set_target_id(Node_3051_retrieved_reference_47_4_target_id, Node_3051_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_3051_retrieved_reference_47_4->references,"5",Node_3051_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_3051_retrieved_reference_47_5;
ishidaopcua_NODE* Node_3051_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3051_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_3051_retrieved_reference_47_5);
*Node_3051_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_3051_retrieved_reference_47_5_typed_id,Node_3051_ref_node_target_id_47_5);
*Node_3051_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3051_retrieved_reference_47_false_5_is_inverse, Node_3051_ref_node_target_id_47_5);
*Node_3051_retrieved_reference_47_5_target_id = 3057;
ishidaopcua_node_set_target_id(Node_3051_retrieved_reference_47_5_target_id, Node_3051_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_3051_retrieved_reference_47_5->references,"6",Node_3051_ref_node_target_id_47_5); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/