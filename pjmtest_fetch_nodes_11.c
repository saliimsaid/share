#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_11(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* ServerDiagnosticsType ********/


case 2020 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2020;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerDiagnosticsType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerDiagnosticsType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The diagnostics information for a server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

puts("finished adding node >> 2020 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2020_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_2020_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2020_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_2020_retrieved_reference_45_inverse_0);
*Node_2020_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2020_retrieved_reference_45_inverse_0_typed_id,Node_2020_ref_node_target_id_45_inverse_0);
*Node_2020_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2020_retrieved_reference_45_inverse_true_0_is_inverse, Node_2020_ref_node_target_id_45_inverse_0);
*Node_2020_retrieved_reference_45_inverse_0_target_id = 58;
ishidaopcua_node_set_target_id(Node_2020_retrieved_reference_45_inverse_0_target_id, Node_2020_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_2020_retrieved_reference_45_inverse_0->references,"1",Node_2020_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2020_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2020_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2020_retrieved_reference_47_0);
*Node_2020_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2020_retrieved_reference_47_0_typed_id,Node_2020_ref_node_target_id_47_0);
*Node_2020_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2020_retrieved_reference_47_false_0_is_inverse, Node_2020_ref_node_target_id_47_0);
*Node_2020_retrieved_reference_47_0_target_id = 2021;
ishidaopcua_node_set_target_id(Node_2020_retrieved_reference_47_0_target_id, Node_2020_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2020_retrieved_reference_47_0->references,"1",Node_2020_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2020_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2020_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2020_retrieved_reference_47_1);
*Node_2020_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2020_retrieved_reference_47_1_typed_id,Node_2020_ref_node_target_id_47_1);
*Node_2020_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2020_retrieved_reference_47_false_1_is_inverse, Node_2020_ref_node_target_id_47_1);
*Node_2020_retrieved_reference_47_1_target_id = 2022;
ishidaopcua_node_set_target_id(Node_2020_retrieved_reference_47_1_target_id, Node_2020_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2020_retrieved_reference_47_1->references,"2",Node_2020_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2020_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2020_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2020_retrieved_reference_47_2);
*Node_2020_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2020_retrieved_reference_47_2_typed_id,Node_2020_ref_node_target_id_47_2);
*Node_2020_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2020_retrieved_reference_47_false_2_is_inverse, Node_2020_ref_node_target_id_47_2);
*Node_2020_retrieved_reference_47_2_target_id = 2023;
ishidaopcua_node_set_target_id(Node_2020_retrieved_reference_47_2_target_id, Node_2020_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2020_retrieved_reference_47_2->references,"3",Node_2020_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2020_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2020_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2020_retrieved_reference_47_3);
*Node_2020_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2020_retrieved_reference_47_3_typed_id,Node_2020_ref_node_target_id_47_3);
*Node_2020_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2020_retrieved_reference_47_false_3_is_inverse, Node_2020_ref_node_target_id_47_3);
*Node_2020_retrieved_reference_47_3_target_id = 2744;
ishidaopcua_node_set_target_id(Node_2020_retrieved_reference_47_3_target_id, Node_2020_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2020_retrieved_reference_47_3->references,"4",Node_2020_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2020_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2020_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2020_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2020_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2020_retrieved_reference_46_0);
*Node_2020_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2020_retrieved_reference_46_0_typed_id,Node_2020_ref_node_target_id_46_0);
*Node_2020_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2020_retrieved_reference_46_false_0_is_inverse, Node_2020_ref_node_target_id_46_0);
*Node_2020_retrieved_reference_46_0_target_id = 2025;
ishidaopcua_node_set_target_id(Node_2020_retrieved_reference_46_0_target_id, Node_2020_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2020_retrieved_reference_46_0->references,"1",Node_2020_ref_node_target_id_46_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerDiagnostics ********/


case 2274 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2274;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerDiagnostics", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerDiagnostics", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Reports diagnostics about the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 2274 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2274_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2274_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2274_retrieved_reference_47_inverse_0);
*Node_2274_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2274_retrieved_reference_47_inverse_0_typed_id,Node_2274_ref_node_target_id_47_inverse_0);
*Node_2274_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2274_retrieved_reference_47_inverse_true_0_is_inverse, Node_2274_ref_node_target_id_47_inverse_0);
*Node_2274_retrieved_reference_47_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_2274_retrieved_reference_47_inverse_0_target_id, Node_2274_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2274_retrieved_reference_47_inverse_0->references,"1",Node_2274_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2274_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2274_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2274_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2274_retrieved_reference_40_0);
*Node_2274_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2274_retrieved_reference_40_0_typed_id,Node_2274_ref_node_target_id_40_0);
*Node_2274_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2274_retrieved_reference_40_false_0_is_inverse, Node_2274_ref_node_target_id_40_0);
*Node_2274_retrieved_reference_40_0_target_id = 2020;
ishidaopcua_node_set_target_id(Node_2274_retrieved_reference_40_0_target_id, Node_2274_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2274_retrieved_reference_40_0->references,"1",Node_2274_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2274_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2274_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2274_retrieved_reference_47_0);
*Node_2274_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2274_retrieved_reference_47_0_typed_id,Node_2274_ref_node_target_id_47_0);
*Node_2274_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2274_retrieved_reference_47_false_0_is_inverse, Node_2274_ref_node_target_id_47_0);
*Node_2274_retrieved_reference_47_0_target_id = 2275;
ishidaopcua_node_set_target_id(Node_2274_retrieved_reference_47_0_target_id, Node_2274_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2274_retrieved_reference_47_0->references,"1",Node_2274_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2274_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2274_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2274_retrieved_reference_47_1);
*Node_2274_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2274_retrieved_reference_47_1_typed_id,Node_2274_ref_node_target_id_47_1);
*Node_2274_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2274_retrieved_reference_47_false_1_is_inverse, Node_2274_ref_node_target_id_47_1);
*Node_2274_retrieved_reference_47_1_target_id = 2289;
ishidaopcua_node_set_target_id(Node_2274_retrieved_reference_47_1_target_id, Node_2274_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2274_retrieved_reference_47_1->references,"2",Node_2274_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2274_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2274_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2274_retrieved_reference_47_2);
*Node_2274_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2274_retrieved_reference_47_2_typed_id,Node_2274_ref_node_target_id_47_2);
*Node_2274_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2274_retrieved_reference_47_false_2_is_inverse, Node_2274_ref_node_target_id_47_2);
*Node_2274_retrieved_reference_47_2_target_id = 2290;
ishidaopcua_node_set_target_id(Node_2274_retrieved_reference_47_2_target_id, Node_2274_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2274_retrieved_reference_47_2->references,"3",Node_2274_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2274_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2274_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2274_retrieved_reference_47_3);
*Node_2274_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2274_retrieved_reference_47_3_typed_id,Node_2274_ref_node_target_id_47_3);
*Node_2274_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2274_retrieved_reference_47_false_3_is_inverse, Node_2274_ref_node_target_id_47_3);
*Node_2274_retrieved_reference_47_3_target_id = 3706;
ishidaopcua_node_set_target_id(Node_2274_retrieved_reference_47_3_target_id, Node_2274_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2274_retrieved_reference_47_3->references,"4",Node_2274_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2274_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2274_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2274_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2274_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2274_retrieved_reference_46_0);
*Node_2274_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2274_retrieved_reference_46_0_typed_id,Node_2274_ref_node_target_id_46_0);
*Node_2274_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2274_retrieved_reference_46_false_0_is_inverse, Node_2274_ref_node_target_id_46_0);
*Node_2274_retrieved_reference_46_0_target_id = 2294;
ishidaopcua_node_set_target_id(Node_2274_retrieved_reference_46_0_target_id, Node_2274_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2274_retrieved_reference_46_0->references,"1",Node_2274_ref_node_target_id_46_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerDiagnosticsSummary ********/


case 2275 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2275;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerDiagnosticsSummary", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerDiagnosticsSummary", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A summary of server level diagnostics.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 859;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2275 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2275_retrieved_reference_47_inverse_0);
*Node_2275_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_inverse_0_typed_id,Node_2275_ref_node_target_id_47_inverse_0);
*Node_2275_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_inverse_true_0_is_inverse, Node_2275_ref_node_target_id_47_inverse_0);
*Node_2275_retrieved_reference_47_inverse_0_target_id = 2274;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_inverse_0_target_id, Node_2275_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_inverse_0->references,"1",Node_2275_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2275_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2275_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2275_retrieved_reference_40_0);
*Node_2275_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_40_0_typed_id,Node_2275_ref_node_target_id_40_0);
*Node_2275_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_40_false_0_is_inverse, Node_2275_ref_node_target_id_40_0);
*Node_2275_retrieved_reference_40_0_target_id = 2150;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_40_0_target_id, Node_2275_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_40_0->references,"1",Node_2275_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_0);
*Node_2275_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_0_typed_id,Node_2275_ref_node_target_id_47_0);
*Node_2275_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_0_is_inverse, Node_2275_ref_node_target_id_47_0);
*Node_2275_retrieved_reference_47_0_target_id = 2276;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_0_target_id, Node_2275_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_0->references,"1",Node_2275_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_1);
*Node_2275_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_1_typed_id,Node_2275_ref_node_target_id_47_1);
*Node_2275_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_1_is_inverse, Node_2275_ref_node_target_id_47_1);
*Node_2275_retrieved_reference_47_1_target_id = 2277;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_1_target_id, Node_2275_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_1->references,"2",Node_2275_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_2);
*Node_2275_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_2_typed_id,Node_2275_ref_node_target_id_47_2);
*Node_2275_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_2_is_inverse, Node_2275_ref_node_target_id_47_2);
*Node_2275_retrieved_reference_47_2_target_id = 2278;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_2_target_id, Node_2275_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_2->references,"3",Node_2275_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_3);
*Node_2275_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_3_typed_id,Node_2275_ref_node_target_id_47_3);
*Node_2275_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_3_is_inverse, Node_2275_ref_node_target_id_47_3);
*Node_2275_retrieved_reference_47_3_target_id = 2279;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_3_target_id, Node_2275_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_3->references,"4",Node_2275_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_4);
*Node_2275_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_4_typed_id,Node_2275_ref_node_target_id_47_4);
*Node_2275_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_4_is_inverse, Node_2275_ref_node_target_id_47_4);
*Node_2275_retrieved_reference_47_4_target_id = 3705;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_4_target_id, Node_2275_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_4->references,"5",Node_2275_ref_node_target_id_47_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_5;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_5);
*Node_2275_retrieved_reference_47_5_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_5_typed_id,Node_2275_ref_node_target_id_47_5);
*Node_2275_retrieved_reference_47_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_5_is_inverse, Node_2275_ref_node_target_id_47_5);
*Node_2275_retrieved_reference_47_5_target_id = 2281;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_5_target_id, Node_2275_ref_node_target_id_47_5);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_5->references,"6",Node_2275_ref_node_target_id_47_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_6;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_6);
*Node_2275_retrieved_reference_47_6_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_6_typed_id,Node_2275_ref_node_target_id_47_6);
*Node_2275_retrieved_reference_47_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_6_is_inverse, Node_2275_ref_node_target_id_47_6);
*Node_2275_retrieved_reference_47_6_target_id = 2282;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_6_target_id, Node_2275_ref_node_target_id_47_6);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_6->references,"7",Node_2275_ref_node_target_id_47_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_7;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_7);
*Node_2275_retrieved_reference_47_7_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_7_typed_id,Node_2275_ref_node_target_id_47_7);
*Node_2275_retrieved_reference_47_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_7_is_inverse, Node_2275_ref_node_target_id_47_7);
*Node_2275_retrieved_reference_47_7_target_id = 2284;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_7_target_id, Node_2275_ref_node_target_id_47_7);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_7->references,"8",Node_2275_ref_node_target_id_47_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_8;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_8);
*Node_2275_retrieved_reference_47_8_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_8_typed_id,Node_2275_ref_node_target_id_47_8);
*Node_2275_retrieved_reference_47_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_8_is_inverse, Node_2275_ref_node_target_id_47_8);
*Node_2275_retrieved_reference_47_8_target_id = 2285;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_8_target_id, Node_2275_ref_node_target_id_47_8);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_8->references,"9",Node_2275_ref_node_target_id_47_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_9;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_9);
*Node_2275_retrieved_reference_47_9_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_9_typed_id,Node_2275_ref_node_target_id_47_9);
*Node_2275_retrieved_reference_47_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_9_is_inverse, Node_2275_ref_node_target_id_47_9);
*Node_2275_retrieved_reference_47_9_target_id = 2286;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_9_target_id, Node_2275_ref_node_target_id_47_9);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_9->references,"10",Node_2275_ref_node_target_id_47_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_10;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_10);
*Node_2275_retrieved_reference_47_10_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_10_typed_id,Node_2275_ref_node_target_id_47_10);
*Node_2275_retrieved_reference_47_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_10_is_inverse, Node_2275_ref_node_target_id_47_10);
*Node_2275_retrieved_reference_47_10_target_id = 2287;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_10_target_id, Node_2275_ref_node_target_id_47_10);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_10->references,"11",Node_2275_ref_node_target_id_47_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2275_retrieved_reference_47_11;
ishidaopcua_NODE* Node_2275_ref_node_target_id_47_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2275_retrieved_reference_47_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2275_retrieved_reference_47_11);
*Node_2275_retrieved_reference_47_11_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2275_retrieved_reference_47_11_typed_id,Node_2275_ref_node_target_id_47_11);
*Node_2275_retrieved_reference_47_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2275_retrieved_reference_47_false_11_is_inverse, Node_2275_ref_node_target_id_47_11);
*Node_2275_retrieved_reference_47_11_target_id = 2288;
ishidaopcua_node_set_target_id(Node_2275_retrieved_reference_47_11_target_id, Node_2275_ref_node_target_id_47_11);
ishidaeutz_put_hashmap(Node_2275_retrieved_reference_47_11->references,"12",Node_2275_ref_node_target_id_47_11); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SessionTimeoutCount ********/


case 2281 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2281;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SessionTimeoutCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SessionTimeoutCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2281 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2281_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2281_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2281_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2281_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2281_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2281_retrieved_reference_47_inverse_0);
*Node_2281_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2281_retrieved_reference_47_inverse_0_typed_id,Node_2281_ref_node_target_id_47_inverse_0);
*Node_2281_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2281_retrieved_reference_47_inverse_true_0_is_inverse, Node_2281_ref_node_target_id_47_inverse_0);
*Node_2281_retrieved_reference_47_inverse_0_target_id = 2275;
ishidaopcua_node_set_target_id(Node_2281_retrieved_reference_47_inverse_0_target_id, Node_2281_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2281_retrieved_reference_47_inverse_0->references,"1",Node_2281_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2281_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2281_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2281_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2281_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2281_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2281_retrieved_reference_40_0);
*Node_2281_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2281_retrieved_reference_40_0_typed_id,Node_2281_ref_node_target_id_40_0);
*Node_2281_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2281_retrieved_reference_40_false_0_is_inverse, Node_2281_ref_node_target_id_40_0);
*Node_2281_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2281_retrieved_reference_40_0_target_id, Node_2281_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2281_retrieved_reference_40_0->references,"1",Node_2281_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SessionAbortCount ********/


case 2282 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2282;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SessionAbortCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SessionAbortCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2282 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2282_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2282_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2282_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2282_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2282_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2282_retrieved_reference_47_inverse_0);
*Node_2282_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2282_retrieved_reference_47_inverse_0_typed_id,Node_2282_ref_node_target_id_47_inverse_0);
*Node_2282_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2282_retrieved_reference_47_inverse_true_0_is_inverse, Node_2282_ref_node_target_id_47_inverse_0);
*Node_2282_retrieved_reference_47_inverse_0_target_id = 2275;
ishidaopcua_node_set_target_id(Node_2282_retrieved_reference_47_inverse_0_target_id, Node_2282_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2282_retrieved_reference_47_inverse_0->references,"1",Node_2282_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2282_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2282_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2282_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2282_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2282_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2282_retrieved_reference_40_0);
*Node_2282_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2282_retrieved_reference_40_0_typed_id,Node_2282_ref_node_target_id_40_0);
*Node_2282_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2282_retrieved_reference_40_false_0_is_inverse, Node_2282_ref_node_target_id_40_0);
*Node_2282_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2282_retrieved_reference_40_0_target_id, Node_2282_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2282_retrieved_reference_40_0->references,"1",Node_2282_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* PublishingIntervalCount ********/


case 2284 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2284;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("PublishingIntervalCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("PublishingIntervalCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2284 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2284_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2284_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2284_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2284_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2284_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2284_retrieved_reference_47_inverse_0);
*Node_2284_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2284_retrieved_reference_47_inverse_0_typed_id,Node_2284_ref_node_target_id_47_inverse_0);
*Node_2284_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2284_retrieved_reference_47_inverse_true_0_is_inverse, Node_2284_ref_node_target_id_47_inverse_0);
*Node_2284_retrieved_reference_47_inverse_0_target_id = 2275;
ishidaopcua_node_set_target_id(Node_2284_retrieved_reference_47_inverse_0_target_id, Node_2284_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2284_retrieved_reference_47_inverse_0->references,"1",Node_2284_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2284_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2284_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2284_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2284_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2284_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2284_retrieved_reference_40_0);
*Node_2284_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2284_retrieved_reference_40_0_typed_id,Node_2284_ref_node_target_id_40_0);
*Node_2284_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2284_retrieved_reference_40_false_0_is_inverse, Node_2284_ref_node_target_id_40_0);
*Node_2284_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2284_retrieved_reference_40_0_target_id, Node_2284_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2284_retrieved_reference_40_0->references,"1",Node_2284_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* CurrentSubscriptionCount ********/


case 2285 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2285;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("CurrentSubscriptionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("CurrentSubscriptionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2285 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2285_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2285_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2285_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2285_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2285_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2285_retrieved_reference_47_inverse_0);
*Node_2285_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2285_retrieved_reference_47_inverse_0_typed_id,Node_2285_ref_node_target_id_47_inverse_0);
*Node_2285_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2285_retrieved_reference_47_inverse_true_0_is_inverse, Node_2285_ref_node_target_id_47_inverse_0);
*Node_2285_retrieved_reference_47_inverse_0_target_id = 2275;
ishidaopcua_node_set_target_id(Node_2285_retrieved_reference_47_inverse_0_target_id, Node_2285_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2285_retrieved_reference_47_inverse_0->references,"1",Node_2285_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2285_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2285_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2285_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2285_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2285_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2285_retrieved_reference_40_0);
*Node_2285_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2285_retrieved_reference_40_0_typed_id,Node_2285_ref_node_target_id_40_0);
*Node_2285_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2285_retrieved_reference_40_false_0_is_inverse, Node_2285_ref_node_target_id_40_0);
*Node_2285_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2285_retrieved_reference_40_0_target_id, Node_2285_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2285_retrieved_reference_40_0->references,"1",Node_2285_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* CumulatedSubscriptionCount ********/


case 2286 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2286;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("CumulatedSubscriptionCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("CumulatedSubscriptionCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2286 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2286_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2286_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2286_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2286_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2286_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2286_retrieved_reference_47_inverse_0);
*Node_2286_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2286_retrieved_reference_47_inverse_0_typed_id,Node_2286_ref_node_target_id_47_inverse_0);
*Node_2286_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2286_retrieved_reference_47_inverse_true_0_is_inverse, Node_2286_ref_node_target_id_47_inverse_0);
*Node_2286_retrieved_reference_47_inverse_0_target_id = 2275;
ishidaopcua_node_set_target_id(Node_2286_retrieved_reference_47_inverse_0_target_id, Node_2286_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2286_retrieved_reference_47_inverse_0->references,"1",Node_2286_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2286_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2286_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2286_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2286_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2286_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2286_retrieved_reference_40_0);
*Node_2286_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2286_retrieved_reference_40_0_typed_id,Node_2286_ref_node_target_id_40_0);
*Node_2286_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2286_retrieved_reference_40_false_0_is_inverse, Node_2286_ref_node_target_id_40_0);
*Node_2286_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2286_retrieved_reference_40_0_target_id, Node_2286_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2286_retrieved_reference_40_0->references,"1",Node_2286_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* SecurityRejectedRequestsCount ********/


case 2287 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2287;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("SecurityRejectedRequestsCount", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("SecurityRejectedRequestsCount", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 2287 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2287_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_2287_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2287_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2287_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2287_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_2287_retrieved_reference_47_inverse_0);
*Node_2287_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2287_retrieved_reference_47_inverse_0_typed_id,Node_2287_ref_node_target_id_47_inverse_0);
*Node_2287_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2287_retrieved_reference_47_inverse_true_0_is_inverse, Node_2287_ref_node_target_id_47_inverse_0);
*Node_2287_retrieved_reference_47_inverse_0_target_id = 2275;
ishidaopcua_node_set_target_id(Node_2287_retrieved_reference_47_inverse_0_target_id, Node_2287_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_2287_retrieved_reference_47_inverse_0->references,"1",Node_2287_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2287_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2287_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2287_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2287_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2287_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2287_retrieved_reference_40_0);
*Node_2287_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2287_retrieved_reference_40_0_typed_id,Node_2287_ref_node_target_id_40_0);
*Node_2287_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2287_retrieved_reference_40_false_0_is_inverse, Node_2287_ref_node_target_id_40_0);
*Node_2287_retrieved_reference_40_0_target_id = 63;
ishidaopcua_node_set_target_id(Node_2287_retrieved_reference_40_0_target_id, Node_2287_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2287_retrieved_reference_40_0->references,"1",Node_2287_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DefaultBinary ********/


case 3062 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 3062;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DefaultBinary", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Default Binary", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The default binary encoding for a data type.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 3062 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

{
ishidaopcua_NODE* Node_3062_retrieved_reference_40_0;
ishidaopcua_NODE* Node_3062_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_3062_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3062_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_3062_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_3062_retrieved_reference_40_0);
*Node_3062_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_3062_retrieved_reference_40_0_typed_id,Node_3062_ref_node_target_id_40_0);
*Node_3062_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_3062_retrieved_reference_40_false_0_is_inverse, Node_3062_ref_node_target_id_40_0);
*Node_3062_retrieved_reference_40_0_target_id = 58;
ishidaopcua_node_set_target_id(Node_3062_retrieved_reference_40_0_target_id, Node_3062_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_3062_retrieved_reference_40_0->references,"1",Node_3062_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/