#include "pjmtest.h"


/*************************************** ISHIDAEUTZ_MEMORY_MANAGER START ***************************************/
char predef_mem_ephemeral[600000];
unsigned int predef_mem_ephemeral_count = 0;
unsigned int predef_mem_ephemeral_total_count = sizeof(predef_mem_ephemeral);

char predef_mem_chronic[300000];
unsigned int predef_mem_chronic_count = 0;
unsigned int predef_mem_chronic_total_count = sizeof(predef_mem_chronic);

predef_mem_index_element predef_mem_ephemeral_index_element_array[6000];
unsigned int predef_mem_ephemeral_index_element_array_count = 0;
unsigned int predef_mem_ephemeral_index_element_array_total_count = sizeof(predef_mem_ephemeral_index_element_array);

predef_mem_index_element predef_mem_chronic_index_element_array[1500];
unsigned int predef_mem_chronic_index_element_array_count = 0;
unsigned int predef_mem_chronic_index_element_array_total_count = sizeof(predef_mem_chronic_index_element_array);

void initiate_memblock(unsigned int iclean){
	predef_mem_ephemeral_count = 0;
	predef_mem_chronic_count = 0;

	if(iclean > 0){
		memset(&predef_mem_ephemeral, 0, predef_mem_ephemeral_total_count);
		memset(&predef_mem_ephemeral_index_element_array[0], 0, (size_t) predef_mem_ephemeral_index_element_array_total_count);

		memset(&predef_mem_chronic[0], 0, (size_t) predef_mem_chronic_total_count);
		memset(&predef_mem_chronic_index_element_array[0], 0, (size_t) predef_mem_chronic_index_element_array_total_count);

		/*VALGRIND_MAKE_MEM_NOACCESS(&predef_mem_ephemeral[0], predef_mem_ephemeral_total_count);
		VALGRIND_MAKE_MEM_NOACCESS(&predef_mem_chronic[0], predef_mem_chronic_total_count);

		VALGRIND_CREATE_MEMPOOL(&predef_mem_ephemeral[0], 0, 1);
		VALGRIND_CREATE_MEMPOOL(&predef_mem_chronic[0], 0, 1);*/
	}
	else{
		/*VALGRIND_MAKE_MEM_NOACCESS(&predef_mem_ephemeral[0], predef_mem_ephemeral_total_count);
		VALGRIND_MAKE_MEM_NOACCESS(&predef_mem_chronic[0], predef_mem_chronic_total_count);

		VALGRIND_CREATE_MEMPOOL(&predef_mem_ephemeral[0], 0, 0);
		VALGRIND_CREATE_MEMPOOL(&predef_mem_chronic[0], 0, 0);*/
	}
}

unsigned int calloc_recorder[30000];
unsigned int calloc_recorder_count = 0;
void reset_memblock(unsigned int debug){
	if(debug > 0){
		/*int i;

		for(i = 0; i < predef_mem_ephemeral_index_element_array_count; i++){
			VALGRIND_FREELIKE_BLOCK(predef_mem_ephemeral_index_element_array[i].ptr_index, 0);
		}*/
	}
	else{
		memset(&predef_mem_ephemeral[0], 0, (size_t) predef_mem_ephemeral_count);
	}

	/*VALGRIND_DESTROY_MEMPOOL(&predef_mem_ephemeral[0]);
	VALGRIND_CREATE_MEMPOOL(&predef_mem_ephemeral[0], 0, 1);*/

	memset(&predef_mem_ephemeral_index_element_array[0], 0, (size_t) predef_mem_ephemeral_index_element_array_count);

	predef_mem_ephemeral_count = 0;
	predef_mem_ephemeral_index_element_array_count = 0;
}

void* memblock_allocate_smartly(unsigned int isize, unsigned int iclean_on_reset, unsigned int idebug_reset){
	void* allocated_ptr = NULL;

	if(isize > 0 && isize < 4294967295U){
		if(iclean_on_reset > 0 && (predef_mem_ephemeral_total_count - predef_mem_ephemeral_count) >= isize && predef_mem_ephemeral_index_element_array_count < predef_mem_ephemeral_index_element_array_total_count){
			allocated_ptr = (void*)(((unsigned int)&predef_mem_ephemeral[0]) + predef_mem_ephemeral_count);
			predef_mem_ephemeral_count += isize;

			predef_mem_ephemeral_index_element_array[predef_mem_ephemeral_index_element_array_count].ptr_index = allocated_ptr;
			predef_mem_ephemeral_index_element_array[predef_mem_ephemeral_index_element_array_count].size = isize;
			predef_mem_ephemeral_index_element_array[predef_mem_ephemeral_index_element_array_count].occupied = (unsigned char) 1;
			predef_mem_ephemeral_index_element_array_count++;

			/*if(idebug_reset > 0){
				VALGRIND_MALLOCLIKE_BLOCK(allocated_ptr,isize, 0, 1);
			}
			else{
				VALGRIND_MEMPOOL_ALLOC(&predef_mem_ephemeral[0], allocated_ptr,isize);
			}*/

			memset(allocated_ptr, 0, (size_t) isize);
		}
		else if(iclean_on_reset == 0 && (predef_mem_chronic_total_count - predef_mem_chronic_count) >= isize && predef_mem_chronic_index_element_array_count < predef_mem_chronic_index_element_array_total_count){
			allocated_ptr = (void*)(((unsigned int)&predef_mem_chronic[0]) + predef_mem_chronic_count);
			predef_mem_chronic_count += isize;

			predef_mem_chronic_index_element_array[predef_mem_chronic_index_element_array_count].ptr_index = allocated_ptr;
			predef_mem_chronic_index_element_array[predef_mem_chronic_index_element_array_count].size = isize;
			predef_mem_chronic_index_element_array[predef_mem_chronic_index_element_array_count].occupied = (unsigned char) 1;
			predef_mem_chronic_index_element_array_count++;

			/*VALGRIND_MALLOCLIKE_BLOCK(allocated_ptr,isize, 0, 1);*/
			memset(allocated_ptr, 0, (size_t) isize);
		}
		else if(iclean_on_reset == 0 && (predef_mem_chronic_total_count - predef_mem_chronic_count) < isize && predef_mem_chronic_index_element_array_count < predef_mem_chronic_index_element_array_total_count){
			int i;

			for(i = (predef_mem_chronic_index_element_array_count - 1); i >= 0; i--){
				if((predef_mem_chronic_index_element_array[i].size == isize
						|| (predef_mem_chronic_index_element_array[i].size > isize && predef_mem_chronic_index_element_array[i].size <= (isize + 12)))
						&& predef_mem_chronic_index_element_array[i].occupied == (unsigned char) 0){

					predef_mem_chronic_index_element_array[i].occupied = (unsigned char) 1;

					allocated_ptr = predef_mem_chronic_index_element_array[i].ptr_index;

					/*VALGRIND_MALLOCLIKE_BLOCK(allocated_ptr,predef_mem_chronic_index_element_array[i].size, 0, 1);*/
					memset(allocated_ptr, 0, (size_t) predef_mem_chronic_index_element_array[i].size);
					break;
				}
			}
		}
		else{
			printf("Error: Target size out of bounds.\n");
			ishidaeu_exit(222);
		}
	}
	return allocated_ptr;
}

unsigned int get_memblock_ephemeral_size(unsigned int imem_index){
	return predef_mem_ephemeral_index_element_array[imem_index].size;
}

void memblock_free(void* iptr){
	float i = 0.0f;
	float range_low = 0.0f;
	float range_high = (float) (predef_mem_chronic_index_element_array_count - 1);
	int search_point = ceil((range_high - range_low) /2.0f);
	float search_limit = ceil(sqrt((float) predef_mem_chronic_index_element_array_count));
	unsigned int freed = 0;

	if(((unsigned int) predef_mem_chronic_index_element_array[(predef_mem_chronic_index_element_array_count - 1)].ptr_index) == ((unsigned int) iptr)){
		predef_mem_chronic_index_element_array[(predef_mem_chronic_index_element_array_count - 1)].occupied = (unsigned char) 0;
		predef_mem_chronic_index_element_array_count = (predef_mem_chronic_index_element_array_count - 1);
		predef_mem_chronic_count -= predef_mem_chronic_index_element_array[(predef_mem_chronic_index_element_array_count - 1)].size;
		freed = 1;
	}
	else if(((unsigned int) predef_mem_chronic_index_element_array[0].ptr_index) == ((unsigned int) iptr)){
		predef_mem_chronic_index_element_array[0].occupied = (unsigned char) 0;
		predef_mem_chronic_index_element_array_count = 0;
		predef_mem_chronic_count -= predef_mem_chronic_index_element_array[0].size;
		freed = 1;
	}
	else{
		while(i < search_limit && search_point > 0 && search_point < (predef_mem_chronic_index_element_array_count - 1)){
			if(((unsigned int) predef_mem_chronic_index_element_array[search_point].ptr_index) == ((unsigned int) iptr)){
				predef_mem_chronic_index_element_array[search_point].occupied = (unsigned char) 0;
				freed = 1;
				break;
			}
			else{
				if(((unsigned int) predef_mem_chronic_index_element_array[search_point].ptr_index) > ((unsigned int) iptr)){
					range_high = ((float) search_point);
					search_point -= ceil((range_high - range_low) / 2.0f);
				}
				else{
					range_low = ((float) search_point);
					search_point += ceil((range_high - range_low) / 2.0f);
				}
			}

			i += 1.0f;
		}
	}

	/*if(freed == 1){
		VALGRIND_FREELIKE_BLOCK(iptr, 0);
	}
	else{
		printf("ERROR: INVALID FREE AT POINTER ADDRESS %p, INVESTIGATE FURTHER WITH VALGRIND\n", iptr);
		VALGRIND_FREELIKE_BLOCK(iptr, 0);
		VALGRIND_FREELIKE_BLOCK(iptr, 0);
		ishidaeu_exit(777);
	}*/
}
/*************************************** ISHIDAEUTZ_MEMORY_MANAGER END ***************************************/

/*************************************** ISHIDAEUTZ_LARGE_NUMBER START ***************************************/
ishidaeutz_LARGE_INT_ENDIANNESS ishidaopcua_SERVER_LARGE_INT_ENDIANNESS;

unsigned char convert_base10_single_digit_unsigned_string_char_to_unsigned_byte_char(const char* ipstring_char)
{
	if(ipstring_char == NULL){
		return  ((unsigned char) -1);
	}

    if(strcmp(ipstring_char, "0") == 0){
        return ((unsigned char) 0);
    }
    else if(strcmp(ipstring_char, "1") == 0){
        return ((unsigned char) 1);
    }
    else if(strcmp(ipstring_char, "2") == 0){
        return ((unsigned char) 2);
    }
    else if(strcmp(ipstring_char, "3") == 0){
        return ((unsigned char) 3);
    }
    else if(strcmp(ipstring_char, "4") == 0){
        return ((unsigned char) 4);
    }
    else if(strcmp(ipstring_char, "5") == 0){
        return ((unsigned char) 5);
    }
    else if(strcmp(ipstring_char, "6") == 0){
        return ((unsigned char) 6);
    }
    else if(strcmp(ipstring_char, "7") == 0){
        return ((unsigned char) 7);
    }
    else if(strcmp(ipstring_char, "8") == 0){
        return ((unsigned char) 8);
    }
    else if(strcmp(ipstring_char, "9") == 0){
        return ((unsigned char) 9);
    }
    else {
    	return ((unsigned char) -1);
    }
}

const char* convert_base10_single_digit_unsigned_byte_char_to_unsigned_string_char(unsigned char ibyte_char){
	char* pstring_char = ishidaeutz_malloc(2,sizeof(unsigned char));
	pstring_char[1] = '\0';

    if(ibyte_char == ((unsigned char) 0)){
    	pstring_char[0] = '0';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 1)){
    	pstring_char[0] = '1';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 2)){
    	pstring_char[0] = '2';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 3)){
    	pstring_char[0] = '3';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 4)){
    	pstring_char[0] = '4';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 5)){
    	pstring_char[0] = '5';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 6)){
    	pstring_char[0] = '6';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 7)){
    	pstring_char[0] = '7';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 8)){
    	pstring_char[0] = '8';
    	return pstring_char;
    }
    else if(ibyte_char == ((unsigned char) 9)){
    	pstring_char[0] = '9';
    	return pstring_char;
    }
    else{
    	/*memblock_free(pstring_char);*/
    	return '\0';
    }
}

ishidaeutz_LARGE_INT* create_ishidaeutz_LARGE_INT_from_int(int inumber_int)
{
	ishidaeutz_LARGE_INT_SIGN sign = ((inumber_int >= 0)? ishidaeutz_LARGE_INT_SIGN_POSITIVE: ishidaeutz_LARGE_INT_SIGN_NEGATIVE);
	return create_ishidaeutz_LARGE_INT_from_unsigned_int(((unsigned int)(sign * inumber_int)), sign);
}


ishidaeutz_LARGE_INT* create_ishidaeutz_LARGE_INT_from_unsigned_int(unsigned int inumber_int, ishidaeutz_LARGE_INT_SIGN isign)
{
	ishidaeutz_LARGE_INT* plarge_int = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
	unsigned char* number_bytestring = ishidaeutz_malloc(10, sizeof(unsigned char));

	unsigned int i;
	unsigned int intermediate_number_int = inumber_int;

	if(inumber_int > 0 && number_bytestring != NULL){
		plarge_int->number_sign = (isign >= 0 || inumber_int == 0)? ishidaeutz_LARGE_INT_SIGN_POSITIVE : ishidaeutz_LARGE_INT_SIGN_NEGATIVE;

		for(i = 0; (i < 10 && intermediate_number_int > 0); i++){
			number_bytestring[9 - i] = (unsigned char)(intermediate_number_int%10);
			intermediate_number_int = (intermediate_number_int/10);
		}

		plarge_int->number_bytestring_size = i;

		if(plarge_int->number_bytestring_size < 10){
			plarge_int->number_bytestring = ishidaeutz_malloc(plarge_int->number_bytestring_size, sizeof(unsigned char));
			memcpy(plarge_int->number_bytestring, (unsigned char*)(((unsigned int) number_bytestring) + (10 - plarge_int->number_bytestring_size)) , plarge_int->number_bytestring_size);
			/*memblock_free(number_bytestring);*/
		}
		else{
			plarge_int->number_bytestring = number_bytestring;
		}
	}
	else{
		plarge_int->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
		plarge_int->number_bytestring_size = 1;
		plarge_int->number_bytestring = ishidaeutz_malloc(plarge_int->number_bytestring_size, sizeof(unsigned char));
	}

	return plarge_int;
}

void stretch_ishidaeutz_LARGE_INT(ishidaeutz_LARGE_INT* iplarge_int, unsigned char istretch_scale)
{
	if(iplarge_int != NULL && iplarge_int->number_bytestring != NULL && iplarge_int->number_bytestring_size > 0 && istretch_scale > 0){

		unsigned char* number_bytestring = ishidaeutz_malloc(istretch_scale, sizeof(unsigned char));

		if(number_bytestring != NULL && istretch_scale > iplarge_int->number_bytestring_size){
			memcpy(number_bytestring, iplarge_int->number_bytestring, iplarge_int->number_bytestring_size);
			iplarge_int->number_bytestring_size = istretch_scale;
			/*memblock_free(iplarge_int->number_bytestring);*/
			iplarge_int->number_bytestring = number_bytestring;
		}
		else if(number_bytestring != NULL && istretch_scale < iplarge_int->number_bytestring_size){
			memcpy(number_bytestring, iplarge_int->number_bytestring, istretch_scale);
			iplarge_int->number_bytestring_size = istretch_scale;
			/*memblock_free(iplarge_int->number_bytestring);*/
			iplarge_int->number_bytestring = number_bytestring;
		}
		else{
			/*memblock_free(number_bytestring);*/
		}
	}
}

ishidaeutz_LARGE_INT* create_ishidaeutz_LARGE_INT_from_ASCII_STRING(const char* ipnumber_string, ishidaeutz_LARGE_INT_SIGN inumber_sign)
{
	ishidaeutz_LARGE_INT* plarge_int = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
	int i;
	int tracking_index = 0;
	unsigned int first_non_zero_digit_spotted = 0;
	char number_string_digit[2];

	if(ipnumber_string == NULL || strlen(ipnumber_string) <= 0){
		return NULL;
	}

	plarge_int->number_bytestring_size = strlen(ipnumber_string);
	plarge_int->number_bytestring = ishidaeutz_malloc(plarge_int->number_bytestring_size, sizeof(unsigned char));

	if(inumber_sign != ishidaeutz_LARGE_INT_SIGN_POSITIVE && inumber_sign != ishidaeutz_LARGE_INT_SIGN_NEGATIVE){
		errno = 378915;
		puts("ishidaeutz_LARGE_INT_ERROR: Number sign is not valid.");
		ishidaeu_exit(378915);
	}

	plarge_int->number_sign = inumber_sign;

    for(i = 0; i < 	plarge_int->number_bytestring_size; i++){
    	number_string_digit[1] = '\0';
        number_string_digit[0] = ipnumber_string[i];
    	plarge_int->number_bytestring[tracking_index] = convert_base10_single_digit_unsigned_string_char_to_unsigned_byte_char(&number_string_digit[0]);

    	if(first_non_zero_digit_spotted == 0 && plarge_int->number_bytestring[tracking_index] > 0){
    		first_non_zero_digit_spotted = 1;
    	}

    	if(first_non_zero_digit_spotted == 1){
    		tracking_index++;
    	}
    }

    if(tracking_index == 0){
    	tracking_index++;
    }

    if(tracking_index> 0 && tracking_index < plarge_int->number_bytestring_size){
    	unsigned char* ptemp_number_bytestring = ishidaeutz_malloc(sizeof(unsigned char), tracking_index);

    	if(ptemp_number_bytestring == NULL){
    		errno = 90267;
    		puts("ishidaeutz_LARGE_INT_ERROR: Failed to create large number.");
    		if(plarge_int != NULL){
    			if(plarge_int->number_bytestring != NULL){
    				/*memblock_free(plarge_int->number_bytestring);*/
					plarge_int->number_bytestring = NULL;
				}

    			/*memblock_free(plarge_int);*/
    			plarge_int = NULL;
			}

    		ishidaeu_exit(errno);
    	}

    	ishidaeutz_copy_byte_array(((char*) plarge_int->number_bytestring), ((char*) ptemp_number_bytestring),0U,0U, plarge_int->number_bytestring_size);

    	plarge_int->number_bytestring = ptemp_number_bytestring;
    	plarge_int->number_bytestring_size = tracking_index;
    }

    return plarge_int;
}

const char* get_ishidaeutz_LARGE_INT_as_ASCII_string_array(ishidaeutz_LARGE_INT* iplarge_int)
{
	if(iplarge_int != NULL && iplarge_int->number_bytestring_size > 0){
		char* plarge_int_string = ishidaeutz_malloc((iplarge_int->number_bytestring_size+1),sizeof(char));
		int i;
		unsigned char all_digits_are_zeros = 1;
		char* converted_string_char;

		if(iplarge_int == NULL || iplarge_int->number_bytestring == NULL){
			return NULL;
		}

		plarge_int_string[iplarge_int->number_bytestring_size] = '\0';

		for(i = 0; i < iplarge_int->number_bytestring_size; i++){
			converted_string_char = (char*) convert_base10_single_digit_unsigned_byte_char_to_unsigned_string_char(iplarge_int->number_bytestring[i]);

			if(iplarge_int->number_bytestring[i] > 0){
				all_digits_are_zeros = 0;
			}

			if(converted_string_char != NULL){
				plarge_int_string[i] = converted_string_char[0];
				/*memblock_free(converted_string_char);*/
			}

		}
		if(all_digits_are_zeros == 1){
			plarge_int_string[0] = '0';
			plarge_int_string[1] = '\0';
		}
		return plarge_int_string;
	}

	return '\0';
}

unsigned int get_ishidaeutz_LARGE_INT_size(ishidaeutz_LARGE_INT* iplarge_int){
	if(iplarge_int == NULL){
		return 0;
	}

	return iplarge_int->number_bytestring_size;
}

unsigned int get_ishidaeutz_LARGE_INT_yielded_int_byte_array_size(ishidaeutz_LARGE_INT* iplarge_int){
	if(iplarge_int == NULL){
		return 0;
	}

	return iplarge_int->int_bytearray_size;
}

ishidaeutz_LARGE_INT_SIGN get_ishidaeutz_LARGE_INT_sign(ishidaeutz_LARGE_INT* iplarge_int){
	if(iplarge_int == NULL){
		return 0;
	}

	return iplarge_int->number_sign;
}

char compare_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2, char icompare_absolute)
{
    int i;
    unsigned char both_large_ints_are_the_same = 1;
    unsigned char first_non_zero_digit_spotted = 0;
    char response = 0;
    ishidaeutz_LARGE_INT_SIGN large_int_1_sign = iplarge_int_1->number_sign;
    ishidaeutz_LARGE_INT_SIGN large_int_2_sign = iplarge_int_2->number_sign;

    if(icompare_absolute == 1){
    	iplarge_int_1->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
    	iplarge_int_2->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
    }

    if(iplarge_int_1 == NULL || iplarge_int_2 == NULL){
    	if(icompare_absolute == 1){
			iplarge_int_1->number_sign = large_int_1_sign;
			iplarge_int_2->number_sign = large_int_2_sign;
		}

    	return 255;
    }
    else if(iplarge_int_1->number_sign == ishidaeutz_LARGE_INT_SIGN_POSITIVE && iplarge_int_1->number_sign == iplarge_int_2->number_sign
    		&& iplarge_int_1->number_bytestring_size > iplarge_int_2->number_bytestring_size){
    	if(icompare_absolute == 1){
			iplarge_int_1->number_sign = large_int_1_sign;
			iplarge_int_2->number_sign = large_int_2_sign;
		}

    	return 1;
    }
    else if(iplarge_int_1->number_sign == ishidaeutz_LARGE_INT_SIGN_POSITIVE && iplarge_int_1->number_sign == iplarge_int_2->number_sign
			&& iplarge_int_1->number_bytestring_size < iplarge_int_2->number_bytestring_size){
    	if(icompare_absolute == 1){
			iplarge_int_1->number_sign = large_int_1_sign;
			iplarge_int_2->number_sign = large_int_2_sign;
		}

		return -1;
	}
    else if(iplarge_int_1->number_sign == ishidaeutz_LARGE_INT_SIGN_NEGATIVE && iplarge_int_1->number_sign == iplarge_int_2->number_sign
			&& iplarge_int_1->number_bytestring_size > iplarge_int_2->number_bytestring_size){
    	if(icompare_absolute == 1){
			iplarge_int_1->number_sign = large_int_1_sign;
			iplarge_int_2->number_sign = large_int_2_sign;
		}

		return -1;
	}
	else if(iplarge_int_1->number_sign == ishidaeutz_LARGE_INT_SIGN_NEGATIVE && iplarge_int_1->number_sign == iplarge_int_2->number_sign
			&& iplarge_int_1->number_bytestring_size < iplarge_int_2->number_bytestring_size){
		if(icompare_absolute == 1){
			iplarge_int_1->number_sign = large_int_1_sign;
			iplarge_int_2->number_sign = large_int_2_sign;
		}

		return 1;
	}
	else if(iplarge_int_1->number_sign == ishidaeutz_LARGE_INT_SIGN_POSITIVE && iplarge_int_1->number_sign != iplarge_int_2->number_sign)
	{
		if(icompare_absolute == 1){
			iplarge_int_1->number_sign = large_int_1_sign;
			iplarge_int_2->number_sign = large_int_2_sign;
		}

		return 1;
	}
	else if(iplarge_int_1->number_sign == ishidaeutz_LARGE_INT_SIGN_NEGATIVE && iplarge_int_1->number_sign != iplarge_int_2->number_sign)
	{
		if(icompare_absolute == 1){
			iplarge_int_1->number_sign = large_int_1_sign;
			iplarge_int_2->number_sign = large_int_2_sign;
		}

		return -1;
	}

    for(i=0; i < iplarge_int_1->number_bytestring_size && i < iplarge_int_2->number_bytestring_size; i++){
        if(response == 0 &&
        		(first_non_zero_digit_spotted == 1 || (first_non_zero_digit_spotted ==0 && (iplarge_int_1->number_bytestring[i] != 0
        				&& iplarge_int_2->number_bytestring[i] != 0)))
				&& ((iplarge_int_2->number_bytestring[i] & iplarge_int_1->number_bytestring[i]) < iplarge_int_1->number_bytestring[i])
				&& (iplarge_int_2->number_bytestring[i] < iplarge_int_1->number_bytestring[i])){

            response = 1;
            both_large_ints_are_the_same = 0;
            break;
        }
        else if(response == 0 && (first_non_zero_digit_spotted == 1 || (first_non_zero_digit_spotted ==0 && (iplarge_int_1->number_bytestring[i] != 0 && iplarge_int_2->number_bytestring[i] != 0)))&& ((iplarge_int_2->number_bytestring[i] & iplarge_int_1->number_bytestring[i]) < iplarge_int_2->number_bytestring[i]) && (iplarge_int_2->number_bytestring[i] > iplarge_int_1->number_bytestring[i])){
            response = -1;
            both_large_ints_are_the_same = 0;
            break;
        }
        else if(iplarge_int_1->number_bytestring[i] > 0 || iplarge_int_2->number_bytestring[i] > 0){
            first_non_zero_digit_spotted = 1;

            if(iplarge_int_1->number_bytestring[i] > iplarge_int_2->number_bytestring[i]){
				response = 1;
				both_large_ints_are_the_same = 0;
            }
            else if(iplarge_int_2->number_bytestring[i] > iplarge_int_1->number_bytestring[i]){
				response = -1;
				both_large_ints_are_the_same = 0;
			}
        }
    }

    if(icompare_absolute == 1){
		iplarge_int_1->number_sign = large_int_1_sign;
		iplarge_int_2->number_sign = large_int_2_sign;
	}

    if(both_large_ints_are_the_same == 1){
        return 0;
    }
    else{
        return (response * iplarge_int_1->number_sign);
    }
}

ishidaeutz_LARGE_INT* sum_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2)
{
	ishidaeutz_LARGE_INT* presult = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
	int i;
	char summation_component_1;
	char summation_component_2;
	char summation_component_total = 0;
	char summation_component_carry_1 = 0;
	char summation_component_carry_2 = 0;
	unsigned int last_non_zero_digit_spotted_index = 0;
	char comparison;

	if(iplarge_int_1 == NULL || iplarge_int_2 == NULL){
		return NULL;
	}

    comparison = compare_ishidaeutz_LARGE_INTs(iplarge_int_1, iplarge_int_2, 1);

    if(comparison == 0){
    	presult->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
    	presult->number_bytestring_size = iplarge_int_1->number_bytestring_size;
    }

    if(comparison == 1){
    	presult->number_bytestring_size = iplarge_int_1->number_bytestring_size + 1;
		presult->number_sign = iplarge_int_1->number_sign;
	}

    if(comparison == -1){
    	presult->number_bytestring_size = iplarge_int_2->number_bytestring_size + 1;
		presult->number_sign = iplarge_int_2->number_sign;
	}

    if(presult->number_bytestring_size > 0){
    	char* number_bytestring = ishidaeutz_malloc(presult->number_bytestring_size, sizeof(char));

		for(i = 0; i < presult->number_bytestring_size; i++){
			summation_component_1 = 0;
			summation_component_2 = 0;

			if(i < iplarge_int_1->number_bytestring_size && (comparison == 1 || comparison == 0)){
				summation_component_1 = iplarge_int_1->number_bytestring[(iplarge_int_1->number_bytestring_size -1) - i];
			}
			else if(i < iplarge_int_2->number_bytestring_size && comparison == -1){
				summation_component_1 = iplarge_int_2->number_bytestring[(iplarge_int_2->number_bytestring_size -1) - i];
			}

			if(i < iplarge_int_2->number_bytestring_size && (comparison == 1 || comparison == 0)){
				summation_component_2 = iplarge_int_2->number_bytestring[(iplarge_int_2->number_bytestring_size -1) - i];
			}
			else if(i < iplarge_int_1->number_bytestring_size && comparison == -1){
				summation_component_2 = iplarge_int_1->number_bytestring[(iplarge_int_1->number_bytestring_size -1) - i];
			}
			summation_component_1 += (summation_component_carry_1 + summation_component_carry_2);
			summation_component_carry_1 = 0;
			summation_component_carry_2 = 0;

			if(summation_component_1 < summation_component_2 && iplarge_int_1->number_sign != iplarge_int_2->number_sign){
				summation_component_carry_1 = -1;
				summation_component_1 += 10;
				summation_component_total  = summation_component_1 - summation_component_2;
			}
			else if(summation_component_1 == summation_component_2 && iplarge_int_1->number_sign != iplarge_int_2->number_sign){
				summation_component_total  = 0;
			}
			else{
				summation_component_total  = (iplarge_int_1->number_sign * summation_component_1) + (iplarge_int_2->number_sign * summation_component_2);
				summation_component_carry_1 = (summation_component_total / 10);
				summation_component_total =	(summation_component_total % 10);
			}
			number_bytestring[(presult->number_bytestring_size - 1) - i] = summation_component_total;

			if(summation_component_total > 0){
				last_non_zero_digit_spotted_index = (i + 1);
			}
		}

		presult->number_bytestring = ishidaeutz_malloc(last_non_zero_digit_spotted_index, sizeof(char));

		for(i = 0; (i < presult->number_bytestring_size  && i < last_non_zero_digit_spotted_index); i++){
			presult->number_bytestring[(last_non_zero_digit_spotted_index - 1) - i] = number_bytestring[(presult->number_bytestring_size -1) - i];
		}

		presult->number_bytestring_size = last_non_zero_digit_spotted_index;

		/*memblock_free(number_bytestring);*/
    }

    return presult;
}

void divide_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2, ishidaeutz_LARGE_INT** ippquotient, ishidaeutz_LARGE_INT** ippreminder)
{
	char comparison = compare_ishidaeutz_LARGE_INTs(iplarge_int_1, iplarge_int_2, 1);

	if(comparison == 1){
		ishidaeutz_LARGE_INT* pintermediate_dividend = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		ishidaeutz_LARGE_INT* pintermediate_divisor = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		ishidaeutz_LARGE_INT* divisions_increment_unit = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		ishidaeutz_LARGE_INT* pintermediate_sum;
		ishidaeutz_LARGE_INT* pintermediate_quotient;
		char stretch;

		pintermediate_dividend->number_bytestring_size = iplarge_int_1->number_bytestring_size;
		pintermediate_dividend->number_bytestring  = ishidaeutz_malloc(pintermediate_dividend->number_bytestring_size, sizeof(char));
		pintermediate_dividend->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
		memcpy(pintermediate_dividend->number_bytestring, iplarge_int_1->number_bytestring, pintermediate_dividend->number_bytestring_size);

		pintermediate_divisor->number_bytestring_size = iplarge_int_2->number_bytestring_size;
		pintermediate_divisor->number_bytestring  = ishidaeutz_malloc(pintermediate_divisor->number_bytestring_size, sizeof(char));
		pintermediate_divisor->number_sign = ishidaeutz_LARGE_INT_SIGN_NEGATIVE;
		memcpy(pintermediate_divisor->number_bytestring, iplarge_int_2->number_bytestring, pintermediate_divisor->number_bytestring_size);

		*ippquotient = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		(*ippquotient)->number_bytestring_size = 1;
		(*ippquotient)->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
		(*ippquotient)->number_bytestring = ishidaeutz_malloc((*ippquotient)->number_bytestring_size, sizeof(char));

		divisions_increment_unit->number_bytestring_size = 1;
		divisions_increment_unit->number_bytestring = ishidaeutz_malloc(divisions_increment_unit->number_bytestring_size, sizeof(char));
		divisions_increment_unit->number_bytestring[0] = 1;
		divisions_increment_unit->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;

		while(comparison == 1 || comparison == 0){
			if(pintermediate_dividend->number_bytestring_size > iplarge_int_2->number_bytestring_size){
				stretch = (pintermediate_dividend->number_bytestring[0] > iplarge_int_2->number_bytestring[0])? pintermediate_dividend->number_bytestring_size: pintermediate_dividend->number_bytestring_size - 1;
				if(stretch >= iplarge_int_2->number_bytestring_size && stretch != pintermediate_divisor->number_bytestring_size){
					stretch_ishidaeutz_LARGE_INT(pintermediate_divisor, stretch);
				}
			}
			else if(pintermediate_dividend->number_bytestring_size <= iplarge_int_2->number_bytestring_size && pintermediate_divisor->number_bytestring_size > iplarge_int_2->number_bytestring_size){
				stretch_ishidaeutz_LARGE_INT(pintermediate_divisor, iplarge_int_2->number_bytestring_size);
			}

			if(pintermediate_divisor->number_bytestring_size < iplarge_int_2->number_bytestring_size){
				ishidaeu_exit(10000);
			}
			pintermediate_sum = sum_ishidaeutz_LARGE_INTs(pintermediate_dividend, pintermediate_divisor);

			if(pintermediate_dividend!= NULL){
				if(pintermediate_dividend->number_bytestring != NULL){
					/*memblock_free(pintermediate_dividend->number_bytestring);*/
				}
				/*memblock_free(pintermediate_dividend);*/
			}

			pintermediate_dividend = pintermediate_sum;

			comparison = compare_ishidaeutz_LARGE_INTs(pintermediate_dividend, iplarge_int_2, 1);

			stretch_ishidaeutz_LARGE_INT(divisions_increment_unit, (pintermediate_divisor->number_bytestring_size - iplarge_int_2->number_bytestring_size) + 1);

			pintermediate_quotient = sum_ishidaeutz_LARGE_INTs(*ippquotient, divisions_increment_unit);

			if((*ippquotient) != NULL){
				if((*ippquotient)->number_bytestring != NULL){
					/*memblock_free((*ippquotient)->number_bytestring);*/
				}
				/*memblock_free(*ippquotient);*/
			}
			*ippquotient = pintermediate_quotient;
		}

		(*ippquotient)->number_sign = iplarge_int_1->number_sign * iplarge_int_2->number_sign;

		(*ippreminder) = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		(*ippreminder)->number_bytestring_size = pintermediate_dividend->number_bytestring_size;
		(*ippreminder)->number_sign = (*ippquotient)->number_sign;
		(*ippreminder)->number_bytestring = ishidaeutz_malloc((*ippreminder)->number_bytestring_size, sizeof(char));

		memcpy((*ippreminder)->number_bytestring, pintermediate_dividend->number_bytestring, (*ippreminder)->number_bytestring_size);

		if(pintermediate_dividend!= NULL){
			if(pintermediate_dividend->number_bytestring != NULL){
				/*memblock_free(pintermediate_dividend->number_bytestring);*/
			}
			/*memblock_free(pintermediate_dividend);*/
		}

		if(pintermediate_divisor!= NULL){
			if(pintermediate_divisor->number_bytestring != NULL){
				/*memblock_free(pintermediate_divisor->number_bytestring);*/
			}
			/*memblock_free(pintermediate_divisor);*/
		}

		if(divisions_increment_unit!= NULL){
			if(divisions_increment_unit->number_bytestring != NULL){
				/*memblock_free(divisions_increment_unit->number_bytestring);*/
			}
			/*memblock_free(divisions_increment_unit);*/
		}
	}
	else if(comparison == 0){
		*ippquotient = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		(*ippquotient)->number_bytestring_size = 1;
		(*ippquotient)->number_bytestring = ishidaeutz_malloc((*ippquotient)->number_bytestring_size, sizeof(char));
		(*ippquotient)->number_bytestring[0] = 1;
		(*ippquotient)->number_sign = iplarge_int_1->number_sign * iplarge_int_2->number_sign;

		(*ippreminder) = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		(*ippreminder)->number_bytestring_size = 1;
		(*ippreminder)->number_sign = (*ippquotient)->number_sign;
		(*ippreminder)->number_bytestring = ishidaeutz_malloc((*ippquotient)->number_bytestring_size, sizeof(char));
	}
	else if(comparison == -1){
		(*ippquotient) = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		(*ippquotient)->number_bytestring_size = 1;
		(*ippquotient)->number_sign = iplarge_int_1->number_sign * iplarge_int_2->number_sign;
		(*ippquotient)->number_bytestring = ishidaeutz_malloc((*ippquotient)->number_bytestring_size, sizeof(char));

		(*ippreminder) = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		(*ippreminder)->number_bytestring_size = iplarge_int_1->number_bytestring_size;
		(*ippreminder)->number_sign = (*ippquotient)->number_sign;
		(*ippreminder)->number_bytestring = ishidaeutz_malloc((*ippreminder)->number_bytestring_size, sizeof(char));

		memcpy((*ippreminder)->number_bytestring, iplarge_int_1->number_bytestring, (*ippreminder)->number_bytestring_size);
	}
}

ishidaeutz_LARGE_INT* multiply_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2)
{
	ishidaeutz_LARGE_INT* pproduct = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
	unsigned char number_bytestring_size = iplarge_int_1->number_bytestring_size + iplarge_int_2->number_bytestring_size;
	unsigned char* number_bytestring  = ishidaeutz_malloc(number_bytestring_size, sizeof(unsigned char));
	unsigned char count = 0;

	if(!((iplarge_int_1->number_bytestring_size == 1 && iplarge_int_1->number_bytestring[0] == 0) ||
			(iplarge_int_2->number_bytestring_size == 1 && iplarge_int_2->number_bytestring[0] == 0))){

		unsigned int multiplicand_index,multiplier_index;
		unsigned short result;
		unsigned char result_carry = 0;
		unsigned short intermediate = 0;
		int carry_over_index;

		pproduct->number_sign = iplarge_int_1->number_sign * iplarge_int_2->number_sign;

		for(multiplicand_index = 0; multiplicand_index < iplarge_int_1->number_bytestring_size ; multiplicand_index++){
			result_carry = 0;
			for(multiplier_index = 0; multiplier_index < iplarge_int_2->number_bytestring_size ; multiplier_index++){
				result = iplarge_int_1->number_bytestring[iplarge_int_1->number_bytestring_size - 1 - multiplicand_index] * iplarge_int_2->number_bytestring[iplarge_int_2->number_bytestring_size - 1 - multiplier_index] ;

				intermediate = result + number_bytestring[number_bytestring_size - 1 - (multiplier_index+multiplicand_index)];
				result_carry = intermediate / 10;
				number_bytestring[number_bytestring_size - 1 - (multiplier_index+multiplicand_index)] = intermediate % 10;
				carry_over_index = (number_bytestring_size - 2 - (multiplier_index + multiplicand_index));

				if(result_carry > 0 && carry_over_index >= 0){
					number_bytestring[carry_over_index]  += result_carry;
				}

				if(count < (multiplier_index+multiplicand_index)){
					count = multiplier_index+multiplicand_index;
				}
			}
		}

		if(result_carry > 0 && carry_over_index >= 0){
			count++;
		}
	}

	pproduct->number_bytestring_size = count + 1;

	if(pproduct->number_bytestring_size < number_bytestring_size)
	{
		pproduct->number_bytestring = ishidaeutz_malloc(pproduct->number_bytestring_size, sizeof(unsigned char));
		memcpy(pproduct->number_bytestring, (unsigned char*)(((unsigned int) number_bytestring) + (number_bytestring_size - pproduct->number_bytestring_size)), pproduct->number_bytestring_size);
		/*memblock_free(number_bytestring);*/
	}
	else{
		pproduct->number_bytestring = number_bytestring;
	}

	return pproduct;
}

void yield_ishidaeutz_LARGE_INT_int_byte_array(ishidaeutz_LARGE_INT* iplarge_int, ishidaeutz_LARGE_INT_ENDIANNESS iexpected_endianness, unsigned char expected_byte_array_size)
{

	if(iplarge_int != NULL && iplarge_int->number_bytestring != NULL && iplarge_int->number_bytestring_size > 0){

		ishidaeutz_LARGE_INT* pbyte_value = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		ishidaeutz_LARGE_INT* pintermediate_dividend = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
		ishidaeutz_LARGE_INT* pquotient = pintermediate_dividend;
		ishidaeutz_LARGE_INT* preminder;
		unsigned char index = 0;
		unsigned char reminder;

		if(iplarge_int->int_bytearray != NULL && iplarge_int->int_bytearray_size > 0){
			/*memblock_free(iplarge_int->int_bytearray);*/
			iplarge_int->int_bytearray_size = 0;
		}

		if(expected_byte_array_size > 0 && expected_byte_array_size < ishidaeutz_LARGE_INT_DEFAULT_MAX_BYTE_ARRAY_SIZE){
			iplarge_int->int_bytearray_size = expected_byte_array_size;
		}
		else{
			iplarge_int->int_bytearray_size = ishidaeutz_LARGE_INT_DEFAULT_MAX_BYTE_ARRAY_SIZE;
		}

		iplarge_int->int_bytearray = ishidaeutz_malloc(iplarge_int->int_bytearray_size, sizeof(char));

		pbyte_value->number_bytestring_size = 3;
		pbyte_value->number_bytestring = ishidaeutz_malloc(pbyte_value->number_bytestring_size, sizeof(char));
		pbyte_value->number_bytestring[0] = 2;
		pbyte_value->number_bytestring[1] = 5;
		pbyte_value->number_bytestring[2] = 6;
		pbyte_value->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;

		pintermediate_dividend->number_bytestring_size = iplarge_int->number_bytestring_size;
		pintermediate_dividend->number_bytestring  = ishidaeutz_malloc(pintermediate_dividend->number_bytestring_size, sizeof(char));
		pintermediate_dividend->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
		memcpy(pintermediate_dividend->number_bytestring, iplarge_int->number_bytestring, pintermediate_dividend->number_bytestring_size);

		while(!(pquotient->number_bytestring_size == 1 && pquotient->number_bytestring[0] == 0)){
			if(iplarge_int->int_bytearray_size <= index){
				errno = 198876;
				puts("ishidaeutz_LARGE_INT_ERROR: Overflow error.");
				ishidaeu_exit(errno);
			}

			divide_ishidaeutz_LARGE_INTs(pintermediate_dividend, pbyte_value, &pquotient, &preminder);

			if(pintermediate_dividend != NULL){
				if(pintermediate_dividend->number_bytestring != NULL){
					/*memblock_free(pintermediate_dividend->number_bytestring);*/
				}
				/*memblock_free(pintermediate_dividend);*/
			}

			pintermediate_dividend = pquotient;
			reminder = 0;
			if(preminder->number_bytestring_size == 1){
				reminder = preminder->number_bytestring[0];

			}
			else if(preminder->number_bytestring_size == 2){
				reminder = (preminder->number_bytestring[0] * 10) + preminder->number_bytestring[1];
			}
			else if(preminder->number_bytestring_size == 3){
				reminder = (preminder->number_bytestring[0] * 100) + (preminder->number_bytestring[1] * 10) + preminder->number_bytestring[2];
			}

			if(iexpected_endianness == ishidaeutz_LARGE_INT_ENDIANNESS_LITTLE_ENDIAN){
				iplarge_int->int_bytearray[index] = reminder;
			}
			else if(iexpected_endianness == ishidaeutz_LARGE_INT_ENDIANNESS_BIG_ENDIAN){
				iplarge_int->int_bytearray[iplarge_int->int_bytearray_size - 1 - index] = reminder;
			}

			if(preminder != NULL){
				if(preminder->number_bytestring != NULL){
					/*memblock_free(preminder->number_bytestring);*/
				}
				/*memblock_free(preminder);*/
			}

			index++;
		}

		if(pintermediate_dividend != NULL){
			if(pintermediate_dividend->number_bytestring != NULL){
				/*memblock_free(pintermediate_dividend->number_bytestring);*/
			}
			/*memblock_free(pintermediate_dividend);*/
		}

		if(pbyte_value != NULL){
			if(pbyte_value->number_bytestring != NULL){
				/*memblock_free(pbyte_value->number_bytestring);*/
			}
			/*memblock_free(pbyte_value);*/
		}

		if(index < iplarge_int->int_bytearray_size && iplarge_int->int_bytearray != NULL && iplarge_int->int_bytearray_size > 0 && index > 0){
			unsigned char* pnumber_byte_array = ishidaeutz_malloc(index, sizeof(unsigned char));
			if(pnumber_byte_array != NULL && iexpected_endianness == ishidaeutz_LARGE_INT_ENDIANNESS_LITTLE_ENDIAN && iplarge_int->int_bytearray != NULL && iplarge_int->int_bytearray_size){
				memcpy(pnumber_byte_array, iplarge_int->int_bytearray, index);

				if(iplarge_int->int_bytearray != NULL){
					/*memblock_free(iplarge_int->int_bytearray);*/
				}
				iplarge_int->int_bytearray = pnumber_byte_array;
				iplarge_int->int_bytearray_size = index;
			}
			else if(pnumber_byte_array != NULL &&  iexpected_endianness == ishidaeutz_LARGE_INT_ENDIANNESS_BIG_ENDIAN && iplarge_int->int_bytearray != NULL && iplarge_int->int_bytearray_size){
				memcpy(pnumber_byte_array, (unsigned char*)(((unsigned int)iplarge_int->int_bytearray) + (iplarge_int->int_bytearray_size - index)), index);

				if(iplarge_int->int_bytearray != NULL){
					/*memblock_free(iplarge_int->int_bytearray);*/
				}
				iplarge_int->int_bytearray = pnumber_byte_array;
				iplarge_int->int_bytearray_size = index;
			}
			else{
				if(pnumber_byte_array != NULL){
					/*memblock_free(pnumber_byte_array);*/
				}
			}
		}
	}
}

/*va_list vargs;
void Message(0,const char* text, ... )
{
	memset(&vargs, 0, sizeof(va_list));
	va_start(vargs, text);
	Message(0, text, vargs);
	puts("\n");
	va_end(vargs);
}*/

void ishidaeu_exit(int ierror_code)
{
	printf("Exit called, error code: %d\n", ierror_code);
	for(;;){};
}

static char HEX [] = { '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9' ,'A', 'B', 'C', 'D', 'E', 'F' };

int ishidaeutz_unsigned_int_to_ASCII_HEX_string_array(unsigned int num,char* buff)
{
    int len=0,k=0;
    do
    {
        buff[len] = HEX[num&0xF];
        len++;
        num>>=4;
    }while(num!=0);

    for(;k<len/2;k++)
    {
        buff[k]^=buff[len-k-1];
        buff[len-k-1]^=buff[k];
        buff[k]^=buff[len-k-1];
    }

    buff[len]='\0';
    return len;
}

const char* get_ishidaeutz_LARGE_INT_as_ASCII_HEX_string_array(ishidaeutz_LARGE_INT* iplarge_int)
{
	unsigned short nibbles = (iplarge_int->int_bytearray_size * 2);
	char* char_array_s = ishidaeutz_malloc((nibbles + 1), sizeof(char));
	int i;
	unsigned char ch;
	char buff1[2], buff2[2];

	char_array_s[nibbles] = '\0';
	for(i = 0; i < iplarge_int->int_bytearray_size; i++){
		ch = *((unsigned char*)(((unsigned int)(iplarge_int->int_bytearray)) + i));

		ishidaeutz_unsigned_int_to_ASCII_HEX_string_array((unsigned int)(ch & 0xF0),(char*) &buff1);
		ishidaeutz_unsigned_int_to_ASCII_HEX_string_array((ch & 0x0F),(char*) &buff2);
		char_array_s[(i*2)] = buff1[0];
		char_array_s[((i*2) + 1)] = buff2[0];
	}

	return char_array_s;
}

void yield_ishidaeutz_LARGE_INT_IEEE754_double_precision_byte_array(ishidaeutz_LARGE_INT* iplarge_int){
	/*printf("Yielding iEEE754 dp for %s\n", get_ishidaeutz_LARGE_INT_as_ASCII_string_array(iplarge_int));*/

	if(iplarge_int != NULL && iplarge_int->number_bytestring != NULL && iplarge_int->number_bytestring_size > 0){
		yield_ishidaeutz_LARGE_INT_int_byte_array(iplarge_int, ishidaeutz_LARGE_INT_ENDIANNESS_LITTLE_ENDIAN, 8);

		if(iplarge_int->int_bytearray != NULL || iplarge_int->int_bytearray_size > 0){
			int num_byte_array_total_bits_size = (iplarge_int->int_bytearray_size * 8);
			int i;
			int decimal_point_displacement = -1;
			short biased_exponent;
			unsigned char bit;

			iplarge_int->double_bytearray_size = 8;
			iplarge_int->double_bytearray = ishidaeutz_malloc(8 , sizeof(unsigned char));
			iplarge_int->double_bytearray[0] = ((iplarge_int->number_sign == ishidaeutz_LARGE_INT_SIGN_POSITIVE)? 0: (1 << 7));

			/*printf("::Sign is %d\n",iplarge_int->number_sign);*/
			/*printf("::Sign is %d\n",iplarge_int->double_bytearray[0]);*/

			for(i=(num_byte_array_total_bits_size - 1); i>=0;i--){
				bit = (iplarge_int->int_bytearray[i/8] & (0x01 << (i%8))) >> (i%8);

				if(decimal_point_displacement >= 0){
					iplarge_int->double_bytearray[((decimal_point_displacement+4)/8) + 1] |= (bit << (7 - ((decimal_point_displacement+4)%8)));
					decimal_point_displacement++;
				}

				if(bit != 0 && decimal_point_displacement == -1){
					decimal_point_displacement = 0;
				}
			}

			/*printf("::Displacement is %d\n",decimal_point_displacement);*/

			if(decimal_point_displacement > 0){
				biased_exponent = 1023 + decimal_point_displacement;

				iplarge_int->double_bytearray[0] = (iplarge_int->double_bytearray[0] | ((biased_exponent >> 4) & 0x7F));

				iplarge_int->double_bytearray[1] |= ((biased_exponent << 4) & 0xF0);

				if(ishidaopcua_SERVER_LARGE_INT_ENDIANNESS == ishidaeutz_LARGE_INT_ENDIANNESS_LITTLE_ENDIAN){
					unsigned char* pswap = ishidaeutz_malloc(8, sizeof(unsigned char));

					for(i = 0; i < 8; i++){
						pswap[7-i] = iplarge_int->double_bytearray[i];
					}

					if(iplarge_int->double_bytearray != NULL){
						/*memblock_free(iplarge_int->double_bytearray);*/
					}

					iplarge_int->double_bytearray = pswap;
				}
			}
		}
		else{
			errno = 988202;
			puts("ishidaeutz_LARGE_INT_ERROR: Could not yield IEEE754 double precision byte array.");
			ishidaeu_exit(errno);
		}
	}
}

const char* get_ishidaeutz_LARGE_INT_as_ASCII_IEE754_double_precision_HEX_string_array(ishidaeutz_LARGE_INT* iplarge_int)
{
	unsigned short nibbles = 16;
	char* char_array_s = ishidaeutz_malloc((nibbles + 1), sizeof(char));
	int i;
	unsigned char ch;
	char buff1[2], buff2[2];

	char_array_s[nibbles] = '\0';
	for(i = 0; i < 8; i++){
		ch = *((unsigned char*)(((unsigned int)(iplarge_int->double_bytearray)) + i));

		ishidaeutz_unsigned_int_to_ASCII_HEX_string_array((unsigned int)(ch & 0xF0),(char*) &buff1);
		ishidaeutz_unsigned_int_to_ASCII_HEX_string_array((ch & 0x0F),(char*) &buff2);
		char_array_s[(i*2)] = buff1[0];
		char_array_s[((i*2) + 1)] = buff2[0];
	}

	return char_array_s;
}

void extract_ishidaeutz_LARGE_INT_from_unsigned_int_byte_array(ishidaeutz_LARGE_INT* iplarge_int, const unsigned char* ipbyte_array, unsigned int ioffset, unsigned char ibytearray_size)
{
	if(ipbyte_array != NULL && ibytearray_size > 0 && iplarge_int != NULL){
		int i;
		ishidaeutz_LARGE_INT* pbase = create_ishidaeutz_LARGE_INT_from_int(256);
		ishidaeutz_LARGE_INT* pnth_base = create_ishidaeutz_LARGE_INT_from_int(1);
		ishidaeutz_LARGE_INT* pintermediate_nth_base = pnth_base;
		ishidaeutz_LARGE_INT* pintermediate_multiplier;
		ishidaeutz_LARGE_INT* presult = create_ishidaeutz_LARGE_INT_from_int(0);
		ishidaeutz_LARGE_INT* presult_intermediate = presult;
		ishidaeutz_LARGE_INT* pproduct_intermediate;

		iplarge_int->int_bytearray_size = ibytearray_size;
		iplarge_int->int_bytearray = ishidaeutz_malloc(iplarge_int->int_bytearray_size, sizeof(unsigned char));
		memcpy(iplarge_int->int_bytearray, (unsigned char*)(((unsigned int) ipbyte_array) + ioffset), iplarge_int->int_bytearray_size);

		if(iplarge_int->int_bytearray_size > 0){
			for(i=0; i<iplarge_int->int_bytearray_size; i++){

				if(iplarge_int->int_bytearray[i] > 0){
					pintermediate_multiplier = create_ishidaeutz_LARGE_INT_from_int(iplarge_int->int_bytearray[i]);

					pproduct_intermediate = multiply_ishidaeutz_LARGE_INTs(pnth_base, pintermediate_multiplier);

					if(pintermediate_multiplier != NULL){
						if(pintermediate_multiplier->number_bytestring != NULL){
							/*memblock_free(pintermediate_multiplier->number_bytestring);*/
						}
						/*memblock_free(pintermediate_multiplier);*/
					}

					presult_intermediate = sum_ishidaeutz_LARGE_INTs(presult, pproduct_intermediate);

					if(pproduct_intermediate != NULL){
						if(pproduct_intermediate->number_bytestring != NULL){
							/*memblock_free(pproduct_intermediate->number_bytestring);*/
						}
						/*memblock_free(pproduct_intermediate);*/
					}

					if(presult != NULL){
						if(presult->number_bytestring != NULL){
							/*memblock_free(presult->number_bytestring);*/
						}

						/*memblock_free(presult);*/
					}

					presult = presult_intermediate;
				}

				pintermediate_nth_base = multiply_ishidaeutz_LARGE_INTs(pnth_base,pbase);
				if(pnth_base != NULL){
					if(pnth_base->number_bytestring != NULL){
						/*memblock_free(pnth_base->number_bytestring);*/
					}
					/*memblock_free(pnth_base);*/
				}
				pnth_base = pintermediate_nth_base;
			}

			if(pnth_base != NULL){
				if(pnth_base->number_bytestring != NULL){
					/*memblock_free(pnth_base->number_bytestring);*/
				}
				/*memblock_free(pnth_base);*/
			}
		}

		iplarge_int->number_bytestring = presult->number_bytestring;
		iplarge_int->number_bytestring_size = presult->number_bytestring_size;
		iplarge_int->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
		if(presult != NULL){
			/*memblock_free(presult);*/
		}

		if(pbase != NULL){
			if(pbase->number_bytestring != NULL){
				/*memblock_free(pbase->number_bytestring);*/
			}
			/*memblock_free(pbase);*/
		}
	}
}

ishidaeutz_LARGE_INT* surface_copy_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1)
{
	ishidaeutz_LARGE_INT* iplarge_int_2 = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));
	if(iplarge_int_1 != NULL && iplarge_int_2 != NULL){
		iplarge_int_2->double_bytearray = iplarge_int_1->double_bytearray;
		iplarge_int_2->int_bytearray = iplarge_int_1->int_bytearray;
		iplarge_int_2->int_bytearray_size = iplarge_int_1->int_bytearray_size;
		iplarge_int_2->number_bytestring = iplarge_int_1->number_bytestring;
		iplarge_int_2->number_bytestring_size = iplarge_int_1->number_bytestring_size;
		iplarge_int_2->number_sign = iplarge_int_1->number_sign;
	}

	return iplarge_int_2;
}

ishidaeutz_LARGE_INT* deep_copy_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1)
{
	ishidaeutz_LARGE_INT* iplarge_int_2 = ishidaeutz_malloc(1, sizeof(ishidaeutz_LARGE_INT));

	if(iplarge_int_1 != NULL && iplarge_int_2 != NULL){

		if(iplarge_int_1->double_bytearray != NULL){
			iplarge_int_2->double_bytearray = ishidaeutz_malloc(8, sizeof(char));

			memcpy(iplarge_int_2->double_bytearray, iplarge_int_1->double_bytearray, 8);
		}

		if(iplarge_int_1->int_bytearray != NULL && iplarge_int_1->int_bytearray_size > 0){
			iplarge_int_2->int_bytearray_size = iplarge_int_1->int_bytearray_size;
			iplarge_int_2->int_bytearray = ishidaeutz_malloc(iplarge_int_2->int_bytearray_size, sizeof(char));

			memcpy(iplarge_int_2->int_bytearray, iplarge_int_1->int_bytearray, iplarge_int_2->int_bytearray_size);
		}

		if(iplarge_int_1->number_bytestring != NULL && iplarge_int_1->number_bytestring_size > 0){
			iplarge_int_2->number_bytestring_size = iplarge_int_1->number_bytestring_size;
			iplarge_int_2->number_bytestring = ishidaeutz_malloc(iplarge_int_2->number_bytestring_size, sizeof(char));

			memcpy(iplarge_int_2->number_bytestring, iplarge_int_1->number_bytestring, iplarge_int_2->number_bytestring_size);
		}

		iplarge_int_2->number_sign = iplarge_int_1->number_sign;
	}

	return iplarge_int_2;
}

void extract_ishidaeutz_LARGE_INT_from_IEEE754_dp_byte_array(ishidaeutz_LARGE_INT* iplarge_int, const unsigned char* ipIEEE754_dp_byte_array, unsigned int ioffset)
{
	int i;
	int decimal_point_displacement = -1;
	short biased_exponent;
	unsigned char bit;

	iplarge_int->double_bytearray = ishidaeutz_malloc(8, sizeof(unsigned char));
	memcpy(iplarge_int->double_bytearray, (unsigned char*)(((unsigned int) ipIEEE754_dp_byte_array) + ioffset), 8);

	if(ishidaopcua_SERVER_LARGE_INT_ENDIANNESS == ishidaeutz_LARGE_INT_ENDIANNESS_LITTLE_ENDIAN){
		unsigned char* pswap = ishidaeutz_malloc(8, sizeof(unsigned char));

		for(i = 0; i < 8; i++){
			pswap[7-i] = iplarge_int->double_bytearray[i];
		}

		iplarge_int->double_bytearray = pswap;
	}

	biased_exponent = (iplarge_int->double_bytearray[0] & 0x7F);
	biased_exponent = (biased_exponent << 4);
	biased_exponent |= ((iplarge_int->double_bytearray[1] & 0xF0) >> 4);

	decimal_point_displacement = biased_exponent - 1023;

	if(decimal_point_displacement > 0){
		iplarge_int->int_bytearray_size = (decimal_point_displacement+1)/8 + ((((decimal_point_displacement+1)%8) > 0)? 1:0);
		iplarge_int->int_bytearray = ishidaeutz_malloc(iplarge_int->int_bytearray_size, sizeof(unsigned char));

		for(i = 0; i < decimal_point_displacement; i++){

			bit = ((iplarge_int->double_bytearray[((i+4)/8) + 1]) >> (7 - ((i+4)%8))) & 0x01;

			iplarge_int->int_bytearray[(decimal_point_displacement - 1 - i)/8] |= (bit << ((decimal_point_displacement - 1 - i)%8));
		}

		iplarge_int->int_bytearray[decimal_point_displacement/8] |= (0x01 << (decimal_point_displacement%8));

		extract_ishidaeutz_LARGE_INT_from_unsigned_int_byte_array(iplarge_int, iplarge_int->int_bytearray, 0, iplarge_int->int_bytearray_size);

		iplarge_int->number_sign = (((iplarge_int->double_bytearray[0] & 0x80) == 0)? ishidaeutz_LARGE_INT_SIGN_POSITIVE: ishidaeutz_LARGE_INT_SIGN_NEGATIVE);
	}
}
/*************************************** ISHIDAEUTZ_LARGE_NUMBER END ***************************************/


/*************************************** ISHIDAEU_UTILS START ***************************************/
time_t ishidaeutz_get_current_unix_timestamp_in_seconds()
{
	time_t current_time;
	time (&current_time);
	return current_time;
}

unsigned int ishidaeutz_get_current_unix_timestamp_in_seconds_uint32()
{
	return ((unsigned int) ishidaeutz_get_current_unix_timestamp_in_seconds());
}

ishidaeutz_LARGE_INT* ishidaeutz_get_current_unix_timestamp_in_milliseconds_uint64()
{
	ishidaeutz_LARGE_INT* ptimestamp = create_ishidaeutz_LARGE_INT_from_int(ishidaeutz_get_current_unix_timestamp_in_seconds_uint32());
	stretch_ishidaeutz_LARGE_INT(ptimestamp, ptimestamp->number_bytestring_size + 3);
	return ptimestamp;
}

void ishidauetz_convert_uint16_to_byte_array_le(unsigned short num, unsigned char* ibyte_array,int ioffset)
{
	ibyte_array[ioffset+1] = (num & 0xff00) >> 8;
	ibyte_array[ioffset] = (num & 0x00ff);
}

void ishidaopcua_convert_uint16_to_byte_array_be(unsigned short num, unsigned char* ibyte_array,int ioffset)
{

	ibyte_array[ioffset] = (num & 0xff00) >> 8;
	ibyte_array[ioffset+1] = (num & 0x00ff);
}

void ishidaeutz_convert_int16_to_byte_array_be(short num, unsigned char* ibyte_array,int ioffset)
{

	ibyte_array[ioffset] = (num & 0xff00) >> 8;
	ibyte_array[ioffset+1] = (num & 0x00ff);
}

short ishidaeutz_convert_byte_array_to_int16_le(unsigned int ioffset,unsigned char* ibyte_array)
{
	short result = ((ibyte_array[ioffset+1] & 0xff) << 8) + (ibyte_array[ioffset+0] & 0xff);
	return result;
}

short ishidaeutz_convert_byte_array_to_int16_be(unsigned int ioffset,unsigned char* ibyte_array)
{
	short result = ((ibyte_array[ioffset+0] & 0xff) << 8) + (ibyte_array[ioffset+1] & 0xff);
	return result;
}

unsigned short ishidaeutz_convert_byte_array_to_uint16_le(unsigned int ioffset,unsigned char* ibyte_array)
{
	unsigned short result = (unsigned short)ishidaeutz_convert_byte_array_to_int16_le(ioffset, ibyte_array);
	return result;
}

unsigned short ishidaeutz_convert_byte_array_to_uint16_be(unsigned int ioffset,unsigned char* ibyte_array)
{
	unsigned short result = (unsigned short)ishidaeutz_convert_byte_array_to_int16_be(ioffset, ibyte_array);
	return result;
}

void ishidaeutz_convert_uint32_to_byte_array_be(unsigned int inum, unsigned char* ibyte_array,int ioffset)
{
	ibyte_array[ioffset] = (inum & 0xff000000) >> 24;
	ibyte_array[ioffset+1] = (inum & 0x00ff0000) >> 16;
	ibyte_array[ioffset+2] = (inum & 0x0000ff00) >> 8;
	ibyte_array[ioffset+3] = (inum & 0x000000ff);
}

void ishidaeutz_convert_uint32_to_byte_array_le(unsigned int inum, unsigned char* ibyte_array,int ioffset)
{
	ibyte_array[ioffset+3] = (inum & 0xff000000) >> 24;
	ibyte_array[ioffset+2] = (inum & 0x00ff0000) >> 16;
	ibyte_array[ioffset+1] = (inum & 0x0000ff00) >> 8;
	ibyte_array[ioffset] = (inum & 0x000000ff);
}

void ishidaeutz_convert_uint64_to_byte_array_be(unsigned long inum, unsigned char* ibyte_array,int ioffset)
{

	ibyte_array[ioffset] = (inum & 0xff00000000000000) >> 56;
	ibyte_array[ioffset+1] = (inum & 0x00ff000000000000) >> 48;
	ibyte_array[ioffset+2] = (inum & 0x0000ff0000000000) >> 40;
	ibyte_array[ioffset+3] = (inum & 0x000000ff00000000) >> 32;
	ibyte_array[ioffset+4] = (inum & 0x00000000ff000000) >> 24;
	ibyte_array[ioffset+5] = (inum & 0x0000000000ff0000) >> 16;
	ibyte_array[ioffset+6] = (inum & 0x000000000000ff00) >> 8;
	ibyte_array[ioffset+7] = (inum & 0x00000000000000ff);
}

unsigned int ishidaeutz_convert_byte_array_to_uint32_be(unsigned int ioffset,unsigned char* ibyte_array)
{
	unsigned int result = ((ibyte_array[ioffset+3] & 0x000000FF) << 0) + ((ibyte_array[ioffset+2] & 0x000000FF) << 8) + ((ibyte_array[ioffset+1] & 0x000000FF) << 16) + ((ibyte_array[ioffset+0] & 0x000000FF) << 24);

    return result;
}

unsigned int ishidaeutz_convert_byte_array_to_uint32_le(unsigned int ioffset,unsigned char* ibyte_array)
{
	unsigned int result = ((ibyte_array[ioffset+3] & 0x000000FF) << 24) + ((ibyte_array[ioffset+2] & 0x000000FF) << 16) + ((ibyte_array[ioffset+1] & 0x000000FF) << 8) +(ibyte_array[ioffset+0] & 0x000000FF);

    return result;
}

int ishidaeutz_convert_byte_array_to_int32_be(unsigned int ioffset,unsigned char* ibyte_array)
{
	int result = (int)ishidaeutz_convert_byte_array_to_uint32_be(ioffset, ibyte_array);

    return result;
}

int ishidaeutz_convert_byte_array_to_int32_le(unsigned int ioffset,unsigned char* ibyte_array)
{
	int result = (int)ishidaeutz_convert_byte_array_to_uint32_le(ioffset, ibyte_array);;

    return result;
}

void ishidaeutz_copy_byte_array(char *isource_byte_array, char *idestination_byte_array, unsigned int isource_offset, unsigned int idestination_offset, unsigned int icopy_length)
{
	memcpy(idestination_byte_array+idestination_offset, isource_byte_array+isource_offset, icopy_length);
}

void ishidaeutz_shift_byte_array(char *i_byte_array, unsigned int ioffset, unsigned int ishift_size)
{
	memmove(i_byte_array + ioffset, i_byte_array, ishift_size);
}

unsigned int ishidaeutz_system_is_little_endian()
{
		unsigned int x = 1;

		if( (int) (((char *)&x)[0]) == 1)
		{
			return 1U;
		}

		return 0U;
}

char* ishidaeutz_unsigned_int_to_string(unsigned int inumber)
{
	char* pstring = ishidaeutz_malloc(6, sizeof(char));
	sprintf(pstring, "%u", inumber);
	return pstring;
}

void ishidaeutz_convert_double_to_byte_array(double idouble, unsigned char *idata_buffer, unsigned int iwrite_size)
{
	memcpy((idata_buffer + iwrite_size),&idouble, (int)sizeof(double));
}

double ishidaeutz_convert_byte_array_to_double(unsigned char *idata_buffer, unsigned int iparsing_offset)
{
	double result;
	memcpy(&result, (idata_buffer + iparsing_offset), sizeof(double));

	return result;
}

unsigned int ishidaeutz_mem_track_array_clean_on_reset = 1;
void* ishidaeutz_malloc(size_t inum_blocks, size_t isize_of_single_block)
{
	return memblock_allocate_smartly((unsigned int)(inum_blocks * isize_of_single_block), ishidaeutz_mem_track_array_clean_on_reset, 0);
}

void ishidaeutz_mtrack_init(){
	initiate_memblock(1);
}

void ishidaeutz_mtrack_pause(){
	ishidaeutz_mem_track_array_clean_on_reset = 0;
}

void ishidaeutz_mtrack_resume(){
	ishidaeutz_mem_track_array_clean_on_reset= 1;
}

unsigned int ishidaeutz_mtrack_state(){
	return ishidaeutz_mem_track_array_clean_on_reset;
}

void ishidaeutz_mtrack_reset()
{
	puts("::::::::::::::::::::::::::::::::::::::::::: Resetting memory.");
	reset_memblock(0);
}
/*************************************** ISHIDAEU_UTILS END ***************************************/

/*************************************** ISHIDAEU_HASHMAP START **************************************/
unsigned int ishidaeutz_HASHMAP_KEYS_REALLOC_RANGE = 100;
unsigned int mtrack_previous_state;
static void ishidaeutz_reallocate_keys_hashmap(ishidaeutz_HASHMAP *iphashmap){
	if(iphashmap != NULL && iphashmap->actual_size == iphashmap->key_size){
		unsigned int new_key_size = iphashmap->key_size + ishidaeutz_HASHMAP_KEYS_REALLOC_RANGE;
		ishidaeutz_HASHMAP_KEYS_ELEMENT* temp;

		if(iphashmap->persistent_memory_elements != 0){
			mtrack_previous_state = ishidaeutz_mtrack_state();
			ishidaeutz_mtrack_pause();
			temp = ishidaeutz_malloc(new_key_size, sizeof(ishidaeutz_HASHMAP_KEYS_ELEMENT));
			if(mtrack_previous_state > 0){
				ishidaeutz_mtrack_resume();
			}
		}
		else{
			temp = ishidaeutz_malloc(new_key_size, sizeof(ishidaeutz_HASHMAP_KEYS_ELEMENT));
		}


		if(temp == NULL){
			/*puts("ERROR: Hashmap keys reallocation failed\n");*/
		}
		else{
			unsigned int i;
			unsigned int copy_limit = (iphashmap->key_size < new_key_size) ? iphashmap->key_size: new_key_size;
			for(i =  0; i < copy_limit; i++){
				temp[i].name = iphashmap->keys[i].name;
			}
		}

		if(iphashmap->keys != NULL){
			/*memblock_free(iphashmap->keys);*/
		}

		iphashmap->keys = temp;
	}
}

static void ishidaopcua_put_next_hashmap(ishidaeutz_HASHMAP *iphashmap, ishidaeutz_HASHMAP_BUCKET_ELEMENT *ipbucket_element, const char *ipkey, void *ipvalue)
{
	if(iphashmap != NULL && ipbucket_element != NULL && strcmp(ipbucket_element->key, ipkey) ==  0)
	{
		/*puts("Update existing bucket element");*/
		ipbucket_element->value = ipvalue;
	}
	else if(iphashmap != NULL && ipbucket_element != NULL && ipbucket_element->next ==  NULL)
	{
		/*puts("Set next on bucket element");*/
		ishidaeutz_HASHMAP_BUCKET_ELEMENT *next_bucket_element;
		if(iphashmap->persistent_memory_elements > 0){
			mtrack_previous_state = ishidaeutz_mtrack_state();
			ishidaeutz_mtrack_pause();
			next_bucket_element = ishidaeutz_malloc(1, sizeof(ishidaeutz_HASHMAP_BUCKET_ELEMENT));
			if(mtrack_previous_state > 0){
				ishidaeutz_mtrack_resume();
			}
		}
		else{
			next_bucket_element = ishidaeutz_malloc(1, sizeof(ishidaeutz_HASHMAP_BUCKET_ELEMENT));
		}

		next_bucket_element->key = ipkey;
		next_bucket_element->value = ipvalue;

		ipbucket_element->next = next_bucket_element;
		iphashmap->keys[iphashmap->actual_size].name = ipkey;
		iphashmap->actual_size += 1;
		ishidaeutz_reallocate_keys_hashmap(iphashmap);
		/*printf("Set next value address %p\n",ipbucket_element->next->value);*/
	}
	else if(iphashmap != NULL && ipbucket_element != NULL && ipbucket_element->next != NULL)
	{
		/*puts("Check next element in bucket");*/
		/*printf("2. next key %s vs key %s\n",ipbucket_element->next->key, ipkey);*/
		ishidaopcua_put_next_hashmap(iphashmap, ipbucket_element->next, ipkey, ipvalue);
	}
}

static void ishidaopcua_get_next_hashmap(ishidaeutz_HASHMAP_BUCKET_ELEMENT *ipbucket_element, const char *ipkey, const void **ippvalue )
{
	if(ipbucket_element != NULL && ipbucket_element->key != NULL && strcmp((ipbucket_element->key), ipkey) ==  0)
	{
		/*puts("Get bucket element");*/
		*ippvalue = ipbucket_element->value;
	}
	else if(ipbucket_element != NULL && ipbucket_element->next != NULL)
	{
		/*puts("Check next element in bucket");*/
		ishidaopcua_get_next_hashmap(ipbucket_element->next, ipkey, ippvalue);
	}
	else
	{
		/*puts("Nothing found next.");*/
		if(ippvalue != NULL){
			*ippvalue = NULL;
		}
	}
}

/** Returns SDBM hash **/
unsigned long ishidaeutz_get_SDBM_hash(const char *ipraw_string)
{
	unsigned long hash =  0;

	if(ipraw_string != NULL){
		int ascii_char;
		while ((ascii_char = *ipraw_string++) > 0)
		{
			hash = ascii_char + (hash << 6) + (hash << 16) - hash;
		}
	}

	return hash;
}

ishidaeutz_HASHMAP* ishidaeutz_init_hashmap(unsigned int isize)
{
	if(isize > 0){
		ishidaeutz_HASHMAP *hashmap = ishidaeutz_malloc(1, sizeof(ishidaeutz_HASHMAP));
		hashmap->buckets = ishidaeutz_malloc(isize, sizeof(ishidaeutz_HASHMAP_BUCKET_ELEMENT));
		hashmap->key_size = isize + ishidaeutz_HASHMAP_KEYS_REALLOC_RANGE;
		hashmap->keys = ishidaeutz_malloc((hashmap->key_size), sizeof(ishidaeutz_HASHMAP_KEYS_ELEMENT));
		hashmap->expected_size = isize;
		hashmap->actual_size =  0;

		if(ishidaeutz_mtrack_state() == 0){
			hashmap->persistent_memory_elements = 1;
		}

		return hashmap;
	}

	return NULL;
}

unsigned int ishidaopcua_get_hashmap_size(ishidaeutz_HASHMAP *iphashmap)
{
	if(iphashmap != NULL){
		return iphashmap->actual_size;
	}

	return 0U;
}

void ishidaeutz_put_hashmap(ishidaeutz_HASHMAP *iphashmap, const char *ipkey, void *ipvalue)
{

	if(iphashmap != NULL && ipkey != NULL){
		/*puts("reset");*/
		unsigned long index = ishidaeutz_get_SDBM_hash(ipkey) % iphashmap->expected_size;
		/*printf("Determined index %lu with key %s\n",index,ipkey);*/

		if(iphashmap->buckets[index%iphashmap->expected_size].key ==  NULL && iphashmap->buckets[index%iphashmap->expected_size].value ==  NULL)
		{
			/*puts("Create new bucket element");*/
			iphashmap->buckets[index%iphashmap->expected_size].key = ipkey;
			iphashmap->buckets[index%iphashmap->expected_size].value = ipvalue;
			iphashmap->keys[iphashmap->actual_size].name = ipkey;
			iphashmap->actual_size += 1;
			ishidaeutz_reallocate_keys_hashmap(iphashmap);

			/*printf("Stored value address %p\n",iphashmap->buckets[index%iphashmap->expected_size].value);*/
			/*puts("got here1");*/

		}
		else if(iphashmap->buckets[index%iphashmap->expected_size].key != NULL && strcmp((iphashmap->buckets[index%iphashmap->expected_size].key), ipkey) ==  0)
		{
			/*printf("%s vs %s\n",iphashmap->buckets[index%iphashmap->expected_size].key, ipkey);*/
			/*puts("Update existing bucket element");*/
			iphashmap->buckets[index%iphashmap->expected_size].value = ipvalue;
			/*printf("Update value address %p\n",iphashmap->buckets[index%iphashmap->expected_size].value);*/
		}
		else if(iphashmap->buckets[index%iphashmap->expected_size].next ==  NULL)
		{
			/*puts("Set next on bucket element");*/
			ishidaeutz_HASHMAP_BUCKET_ELEMENT *next_bucket_element;
			if(iphashmap->persistent_memory_elements > 0){
				mtrack_previous_state = ishidaeutz_mtrack_state();
				ishidaeutz_mtrack_pause();
				next_bucket_element = ishidaeutz_malloc(1, sizeof(ishidaeutz_HASHMAP_BUCKET_ELEMENT));
				if(mtrack_previous_state > 0){
					ishidaeutz_mtrack_resume();
				}
			}
			else{
				next_bucket_element = ishidaeutz_malloc(1, sizeof(ishidaeutz_HASHMAP_BUCKET_ELEMENT));
			}

			next_bucket_element->key = ipkey;
			next_bucket_element->value = ipvalue;
			iphashmap->buckets[index%iphashmap->expected_size].next = next_bucket_element;
			iphashmap->keys[iphashmap->actual_size].name = ipkey;
			iphashmap->actual_size += 1;
			ishidaeutz_reallocate_keys_hashmap(iphashmap);
			/*printf("Set next value address %p\n",iphashmap->buckets[index%iphashmap->expected_size].next->value);*/

		}
		else if(iphashmap->buckets[index%iphashmap->expected_size].next != NULL)
		{
			/*puts("Check next element in bucket");*/
			/*printf("1. next key %s vs key %s\n",iphashmap->buckets[index%iphashmap->expected_size].next->key, ipkey);*/
			ishidaopcua_put_next_hashmap(iphashmap,iphashmap->buckets[index%iphashmap->expected_size].next, ipkey, ipvalue);
		}
		/*puts("got here2");*/
	}
}

void ishidaeutz_get_hashmap(ishidaeutz_HASHMAP *iphashmap, const char *ipkey, const void **ippvalue)
{
	if(iphashmap != NULL && ipkey != NULL){
		unsigned long index = ishidaeutz_get_SDBM_hash(ipkey) % iphashmap->expected_size;
		/*printf("Index %lu with key %s\n",index, ipkey);*/

		if(iphashmap != NULL && iphashmap->buckets[index%iphashmap->expected_size].key != NULL && strcmp((iphashmap->buckets[index%iphashmap->expected_size].key), ipkey) ==  0)
		{
			/*printf("Found key %s\n",(iphashmap->buckets[index%iphashmap->expected_size].key));*/
			/*puts("Get bucket element");*/
			*ippvalue = iphashmap->buckets[index%iphashmap->expected_size].value;
			/*printf("Obtained value address %p\n",*ippvalue);*/
		}
		else if(iphashmap != NULL &&  iphashmap->buckets[index%iphashmap->expected_size].next != NULL)
		{
			/*puts("Check next element in bucket");*/
			ishidaopcua_get_next_hashmap(iphashmap->buckets[index%iphashmap->expected_size].next, ipkey, ippvalue);
		}
		else
		{
			/*puts("Nothing found");*/
			if(ippvalue != NULL){
				*ippvalue = NULL;
			}
		}
	}
	else
	{
		/*puts("Nothing found");*/
		if(ippvalue != NULL){
			*ippvalue = NULL;
		}
	}
}
/*************************************** ISHIDAEU_HASHMAP END ***************************************/

/*************************************** ISHIDAOPCUA_TYPES START ***************************************/
ishidaopcua_ARRAY* ishidaopcua_init_array()
{
	ishidaopcua_ARRAY* parray = ishidaeutz_malloc(1, sizeof(ishidaopcua_ARRAY));
	parray->size = 0;
	parray->elements = 0;
	parray->type = 0;

	return parray;
}

void ishidaopcua_set_array_size(ishidaopcua_UINT32 isize, ishidaopcua_ARRAY* iarray)
{
	iarray->size = isize;
}

void ishidaopcua_set_array_type(ishidaopcua_UINT16 itype, ishidaopcua_ARRAY* iarray)
{
	iarray->type = itype;
}

void ishidaopcua_set_array_elements(void* ielements, ishidaopcua_ARRAY* iarray)
{
	iarray->elements = ielements;
}

void ishidaopcua_set_array(ishidaopcua_UINT32 isize, ishidaopcua_UINT16 itype, void* ielements, ishidaopcua_ARRAY* iarray)
{
	ishidaopcua_set_array_size(isize, iarray);
	ishidaopcua_set_array_type(itype, iarray);
	ishidaopcua_set_array_elements(ielements, iarray);
}

ishidaopcua_UINT32 ishidaopcua_get_array_size(ishidaopcua_ARRAY* iarray)
{
	return iarray->size;
}

ishidaopcua_UINT16 ishidaopcua_get_array_type(ishidaopcua_ARRAY* iarray)
{
	return iarray->type;
}

void* ishidaopcua_get_array_elements(ishidaopcua_ARRAY* iarray)
{
	return iarray->elements;
}

ishidaopcua_STRING* ishidaopcua_init_string()
{
	ishidaopcua_STRING* pstring = ishidaeutz_malloc(1, sizeof(ishidaopcua_STRING));
	pstring->length = 0xFFFFFFFF;
	pstring->text = 0;
	return pstring;
}

void ishidaopcua_set_string(ishidaopcua_BYTE* itext, ishidaopcua_STRING* istring)
{
	if(itext == 0){
		istring->length = 4294967295U;
		istring->text = 0;
	}
	else{
		istring->length = strlen((const char*)itext);
	}

	if(istring->length  > 0 && istring->length < 4294967295U){
		istring->text = itext;
	}
}

ishidaopcua_UINT32 ishidaopcua_get_string_length(ishidaopcua_STRING* istring)
{
	return istring->length;
}

ishidaopcua_BYTE* ishidaopcua_get_string_text(ishidaopcua_STRING* istring)
{
	return istring->text;
}

ishidaopcua_GUID* ishidaopcua_init_guid()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_GUID));
}

void ishidaopcua_set_guid_data1(ishidaopcua_UINT32 idata1, ishidaopcua_GUID* iguid)
{
	iguid->data1 = idata1;
}

void ishidaopcua_set_guid_data2(ishidaopcua_UINT16 idata2, ishidaopcua_GUID* iguid)
{
	iguid->data2 = idata2;
}

void ishidaopcua_set_guid_data3(ishidaopcua_UINT16 idata3, ishidaopcua_GUID* iguid)
{
	iguid->data3 = idata3;
}

void ishidaopcua_set_guid_data4(ishidaopcua_BYTE idata4[8], ishidaopcua_GUID* iguid)
{
	ishidaeutz_copy_byte_array((char*)&idata4[0], (char*)&(iguid->data4[0]), (ishidaopcua_UINT32)0, (ishidaopcua_UINT32)0, (ishidaopcua_UINT32)8);
}

void ishidaopcua_set_guid(ishidaopcua_UINT32 idata1, ishidaopcua_UINT16 idata2, ishidaopcua_UINT16 idata3, ishidaopcua_BYTE idata4[8], ishidaopcua_GUID* iguid)
{
	ishidaopcua_set_guid_data1(idata1, iguid);
	ishidaopcua_set_guid_data2(idata2, iguid);
	ishidaopcua_set_guid_data3(idata3, iguid);
	ishidaopcua_set_guid_data4(idata4, iguid);
}

ishidaopcua_UINT32 ishidaopcua_get_guid_data1(ishidaopcua_GUID* iguid)
{
	return iguid->data1;
}

ishidaopcua_UINT16 ishidaopcua_get_guid_data2(ishidaopcua_GUID* iguid)
{
	return iguid->data2;
}

ishidaopcua_UINT16 ishidaopcua_get_guid_data3(ishidaopcua_GUID* iguid)
{
	return iguid->data3;
}

ishidaopcua_BYTE* ishidaopcua_get_guid_data4(ishidaopcua_GUID* iguid)
{
	return (&(iguid->data4[0]));
}

ishidaopcua_NODE_ID* ishidaopcua_init_node_id()
{
	ishidaopcua_NODE_ID* node_id = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE_ID));
	return node_id;
}

void ishidaopcua_set_node_id_namespace_index(ishidaopcua_UINT16 inamespace_index, ishidaopcua_NODE_ID* inode_id)
{
	inode_id->namespace_index = inamespace_index;
}

void ishidaopcua_set_node_id_identifier_numeric(ishidaopcua_UINT32 iidentifier_numeric, ishidaopcua_NODE_ID* inode_id)
{
	inode_id->identifier.numeric = iidentifier_numeric;
	inode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;

	if(iidentifier_numeric > 65535 && iidentifier_numeric <= 4294967295U){
		inode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_NUMERIC_FOUR_BYTE;
	}
	else{
		inode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_NUMERIC_TWO_BYTE;
	}

}

void ishidaopcua_set_node_id_identifier_string(ishidaopcua_STRING* iidentifier_string, ishidaopcua_NODE_ID* inode_id)
{
	inode_id->identifier.string = iidentifier_string;
	inode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_STRING;
	inode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_STRING;
}

void ishidaopcua_set_node_id_identifier_guid(ishidaopcua_GUID* iidentifier_guid, ishidaopcua_NODE_ID* inode_id)
{
	inode_id->identifier.guid = iidentifier_guid;
	inode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_GUID;
	inode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_GUID;
}

void ishidaopcua_set_node_id_identifier_bytestring(ishidaopcua_BYTESTRING* iidentifier_bytestring, ishidaopcua_NODE_ID* inode_id)
{
	inode_id->identifier.bytestring = iidentifier_bytestring;
	inode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_BYTESTRING;
	inode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_BYTESTRING;
}

ishidaopcua_BYTE ishidaopcua_get_node_id_encoding_mask(ishidaopcua_NODE_ID* inode_id)
{
	return inode_id->encoding_mask;
}

ishidaopcua_UINT16 ishidaopcua_get_node_id_namespace_index(ishidaopcua_NODE_ID* inode_id)
{
	return inode_id->namespace_index;
}

ishidaopcua_UINT32 ishidaopcua_get_node_id_identifier_type(ishidaopcua_NODE_ID* inode_id)
{
	return inode_id->identifier_type;
}

ishidaopcua_UINT32 ishidaopcua_get_node_id_identifier_numeric(ishidaopcua_NODE_ID* inode_id)
{
	return inode_id->identifier.numeric;
}

ishidaopcua_STRING* ishidaopcua_get_node_id_identifier_string(ishidaopcua_NODE_ID* inode_id)
{
	return inode_id->identifier.string;
}

ishidaopcua_GUID* ishidaopcua_get_node_id_identifier_guid(ishidaopcua_NODE_ID* inode_id)
{
	return inode_id->identifier.guid;
}

ishidaopcua_BYTESTRING* ishidaopcua_get_node_id_identifier_bytestring(ishidaopcua_NODE_ID* inode_id)
{
	return inode_id->identifier.bytestring;
}

ishidaopcua_EXPANDED_NODE_ID* ishidaopcua_init_expanded_node_id()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_EXPANDED_NODE_ID));
}

void ishidaopcua_set_expanded_node_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id)
{
	iexpanded_node_id->node_id = inode_id;
}

void ishidaopcua_set_namespace_uri(ishidaopcua_STRING* inamespace_uri, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id)
{
	iexpanded_node_id->namespace_uri = inamespace_uri;
	iexpanded_node_id->node_id->encoding_mask |= ((ishidaopcua_BYTE) 0x80);
}

void ishidaopcua_set_server_index(ishidaopcua_UINT32 iserver_index, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id)
{
	iexpanded_node_id->server_index = iserver_index;
	iexpanded_node_id->node_id->encoding_mask |= ((ishidaopcua_BYTE) 0x40);
}

void ishidaopcua_set_expanded_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_STRING* inamespace_uri, ishidaopcua_UINT32 iserver_index, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id)
{
	ishidaopcua_set_expanded_node_node_id(inode_id, iexpanded_node_id);
	ishidaopcua_set_namespace_uri(inamespace_uri, iexpanded_node_id);
	ishidaopcua_set_server_index(iserver_index, iexpanded_node_id);

}

ishidaopcua_NODE_ID* ishidaopcua_get_node_id(ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id)
{
	return iexpanded_node_id->node_id;
}

ishidaopcua_STRING* ishidaopcua_get_namespace_uri(ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id)
{
	return iexpanded_node_id->namespace_uri;
}

ishidaopcua_UINT32 ishidaopcua_get_server_index(ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id)
{
	return iexpanded_node_id->server_index;
}

ishidaopcua_QUALIFIED_NAME* ishidaopcua_init_qualified_name()
{
	ishidaopcua_QUALIFIED_NAME* pqualified_name = ishidaeutz_malloc(1, sizeof(ishidaopcua_QUALIFIED_NAME));
	pqualified_name->name = 0;
	pqualified_name->namespace_index = 0;
	return pqualified_name;
}

void ishidaopcua_set_qualified_name_namespace_index(ishidaopcua_UINT16 inamespace_index, ishidaopcua_QUALIFIED_NAME* iqualified_name)
{
	iqualified_name->namespace_index = inamespace_index;
}

void ishidaopcua_set_qualified_name_name(ishidaopcua_STRING* iname, ishidaopcua_QUALIFIED_NAME* iqualified_name)
{
	iqualified_name->name = iname;
}

void ishidaopcua_set_qualified_name(ishidaopcua_UINT16 inamespace_index, ishidaopcua_STRING* iname, ishidaopcua_QUALIFIED_NAME* iqualified_name)
{
	ishidaopcua_set_qualified_name_namespace_index(inamespace_index, iqualified_name);
	ishidaopcua_set_qualified_name_name(iname, iqualified_name);
}

ishidaopcua_UINT16 ishidaopcua_get_qualified_name_namespace_index(ishidaopcua_QUALIFIED_NAME* iqualified_name)
{
	return iqualified_name->namespace_index;
}

ishidaopcua_STRING* ishidaopcua_get_qualified_name_name(ishidaopcua_QUALIFIED_NAME* iqualified_name)
{
	return iqualified_name->name;
}

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_init_localized_text()
{
	ishidaopcua_LOCALIZED_TEXT* localized_text = ishidaeutz_malloc(1, sizeof(ishidaopcua_LOCALIZED_TEXT));
	localized_text->encoding = ((ishidaopcua_BYTE) 0x00);
	return localized_text;
}

void ishidaopcua_set_localized_text_locale(ishidaopcua_STRING* ilocale, ishidaopcua_LOCALIZED_TEXT* ilocalized_text)
{
	ilocalized_text->locale = ilocale;
	ilocalized_text->encoding |= ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE;
}

void ishidaopcua_set_localized_text_text(ishidaopcua_STRING* itext, ishidaopcua_LOCALIZED_TEXT* ilocalized_text)
{
	ilocalized_text->text = itext;
	ilocalized_text->encoding |= ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT;
}

void ishidaopcua_set_localized_text(ishidaopcua_STRING* ilocale, ishidaopcua_STRING* itext, ishidaopcua_LOCALIZED_TEXT* ilocalized_text)
{
	ishidaopcua_set_localized_text_locale(ilocale, ilocalized_text);
	ishidaopcua_set_localized_text_text(itext, ilocalized_text);
}

ishidaopcua_BYTE ishidaopcua_get_localized_text_encoding(ishidaopcua_LOCALIZED_TEXT* ilocalized_text)
{
	return ilocalized_text->encoding;
}

ishidaopcua_STRING* ishidaopcua_get_localized_text_locale(ishidaopcua_LOCALIZED_TEXT* ilocalized_text)
{
	return ilocalized_text->locale;
}

ishidaopcua_STRING* ishidaopcua_get_localized_text_text(ishidaopcua_LOCALIZED_TEXT* ilocalized_text)
{
	return ilocalized_text->text;
}

ishidaopcua_EXTENSION_OBJECT* ishidaopcua_init_extension_object()
{
	ishidaopcua_EXTENSION_OBJECT* pextension_object = ishidaeutz_malloc(1, sizeof(ishidaopcua_EXTENSION_OBJECT));

	pextension_object->body = 0;

	pextension_object->length = 0;

	pextension_object->encoding = ((ishidaopcua_BYTE) 0);

	return pextension_object;
}

void ishidaopcua_set_extension_object_type_id(ishidaopcua_EXPANDED_NODE_ID* ityped_id, ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	iextension_object->typed_id = ityped_id;
}

void ishidaopcua_set_extension_object_length(ishidaopcua_UINT32 ilength, ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	iextension_object->length = ilength;
}

void ishidaopcua_set_extension_object_body(ishidaopcua_BYTE iencoding, ishidaopcua_BYTE* ibody, ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	iextension_object->encoding = iencoding;
	iextension_object->body = ibody;
}

void ishidaopcua_set_extension_object(ishidaopcua_EXPANDED_NODE_ID* ityped_id, ishidaopcua_UINT32 ilength, ishidaopcua_BYTE iencoding, ishidaopcua_BYTE* ibody, ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	ishidaopcua_set_extension_object_type_id(ityped_id, iextension_object);
	ishidaopcua_set_extension_object_length(ilength, iextension_object);
	ishidaopcua_set_extension_object_body(iencoding, ibody, iextension_object);
}

ishidaopcua_EXPANDED_NODE_ID* ishidaopcua_get_extension_object_type_id(ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	return iextension_object->typed_id;
}

ishidaopcua_BYTE ishidaopcua_get_extension_object_encoding(ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	return iextension_object->encoding;
}

ishidaopcua_UINT32 ishidaopcua_get_extension_object_length(ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	return iextension_object->length;
}

ishidaopcua_BYTE* ishidaopcua_get_extension_object_body(ishidaopcua_EXTENSION_OBJECT* iextension_object)
{
	return iextension_object->body;
}

ishidaopcua_VARIANT* ishidaopcua_init_variant()
{
	ishidaopcua_VARIANT* variant = ishidaeutz_malloc(1, sizeof(ishidaopcua_VARIANT));
	variant->encoding_mask = ((ishidaopcua_BYTE) 0x00);
	return variant;
}

void ishidaopcua_set_variant_array_length(ishidaopcua_UINT32 iarray_length, ishidaopcua_VARIANT* ivariant)
{
	ivariant->array_length = iarray_length;
	ivariant->encoding_mask |= ((ishidaopcua_BYTE) 0x80);
}

void ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID itype_id, ishidaopcua_VARIANT* ivariant)
{
	ivariant->encoding_mask |= (ishidaopcua_BYTE) (0x3F & ((ishidaopcua_BYTE) itype_id));
	/*printf("variant encoding mask %u\n", ivariant->encoding_mask);*/
}

void ishidaopcua_set_variant_value(ishidaopcua_TYPE_ID itype_id, void* ivalue, ishidaopcua_VARIANT* ivariant)
{
	ishidaopcua_set_variant_type_id(itype_id, ivariant);
	/*printf("variant encoding mask %u\n", ivariant->encoding_mask);*/
	ivariant->value = ivalue;
}

void ishidaopcua_set_variant_array_dimensions(ishidaopcua_ARRAY* iarray_dimensions, ishidaopcua_VARIANT* ivariant)
{
	ivariant->array_dimensions = iarray_dimensions;
	ivariant->encoding_mask |= ((ishidaopcua_BYTE) 0x40);
}

void ishidaopcua_set_variant(ishidaopcua_UINT32 iarray_length,ishidaopcua_ARRAY* iarray_dimensions, ishidaopcua_TYPE_ID itype_id, void* ivalue, ishidaopcua_VARIANT* ivariant)
{
	ishidaopcua_set_variant_array_length(iarray_length, ivariant);
	ishidaopcua_set_variant_value(itype_id, ivalue, ivariant);
	ishidaopcua_set_variant_array_dimensions(iarray_dimensions, ivariant);
}

ishidaopcua_BYTE ishidaopcua_get_variant_encoding_mask(ishidaopcua_VARIANT* ivariant)
{
	return ivariant->encoding_mask;
}

ishidaopcua_UINT32 ishidaopcua_get_variant_array_length(ishidaopcua_VARIANT* ivariant)
{
	return ivariant->array_length;
}

void* ishidaopcua_get_variant_value(ishidaopcua_VARIANT* ivariant)
{
	return ivariant->value;
}

ishidaopcua_ARRAY* ishidaopcua_get_variant_array_dimensions(ishidaopcua_VARIANT* ivariant)
{
	return ivariant->array_dimensions;
}

ishidaopcua_TYPE_ID ishidaopcua_get_variant_type_id(ishidaopcua_VARIANT* ivariant)
{
	return (ishidaopcua_TYPE_ID)((ivariant->encoding_mask & ((ishidaopcua_BYTE) 0x3F)));
}

ishidaopcua_DATA_VALUE* ishidaopcua_init_data_value()
{
	ishidaopcua_DATA_VALUE* data_value = ishidaeutz_malloc(1, sizeof(ishidaopcua_DATA_VALUE));
	data_value->encoding_mask = ((ishidaopcua_BYTE) 0x00);
	data_value->value = 0;
	data_value->status_code = 0;
	data_value->psource_timestamp = 0;
	data_value->pserver_timestamp = 0;
	data_value->encoding_mask = 0;
	return data_value;
}

void ishidaopcua_set_data_value_value(ishidaopcua_VARIANT* ivalue, ishidaopcua_DATA_VALUE* idata_value)
{
	idata_value->value = ivalue;
	idata_value->encoding_mask |= ((ishidaopcua_BYTE) 0x01);
}

void ishidaopcua_set_data_value_status_code(ishidaopcua_STATUS_CODE istatus_code, ishidaopcua_DATA_VALUE* idata_value)
{
	idata_value->status_code = istatus_code;

	if(istatus_code == ishidaopcua_STATUS_CODE_SEVERITY_GOOD){
		idata_value->encoding_mask ^= ((ishidaopcua_BYTE) 0x02);
	}
	else{
		idata_value->encoding_mask |= ((ishidaopcua_BYTE) 0x02);
	}
}

void ishidaopcua_set_data_value_source_timestamp(ishidaopcua_DATE_TIME* ipsource_timestamp, ishidaopcua_DATA_VALUE* idata_value)
{
	idata_value->psource_timestamp = ipsource_timestamp;

	if(compare_ishidaeutz_LARGE_INTs(ipsource_timestamp, pishidaopcua_DATE_TIME_MIN, 0) == 0){
		idata_value->encoding_mask ^= ((ishidaopcua_BYTE) 0x04);
	}
	else{
		idata_value->encoding_mask |= ((ishidaopcua_BYTE) 0x04);
	}
}

void ishidaopcua_set_data_value_source_pico_seconds(ishidaopcua_UINT16 isource_pico_seconds, ishidaopcua_DATA_VALUE* idata_value)
{
	idata_value->source_pico_seconds = isource_pico_seconds;

	if(isource_pico_seconds == 0){
		idata_value->encoding_mask ^= ((ishidaopcua_BYTE) 0x10);
	}
	else{
		idata_value->encoding_mask |= ((ishidaopcua_BYTE) 0x10);
	}
}

void ishidaopcua_set_data_value_server_timestamp(ishidaopcua_DATE_TIME* ipserver_timestamp, ishidaopcua_DATA_VALUE* idata_value)
{
	idata_value->pserver_timestamp = ipserver_timestamp;

	if(compare_ishidaeutz_LARGE_INTs(ipserver_timestamp, pishidaopcua_DATE_TIME_MIN, 0) == 0){
		idata_value->encoding_mask ^= ((ishidaopcua_BYTE) 0x08);
	}
	else{
		idata_value->encoding_mask |= ((ishidaopcua_BYTE) 0x08);
	}
}

void ishidaopcua_set_data_value_server_pico_seconds(ishidaopcua_UINT16 iserver_pico_seconds, ishidaopcua_DATA_VALUE* idata_value)
{
	idata_value->server_pico_seconds = iserver_pico_seconds;

	if(iserver_pico_seconds == 0){
		idata_value->encoding_mask ^= ((ishidaopcua_BYTE) 0x20);
	}
	else{
		idata_value->encoding_mask |= ((ishidaopcua_BYTE) 0x20);
	}
}

ishidaopcua_STATUS_CODE ishidaopcua_get_data_value_status_code(ishidaopcua_DATA_VALUE* idata_value)
{
	return idata_value->status_code;
}

ishidaopcua_DATE_TIME* ishidaopcua_get_data_value_source_timestamp(ishidaopcua_DATA_VALUE* idata_value)
{
	return idata_value->psource_timestamp;
}

ishidaopcua_UINT16 ishidaopcua_get_data_value_source_pico_seconds(ishidaopcua_DATA_VALUE* idata_value)
{
	return idata_value->source_pico_seconds;
}

ishidaopcua_DATE_TIME* ishidaopcua_get_data_value_server_timestamp(ishidaopcua_DATA_VALUE* idata_value)
{
	return idata_value->pserver_timestamp;
}

ishidaopcua_UINT16 ishidaopcua_get_data_value_server_pico_seconds(ishidaopcua_DATA_VALUE* idata_value)
{
	return idata_value->server_pico_seconds;
}

ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_SYMBOLIC_ID				= ((ishidaopcua_BYTE) 0x01);
ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_NAMESPACE_URI			= ((ishidaopcua_BYTE) 0x02);
ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALIZED_TEXT			= ((ishidaopcua_BYTE) 0x04);
ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALE					= ((ishidaopcua_BYTE) 0x08);
ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_ADDITIONAL_INFO			= ((ishidaopcua_BYTE) 0x10);
ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_STATUS_CODE		= ((ishidaopcua_BYTE) 0x20);
ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_DIAGNOSTIC_INFO	= ((ishidaopcua_BYTE) 0x40);

ishidaopcua_DIAGNOSTIC_INFO* ishidaopcua_init_diagnostic_info()
{
	ishidaopcua_DIAGNOSTIC_INFO* diagnostic_info = ishidaeutz_malloc(1, sizeof(ishidaopcua_DIAGNOSTIC_INFO));
	diagnostic_info->encoding_mask = ((ishidaopcua_BYTE) 0x00);

	diagnostic_info->symbolic_id = -1;
	diagnostic_info->namespace_uri = -1;
	diagnostic_info->localized_text = -1;
	diagnostic_info->locale = -1;

	diagnostic_info->additional_info = ishidaopcua_init_string();
	ishidaopcua_set_string(0,diagnostic_info->additional_info);

	diagnostic_info->inner_status_code = ishidaopcua_STATUS_CODE_SEVERITY_GOOD;

	diagnostic_info->inner_diagnostic_info = 0;

	return diagnostic_info;
}

void ishidaopcua_set_diagnostic_info_symbolic_id(ishidaopcua_UINT32 isymbolic_id, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	idiagnostic_info->symbolic_id = isymbolic_id;
}

void ishidaopcua_set_diagnostic_info_namespace_uri(ishidaopcua_UINT32 inamespace_uri, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	idiagnostic_info->namespace_uri = inamespace_uri;
}

void ishidaopcua_set_diagnostic_info_localized_text(ishidaopcua_UINT32 ilocalized_text, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	idiagnostic_info->localized_text = ilocalized_text;
}

void ishidaopcua_set_diagnostic_info_locale(ishidaopcua_UINT32 ilocale, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	idiagnostic_info->locale = ilocale;
}

void ishidaopcua_set_diagnostic_info_additional_info(ishidaopcua_STRING* iadditional_info, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	idiagnostic_info->additional_info = iadditional_info;
}

void ishidaopcua_set_diagnostic_info_inner_status_code(ishidaopcua_STATUS_CODE iinner_status_code, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	idiagnostic_info->inner_status_code = iinner_status_code;
}

void ishidaopcua_set_diagnostic_info_inner_diagnostic_info(ishidaopcua_DIAGNOSTIC_INFO* iinner_diagnostic_info, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	idiagnostic_info->inner_diagnostic_info = iinner_diagnostic_info;
}

void ishidaopcua_set_diagnostic_info(ishidaopcua_UINT32 isymbolic_id, ishidaopcua_UINT32 inamespace_uri,
								ishidaopcua_UINT32 ilocalized_text, ishidaopcua_UINT32 ilocale, ishidaopcua_STRING* iadditional_info,
								ishidaopcua_STATUS_CODE iinner_status_code, ishidaopcua_DIAGNOSTIC_INFO* iinner_diagnostic_info, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	ishidaopcua_set_diagnostic_info_symbolic_id(isymbolic_id, idiagnostic_info);
	ishidaopcua_set_diagnostic_info_namespace_uri(inamespace_uri, idiagnostic_info);
	ishidaopcua_set_diagnostic_info_localized_text(ilocalized_text, idiagnostic_info);
	ishidaopcua_set_diagnostic_info_locale(ilocale, idiagnostic_info);
	ishidaopcua_set_diagnostic_info_additional_info(iadditional_info, idiagnostic_info);
	ishidaopcua_set_diagnostic_info_inner_status_code(iinner_status_code, idiagnostic_info);
	ishidaopcua_set_diagnostic_info_inner_diagnostic_info(iinner_diagnostic_info, idiagnostic_info);
}

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_symbolic_id(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	return idiagnostic_info->symbolic_id;
}

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_namespace_uri(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	return idiagnostic_info->namespace_uri;
}

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_localized_text(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	return idiagnostic_info->localized_text;
}

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_locale(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	return idiagnostic_info->locale;
}

ishidaopcua_STRING* ishidaopcua_get_diagnostic_info_additional_info(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	return idiagnostic_info->additional_info;
}

ishidaopcua_STATUS_CODE ishidaopcua_get_diagnostic_info_inner_status_code(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	return idiagnostic_info->inner_status_code;
}

ishidaopcua_DIAGNOSTIC_INFO* ishidaopcua_get_diagnostic_info_inner_diagnostic_info(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info)
{
	return idiagnostic_info->inner_diagnostic_info;
}
/*************************************** ISHIDAOPCUA_TYPES END ***************************************/

/*************************************** ISHIDAOPCUA_CUSTOM_TYPES START ***************************************/
ishidaopcua_REQUEST_HEADER* ishidaopcua_init_request_header()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_REQUEST_HEADER));
}

void ishidaopcua_set_request_header_authentication_token(ishidaopcua_EXPANDED_NODE_ID* iauthentication_token, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	irequest_header->authentication_token = iauthentication_token;
}

void ishidaopcua_set_request_header_timestamp(ishidaopcua_UTC_TIME* iptimestamp, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	irequest_header->ptimestamp = iptimestamp;
}

void ishidaopcua_set_request_header_request_handle(ishidaopcua_UINT32 irequest_handle, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	irequest_header->request_handle = irequest_handle;
}

void ishidaopcua_set_request_header_return_diagnostics(ishidaopcua_UINT32 ireturn_diagnostics, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	irequest_header->return_diagnostics = ireturn_diagnostics;
}

void ishidaopcua_set_request_header_audit_entry_id(ishidaopcua_STRING* iaudit_entry_id, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	irequest_header->audit_entry_id = iaudit_entry_id;
}

void ishidaopcua_set_request_header_timeout_hint(ishidaopcua_UINT32 itimeout_hint, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	irequest_header->timeout_hint = itimeout_hint;
}

void ishidaopcua_set_request_header_additional_header(ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	irequest_header->additional_header = iadditional_header;
}

void ishidaopcua_set_request_header(ishidaopcua_EXPANDED_NODE_ID* iauthentication_token, ishidaopcua_UTC_TIME* iptimestamp, ishidaopcua_UINT32 irequest_handle,
									ishidaopcua_UINT32 ireturn_diagnostics, ishidaopcua_STRING* iaudit_entry_id,
									ishidaopcua_UINT32 itimeout_hint, ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_REQUEST_HEADER* irequest_header)
{
	ishidaopcua_set_request_header_authentication_token(iauthentication_token, irequest_header);
	ishidaopcua_set_request_header_timestamp(iptimestamp, irequest_header);
	ishidaopcua_set_request_header_request_handle(irequest_handle, irequest_header);
	ishidaopcua_set_request_header_return_diagnostics(ireturn_diagnostics, irequest_header);
	ishidaopcua_set_request_header_audit_entry_id(iaudit_entry_id, irequest_header);
	ishidaopcua_set_request_header_timeout_hint(itimeout_hint, irequest_header);
	ishidaopcua_set_request_header_additional_header(iadditional_header, irequest_header);
}

ishidaopcua_EXPANDED_NODE_ID* ishidaopcua_get_request_header_authentication_token(ishidaopcua_REQUEST_HEADER* irequest_header)
{
	return irequest_header->authentication_token;
}

ishidaopcua_UTC_TIME* ishidaopcua_get_request_header_timestamp(ishidaopcua_REQUEST_HEADER* irequest_header)
{
	return irequest_header->ptimestamp;
}

ishidaopcua_UINT32 ishidaopcua_get_request_header_request_handle(ishidaopcua_REQUEST_HEADER* irequest_header)
{
	return irequest_header->request_handle;
}

ishidaopcua_UINT32 ishidaopcua_get_request_header_return_diagnostics(ishidaopcua_REQUEST_HEADER* irequest_header)
{
	return irequest_header->return_diagnostics;
}

ishidaopcua_STRING* ishidaopcua_get_request_header_audit_entry_id(ishidaopcua_REQUEST_HEADER* irequest_header)
{
	return irequest_header->audit_entry_id;
}

ishidaopcua_UINT32 ishidaopcua_get_request_header_timeout_hint(ishidaopcua_REQUEST_HEADER* irequest_header)
{
	return irequest_header->timeout_hint;
}

ishidaopcua_EXTENSION_OBJECT* ishidaopcua_get_request_header_additional_header(ishidaopcua_REQUEST_HEADER* irequest_header)
{
	return irequest_header->additional_header;
}


ishidaopcua_RESPONSE_HEADER* ishidaopcua_init_response_header()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_RESPONSE_HEADER));
}

void ishidaopcua_set_response_header_timestamp(ishidaeutz_LARGE_INT* iptimestamp, ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	iresponse_header->ptimestamp = iptimestamp;
}

void ishidaopcua_set_response_header_request_handle(ishidaopcua_UINT32 irequest_handle, ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	iresponse_header->request_handle = irequest_handle;
}

void ishidaopcua_set_response_header_service_result(ishidaopcua_STATUS_CODE iservice_result, ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	iresponse_header->service_result = iservice_result;
}

void ishidaopcua_set_response_header_service_diagnostics(ishidaopcua_DIAGNOSTIC_INFO* iservice_diagnostics, ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	iresponse_header->service_diagnostics = iservice_diagnostics;
}

void ishidaopcua_set_response_header_string_table(ishidaopcua_ARRAY* istring_table, ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	iresponse_header->string_table = istring_table;
}

void ishidaopcua_set_response_header_additional_header(ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	iresponse_header->additional_header = iadditional_header;
}

void ishidaopcua_set_response_header(ishidaeutz_LARGE_INT* iptimestamp, ishidaopcua_UINT32 irequest_handle, ishidaopcua_STATUS_CODE iservice_result,
										ishidaopcua_DIAGNOSTIC_INFO* iservice_diagnostics, ishidaopcua_ARRAY* istring_table,
										ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	ishidaopcua_set_response_header_timestamp(iptimestamp, iresponse_header);
	ishidaopcua_set_response_header_request_handle(irequest_handle, iresponse_header);
	ishidaopcua_set_response_header_service_result(iservice_result, iresponse_header);
	ishidaopcua_set_response_header_service_diagnostics(iservice_diagnostics, iresponse_header);
	ishidaopcua_set_response_header_string_table(istring_table, iresponse_header);
	ishidaopcua_set_response_header_additional_header(iadditional_header, iresponse_header);
}

ishidaeutz_LARGE_INT* ishidaopcua_get_response_header_timestamp(ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	return iresponse_header->ptimestamp;
}

ishidaopcua_UINT32 ishidaopcua_get_response_header_request_handle(ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	return iresponse_header->request_handle;
}

ishidaopcua_STATUS_CODE ishidaopcua_get_response_header_service_result(ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	return iresponse_header->service_result;
}

ishidaopcua_DIAGNOSTIC_INFO* ishidaopcua_get_response_header_service_diagnostics(ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	return iresponse_header->service_diagnostics;
}

ishidaopcua_ARRAY* ishidaopcua_get_response_header_string_table(ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	return iresponse_header->string_table;
}

ishidaopcua_EXTENSION_OBJECT* ishidaopcua_get_response_header_additional_header(ishidaopcua_RESPONSE_HEADER* iresponse_header)
{
	return iresponse_header->additional_header;
}


ishidaopcua_SECURITY_TOKEN* ishidaopcua_init_security_token()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_SECURITY_TOKEN));
}

void ishidaopcua_set_security_token_secure_channel_id(ishidaopcua_UINT32 isecure_channel_id, ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	isecurity_token->secure_channel_id = isecure_channel_id;
}

void ishidaopcua_set_security_token_token_id(ishidaopcua_UINT32 itoken_id, ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	isecurity_token->token_id = itoken_id;
}

void ishidaopcua_set_security_token_created_at(ishidaopcua_UTC_TIME* ipcreated_at, ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	isecurity_token->pcreated_at = ipcreated_at;
}

void ishidaopcua_set_security_token_revised_lifetime(ishidaopcua_UINT32 irevised_lifetime, ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	isecurity_token->revised_lifetime = irevised_lifetime;
}

void ishidaopcua_set_security_token(ishidaopcua_UINT32 isecure_channel_id, ishidaopcua_UINT32 itoken_id, ishidaopcua_UTC_TIME* ipcreated_at,
									ishidaopcua_UINT32 irevised_lifetime, ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	ishidaopcua_set_security_token_secure_channel_id(isecure_channel_id, isecurity_token);
	ishidaopcua_set_security_token_token_id(itoken_id, isecurity_token);
	ishidaopcua_set_security_token_created_at(ipcreated_at, isecurity_token);
	ishidaopcua_set_security_token_revised_lifetime(irevised_lifetime, isecurity_token);
}

ishidaopcua_UINT32 ishidaopcua_get_security_token_secure_channel_id(ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	return isecurity_token->secure_channel_id;
}

ishidaopcua_UINT32 ishidaopcua_get_security_token_token_id(ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	return isecurity_token->token_id;
}

ishidaopcua_UTC_TIME* ishidaopcua_get_security_token_created_at(ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	return isecurity_token->pcreated_at;
}

ishidaopcua_UINT32 ishidaopcua_get_security_token_revised_lifetime(ishidaopcua_SECURITY_TOKEN* isecurity_token)
{
	return isecurity_token->revised_lifetime;
}

ishidaopcua_SIGNATURE* ishidaopcua_init_signature()
{
	ishidaopcua_SIGNATURE*  psignature = ishidaeutz_malloc(1, sizeof(ishidaopcua_SIGNATURE));
	psignature->algorithm = 0;
	psignature->signature = 0;
	return psignature;
}

void ishidaopcua_set_signature_algorithm(ishidaopcua_STRING* ialgorithm, ishidaopcua_SIGNATURE* isignature)
{
	isignature->algorithm = ialgorithm;
}

void ishidaopcua_set_signature_signature(ishidaopcua_STRING* isignature_string, ishidaopcua_SIGNATURE* isignature)
{
	isignature->signature = isignature_string;
}

void ishidaopcua_set_signature(ishidaopcua_STRING* ialgorithm, ishidaopcua_STRING* isignature_string, ishidaopcua_SIGNATURE* isignature)
{
	ishidaopcua_set_signature_algorithm(ialgorithm, isignature);
	ishidaopcua_set_signature_signature(isignature_string, isignature);
}

ishidaopcua_STRING* ishidaopcua_get_signature_algorithm(ishidaopcua_SIGNATURE* isignature)
{
	return isignature->algorithm;
}

ishidaopcua_STRING* ishidaopcua_get_signature_signature(ishidaopcua_SIGNATURE* isignature)
{
	return isignature->signature;
}

ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* ishidaopcua_init_signed_software_certificate()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE));
}

void ishidaopcua_set_signed_software_certificate_certificate_data(ishidaopcua_STRING* icertificate_data, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate)
{
	isigned_software_certificate->certificate_data = icertificate_data;
}

void ishidaopcua_set_signed_software_certificate_signature(ishidaopcua_STRING* isignature, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate)
{
	isigned_software_certificate->signature = isignature;
}

void ishidaopcua_set_signed_software_certificate(ishidaopcua_STRING* icertificate_data, ishidaopcua_STRING* isignature, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate)
{
	ishidaopcua_set_signed_software_certificate_certificate_data(icertificate_data, isigned_software_certificate);
	ishidaopcua_set_signed_software_certificate_signature(isignature, isigned_software_certificate);
}

ishidaopcua_STRING* ishidaopcua_get_signed_software_certificate_certificate_data(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate)
{
	return isigned_software_certificate->certificate_data;
}

ishidaopcua_STRING* ishidaopcua_get_signed_software_certificate_signature(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate)
{
	return isigned_software_certificate->signature;
}

ishidaopcua_APPLICATION_DESCRIPTION* ishidaopcua_init_application_description()
{
	ishidaopcua_APPLICATION_DESCRIPTION* application_description = ishidaeutz_malloc(1, sizeof(ishidaopcua_APPLICATION_DESCRIPTION));
	application_description->discovery_urls = 0;
	return application_description;
}

void ishidaopcua_set_application_description_application_uri(ishidaopcua_STRING* iapplication_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	iapplication_description->application_uri = iapplication_uri;
}

void ishidaopcua_set_application_description_product_uri(ishidaopcua_STRING* iproduct_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	iapplication_description->product_uri = iproduct_uri;
}

void ishidaopcua_set_application_description_application_name(ishidaopcua_LOCALIZED_TEXT* iapplication_name, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	iapplication_description->application_name = iapplication_name;
}

void ishidaopcua_set_application_description_application_type(ishidaopcua_UINT32 iapplication_type, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	iapplication_description->application_type = iapplication_type;
}

void ishidaopcua_set_application_description_gateway_server_uri(ishidaopcua_STRING* igateway_server_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	iapplication_description->gateway_server_uri = igateway_server_uri;
}

void ishidaopcua_set_application_description_discovery_profile_uri(ishidaopcua_STRING* idiscovery_profile_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	iapplication_description->discovery_profile_uri = idiscovery_profile_uri;
}

void ishidaopcua_set_application_description_discovery_urls(ishidaopcua_UINT32 idiscovery_urls_size, ishidaopcua_STRING* idiscovery_urls, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	if(iapplication_description->discovery_urls == 0)
	{
		iapplication_description->discovery_urls = ishidaopcua_init_array();
	}

	ishidaopcua_set_array(idiscovery_urls_size, ishidaopcua_TYPE_ID_STRING, idiscovery_urls, iapplication_description->discovery_urls);
}

void ishidaopcua_set_application_description(ishidaopcua_STRING* iapplication_uri, ishidaopcua_STRING* iproduct_uri, ishidaopcua_LOCALIZED_TEXT* iapplication_name,
												ishidaopcua_UINT32 iapplication_type, ishidaopcua_STRING* igateway_server_uri, ishidaopcua_STRING* idiscovery_profile_uri,
												ishidaopcua_UINT32 idiscovery_urls_size, ishidaopcua_STRING* idiscovery_urls, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	ishidaopcua_set_application_description_application_uri(iapplication_uri, iapplication_description);
	ishidaopcua_set_application_description_product_uri(iproduct_uri, iapplication_description);
	ishidaopcua_set_application_description_application_name(iapplication_name, iapplication_description);
	ishidaopcua_set_application_description_application_type(iapplication_type, iapplication_description);
	ishidaopcua_set_application_description_gateway_server_uri(igateway_server_uri, iapplication_description);
	ishidaopcua_set_application_description_discovery_profile_uri(idiscovery_profile_uri, iapplication_description);
	ishidaopcua_set_application_description_discovery_urls(idiscovery_urls_size, idiscovery_urls, iapplication_description);
}

ishidaopcua_STRING* ishidaopcua_get_application_description_application_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	return iapplication_description->application_uri;
}

ishidaopcua_STRING* ishidaopcua_get_application_description_product_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	return iapplication_description->product_uri;
}

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_get_application_description_application_name(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	return iapplication_description->application_name;
}

ishidaopcua_UINT32 ishidaopcua_get_application_description_application_type(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	return iapplication_description->application_type;
}

ishidaopcua_STRING* ishidaopcua_get_application_description_gateway_server_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	return iapplication_description->gateway_server_uri;
}

ishidaopcua_STRING* ishidaopcua_get_application_description_discovery_profile_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	return iapplication_description->discovery_profile_uri;
}

ishidaopcua_ARRAY* ishidaopcua_get_application_description_discovery_urls(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description)
{
	return iapplication_description->discovery_urls;
}

ishidaopcua_STRUCTURE* ishidaopcua_init_structure()
{
	ishidaopcua_STRUCTURE* pstructure = ishidaeutz_malloc(1, sizeof(ishidaopcua_STRUCTURE));
	pstructure->length = 0;
	pstructure->data = 0;
	pstructure->encoding = 0;
	pstructure->type_id = 0;
	return pstructure;
}

void ishidaopcua_set_structure_type_id(ishidaopcua_UINT32 itype_id, ishidaopcua_STRUCTURE* istructure)
{
	istructure->type_id = itype_id;
}

void ishidaopcua_set_structure_encoding(ishidaopcua_BYTE iencoding, ishidaopcua_STRUCTURE* istructure)
{
	istructure->encoding = iencoding;
}

void ishidaopcua_set_structure_length(ishidaopcua_UINT32 ilength, ishidaopcua_STRUCTURE* istructure)
{
	istructure->length = ilength;
}

void ishidaopcua_set_structure_data(ishidaopcua_BYTE* idata, ishidaopcua_STRUCTURE* istructure)
{
	istructure->data = idata;
}

void ishidaopcua_set_structure(ishidaopcua_UINT32 itype_id, ishidaopcua_BYTE iencoding, ishidaopcua_UINT32 ilength,
								ishidaopcua_BYTE* idata, ishidaopcua_STRUCTURE* istructure)
{
	ishidaopcua_set_structure_type_id(itype_id, istructure);
	ishidaopcua_set_structure_encoding(iencoding, istructure);
	ishidaopcua_set_structure_length(ilength, istructure);
	ishidaopcua_set_structure_data(idata, istructure);
}

ishidaopcua_UINT32 ishidaopcua_get_structure_type_id(ishidaopcua_STRUCTURE* istructure)
{
	return istructure->type_id;
}

ishidaopcua_BYTE ishidaopcua_get_structure_encoding(ishidaopcua_STRUCTURE* istructure)
{
	return istructure->encoding;
}

ishidaopcua_UINT32 ishidaopcua_get_structure_length(ishidaopcua_STRUCTURE* istructure)
{
	return istructure->length;
}

ishidaopcua_BYTE* ishidaopcua_get_structure_data(ishidaopcua_STRUCTURE* istructure)
{
	return istructure->data;
}

ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* ishidaopcua_init_application_instance_certificate()
{
	ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* application_instance_certificate = ishidaeutz_malloc(1, sizeof(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE));
	application_instance_certificate->hostnames = 0;
	application_instance_certificate->key_usage = 0;
	return application_instance_certificate;
}

void ishidaopcua_set_application_instance_certificate_version(ishidaopcua_STRING* iversion, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->version = iversion;
}

void ishidaopcua_set_application_instance_certificate_serial_no(ishidaopcua_BYTESTRING* iserial_no, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->serial_no = iserial_no;
}

void ishidaopcua_set_application_instance_certificate_signature_algorithm(ishidaopcua_STRING* isignature_algorithm, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->signature_algorithm = isignature_algorithm;
}

void ishidaopcua_set_application_instance_certificate_signature(ishidaopcua_BYTESTRING* isignature, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->signature = isignature;
}

void ishidaopcua_set_application_instance_certificate_issuer(ishidaopcua_STRUCTURE* iissuer, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->issuer = iissuer;
}

void ishidaopcua_set_application_instance_certificate_valid_from(ishidaopcua_UTC_TIME* ipvalid_from, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->pvalid_from = ipvalid_from;
}

void ishidaopcua_set_application_instance_certificate_valid_to(ishidaopcua_UTC_TIME* ipvalid_to, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->pvalid_to = ipvalid_to;
}

void ishidaopcua_set_application_instance_certificate_subject(ishidaopcua_STRUCTURE* isubject, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->subject  = isubject;
}

void ishidaopcua_set_application_instance_certificate_application_uri(ishidaopcua_STRING* iapplication_uri, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->application_uri = iapplication_uri;
}

void ishidaopcua_set_application_instance_certificate_hostnames(ishidaopcua_UINT32 ihostnames_size, ishidaopcua_STRING* ihostnames, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	if(iapplication_instance_certificate->hostnames == 0)
	{
		iapplication_instance_certificate->hostnames = ishidaopcua_init_array();
	}

	ishidaopcua_set_array(ihostnames_size, ishidaopcua_TYPE_ID_STRING, ihostnames, iapplication_instance_certificate->hostnames);
}

void ishidaopcua_set_application_instance_certificate_public_key(ishidaopcua_BYTESTRING* ipublic_key, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	iapplication_instance_certificate->public_key = ipublic_key;
}

void ishidaopcua_set_application_instance_certificate_key_usage(ishidaopcua_UINT32 ikey_usage_size, ishidaopcua_STRING* ikey_usage, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	if(iapplication_instance_certificate->key_usage == 0)
	{
		iapplication_instance_certificate->key_usage = ishidaopcua_init_array();
	}

	ishidaopcua_set_array(ikey_usage_size, ishidaopcua_TYPE_ID_STRING, ikey_usage, iapplication_instance_certificate->key_usage);
}

void ishidaopcua_set_application_instance_certificate(ishidaopcua_STRING* iversion, ishidaopcua_BYTESTRING* iserial_no, ishidaopcua_STRING* isignature_algorithm, ishidaopcua_BYTESTRING* isignature,
														ishidaopcua_STRUCTURE* iissuer, ishidaopcua_UTC_TIME* ipvalid_from, ishidaopcua_UTC_TIME* ipvalid_to, ishidaopcua_STRUCTURE* isubject, ishidaopcua_STRING* iapplication_uri,
														ishidaopcua_UINT32 ihostnames_size, ishidaopcua_STRING* ihostnames, ishidaopcua_BYTESTRING* ipublic_key,
														ishidaopcua_UINT32 ikey_usage_size, ishidaopcua_STRING* ikey_usage, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	ishidaopcua_set_application_instance_certificate_version(iversion, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_serial_no(iserial_no, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_signature_algorithm(isignature_algorithm, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_signature(isignature, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_issuer(iissuer, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_valid_from(ipvalid_from, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_valid_to(ipvalid_to, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_subject(isubject, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_application_uri(iapplication_uri, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_hostnames(ihostnames_size, ihostnames, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_public_key(ipublic_key, iapplication_instance_certificate);
	ishidaopcua_set_application_instance_certificate_key_usage(ikey_usage_size, ikey_usage, iapplication_instance_certificate);
}

ishidaopcua_STRING* ishidaopcua_get_application_instance_certificate_version(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->version;
}

ishidaopcua_BYTESTRING* ishidaopcua_get_application_instance_certificate_serial_no(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->serial_no;
}

ishidaopcua_STRING* ishidaopcua_get_application_instance_certificate_signature_algorithm(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->signature_algorithm;
}

ishidaopcua_BYTESTRING* ishidaopcua_get_application_instance_certificate_signature(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->signature;
}

ishidaopcua_STRUCTURE* ishidaopcua_get_application_instance_certificate_issuer(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->issuer;
}

ishidaopcua_UTC_TIME* ishidaopcua_get_application_instance_certificate_valid_from(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->pvalid_from;
}

ishidaopcua_UTC_TIME* ishidaopcua_get_application_instance_certificate_valid_to(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->pvalid_to;
}

ishidaopcua_STRUCTURE* ishidaopcua_get_application_instance_certificate_subject(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->subject;
}

ishidaopcua_STRING* ishidaopcua_get_application_instance_certificate_application_uri(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->application_uri;
}

ishidaopcua_ARRAY* ishidaopcua_get_application_instance_certificate_hostnames(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->hostnames;
}

ishidaopcua_BYTESTRING* ishidaopcua_get_application_instance_certificate_public_key(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->public_key;
}

ishidaopcua_ARRAY* ishidaopcua_get_application_instance_certificate_key_usage(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate)
{
	return iapplication_instance_certificate->key_usage;
}

ishidaopcua_USER_TOKEN_POLICY* ishidaopcua_init_user_token_policy()
{
		return ishidaeutz_malloc(1, sizeof(ishidaopcua_USER_TOKEN_POLICY));
}

void ishidaopcua_set_user_token_policy_policy_id(ishidaopcua_STRING* ipolicy_id, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	iuser_token_policy->policy_id = ipolicy_id;
}

void ishidaopcua_set_user_token_policy_token_type(ishidaopcua_UINT32 itoken_type, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	iuser_token_policy->token_type = itoken_type;
}

void ishidaopcua_set_user_token_policy_issued_token_type(ishidaopcua_STRING* iissued_token_type, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	iuser_token_policy->issued_token_type = iissued_token_type;
}

void ishidaopcua_set_user_token_policy_issuer_endpoint_url(ishidaopcua_STRING* iissuer_endpoint_url, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	iuser_token_policy->issuer_endpoint_url = iissuer_endpoint_url;
}

void ishidaopcua_set_user_token_policy_security_policy_uri(ishidaopcua_STRING* isecurity_policy_uri, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	iuser_token_policy->security_policy_uri = isecurity_policy_uri;
}

void ishidaopcua_set_user_token_policy(ishidaopcua_STRING* ipolicy_id, ishidaopcua_UINT32 itoken_type, ishidaopcua_STRING* iissued_token_type, ishidaopcua_STRING* iissuer_endpoint_url,
										ishidaopcua_STRING* isecurity_policy_uri, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	ishidaopcua_set_user_token_policy_policy_id(ipolicy_id, iuser_token_policy);
	ishidaopcua_set_user_token_policy_token_type(itoken_type, iuser_token_policy);
	ishidaopcua_set_user_token_policy_issued_token_type(iissued_token_type, iuser_token_policy);
	ishidaopcua_set_user_token_policy_issuer_endpoint_url(iissuer_endpoint_url, iuser_token_policy);
	ishidaopcua_set_user_token_policy_security_policy_uri(isecurity_policy_uri, iuser_token_policy);
}

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_policy_id(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	return iuser_token_policy->policy_id;
}

ishidaopcua_UINT32 ishidaopcua_get_user_token_policy_token_type(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	return iuser_token_policy->token_type;
}

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_issued_token_type(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	return iuser_token_policy->issued_token_type;
}

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_issuer_endpoint_url(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	return iuser_token_policy->issuer_endpoint_url;
}

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_security_policy_uri(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy)
{
	return iuser_token_policy->security_policy_uri;
}

ishidaopcua_ENDPOINT_DESCRIPTION* ishidaopcua_init_endpoint_description()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_ENDPOINT_DESCRIPTION));
}

void ishidaopcua_set_endpoint_description_endpoint_url(ishidaopcua_STRING* iendpoint_url, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	iendpoint_description->endpoint_url = iendpoint_url;
}

void ishidaopcua_set_endpoint_description_server(ishidaopcua_APPLICATION_DESCRIPTION* iserver, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	iendpoint_description->server = iserver;
}

void ishidaopcua_set_endpoint_description_server_certificate(ishidaopcua_BYTESTRING* iserver_certificate, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	iendpoint_description->server_certificate = iserver_certificate;
}

void ishidaopcua_set_endpoint_description_security_mode(ishidaopcua_UINT32 isecurity_mode, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	iendpoint_description->security_mode = isecurity_mode;
}

void ishidaopcua_set_endpoint_description_security_policy_uri(ishidaopcua_STRING* isecurity_policy_uri, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	iendpoint_description->security_policy_uri = isecurity_policy_uri;
}

void ishidaopcua_set_endpoint_description_user_identity_tokens(ishidaopcua_UINT32 iuser_identity_tokens_size, ishidaopcua_USER_TOKEN_POLICY* iuser_identity_tokens, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	ishidaopcua_set_array(iuser_identity_tokens_size, ishidaopcua_TYPE_ID_USER_TOKEN_POLICY, iuser_identity_tokens, iendpoint_description->user_identity_tokens );
}

void ishidaopcua_set_endpoint_description_transport_profile_uri(ishidaopcua_STRING* itransport_profile_uri, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	iendpoint_description->transport_profile_uri = itransport_profile_uri;
}

void ishidaopcua_set_endpoint_description_security_level(ishidaopcua_BYTE isecurity_level, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	iendpoint_description->security_level = isecurity_level;
}

ishidaopcua_STRING* ishidaopcua_get_endpoint_description_endpoint_url(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->endpoint_url;
}

ishidaopcua_APPLICATION_DESCRIPTION* ishidaopcua_get_endpoint_description_server(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->server;
}

ishidaopcua_BYTESTRING* ishidaopcua_get_endpoint_description_server_certificate(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->server_certificate;
}

ishidaopcua_UINT32 ishidaopcua_get_endpoint_description_security_mode(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->security_mode;
}

ishidaopcua_STRING* ishidaopcua_get_endpoint_description_security_policy_uri(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->security_policy_uri;
}

ishidaopcua_ARRAY* ishidaopcua_get_endpoint_description_user_identity_tokens(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->user_identity_tokens;
}

ishidaopcua_STRING* ishidaopcua_get_endpoint_description_transport_profile_uri(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->transport_profile_uri;
}

ishidaopcua_BYTE ishidaopcua_get_endpoint_description_security_level(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description)
{
	return iendpoint_description->security_level;
}

ishidaopcua_READ_VALUE_ID* ishidaopcua_init_read_value_id()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_READ_VALUE_ID));
}

void ishidaopcua_set_read_value_id_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	iread_value_id->node_id = inode_id;
}

void ishidaopcua_set_read_value_id_attribute_id(ishidaopcua_UINT32 iattribute_id, ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	iread_value_id->attribute_id = iattribute_id;
}

void ishidaopcua_set_read_value_id_index_range(ishidaopcua_STRING* iindex_range, ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	iread_value_id->index_range = iindex_range;
}

void ishidaopcua_set_read_value_id_data_encoding(ishidaopcua_QUALIFIED_NAME* idata_encoding, ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	iread_value_id->data_encoding = idata_encoding;
}

void ishidaopcua_set_read_value_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_UINT32 iattribute_id, ishidaopcua_STRING* iindex_range,
									ishidaopcua_QUALIFIED_NAME* idata_encoding, ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	ishidaopcua_set_read_value_id_node_id(inode_id, iread_value_id);
	ishidaopcua_set_read_value_id_attribute_id(iattribute_id, iread_value_id);
	ishidaopcua_set_read_value_id_index_range(iindex_range, iread_value_id);
	ishidaopcua_set_read_value_id_data_encoding(idata_encoding, iread_value_id);
}

ishidaopcua_NODE_ID* ishidaopcua_get_read_value_id_node_id(ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	return iread_value_id->node_id;
}

ishidaopcua_UINT32 ishidaopcua_get_read_value_id_attribute_id(ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	return iread_value_id->attribute_id;
}

ishidaopcua_STRING* ishidaopcua_get_read_value_id_index_range(ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	return iread_value_id->index_range;
}

ishidaopcua_QUALIFIED_NAME* ishidaopcua_get_read_value_id_data_encoding(ishidaopcua_READ_VALUE_ID* iread_value_id)
{
	return iread_value_id->data_encoding;
}

ishidaopcua_NODE* ishidaopcua_node_init(ishidaopcua_UINT32 inum_attributes, ishidaopcua_UINT32 inum_references, ishidaopcua_UINT32 inum_standard_properties)
{
	ishidaopcua_NODE* node = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE));
	if(inum_attributes > 0){
		node->attributes = ishidaeutz_init_hashmap(inum_attributes);
	}

	if(inum_references > 0){
		node->references = ishidaeutz_init_hashmap(inum_references);
	}

	if(inum_standard_properties > 0){
		node->standard_properties = ishidaeutz_init_hashmap(inum_standard_properties);
	}

	return node;
}

void ishidaopcua_node_set_value(ishidaopcua_VARIANT* ivalue, ishidaopcua_NODE* inode)
{
	ishidaopcua_NODE_ID* data_type;
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_VALUE_ATTRIBUTE_KEY),ivalue);

	data_type = ishidaopcua_init_node_id();
	ishidaopcua_set_node_id_identifier_numeric((ishidaopcua_UINT16)(ivalue->encoding_mask & 0x3F), data_type);
	/*printf("Node set value encoding mask %u\n", data_type->identifier.numeric);*/

	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_DATA_TYPE_ATTRIBUTE_KEY),data_type);


}

ishidaopcua_VARIANT* ishidaopcua_node_get_value(ishidaopcua_NODE* inode)
{
	ishidaopcua_VARIANT* value;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_VALUE_ATTRIBUTE_KEY),(const void**)&value);
	return value;
}

void ishidaopcua_node_set_data_type(ishidaopcua_NODE_ID* idata_type, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_DATA_TYPE_ATTRIBUTE_KEY),idata_type);
}

ishidaopcua_NODE_ID* ishidaopcua_node_get_data_type(ishidaopcua_NODE* inode)
{
	ishidaopcua_NODE_ID* data_type;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_DATA_TYPE_ATTRIBUTE_KEY),(const void**)&data_type);
	return data_type;
}

void ishidaopcua_node_set_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_NODE_ID_ATTRIBUTE_KEY),inode_id);
}

ishidaopcua_NODE_ID* ishidaopcua_node_get_node_id(ishidaopcua_NODE* inode)
{
	ishidaopcua_NODE_ID* node_id;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_NODE_ID_ATTRIBUTE_KEY),(const void**)&node_id);
	return node_id;
}

void ishidaopcua_node_set_node_class(ishidaopcua_NODE_CLASS* inode_class, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_NODE_CLASS_ATTRIBUTE_KEY),inode_class);
}

ishidaopcua_NODE_CLASS* ishidaopcua_node_get_node_class(ishidaopcua_NODE* inode)
{
	ishidaopcua_NODE_CLASS* node_class;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_NODE_CLASS_ATTRIBUTE_KEY),(const void**)&node_class);
	return node_class;
}

void ishidaopcua_node_set_browse_name(ishidaopcua_QUALIFIED_NAME* ibrowse_name, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_BROWSE_NAME_ATTRIBUTE_KEY),ibrowse_name);
}

ishidaopcua_QUALIFIED_NAME* ishidaopcua_node_get_browse_name(ishidaopcua_NODE* inode)
{
	ishidaopcua_QUALIFIED_NAME* browse_name;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_BROWSE_NAME_ATTRIBUTE_KEY),(const void**)&browse_name);
	return browse_name;
}

void ishidaopcua_node_set_display_name(ishidaopcua_LOCALIZED_TEXT* idisplay_name, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_DISPLAY_NAME_ATTRIBUTE_KEY),idisplay_name);
}

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_node_get_display_name(ishidaopcua_NODE* inode)
{
	ishidaopcua_LOCALIZED_TEXT* display_name;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_DISPLAY_NAME_ATTRIBUTE_KEY),(const void**)&display_name);
	return display_name;
}

void ishidaopcua_node_set_description(ishidaopcua_LOCALIZED_TEXT* idescription, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_DESCRIPTION_ATTRIBUTE_KEY),idescription);
}

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_node_get_description(ishidaopcua_NODE* inode)
{
	ishidaopcua_LOCALIZED_TEXT* description;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_DESCRIPTION_ATTRIBUTE_KEY),(const void**)&description);
	return description;
}

void ishidaopcua_node_set_write_mask(ishidaopcua_UINT32* iwrite_mask, ishidaopcua_NODE* inode)
{
	/*printf("Set write mask %u at %p and address %d\n",*iwrite_mask,iwrite_mask,iwrite_mask);*/
	ishidaeutz_put_hashmap(inode->attributes,"6",iwrite_mask);
}

ishidaopcua_UINT32* ishidaopcua_node_get_write_mask(ishidaopcua_NODE* inode)
{
	ishidaopcua_UINT32* write_mask;
	ishidaeutz_get_hashmap(inode->attributes,"6",(const void**)&write_mask);
	return write_mask;
}

void ishidaopcua_node_set_user_write_mask(ishidaopcua_UINT32* iuser_write_mask, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_USER_WRITE_MASK_ATTRIBUTE_KEY),iuser_write_mask);
}

ishidaopcua_UINT32* ishidaopcua_node_get_user_write_mask(ishidaopcua_NODE* inode)
{
	ishidaopcua_UINT32* user_write_mask;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_USER_WRITE_MASK_ATTRIBUTE_KEY),(const void**)&user_write_mask);
	return user_write_mask;
}

void ishidaopcua_node_set_is_abstract(ishidaopcua_BOOLEAN* iis_abstract, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_IS_ABSTRACT_ATTRIBUTE_KEY),iis_abstract);
}

ishidaopcua_BOOLEAN* ishidaopcua_node_get_is_abstract(ishidaopcua_NODE* inode)
{
	ishidaopcua_BOOLEAN* is_abstract;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_IS_ABSTRACT_ATTRIBUTE_KEY),(const void**)&is_abstract);
	return is_abstract;
}

void ishidaopcua_node_set_symmetric(ishidaopcua_BOOLEAN* isymmetric, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_SYMMETRIC_ATTRIBUTE_KEY),isymmetric);
}

ishidaopcua_BOOLEAN* ishidaopcua_node_get_symmetric(ishidaopcua_NODE* inode)
{
	ishidaopcua_BOOLEAN* symmetric;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_SYMMETRIC_ATTRIBUTE_KEY),(const void**)&symmetric);
	return symmetric;
}

void ishidaopcua_node_set_inverse_name(ishidaopcua_LOCALIZED_TEXT* iinverse_name, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_INVERSE_NAME_ATTRIBUTE_KEY),iinverse_name);
}

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_node_get_inverse_name(ishidaopcua_NODE* inode)
{
	ishidaopcua_LOCALIZED_TEXT* inverse_name;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_INVERSE_NAME_ATTRIBUTE_KEY),(const void**)&inverse_name);
	return inverse_name;
}

void ishidaopcua_node_set_contains_no_loops(ishidaopcua_BOOLEAN* icontains_no_loops, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_CONTAINS_NO_LOOPS_ATTRIBUTE_KEY),icontains_no_loops);
}

ishidaopcua_BOOLEAN* ishidaopcua_node_get_contains_no_loops(ishidaopcua_NODE* inode)
{
	ishidaopcua_BOOLEAN* contains_no_loops;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_CONTAINS_NO_LOOPS_ATTRIBUTE_KEY),(const void**)&contains_no_loops);
	return contains_no_loops;
}

void ishidaopcua_node_set_event_notifier(ishidaopcua_BYTE* ievent_notifier, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_EVENT_NOTIFIER_ATTRIBUTE_KEY),ievent_notifier);
}

ishidaopcua_BYTE* ishidaopcua_node_get_event_notifier(ishidaopcua_NODE* inode)
{
	ishidaopcua_BYTE* event_notifier;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_EVENT_NOTIFIER_ATTRIBUTE_KEY),(const void**)&event_notifier);
	return event_notifier;
}

void ishidaopcua_node_set_value_rank(ishidaopcua_INT32* ivalue_rank, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_VALUE_RANK_ATTRIBUTE_KEY),ivalue_rank);
}

ishidaopcua_INT32* ishidaopcua_node_get_value_rank(ishidaopcua_NODE* inode)
{
	ishidaopcua_INT32* value_rank;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_VALUE_RANK_ATTRIBUTE_KEY),(const void**)&value_rank);
	return value_rank;
}

void ishidaopcua_node_set_array_dimensions(ishidaopcua_ARRAY* iparray_dimensions, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_ARRAY_DIMENSIONS_ATTRIBUTE_KEY),iparray_dimensions);
}

ishidaopcua_ARRAY* ishidaopcua_node_get_array_dimensions(ishidaopcua_NODE* inode)
{
	ishidaopcua_ARRAY* parray_dimensions;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_ARRAY_DIMENSIONS_ATTRIBUTE_KEY),(const void**)&parray_dimensions);
	return parray_dimensions;
}

void ishidaopcua_node_set_access_level(ishidaopcua_BYTE* iaccess_level, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_ACCESS_LEVEL_ATTRIBUTE_KEY),iaccess_level);
}

ishidaopcua_BYTE* ishidaopcua_node_get_access_level(ishidaopcua_NODE* inode)
{
	ishidaopcua_BYTE* access_level;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_ACCESS_LEVEL_ATTRIBUTE_KEY),(const void**)&access_level);
	return access_level;
}

void ishidaopcua_node_set_user_access_level(ishidaopcua_BYTE* iuser_access_level, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_USER_ACCESS_LEVEL_ATTRIBUTE_KEY),iuser_access_level);
}

ishidaopcua_BYTE* ishidaopcua_node_get_user_access_level(ishidaopcua_NODE* inode)
{
	ishidaopcua_BYTE* user_access_level;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_USER_ACCESS_LEVEL_ATTRIBUTE_KEY),(const void**)&user_access_level);
	return user_access_level;
}

void ishidaopcua_node_set_minimum_sampling_interval(ishidaopcua_DURATION* iminimum_sampling_interval, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_MINIMUM_SAMPLING_INTERVAL_ATTRIBUTE_KEY),iminimum_sampling_interval);
}

ishidaopcua_DURATION* ishidaopcua_node_get_minimum_sampling_interval(ishidaopcua_NODE* inode)
{
	ishidaopcua_DURATION* minimum_sampling_interval;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_MINIMUM_SAMPLING_INTERVAL_ATTRIBUTE_KEY),(const void**)&minimum_sampling_interval);

	return minimum_sampling_interval;
}

void ishidaopcua_node_set_historizing(ishidaopcua_BOOLEAN* ihistorizing, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_HISTORIZING_ATTRIBUTE_KEY),ihistorizing);
}

ishidaopcua_BOOLEAN* ishidaopcua_node_get_historizing(ishidaopcua_NODE* inode)
{
	ishidaopcua_BOOLEAN* historizing;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_HISTORIZING_ATTRIBUTE_KEY),(const void**)&historizing);
	return historizing;
}

void ishidaopcua_node_set_executable(ishidaopcua_BOOLEAN* iexecutable, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_EXECUTABLE_ATTRIBUTE_KEY),iexecutable);
}

ishidaopcua_BOOLEAN* ishidaopcua_node_get_executable(ishidaopcua_NODE* inode)
{
	ishidaopcua_BOOLEAN* executable;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_EXECUTABLE_ATTRIBUTE_KEY),(const void**)&executable);
	return executable;
}

void ishidaopcua_node_set_user_executable(ishidaopcua_BOOLEAN* iuser_executable, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_USER_EXECUTABLE_ATTRIBUTE_KEY),iuser_executable);
}

ishidaopcua_BOOLEAN* ishidaopcua_node_get_user_executable(ishidaopcua_NODE* inode)
{
	ishidaopcua_BOOLEAN* user_executable;
	ishidaeutz_get_hashmap(inode->attributes,ishidaeutz_unsigned_int_to_string(ISHIDAOPCUA_NODE_USER_EXECUTABLE_ATTRIBUTE_KEY),(const void**)&user_executable);
	return user_executable;
}


void ishidaopcua_node_set_reference_type_id(ishidaopcua_UINT32* ireference_type_id, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ISHIDAOPCUA_REFERENCE_TYPE_ID_ATTRIBUTE_KEY,ireference_type_id);
}

ishidaopcua_UINT32* ishidaopcua_node_get_reference_type_id(ishidaopcua_NODE* inode)
{
	ishidaopcua_UINT32* reference_type_id;
	ishidaeutz_get_hashmap(inode->attributes, ISHIDAOPCUA_REFERENCE_TYPE_ID_ATTRIBUTE_KEY, ((const void**) &reference_type_id));
	return reference_type_id;
}

void ishidaopcua_node_set_target_id(ishidaopcua_UINT32* itarget_id, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ISHIDAOPCUA_TARGET_ID_ATTRIBUTE_KEY,itarget_id);
}

ishidaopcua_UINT32* ishidaopcua_node_get_target_id(ishidaopcua_NODE* inode)
{
	ishidaopcua_UINT32* target_id;
	ishidaeutz_get_hashmap(inode->attributes, ISHIDAOPCUA_TARGET_ID_ATTRIBUTE_KEY, ((const void**) &target_id));
	return target_id;
}

void ishidaopcua_node_set_is_inverse(ishidaopcua_UINT32* iis_inverse, ishidaopcua_NODE* inode)
{
	ishidaeutz_put_hashmap(inode->attributes,ISHIDAOPCUA_IS_INVERSE_ATTRIBUTE_KEY,iis_inverse);
}

ishidaopcua_UINT32* ishidaopcua_node_get_is_inverse(ishidaopcua_NODE* inode)
{
	ishidaopcua_UINT32* is_inverse;
	ishidaeutz_get_hashmap(inode->attributes,ISHIDAOPCUA_IS_INVERSE_ATTRIBUTE_KEY, ((const void**) &is_inverse));
	return is_inverse;
}

ishidaopcua_VIEW_DESCRIPTION* ishidaopcua_init_view_description()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_VIEW_DESCRIPTION));
}

void ishidaopcua_set_view_description_pview_id(ishidaopcua_NODE_ID* ipview_id, ishidaopcua_VIEW_DESCRIPTION* iview_description)
{
	iview_description->pview_id = ipview_id;
}

void ishidaopcua_set_view_description_timestamp(ishidaopcua_UTC_TIME* iptimestamp, ishidaopcua_VIEW_DESCRIPTION* iview_description)
{
	iview_description->ptimestamp = iptimestamp;
}

void ishidaopcua_set_view_description_view_version(ishidaopcua_UINT32 iview_version, ishidaopcua_VIEW_DESCRIPTION* iview_description)
{
	iview_description->view_version = iview_version;
}

void ishidaopcua_set_view_description(ishidaopcua_NODE_ID* ipview_id, ishidaopcua_UTC_TIME* iptimestamp,  ishidaopcua_UINT32 iview_version, ishidaopcua_VIEW_DESCRIPTION* iview_description)
{
	ishidaopcua_set_view_description_pview_id(ipview_id, iview_description);
	ishidaopcua_set_view_description_timestamp(iptimestamp, iview_description);
	ishidaopcua_set_view_description_view_version(iview_version, iview_description);
}

ishidaopcua_NODE_ID* ishidaopcua_get_view_description_pview_id(ishidaopcua_VIEW_DESCRIPTION* iview_description)
{
	return iview_description->pview_id;
}

ishidaopcua_UTC_TIME* ishidaopcua_get_view_description_timestamp(ishidaopcua_VIEW_DESCRIPTION* iview_description)
{
	return iview_description->ptimestamp;
}

ishidaopcua_UINT32 ishidaopcua_get_view_description_view_version(ishidaopcua_VIEW_DESCRIPTION* iview_description)
{
	return iview_description->view_version;
}

ishidaopcua_BROWSE_DESCRIPTION* ishidaopcua_init_browse_description()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_BROWSE_DESCRIPTION));
}

void ishidaopcua_set_browse_description_pnode_id(ishidaopcua_NODE_ID* ipnode_id, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	ibrowse_description->pnode_id = ipnode_id;
}

void ishidaopcua_set_browse_description_browse_direction(ishidaopcua_UINT32 ibrowse_direction, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	ibrowse_description->browse_direction = ibrowse_direction;
}

void ishidaopcua_set_browse_description_preference_type_id(ishidaopcua_NODE_ID* ipreference_type_id, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	ibrowse_description->preference_type_id = ipreference_type_id;
}

void ishidaopcua_set_browse_description_include_subtypes(ishidaopcua_BOOLEAN iinclude_subtypes, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	ibrowse_description->include_subtypes = iinclude_subtypes;
}

void ishidaopcua_set_browse_description_node_class_mask(ishidaopcua_UINT32 inode_class_mask, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	ibrowse_description->node_class_mask = inode_class_mask;
}

void ishidaopcua_set_browse_description_result_class_mask(ishidaopcua_UINT32 iresult_class_mask, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	ibrowse_description->result_class_mask = iresult_class_mask;
}

void ishidaopcua_set_browse_description(ishidaopcua_NODE_ID* ipnode_id, ishidaopcua_UINT32 ibrowse_direction, ishidaopcua_NODE_ID* ipreference_type_id, ishidaopcua_BOOLEAN iinclude_subtypes,
										ishidaopcua_UINT32 inode_class_mask, ishidaopcua_UINT32 iresult_class_mask,  ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	ishidaopcua_set_browse_description_pnode_id(ipnode_id, ibrowse_description);
	ishidaopcua_set_browse_description_browse_direction(ibrowse_direction, ibrowse_description);
	ishidaopcua_set_browse_description_preference_type_id(ipreference_type_id, ibrowse_description);
	ishidaopcua_set_browse_description_include_subtypes(iinclude_subtypes, ibrowse_description);
	ishidaopcua_set_browse_description_node_class_mask(inode_class_mask, ibrowse_description);
	ishidaopcua_set_browse_description_result_class_mask(iresult_class_mask, ibrowse_description);
}

ishidaopcua_NODE_ID* ishidaopcua_get_view_description_pnode_id(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	return ibrowse_description->pnode_id;
}

ishidaopcua_UINT32 ishidaopcua_get_view_description_browse_direction(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	return ibrowse_description->browse_direction;
}

ishidaopcua_NODE_ID* ishidaopcua_get_view_description_preference_type_id(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	return ibrowse_description->preference_type_id;
}

ishidaopcua_BOOLEAN ishidaopcua_get_view_description_include_subtypes(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	return ibrowse_description->include_subtypes;
}

ishidaopcua_UINT32 ishidaopcua_get_view_description_node_class_mask(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	return ibrowse_description->node_class_mask;
}

ishidaopcua_UINT32 ishidaopcua_get_view_description_result_class_mask(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description)
{
	return ibrowse_description->result_class_mask;
}

ishidaopcua_BROWSE_RESULT* ishidaopcua_init_browse_result()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_BROWSE_RESULT));
}

ishidaopcua_WRITE_VALUE* ishidaopcua_init_write_value()
{
	return ishidaeutz_malloc(1, sizeof(ishidaopcua_WRITE_VALUE));
}

void ishidaopcua_set_write_value_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_WRITE_VALUE* iwrite_value)
{
	iwrite_value->node_id = inode_id;
}

void ishidaopcua_set_write_value_attribute_id(ishidaopcua_INTEGER_ID iattribute_id, ishidaopcua_WRITE_VALUE* iwrite_value)
{
	iwrite_value->attribute_id = iattribute_id;
}

void ishidaopcua_set_write_value_index_range(ishidaopcua_NUMERIC_RANGE* iindex_range, ishidaopcua_WRITE_VALUE* iwrite_value)
{
	iwrite_value->index_range = iindex_range;
}

void ishidaopcua_set_write_value_value(ishidaopcua_DATA_VALUE* ivalue, ishidaopcua_WRITE_VALUE* iwrite_value)
{
	iwrite_value->value = ivalue;
}

void ishidaopcua_set_write_value(ishidaopcua_NODE_ID* inode_id, ishidaopcua_INTEGER_ID iattribute_id, ishidaopcua_NUMERIC_RANGE* iindex_range,
									ishidaopcua_DATA_VALUE* ivalue, ishidaopcua_WRITE_VALUE* iwrite_value)
{
	ishidaopcua_set_write_value_node_id(inode_id, iwrite_value);
	ishidaopcua_set_write_value_attribute_id(iattribute_id, iwrite_value);
	ishidaopcua_set_write_value_index_range(iindex_range, iwrite_value);
	ishidaopcua_set_write_value_value(ivalue, iwrite_value);
}

ishidaopcua_NODE_ID* ishidaopcua_get_write_value_node_id(ishidaopcua_WRITE_VALUE* iwrite_value)
{
	return iwrite_value->node_id;
}

ishidaopcua_INTEGER_ID ishidaopcua_get_write_value_attribute_id(ishidaopcua_WRITE_VALUE* iwrite_value)
{
	return iwrite_value->attribute_id;
}

ishidaopcua_NUMERIC_RANGE* ishidaopcua_get_write_value_index_range(ishidaopcua_WRITE_VALUE* iwrite_value)
{
	return iwrite_value->index_range;
}

ishidaopcua_DATA_VALUE* ishidaopcua_get_write_value_value(ishidaopcua_WRITE_VALUE* iwrite_value)
{
	return iwrite_value->value;
}

ishidaopcua_REFERENCE_DESCRIPTION* ishidaopcua_init_reference_description()
{
	ishidaopcua_REFERENCE_DESCRIPTION* preference_description = ishidaeutz_malloc(1, sizeof(ishidaopcua_REFERENCE_DESCRIPTION));
	preference_description->preference_type_id = 0;
	preference_description->pnode_id = 0;
	preference_description->pbrowse_name = 0;
	preference_description->pdisplay_name = 0;
	preference_description->ptype_definition = 0;
	return preference_description;
}

ishidaopcua_TCP_MESSAGE_HEADER* ishidaopcua_init_tcp_message_header()
{
	ishidaopcua_TCP_MESSAGE_HEADER* ptcp_message_header = ishidaeutz_malloc(1, sizeof(ishidaopcua_TCP_MESSAGE_HEADER));
	ptcp_message_header->message_type[3] = '\0';
	ptcp_message_header->reserved = (ishidaopcua_BYTE)'F';
	return ptcp_message_header;
}

void ishidaopcua_set_tcp_message_header_message_type(ishidaopcua_BYTE imessage_type[3], ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header)
{
	ishidaeutz_copy_byte_array((char*)&imessage_type[0], (char*)&(itcp_message_header->message_type [0]), (ishidaopcua_UINT32)0, (ishidaopcua_UINT32)0, (ishidaopcua_UINT32)3);
}

void ishidaopcua_set_tcp_message_header_reserved(ishidaopcua_BYTE ireserved, ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header)
{
	itcp_message_header->reserved = ireserved;
}

void ishidaopcua_set_tcp_message_header_message_size(ishidaopcua_UINT32 imessage_size, ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header)
{
	itcp_message_header->message_size = imessage_size;
}

void ishidaopcua_set_tcp_message_header(ishidaopcua_BYTE imessage_type[3], ishidaopcua_BYTE ireserved, ishidaopcua_UINT32 imessage_size, ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header)
{
	ishidaopcua_set_tcp_message_header_message_type(imessage_type, itcp_message_header);
	ishidaopcua_set_tcp_message_header_reserved(ireserved, itcp_message_header);
	ishidaopcua_set_tcp_message_header_message_size(imessage_size, itcp_message_header);
}

ishidaopcua_BYTE* ishidaopcua_get_tcp_message_header_message_type(ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header)
{
	return &(itcp_message_header->message_type[0]);
}

ishidaopcua_BYTE ishidaopcua_get_tcp_message_header_reserved(ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header)
{
	return itcp_message_header->reserved;
}

ishidaopcua_UINT32 ishidaopcua_get_tcp_message_header_message_size(ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header)
{
	return itcp_message_header->message_size;
}

void* ishidaopcua_malloc(ishidaopcua_TYPE_ID itype_id, ishidaopcua_UINT32 isize_factor)
{
	void* ptr = NULL;
	switch(itype_id){
		case(ishidaopcua_TYPE_ID_SBYTE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_SBYTE));
			break;
		}
		case(ishidaopcua_TYPE_ID_BYTE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_BYTE));
			break;
		}
		case(ishidaopcua_TYPE_ID_BOOLEAN):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_BOOLEAN));
			break;
		}
		case(ishidaopcua_TYPE_ID_INT16):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_INT16));
			break;
		}
		case(ishidaopcua_TYPE_ID_UINT16):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_UINT16));
			break;
		}
		case(ishidaopcua_TYPE_ID_INT32):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_INT32));
			break;
		}
		case(ishidaopcua_TYPE_ID_UINT32):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_UINT32));
			break;
		}
		case(ishidaopcua_TYPE_ID_INT64):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_INT64));
			break;
		}
		case(ishidaopcua_TYPE_ID_UINT64):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_UINT64));
			break;
		}
		case(ishidaopcua_TYPE_ID_FLOAT):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_FLOAT));
			break;
		}
		case(ishidaopcua_TYPE_ID_DOUBLE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_DOUBLE));
			break;
		}
		case(ishidaopcua_TYPE_ID_STRING):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_STRING));
			break;
		}
		case(ishidaopcua_TYPE_ID_DATE_TIME):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_DATE_TIME));
			break;
		}
		case(ishidaopcua_TYPE_ID_GUID):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_GUID));
			break;
		}
		case(ishidaopcua_TYPE_ID_BYTESTRING):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_BYTESTRING));
			break;
		}
		case(ishidaopcua_TYPE_ID_XML_ELEMENT):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_XML_ELEMENT));
			break;
		}
		case(ishidaopcua_TYPE_ID_NODE_ID):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_NODE_ID));
			break;
		}
		case(ishidaopcua_TYPE_ID_EXPANDED_NODE_ID):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_EXPANDED_NODE_ID));
			break;
		}
		case(ishidaopcua_TYPE_ID_STATUS_CODE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_STATUS_CODE));
			break;
		}
		case(ishidaopcua_TYPE_ID_QUALIFIED_NAME):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_QUALIFIED_NAME));
			break;
		}
		case(ishidaopcua_TYPE_ID_LOCALIZED_TEXT):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_LOCALIZED_TEXT));
			break;
		}
		case(ishidaopcua_TYPE_ID_EXTENSION_OBJECT):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_EXTENSION_OBJECT));
			break;
		}
		case(ishidaopcua_TYPE_ID_VARIANT):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_VARIANT));
			break;
		}
		case(ishidaopcua_TYPE_ID_DATA_VALUE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_DATA_VALUE));
			break;
		}
		case(ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_DIAGNOSTIC_INFO));
			break;
		}
		case(ishidaopcua_TYPE_ID_MESSAGE_SECURITY_MODE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_MESSAGE_SECURITY_MODE));
			break;
		}
		case(ishidaopcua_TYPE_ID_USER_IDENTITY_TOKEN_TYPE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_USER_IDENTITY_TOKEN_TYPE));
			break;
		}
		case(ishidaopcua_TYPE_ID_UTC_TIME):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_UTC_TIME));
			break;
		}
		case(ishidaopcua_TYPE_ID_DURATION):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_DURATION));
			break;
		}
		case(ishidaopcua_TYPE_ID_REQUEST_HEADER):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_REQUEST_HEADER));
			break;
		}
		case(ishidaopcua_TYPE_ID_RESPONSE_HEADER):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_RESPONSE_HEADER));
			break;
		}
		case(ishidaopcua_TYPE_ID_SECURITY_TOKEN):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_SECURITY_TOKEN));
			break;
		}
		case(ishidaopcua_TYPE_ID_SIGNATURE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_SIGNATURE));
			break;
		}
		case(ishidaopcua_TYPE_ID_SIGNED_SOFTWARE_CERTIFICATE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE));
			break;
		}
		case(ishidaopcua_TYPE_ID_APPLICATION_DESCRIPTION):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_APPLICATION_DESCRIPTION));
			break;
		}
		case(ishidaopcua_TYPE_ID_STRUCTURE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_STRUCTURE));
			break;
		}
		case(ishidaopcua_TYPE_ID_APPLICATION_INSTANCE_CERTIFICATE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE));
			break;
		}
		case(ishidaopcua_TYPE_ID_USER_TOKEN_POLICY):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_USER_TOKEN_POLICY));
			break;
		}
		case(ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_ENDPOINT_DESCRIPTION));
			break;
		}
		case(ishidaopcua_TYPE_ID_READ_VALUE_ID):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_READ_VALUE_ID));
			break;
		}
		case(ishidaopcua_TYPE_ID_NODE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_NODE));
			break;
		}
		case(ishidaopcua_TYPE_ID_VIEW_DESCRIPTION):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_VIEW_DESCRIPTION));
			break;
		}
		case(ishidaopcua_TYPE_ID_COUNTER):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_COUNTER));
			break;
		}
		case(ishidaopcua_TYPE_ID_BROWSE_DESCRIPTION):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_BROWSE_DESCRIPTION));
			break;
		}
		case(ishidaopcua_TYPE_ID_INTEGER_ID):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_INTEGER_ID));
			break;
		}
		case(ishidaopcua_TYPE_ID_NUMERIC_RANGE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_NUMERIC_RANGE));
			break;
		}
		case(ishidaopcua_TYPE_ID_WRITE_VALUE):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_WRITE_VALUE));
			break;
		}
		case(ishidaopcua_TYPE_ID_TCP_MESSAGE_HEADER):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_TCP_MESSAGE_HEADER));
			break;
		}
		case(ishidaopcua_TYPE_ID_ARRAY):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_ARRAY));
			break;
		}
		case(ishidaopcua_TYPE_ID_BROWSE_RESULT):{
			ptr = ishidaeutz_malloc(isize_factor, sizeof(ishidaopcua_BROWSE_RESULT));
			break;
		}
		default:{
		   break;
		}
	}
	return ptr;
}
/*************************************** ISHIDAOPCUA_CUSTOM_TYPES END ***************************************/

/*************************************** ISHIDAOPCUA_ENCODE_DECODER_PAIRS START ***************************************/
void ishidaopcua_decode_BYTE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BYTE* ipbyte, ishidaopcua_UINT32 *ipparsing_offset)
{
	*ipbyte = ipdata_buffer[*ipparsing_offset];
	*ipparsing_offset += 1;
}

void ishidaopcua_encode_BYTE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_BYTE* ipbyte, ishidaopcua_UINT32* ipwrite_size)
{
	ipdata_buffer[*ipwrite_size] = *ipbyte;
	*ipwrite_size += 1;
}

void ishidaopcua_decode_SBYTE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SBYTE* ipsbyte, ishidaopcua_UINT32 *ipparsing_offset)
{
	*ipsbyte = ((ishidaopcua_SBYTE) ipdata_buffer[*ipparsing_offset]);
	*ipparsing_offset += 1;
}

void ishidaopcua_encode_SBYTE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SBYTE* ipsbyte, ishidaopcua_UINT32 *ipwrite_size)
{
	ipdata_buffer[*ipwrite_size]= ((ishidaopcua_BYTE) *ipsbyte);
	*ipwrite_size += 1;
}

void ishidaopcua_decode_BOOLEAN(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BOOLEAN* ipboolean, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) ipboolean, ipparsing_offset);
}

void ishidaopcua_encode_BOOLEAN(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BOOLEAN* ipboolean, ishidaopcua_UINT32 *ipwrite_size)
{
	ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) ipboolean, ipwrite_size);
}

void ishidaopcua_decode_UINT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT16* ipuint16, ishidaopcua_UINT32 *ipparsing_offset)
{
	*ipuint16 = ((ipdata_buffer[(*ipparsing_offset)+1] & 0xff) << 8) + (ipdata_buffer[(*ipparsing_offset)+0] & 0xff);
	*ipparsing_offset += 2;
}

void ishidaopcua_encode_UINT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT16* ipuint16, ishidaopcua_UINT32 *ipwrite_size)
{
	ipdata_buffer[(*ipwrite_size)+1] = ((*ipuint16) & 0xff00) >> 8;
	ipdata_buffer[(*ipwrite_size)] = ((*ipuint16) & 0x00ff);
	*ipwrite_size += 2;
}

void ishidaopcua_decode_INT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT16* ipint16, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*) ipint16, ipparsing_offset);
}

void ishidaopcua_encode_INT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT16* ipint16, ishidaopcua_UINT32 *ipwrite_size)
{
	ishidaopcua_encode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*) ipint16, ipwrite_size);
}

void ishidaopcua_decode_UINT32(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UINT32* ipuint32, ishidaopcua_UINT32 *ipparsing_offset)
{
	*ipuint32 = ((ipdata_buffer[(*ipparsing_offset)+3] & 0x000000FF) << 24) + ((ipdata_buffer[(*ipparsing_offset)+2] & 0x000000FF) << 16) +
					((ipdata_buffer[(*ipparsing_offset)+1] & 0x000000FF) << 8) +(ipdata_buffer[(*ipparsing_offset)+0] & 0x000000FF);
	*ipparsing_offset += 4;
}

void ishidaopcua_encode_UINT32(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UINT32* ipuint32, ishidaopcua_UINT32 *ipwrite_size)
{
	ipdata_buffer[(*ipwrite_size)+3] = ((*ipuint32) & 0xff000000) >> 24;
	ipdata_buffer[(*ipwrite_size)+2] = ((*ipuint32) & 0x00ff0000) >> 16;
	ipdata_buffer[(*ipwrite_size)+1] = ((*ipuint32) & 0x0000ff00) >> 8;
	ipdata_buffer[(*ipwrite_size)] = ((*ipuint32) & 0x000000ff);

	*ipwrite_size += 4;
}

void ishidaopcua_decode_INT32(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT32* ipint32, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) ipint32, ipparsing_offset);
}

void ishidaopcua_encode_INT32(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_INT32* ipint32, ishidaopcua_UINT32 *ipwrite_size)
{
	/*Message(0,"Encoding INT32 value %d",*ipint32);*/
	ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) ipint32, ipwrite_size);
}

void ishidaopcua_decode_UINT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT64* ipuint64, ishidaopcua_UINT32 *ipparsing_offset)
{
	/*extract_ishidaeutz_LARGE_INT_from_unsigned_int_byte_array(ipuint64, ipdata_buffer, *ipparsing_offset, 8);*/
	*ipparsing_offset += 8;
}

void ishidaopcua_encode_UINT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT64* ipuint64, ishidaopcua_UINT32 *ipwrite_size)
{
	/*if(ipuint64 != NULL && ipuint64->number_bytestring != NULL && ipuint64->number_bytestring_size > 0 && ipuint64->int_bytearray_size <= 0 && ipuint64->int_bytearray == NULL){
		yield_ishidaeutz_LARGE_INT_int_byte_array(ipuint64, ishidaopcua_SERVER_LARGE_INT_ENDIANNESS, 8);
	}

	if(ipuint64 != NULL && ipuint64->number_bytestring != NULL && ipuint64->number_bytestring_size > 0 && ipuint64->int_bytearray_size > 0 && ipuint64->int_bytearray != NULL){
		ishidaeutz_copy_byte_array(((char*) ipuint64->int_bytearray), ((char*) ipdata_buffer), 0, *ipwrite_size, ipuint64->int_bytearray_size);
	}*/

	*ipwrite_size += 8;
}

void ishidaopcua_decode_INT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT64* ipint64, ishidaopcua_UINT32 *ipparsing_offset)
{
	*ipparsing_offset += 8;
	/*TODO*/
}

void ishidaopcua_encode_INT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT64* ipint64, ishidaopcua_UINT32 *ipwrite_size)
{
	*ipwrite_size += 8;
	/*TODO*/
}

void ishidaopcua_decode_FLOAT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_FLOAT* ipfloat, ishidaopcua_UINT32 *ipparsing_offset)
{
	memcpy(ipfloat, (ipdata_buffer + (*ipparsing_offset)), sizeof(ishidaopcua_FLOAT));

	*ipparsing_offset += 4;
}

void ishidaopcua_encode_FLOAT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_FLOAT* ipfloat, ishidaopcua_UINT32 *ipwrite_size)
{
	memcpy((ipdata_buffer + (*ipwrite_size)), ipfloat, (int)sizeof(ishidaopcua_FLOAT));
	*ipwrite_size += 4;
}

void ishidaopcua_decode_DOUBLE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DOUBLE* ipdouble, ishidaopcua_UINT32 *ipparsing_offset)
{
	if(ipdouble != NULL){
		/*extract_ishidaeutz_LARGE_INT_from_IEEE754_dp_byte_array(ipdouble, ipdata_buffer, *ipparsing_offset);*/
	}

	*ipparsing_offset += 8;
}

void ishidaopcua_encode_DOUBLE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DOUBLE* ipdouble, ishidaopcua_UINT32 *ipwrite_size)
{
	/*if(ipdouble != NULL && ipdouble->number_bytestring != NULL && ipdouble->number_bytestring_size > 0 && ipdouble->double_bytearray == NULL){
		yield_ishidaeutz_LARGE_INT_IEEE754_double_precision_byte_array(ipdouble);
	}

	if(ipdouble != NULL && ipdouble->number_bytestring != NULL && ipdouble->number_bytestring_size > 0 && ipdouble->double_bytearray != NULL && ipdouble->double_bytearray_size == 8){
		memcpy(((ishidaopcua_DOUBLE*)(ipdata_buffer + (*ipwrite_size))), ipdouble->double_bytearray, 8);
	}*/

	*ipwrite_size += 8;
}

void ishidaopcua_decode_X_ARRAY(ishidaopcua_BYTE *ipdata_buffer, void* ipx_array, ishidaopcua_TYPE_ID ix_array_type_id, ishidaopcua_UINT32 ix_array_target_size, ishidaopcua_UINT32 *ipparsing_offset)
{
	/*printf("X-ARRAY DECODER: type id %u\n",ix_array_type_id);*/
	if(ix_array_target_size > 0 && ix_array_target_size < 0xFFFFFFFF)
	{
		ishidaopcua_UINT32 i;
		for(i=0; i<ix_array_target_size; i++)
		{
			switch(ix_array_type_id){
				case(ishidaopcua_TYPE_ID_APPLICATION_DESCRIPTION):{
				   ishidaopcua_decode_APPLICATION_DESCRIPTION(ipdata_buffer, (ishidaopcua_APPLICATION_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_APPLICATION_DESCRIPTION)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_APPLICATION_INSTANCE_CERTIFICATE):{
				   ishidaopcua_decode_APPLICATION_INSTANCE_CERTIFICATE(ipdata_buffer, (ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_ARRAY):{
				   ishidaopcua_decode_ARRAY(ipdata_buffer, (ishidaopcua_ARRAY*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_ARRAY)), ((ishidaopcua_ARRAY*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_ARRAY)))->type, 0, ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BOOLEAN):{
				   ishidaopcua_decode_BOOLEAN(ipdata_buffer, (ishidaopcua_BOOLEAN*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BOOLEAN)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BROWSE_DESCRIPTION):{
				   ishidaopcua_decode_BROWSE_DESCRIPTION(ipdata_buffer, (ishidaopcua_BROWSE_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BROWSE_DESCRIPTION)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BROWSE_RESULT):{
				   ishidaopcua_decode_BROWSE_RESULT(ipdata_buffer, (ishidaopcua_BROWSE_RESULT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BROWSE_RESULT)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BYTE):{
				   ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BYTE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BYTESTRING):{
				   ishidaopcua_decode_BYTESTRING(ipdata_buffer, (ishidaopcua_BYTESTRING*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BYTESTRING)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_COUNTER):{
				   ishidaopcua_decode_COUNTER(ipdata_buffer, (ishidaopcua_COUNTER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_COUNTER)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DATA_VALUE):{
				   ishidaopcua_decode_DATA_VALUE(ipdata_buffer, (ishidaopcua_DATA_VALUE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DATA_VALUE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DATE_TIME):{
				   ishidaopcua_decode_DATE_TIME(ipdata_buffer, (ishidaopcua_DATE_TIME*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DATE_TIME)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO):{
				   ishidaopcua_decode_DIAGNOSTIC_INFO(ipdata_buffer, (ishidaopcua_DIAGNOSTIC_INFO*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DIAGNOSTIC_INFO)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DOUBLE):{
				   ishidaopcua_decode_DOUBLE(ipdata_buffer, (ishidaopcua_DOUBLE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DOUBLE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DURATION):{
				   ishidaopcua_decode_DURATION(ipdata_buffer, (ishidaopcua_DURATION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DURATION)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION):{
				   ishidaopcua_decode_ENDPOINT_DESCRIPTION(ipdata_buffer, (ishidaopcua_ENDPOINT_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_ENDPOINT_DESCRIPTION)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_EXPANDED_NODE_ID):{
				   ishidaopcua_decode_EXPANDED_NODE_ID(ipdata_buffer, (ishidaopcua_EXPANDED_NODE_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_EXPANDED_NODE_ID)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_EXTENSION_OBJECT):{
				   ishidaopcua_decode_EXTENSION_OBJECT(ipdata_buffer, (ishidaopcua_EXTENSION_OBJECT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_EXTENSION_OBJECT)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_FLOAT):{
				   ishidaopcua_decode_FLOAT(ipdata_buffer, (ishidaopcua_FLOAT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_FLOAT)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_GUID):{
				   ishidaopcua_decode_GUID(ipdata_buffer, (ishidaopcua_GUID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_GUID)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INT16):{
				   ishidaopcua_decode_INT16(ipdata_buffer, (ishidaopcua_INT16*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INT16)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INT32):{
				   ishidaopcua_decode_INT32(ipdata_buffer, (ishidaopcua_INT32*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INT32)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INT64):{
				   ishidaopcua_decode_INT64(ipdata_buffer, (ishidaopcua_INT64*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INT64)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INTEGER_ID):{
				   ishidaopcua_decode_INTEGER_ID(ipdata_buffer, (ishidaopcua_INTEGER_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INTEGER_ID)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_LOCALIZED_TEXT):{
				   ishidaopcua_decode_LOCALIZED_TEXT(ipdata_buffer, (ishidaopcua_LOCALIZED_TEXT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_LOCALIZED_TEXT)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_MESSAGE_SECURITY_MODE):{
				   ishidaopcua_decode_MESSAGE_SECURITY_MODE(ipdata_buffer, (ishidaopcua_MESSAGE_SECURITY_MODE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_MESSAGE_SECURITY_MODE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_NODE_ID):{
				   ishidaopcua_decode_NODE_ID(ipdata_buffer, (ishidaopcua_NODE_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_NODE_ID)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_NUMERIC_RANGE):{
				   ishidaopcua_decode_NUMERIC_RANGE(ipdata_buffer, (ishidaopcua_NUMERIC_RANGE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_NUMERIC_RANGE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_QUALIFIED_NAME):{
				   ishidaopcua_decode_QUALIFIED_NAME(ipdata_buffer, (ishidaopcua_QUALIFIED_NAME*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_QUALIFIED_NAME)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_READ_VALUE_ID):{
				   ishidaopcua_decode_READ_VALUE_ID(ipdata_buffer, (ishidaopcua_READ_VALUE_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_READ_VALUE_ID)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_REFERENCE_DESCRIPTION):{
				   ishidaopcua_decode_REFERENCE_DESCRIPTION(ipdata_buffer, (ishidaopcua_REFERENCE_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_REFERENCE_DESCRIPTION)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_REQUEST_HEADER):{
				   ishidaopcua_decode_REQUEST_HEADER(ipdata_buffer, (ishidaopcua_REQUEST_HEADER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_REQUEST_HEADER)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_RESPONSE_HEADER):{
				   ishidaopcua_decode_RESPONSE_HEADER(ipdata_buffer, (ishidaopcua_RESPONSE_HEADER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_RESPONSE_HEADER)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SBYTE):{
				   ishidaopcua_decode_SBYTE(ipdata_buffer, (ishidaopcua_SBYTE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SBYTE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SECURITY_TOKEN):{
				   ishidaopcua_decode_SECURITY_TOKEN(ipdata_buffer, (ishidaopcua_SECURITY_TOKEN*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SECURITY_TOKEN)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SIGNATURE):{
				   ishidaopcua_decode_SIGNATURE(ipdata_buffer, (ishidaopcua_SIGNATURE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SIGNATURE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SIGNED_SOFTWARE_CERTIFICATE):{
				   ishidaopcua_decode_SIGNED_SOFTWARE_CERTIFICATE(ipdata_buffer, (ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_STATUS_CODE):{
				   ishidaopcua_decode_STATUS_CODE(ipdata_buffer, (ishidaopcua_STATUS_CODE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_STATUS_CODE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_STRING):{
				   ishidaopcua_decode_STRING(ipdata_buffer, (ishidaopcua_STRING*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_STRING)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_STRUCTURE):{
				   ishidaopcua_decode_STRUCTURE(ipdata_buffer, (ishidaopcua_STRUCTURE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_STRUCTURE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_TCP_MESSAGE_HEADER):{
				   ishidaopcua_decode_TCP_MESSAGE_HEADER(ipdata_buffer, (ishidaopcua_TCP_MESSAGE_HEADER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_TCP_MESSAGE_HEADER)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UINT16):{
				   ishidaopcua_decode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UINT16)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UINT32):{
				   ishidaopcua_decode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UINT32)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UINT64):{
				   ishidaopcua_decode_UINT64(ipdata_buffer, (ishidaopcua_UINT64*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UINT64)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_USER_IDENTITY_TOKEN_TYPE):{
				   ishidaopcua_decode_USER_IDENTITY_TOKEN_TYPE(ipdata_buffer, (ishidaopcua_USER_IDENTITY_TOKEN_TYPE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_USER_IDENTITY_TOKEN_TYPE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_USER_TOKEN_POLICY):{
				   ishidaopcua_decode_USER_TOKEN_POLICY(ipdata_buffer, (ishidaopcua_USER_TOKEN_POLICY*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_USER_TOKEN_POLICY)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UTC_TIME):{
				   ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, (ishidaopcua_UTC_TIME*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UTC_TIME)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_VARIANT):{
				   ishidaopcua_decode_VARIANT(ipdata_buffer, (ishidaopcua_VARIANT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_VARIANT)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_VIEW_DESCRIPTION):{
				   ishidaopcua_decode_VIEW_DESCRIPTION(ipdata_buffer, (ishidaopcua_VIEW_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_VIEW_DESCRIPTION)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_WRITE_VALUE):{
				   /*puts("Decoding write value");*/
				   ishidaopcua_decode_WRITE_VALUE(ipdata_buffer, (ishidaopcua_WRITE_VALUE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_WRITE_VALUE)), ipparsing_offset);
				   break;
				}
				case(ishidaopcua_TYPE_ID_XML_ELEMENT):{
				   ishidaopcua_decode_XML_ELEMENT(ipdata_buffer, (ishidaopcua_XML_ELEMENT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_XML_ELEMENT)), ipparsing_offset);
				   break;
				}
				default:
				{
				   break;
				}
			}
		}
	}
}

void ishidaopcua_encode_X_ARRAY(ishidaopcua_BYTE *ipdata_buffer, void* ipx_array, ishidaopcua_TYPE_ID ix_array_type_id, ishidaopcua_UINT32 ix_array_target_size, ishidaopcua_UINT32 *iwrite_size)
{
	if(ix_array_target_size > 0 && ix_array_target_size < 0xFFFFFFFF){
		ishidaopcua_UINT32 i;
		for(i=0; i<ix_array_target_size; i++){
			switch(ix_array_type_id){
				case(ishidaopcua_TYPE_ID_APPLICATION_DESCRIPTION):{
				   ishidaopcua_encode_APPLICATION_DESCRIPTION(ipdata_buffer, (ishidaopcua_APPLICATION_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_APPLICATION_DESCRIPTION)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_APPLICATION_INSTANCE_CERTIFICATE):{
				   ishidaopcua_encode_APPLICATION_INSTANCE_CERTIFICATE(ipdata_buffer, (ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_ARRAY):{
				   ishidaopcua_encode_ARRAY(ipdata_buffer, (ishidaopcua_ARRAY*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_ARRAY)),((ishidaopcua_ARRAY*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_ARRAY)))->type,0, iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BOOLEAN):{
				   ishidaopcua_encode_BOOLEAN(ipdata_buffer, (ishidaopcua_BOOLEAN*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BOOLEAN)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BROWSE_DESCRIPTION):{
				   ishidaopcua_encode_BROWSE_DESCRIPTION(ipdata_buffer, (ishidaopcua_BROWSE_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BROWSE_DESCRIPTION)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BROWSE_RESULT):{
				   ishidaopcua_encode_BROWSE_RESULT(ipdata_buffer, (ishidaopcua_BROWSE_RESULT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BROWSE_RESULT)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BYTE):{
				   ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BYTE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_BYTESTRING):{
				   ishidaopcua_encode_BYTESTRING(ipdata_buffer, (ishidaopcua_BYTESTRING*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_BYTESTRING)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_COUNTER):{
				   ishidaopcua_encode_COUNTER(ipdata_buffer, (ishidaopcua_COUNTER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_COUNTER)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DATA_VALUE):{
				   ishidaopcua_encode_DATA_VALUE(ipdata_buffer, (ishidaopcua_DATA_VALUE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DATA_VALUE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DATE_TIME):{
				   ishidaopcua_encode_DATE_TIME(ipdata_buffer, (ishidaopcua_DATE_TIME*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DATE_TIME)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO):{
				   ishidaopcua_encode_DIAGNOSTIC_INFO(ipdata_buffer, (ishidaopcua_DIAGNOSTIC_INFO*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DIAGNOSTIC_INFO)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DOUBLE):{
				   ishidaopcua_encode_DOUBLE(ipdata_buffer, (ishidaopcua_DOUBLE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DOUBLE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_DURATION):{
				   ishidaopcua_encode_DURATION(ipdata_buffer, (ishidaopcua_DURATION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_DURATION)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION):{
				   ishidaopcua_encode_ENDPOINT_DESCRIPTION(ipdata_buffer, (ishidaopcua_ENDPOINT_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_ENDPOINT_DESCRIPTION)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_EXPANDED_NODE_ID):{
				   ishidaopcua_encode_EXPANDED_NODE_ID(ipdata_buffer, (ishidaopcua_EXPANDED_NODE_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_EXPANDED_NODE_ID)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_EXTENSION_OBJECT):{
				   ishidaopcua_encode_EXTENSION_OBJECT(ipdata_buffer, (ishidaopcua_EXTENSION_OBJECT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_EXTENSION_OBJECT)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_FLOAT):{
				   ishidaopcua_encode_FLOAT(ipdata_buffer, (ishidaopcua_FLOAT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_FLOAT)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_GUID):{
				   ishidaopcua_encode_GUID(ipdata_buffer, (ishidaopcua_GUID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_GUID)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INT16):{
				   ishidaopcua_encode_INT16(ipdata_buffer, (ishidaopcua_INT16*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INT16)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INT32):{
				   ishidaopcua_encode_INT32(ipdata_buffer, (ishidaopcua_INT32*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INT32)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INT64):{
				   ishidaopcua_encode_INT64(ipdata_buffer, (ishidaopcua_INT64*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INT64)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_INTEGER_ID):{
				   ishidaopcua_encode_INTEGER_ID(ipdata_buffer, (ishidaopcua_INTEGER_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_INTEGER_ID)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_LOCALIZED_TEXT):{
				   ishidaopcua_encode_LOCALIZED_TEXT(ipdata_buffer, (ishidaopcua_LOCALIZED_TEXT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_LOCALIZED_TEXT)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_MESSAGE_SECURITY_MODE):{
				   ishidaopcua_encode_MESSAGE_SECURITY_MODE(ipdata_buffer, (ishidaopcua_MESSAGE_SECURITY_MODE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_MESSAGE_SECURITY_MODE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_NODE_ID):{
				   ishidaopcua_encode_NODE_ID(ipdata_buffer, (ishidaopcua_NODE_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_NODE_ID)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_NUMERIC_RANGE):{
				   ishidaopcua_encode_NUMERIC_RANGE(ipdata_buffer, (ishidaopcua_NUMERIC_RANGE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_NUMERIC_RANGE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_QUALIFIED_NAME):{
				   ishidaopcua_encode_QUALIFIED_NAME(ipdata_buffer, (ishidaopcua_QUALIFIED_NAME*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_QUALIFIED_NAME)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_READ_VALUE_ID):{
				   ishidaopcua_encode_READ_VALUE_ID(ipdata_buffer, (ishidaopcua_READ_VALUE_ID*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_READ_VALUE_ID)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_REFERENCE_DESCRIPTION):{
				   ishidaopcua_encode_REFERENCE_DESCRIPTION(ipdata_buffer, (ishidaopcua_REFERENCE_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_REFERENCE_DESCRIPTION)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_REQUEST_HEADER):{
				   ishidaopcua_encode_REQUEST_HEADER(ipdata_buffer, (ishidaopcua_REQUEST_HEADER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_REQUEST_HEADER)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_RESPONSE_HEADER):{
				   ishidaopcua_encode_RESPONSE_HEADER(ipdata_buffer, (ishidaopcua_RESPONSE_HEADER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_RESPONSE_HEADER)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SBYTE):{
				   ishidaopcua_encode_SBYTE(ipdata_buffer, (ishidaopcua_SBYTE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SBYTE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SECURITY_TOKEN):{
				   ishidaopcua_encode_SECURITY_TOKEN(ipdata_buffer, (ishidaopcua_SECURITY_TOKEN*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SECURITY_TOKEN)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SIGNATURE):{
				   ishidaopcua_encode_SIGNATURE(ipdata_buffer, (ishidaopcua_SIGNATURE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SIGNATURE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_SIGNED_SOFTWARE_CERTIFICATE):{
				   ishidaopcua_encode_SIGNED_SOFTWARE_CERTIFICATE(ipdata_buffer, (ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_STATUS_CODE):{
				   ishidaopcua_encode_STATUS_CODE(ipdata_buffer, (ishidaopcua_STATUS_CODE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_STATUS_CODE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_STRING):{
				   ishidaopcua_encode_STRING(ipdata_buffer, (ishidaopcua_STRING*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_STRING)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_STRUCTURE):{
				   ishidaopcua_encode_STRUCTURE(ipdata_buffer, (ishidaopcua_STRUCTURE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_STRUCTURE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_TCP_MESSAGE_HEADER):{
				   ishidaopcua_encode_TCP_MESSAGE_HEADER(ipdata_buffer, (ishidaopcua_TCP_MESSAGE_HEADER*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_TCP_MESSAGE_HEADER)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UINT16):{
				   ishidaopcua_encode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UINT16)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UINT32):{
				   ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UINT32)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UINT64):{
				   ishidaopcua_encode_UINT64(ipdata_buffer, (ishidaopcua_UINT64*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UINT64)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_USER_IDENTITY_TOKEN_TYPE):{
				   ishidaopcua_encode_USER_IDENTITY_TOKEN_TYPE(ipdata_buffer, (ishidaopcua_USER_IDENTITY_TOKEN_TYPE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_USER_IDENTITY_TOKEN_TYPE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_USER_TOKEN_POLICY):{
				   ishidaopcua_encode_USER_TOKEN_POLICY(ipdata_buffer, (ishidaopcua_USER_TOKEN_POLICY*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_USER_TOKEN_POLICY)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_UTC_TIME):{
				   ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, (ishidaopcua_UTC_TIME*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_UTC_TIME)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_VARIANT):{
				   ishidaopcua_encode_VARIANT(ipdata_buffer, (ishidaopcua_VARIANT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_VARIANT)), ishidaopcua_get_variant_type_id((ishidaopcua_VARIANT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_VARIANT))), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_VIEW_DESCRIPTION):{
				   ishidaopcua_encode_VIEW_DESCRIPTION(ipdata_buffer, (ishidaopcua_VIEW_DESCRIPTION*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_VIEW_DESCRIPTION)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_WRITE_VALUE):{
				   ishidaopcua_encode_WRITE_VALUE(ipdata_buffer, (ishidaopcua_WRITE_VALUE*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_WRITE_VALUE)), iwrite_size);
				   break;
				}
				case(ishidaopcua_TYPE_ID_XML_ELEMENT):{
				   ishidaopcua_encode_XML_ELEMENT(ipdata_buffer, (ishidaopcua_XML_ELEMENT*)(((unsigned int) ipx_array) + i*sizeof(ishidaopcua_XML_ELEMENT)), iwrite_size);
				   break;
				}
				default:{
				   break;
				}
			}
		}
	}
}

void ishidaopcua_decode_ARRAY(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_ARRAY* iparray, ishidaopcua_TYPE_ID iarray_type_id, ishidaopcua_UINT32 iarray_target_size, ishidaopcua_UINT32 *ipparsing_offset)
{
	iparray->size = 0;

	if(iarray_target_size == 0){
		ishidaopcua_decode_UINT32(ipdata_buffer, &(iparray->size), ipparsing_offset);
	}
	else{
		iparray->size = iarray_target_size;
	}

	/*printf("ARRAY DECODER: decoded size %u\n",iparray->size);*/

	if(iparray->size > 0 && iparray->size < 0xFFFFFFFF){
		iparray->elements = ishidaopcua_malloc(iarray_type_id, iparray->size);

		ishidaopcua_decode_X_ARRAY(ipdata_buffer, (void*) (iparray->elements), iarray_type_id, iparray->size, ipparsing_offset);
	}
	else{
		iparray->size = 0;
	}
}

void ishidaopcua_encode_ARRAY(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_ARRAY* iparray, ishidaopcua_TYPE_ID iarray_type_id, ishidaopcua_UINT32 iarray_target_size, ishidaopcua_UINT32 *ipwrite_size)
{
	if(iparray->size !=0)
	{
		ishidaopcua_encode_UINT32(ipdata_buffer, &iparray->size, ipwrite_size);
	}
	else
	{
		ishidaopcua_UINT32 empty = -1;
		ishidaopcua_encode_UINT32(ipdata_buffer, &empty, ipwrite_size);
	}

	if(iparray->size > 0 && iparray->size < 0xFFFFFFFF){
		/*printf("size not zero encode array %u type %u\n",iparray->size, iarray_type_id);*/
		ishidaopcua_encode_X_ARRAY(ipdata_buffer, (void*) iparray->elements, iarray_type_id, iparray->size, ipwrite_size);
	}
	else
	{
		iparray->size = 0;
	}
}

void ishidaopcua_decode_STRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_STRING* ipstring, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipstring->length), ipparsing_offset);
	/*printf("String length %u\n", ipstring->length);*/
	if(ipstring->length > 0 && ipstring->length < 0xFFFFFFFF){
		ipstring->text = ishidaeutz_malloc((ipstring->length + 1), sizeof(ishidaopcua_BYTE));

		ishidaeutz_copy_byte_array((char *) ipdata_buffer, (char *) ipstring->text, *ipparsing_offset,0, ipstring->length);

		*ipparsing_offset += ipstring->length;
	}
}

void ishidaopcua_encode_STRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_STRING* ipstring, ishidaopcua_UINT32* ipwrite_size)
{
	/*puts("1");*/
	if(ipstring == 0 || (ipstring != 0 && (ipstring->length == 0 || ipstring->length == 0xFFFFFFFF))){
		/*puts("1.1");*/
		ishidaopcua_UINT32 string_length = 0;
		if(ipstring != 0){
			string_length = ipstring->length;
		}
		ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) &string_length,ipwrite_size);
	}
	else{
	/*puts("1.5");*/
	ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*)  &ipstring->length,ipwrite_size);

	if(ipstring->length > 0 && ipstring->length < 0xFFFFFFFF){
		ishidaeutz_copy_byte_array((char *)ipstring->text, (char *)ipdata_buffer,0,*ipwrite_size, ipstring->length);
		*ipwrite_size = *ipwrite_size + ipstring->length;
	}
	}
}

void ishidaopcua_decode_DATE_TIME(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DATE_TIME* ipdate_time, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, ipdate_time, ipparsing_offset);
}

void ishidaopcua_encode_DATE_TIME(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DATE_TIME* ipdate_time, ishidaopcua_UINT32 *ipwrite_size)
{
	ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipdate_time, ipwrite_size);
}

void ishidaopcua_decode_GUID(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_GUID* ipguid, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipguid->data1), ipparsing_offset);
	ishidaopcua_decode_UINT16(ipdata_buffer, &(ipguid->data2), ipparsing_offset);
	ishidaopcua_decode_UINT16(ipdata_buffer, &(ipguid->data3), ipparsing_offset);
	ishidaopcua_decode_X_ARRAY(ipdata_buffer, (void*) &(ipguid->data4[0]), ishidaopcua_TYPE_ID_BYTE, 8, ipparsing_offset);
}

void ishidaopcua_encode_GUID(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_GUID* ipguid, ishidaopcua_UINT32 *ipwrite_size)
{
	ishidaopcua_encode_UINT32(ipdata_buffer, &(ipguid->data1), ipwrite_size);
	ishidaopcua_encode_UINT16(ipdata_buffer, &(ipguid->data2), ipwrite_size);
	ishidaopcua_encode_UINT16(ipdata_buffer, &(ipguid->data3), ipwrite_size);
	ishidaopcua_encode_X_ARRAY(ipdata_buffer, &(ipguid->data4[0]), ishidaopcua_TYPE_ID_BYTE, 8, ipwrite_size);
}

void ishidaopcua_decode_BYTESTRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_BYTESTRING* ipbytestring, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_STRING(ipdata_buffer, (ishidaopcua_STRING*) ipbytestring, ipparsing_offset);
}

void ishidaopcua_encode_BYTESTRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_BYTESTRING* ipbytestring, ishidaopcua_UINT32 *ipwrite_size)
{
	ishidaopcua_encode_STRING(ipdata_buffer, (ishidaopcua_STRING*) ipbytestring, ipwrite_size);
}

void ishidaopcua_decode_XML_ELEMENT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_XML_ELEMENT* ipxml_element, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_STRING(ipdata_buffer, (ishidaopcua_STRING*) ipxml_element, ipparsing_offset);
}

void ishidaopcua_encode_XML_ELEMENT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_XML_ELEMENT* ipxml_element, ishidaopcua_UINT32 *ipwrite_size)
{
	ishidaopcua_encode_STRING(ipdata_buffer, (ishidaopcua_STRING*) ipxml_element, ipwrite_size);
}

ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_NUMERIC_TWO_BYTE = ((ishidaopcua_BYTE) 0x00);
ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_NUMERIC_FOUR_BYTE = ((ishidaopcua_BYTE) 0x01);
ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_NUMERIC_OVER_FOUR_BYTE = ((ishidaopcua_BYTE) 0x02);
ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_STRING = ((ishidaopcua_BYTE) 0x03);
ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_GUID = ((ishidaopcua_BYTE) 0x04);
ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_BYTESTRING = ((ishidaopcua_BYTE) 0x05);

void ishidaopcua_decode_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_NODE_ID* ipnode_id,ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->encoding_mask), ipparsing_offset);

	printf("NODE ID encoding mask %d\n", ((int)(ipnode_id->encoding_mask & ((ishidaopcua_BYTE)0x0F))));

	switch((int)(ipnode_id->encoding_mask & ((ishidaopcua_BYTE)0x0F)))
	{
		case ((int) 0x00):/*Two byte*/
		{
			ipnode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;

			ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &ipnode_id->identifier.numeric, ipparsing_offset);
			break;
		}
		case ((int) 0x01):/*Four byte*/
		{
			ipnode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;

			ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->namespace_index), ipparsing_offset);

			ishidaopcua_decode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*) &(ipnode_id->identifier.numeric), ipparsing_offset);
			break;
		}
		case ((int) 0x02):/*Numeric*/
		{
			ipnode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;

			ishidaopcua_decode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipparsing_offset);

			ishidaopcua_decode_UINT32(ipdata_buffer, &(ipnode_id->identifier.numeric), ipparsing_offset);
			break;
		}
		case ((int) 0x03):/*String*/
		{
			ipnode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_STRING;

			ishidaopcua_decode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipparsing_offset);

			ipnode_id->identifier.string = ishidaopcua_init_string();
			ishidaopcua_decode_STRING(ipdata_buffer, ipnode_id->identifier.string, ipparsing_offset);
			break;
		}
		case ((int) 0x04):/*GUID*/
		{
			ipnode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_GUID;

			ishidaopcua_decode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipparsing_offset);

			ipnode_id->identifier.guid = ishidaopcua_init_guid();
			ishidaopcua_decode_GUID(ipdata_buffer, ipnode_id->identifier.guid, ipparsing_offset);
			break;
		}
		case ((int) 0x05):/*ByteString*/
		{
			ipnode_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_BYTESTRING;

			ishidaopcua_decode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipparsing_offset);

			ipnode_id->identifier.bytestring = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTESTRING,1);
			ishidaopcua_decode_BYTESTRING(ipdata_buffer, ipnode_id->identifier.bytestring, ipparsing_offset);
			break;
		}
		default:{
			break;
		}
	}
}

void ishidaopcua_encode_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_NODE_ID* ipnode_id,ishidaopcua_UINT32* ipwrite_size)
{
	/*puts("NODE ID ENCODER: 0");*/
	/*printf("NODE ID ENCODER: node id address %p\n", ipnode_id);*/
	if(ipnode_id != 0){
		/*printf("NODE ID ENCODER: Identifier type %u\n", ipnode_id->identifier_type);*/

		switch(ipnode_id->identifier_type){
			case ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC:
			{
				/*puts("Identifier numeric.");*/
				if(ipnode_id->identifier.numeric <= 255){
					/*puts("Two byte numeric");*/
					ipnode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_NUMERIC_TWO_BYTE;
					ipnode_id->namespace_index = 0;
					ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->encoding_mask), ipwrite_size);
					ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) (&ipnode_id->identifier.numeric), ipwrite_size);
				}
				else if(ipnode_id->identifier.numeric <= 65535){
					/*puts("Four byte numeric");*/
					ipnode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_NUMERIC_FOUR_BYTE;
					ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->encoding_mask), ipwrite_size);
					ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->namespace_index), ipwrite_size);
					ishidaopcua_encode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*) &(ipnode_id->identifier.numeric), ipwrite_size);
				}
				else{ /*numeric value cannot fit in 2 or 4 byte representation*/
					/*puts("Over 4 byte numeric");*/
					ipnode_id->encoding_mask = ishidaopcua_NODE_ID_ENCODING_NUMERIC_OVER_FOUR_BYTE;
					ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->encoding_mask), ipwrite_size);
					ishidaopcua_encode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipwrite_size);
					ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) &(ipnode_id->identifier.numeric), ipwrite_size);
				}

				break;
			}
			case ishidaopcua_NODE_ID_IDENTIFIER_TYPE_STRING:
			{
				ipnode_id->encoding_mask = ((ishidaopcua_BYTE) 3);
				ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->encoding_mask), ipwrite_size);
				ishidaopcua_encode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipwrite_size);

				ishidaopcua_encode_STRING(ipdata_buffer, ipnode_id->identifier.string , ipwrite_size);
				break;
			}
			case ishidaopcua_NODE_ID_IDENTIFIER_TYPE_GUID:
			{
				ipnode_id->encoding_mask = ((ishidaopcua_BYTE) 4);
				ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->encoding_mask), ipwrite_size);
				ishidaopcua_encode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipwrite_size);

				ishidaopcua_encode_GUID(ipdata_buffer, ipnode_id->identifier.guid , ipwrite_size);
				break;
			}
			case ishidaopcua_NODE_ID_IDENTIFIER_TYPE_BYTESTRING:
			{
				ipnode_id->encoding_mask = ((ishidaopcua_BYTE) 5);
				ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipnode_id->encoding_mask), ipwrite_size);
				ishidaopcua_encode_UINT16(ipdata_buffer, &(ipnode_id->namespace_index), ipwrite_size);

				ishidaopcua_encode_BYTESTRING(ipdata_buffer, ipnode_id->identifier.bytestring , ipwrite_size);
				break;
			}
			default:{
				break;
			}
		}
	}
}

void ishidaopcua_decode_EXPANDED_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXPANDED_NODE_ID* ipexpanded_node_id,ishidaopcua_UINT32* ipparsing_offset)
{
	printf("Decoding expanded node id node id element\n");
	ipexpanded_node_id->node_id = ishidaopcua_init_node_id();
	ishidaopcua_decode_NODE_ID(ipdata_buffer, ipexpanded_node_id->node_id, ipparsing_offset);

	switch((int)(ipexpanded_node_id->node_id->encoding_mask & ((ishidaopcua_BYTE) 0xF0)))
	{
		case ((int) 0x80):/*Namespace flag*/
		{
			/*puts("EXPANDED NODE ID DECODER: Namespace flag turned on.");*/
			/*puts("0");*/
			/*puts("Decoding expanded node id namespace uri element");*/

			ipexpanded_node_id->namespace_uri = ishidaopcua_init_string();
			/*puts("progress 0");*/
			ishidaopcua_decode_STRING(ipdata_buffer, ipexpanded_node_id->namespace_uri, ipparsing_offset);
			/*puts("done 0");*/
			break;
		}
		case ((int) 0x40):/*Server index flag*/
		{
			/*puts("NODEID DECODER: Server flag turned on.");*/
			/*puts("Decoding expanded node id server index element");*/
			/*puts("after 0");*/

			ishidaopcua_decode_UINT32(ipdata_buffer, &ipexpanded_node_id->server_index, ipparsing_offset);
			break;
		}
		case ((int) 0xC0):/*Namespace and Server index flag*/
		{
			/*puts("NODEID DECODER: Both namespace and server flags turned on.");*/

			/*puts("1");*/
			/*puts("Decoding expanded node id namespace uri element");*/

			ipexpanded_node_id->namespace_uri = ishidaopcua_init_string();
			ishidaopcua_decode_STRING(ipdata_buffer, ipexpanded_node_id->namespace_uri, ipparsing_offset);

			/*puts("Decoding expanded node id server index element");*/
			/*puts("after 1");*/
			ishidaopcua_decode_UINT32(ipdata_buffer, &ipexpanded_node_id->server_index, ipparsing_offset);
			/*puts("after 2");*/
			break;
		}
		default:{
			break;
		}
	}
}

void ishidaopcua_encode_EXPANDED_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXPANDED_NODE_ID* ipexpanded_node_id,ishidaopcua_UINT32* ipwrite_size)
{
	if(ipexpanded_node_id != 0){
		ishidaopcua_encode_NODE_ID(ipdata_buffer, ipexpanded_node_id->node_id, ipwrite_size);

		switch((int)(((ishidaopcua_UINT32)ipexpanded_node_id->node_id->encoding_mask) & ((ishidaopcua_BYTE) 0xF0)))
		{
			case ((int) 0x80):/*Namespace flag*/
			{
				ishidaopcua_encode_STRING(ipdata_buffer, ipexpanded_node_id->namespace_uri, ipwrite_size);
				break;
			}
			case ((int) 0x40):/*Server index flag*/
			{
				ishidaopcua_encode_UINT32(ipdata_buffer, &(ipexpanded_node_id->server_index), ipwrite_size);
				break;
			}
			case ((int) 0xC0):/*Namespace and Server index flag*/
			{
				ishidaopcua_encode_STRING(ipdata_buffer, ipexpanded_node_id->namespace_uri, ipwrite_size);

				ishidaopcua_encode_UINT32(ipdata_buffer, &(ipexpanded_node_id->server_index), ipwrite_size);
				break;
			}
			default:{
				break;
			}
		}
	}
}

void ishidaopcua_decode_STATUS_CODE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STATUS_CODE* ipstatus_code,ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) ipstatus_code, ipparsing_offset);
}

void ishidaopcua_encode_STATUS_CODE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STATUS_CODE* ipstatus_code,ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) ipstatus_code, ipwrite_size);
}

void ishidaopcua_decode_QUALIFIED_NAME(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_QUALIFIED_NAME* ipqualified_name,ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*) &(ipqualified_name->namespace_index), ipparsing_offset);

	ipqualified_name->name = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipqualified_name->name, ipparsing_offset);
}

void ishidaopcua_encode_QUALIFIED_NAME(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_QUALIFIED_NAME* ipqualified_name,ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*) &(ipqualified_name->namespace_index), ipwrite_size);
	ishidaopcua_encode_STRING(ipdata_buffer, ipqualified_name->name, ipwrite_size);
}

ishidaopcua_BYTE ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE = ((ishidaopcua_BYTE) 0x01);
ishidaopcua_BYTE ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT = ((ishidaopcua_BYTE) 0x02);

void ishidaopcua_decode_LOCALIZED_TEXT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_LOCALIZED_TEXT* iplocalized_text,ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(iplocalized_text->encoding), ipparsing_offset);

	if((iplocalized_text->encoding & ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE) == ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE){
		iplocalized_text->locale = ishidaopcua_init_string();
		ishidaopcua_decode_STRING(ipdata_buffer, iplocalized_text->locale, ipparsing_offset);
	}

	if((iplocalized_text->encoding & ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT) == ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT){
		iplocalized_text->text = ishidaopcua_init_string();
		ishidaopcua_decode_STRING(ipdata_buffer, iplocalized_text->text, ipparsing_offset);
	}
}

void ishidaopcua_encode_LOCALIZED_TEXT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_LOCALIZED_TEXT* iplocalized_text,ishidaopcua_UINT32* ipwrite_size)
{
	if(iplocalized_text->locale != 0 && iplocalized_text->locale->length > 0 && iplocalized_text->locale->text != 0){
		iplocalized_text->encoding |= ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE;
	}

	if(iplocalized_text->text != 0 && iplocalized_text->text->length > 0 && iplocalized_text->text->text != 0 ){
		iplocalized_text->encoding |= ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT;
	}

	ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(iplocalized_text->encoding), ipwrite_size);

	if((iplocalized_text->encoding & ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE) == ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE)
	{
		ishidaopcua_encode_STRING(ipdata_buffer, iplocalized_text->locale, ipwrite_size);
	}

	if((iplocalized_text->encoding & ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT) == ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT)
	{
		ishidaopcua_encode_STRING(ipdata_buffer, iplocalized_text->text, ipwrite_size);
	}
}

ishidaopcua_BYTE ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY = ((ishidaopcua_BYTE) 0x00);
ishidaopcua_BYTE ishidaopcua_EXTENSION_OBJECT_ENCODE_BYTESTRING = ((ishidaopcua_BYTE) 0x01);
ishidaopcua_BYTE ishidaopcua_EXTENSION_OBJECT_ENCODE_XML = ((ishidaopcua_BYTE) 0x02);

void ishidaopcua_decode_EXTENSION_OBJECT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXTENSION_OBJECT* ipextension_object,ishidaopcua_UINT32* ipparsing_offset)
{
	ipextension_object->typed_id = ishidaopcua_init_expanded_node_id();
	ishidaopcua_decode_EXPANDED_NODE_ID(ipdata_buffer, ipextension_object->typed_id, ipparsing_offset);

	ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE* ) &(ipextension_object->encoding), ipparsing_offset);

	if(ipextension_object->encoding != ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY){
		ishidaopcua_decode_UINT32(ipdata_buffer, &(ipextension_object->length), ipparsing_offset);

		ipextension_object->body = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, ipextension_object->length);

		ishidaopcua_decode_X_ARRAY(ipdata_buffer, ipextension_object->body, ishidaopcua_TYPE_ID_BYTE, ipextension_object->length, ipparsing_offset);
	}
}

void ishidaopcua_encode_EXTENSION_OBJECT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXTENSION_OBJECT* ipextension_object,ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_EXPANDED_NODE_ID(ipdata_buffer, ipextension_object->typed_id, ipwrite_size);
	ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipextension_object->encoding), ipwrite_size);

	if(ipextension_object->encoding != ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY){
		ishidaopcua_encode_UINT32(ipdata_buffer, &(ipextension_object->length), ipwrite_size);
		ishidaopcua_encode_X_ARRAY(ipdata_buffer, ipextension_object->body, ishidaopcua_TYPE_ID_BYTE, ipextension_object->length, ipwrite_size);
	}
}

void ishidaopcua_decode_VARIANT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_VARIANT* ipvariant, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_UINT32 if_array_exists_loop = 1;
	int i = 0;

	ishidaopcua_decode_BYTE(ipdata_buffer, &(ipvariant->encoding_mask), ipparsing_offset);

	if((ipvariant->encoding_mask & ((ishidaopcua_BYTE) 0x80)) == ((ishidaopcua_BYTE) 0x80)){
		/*puts("Checking out array length.");*/
		ishidaopcua_decode_UINT32(ipdata_buffer, &(ipvariant->array_length), ipparsing_offset);

		if(ipvariant->array_length > 0 && ipvariant->array_length < 4294967295U){
			if_array_exists_loop = ipvariant->array_length;
		}
	}

	/*printf("VARIANT DECODER: loop size %u\n",if_array_exists_loop);*/
	ipvariant->value = ishidaopcua_malloc(ishidaopcua_get_variant_type_id(ipvariant),if_array_exists_loop);

	for(i = 0; i < if_array_exists_loop; i++){
		switch(ishidaopcua_get_variant_type_id(ipvariant)){
			case(ishidaopcua_TYPE_ID_BOOLEAN):{
				ishidaopcua_decode_BOOLEAN(ipdata_buffer, (ishidaopcua_BOOLEAN*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_BOOLEAN))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_BYTE):{
				ishidaopcua_decode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_BYTE))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_BYTESTRING):{
				ishidaopcua_decode_BYTESTRING(ipdata_buffer, (ishidaopcua_BYTESTRING*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_BYTESTRING))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_DATA_VALUE):{
				ishidaopcua_decode_DATA_VALUE(ipdata_buffer, (ishidaopcua_DATA_VALUE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DATA_VALUE))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_DATE_TIME):{
				ishidaopcua_decode_DATE_TIME(ipdata_buffer, (ishidaopcua_DATE_TIME*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DATE_TIME))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO):{
				ishidaopcua_decode_DIAGNOSTIC_INFO(ipdata_buffer, (ishidaopcua_DIAGNOSTIC_INFO*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DIAGNOSTIC_INFO))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_DOUBLE):{
				ishidaopcua_decode_DOUBLE(ipdata_buffer, (ishidaopcua_DOUBLE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DOUBLE))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_DURATION):{
				ishidaopcua_decode_DURATION(ipdata_buffer, (ishidaopcua_DURATION*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DURATION))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_EXPANDED_NODE_ID):{
				ishidaopcua_decode_EXPANDED_NODE_ID(ipdata_buffer, (ishidaopcua_EXPANDED_NODE_ID*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_EXPANDED_NODE_ID))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_EXTENSION_OBJECT):{
				ishidaopcua_decode_EXTENSION_OBJECT(ipdata_buffer, (ishidaopcua_EXTENSION_OBJECT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_EXTENSION_OBJECT))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_FLOAT):{
				ishidaopcua_decode_FLOAT(ipdata_buffer, (ishidaopcua_FLOAT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_FLOAT))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_GUID):{
				ishidaopcua_decode_GUID(ipdata_buffer, (ishidaopcua_GUID*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_GUID))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_INT16):{
				ishidaopcua_decode_INT16(ipdata_buffer, (ishidaopcua_INT16*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_INT16))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_INT32):{
				ishidaopcua_decode_INT32(ipdata_buffer, (ishidaopcua_INT32*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_INT32))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_INT64):{
				ishidaopcua_decode_INT64(ipdata_buffer, (ishidaopcua_INT64*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_INT64))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_LOCALIZED_TEXT):{
				ishidaopcua_decode_LOCALIZED_TEXT(ipdata_buffer, (ishidaopcua_LOCALIZED_TEXT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_LOCALIZED_TEXT))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_NODE_ID):{
				ishidaopcua_decode_NODE_ID(ipdata_buffer, (ishidaopcua_NODE_ID*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_NODE_ID))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_QUALIFIED_NAME):{
				ishidaopcua_decode_QUALIFIED_NAME(ipdata_buffer, (ishidaopcua_QUALIFIED_NAME*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_QUALIFIED_NAME))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_SBYTE):{
				ishidaopcua_decode_SBYTE(ipdata_buffer, (ishidaopcua_SBYTE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_SBYTE))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_STATUS_CODE):{
				ishidaopcua_decode_STATUS_CODE(ipdata_buffer, (ishidaopcua_STATUS_CODE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_STATUS_CODE))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_STRING):{
				ishidaopcua_decode_STRING(ipdata_buffer, (ishidaopcua_STRING*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_STRING))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_UINT16):{
				ishidaopcua_decode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_UINT16))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_UINT32):{
				/*printf("..uint32 address %p\n", (((unsigned int) ipvariant->value) + i*sizeof(ishidaopcua_UINT32)));*/
				/*printf("..parsing offset %u\n", *ipparsing_offset);*/
				ishidaopcua_decode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_UINT32))), ipparsing_offset);
				/*printf("..uint32 value %u\n", *((ishidaopcua_UINT32*)(((unsigned int) ipvariant->value) + i*sizeof(ishidaopcua_UINT32))));*/
				break;
			}
			case(ishidaopcua_TYPE_ID_UINT64):{
				ishidaopcua_decode_UINT64(ipdata_buffer, (ishidaopcua_UINT64*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_UINT64))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_VARIANT):{
				ishidaopcua_decode_VARIANT(ipdata_buffer, (ishidaopcua_VARIANT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_VARIANT))), ipparsing_offset);
				break;
			}
			case(ishidaopcua_TYPE_ID_XML_ELEMENT):{
				ishidaopcua_decode_XML_ELEMENT(ipdata_buffer, (ishidaopcua_XML_ELEMENT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_XML_ELEMENT))), ipparsing_offset);
				break;
			}
			default:
			{
				break;
			}
		}
	}

	if((ipvariant->encoding_mask  & ((ishidaopcua_BYTE) 0x40)) == ((ishidaopcua_BYTE) 0x40)){
		ishidaopcua_decode_ARRAY(ipdata_buffer, ipvariant->array_dimensions, ishidaopcua_TYPE_ID_INT32, 0, ipparsing_offset);
	}
}

void ishidaopcua_encode_VARIANT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_VARIANT* ipvariant, ishidaopcua_TYPE_ID itype_id, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_UINT32 i = 0;
	ishidaopcua_UINT32 if_array_exists_loop = 1;
	ipvariant->encoding_mask = 0x00;

	if(ipvariant->array_length > 0 && ipvariant->array_length < 4294967295U){
		ipvariant->encoding_mask |= 0x80;
	}

	if(ipvariant->array_dimensions != 0){
		ipvariant->encoding_mask |= 0x40;
	}

	ipvariant->encoding_mask |= (0x3F & itype_id);
	/*printf("VARIANT ENCODER: type ID %u, encoding mask %u\n", itype_id, ipvariant->encoding_mask);*/

	ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(ipvariant->encoding_mask), ipwrite_size);

	if(ipvariant->array_length > 0 && ipvariant->array_length < 4294967295U){
		/*puts("VEAL: 1");*/
		ishidaopcua_encode_UINT32(ipdata_buffer, &(ipvariant->array_length), ipwrite_size);
		if_array_exists_loop = ipvariant->array_length;
		/*puts("VEAL: 2");*/
	}

	for(i = 0; i < if_array_exists_loop; i++){
		switch(itype_id){
			case(ishidaopcua_TYPE_ID_BOOLEAN):{
				ishidaopcua_encode_BOOLEAN(ipdata_buffer, (ishidaopcua_BOOLEAN*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_BOOLEAN))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_BYTE):{
				ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_BYTE))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_BYTESTRING):{
				ishidaopcua_encode_BYTESTRING(ipdata_buffer, (ishidaopcua_BYTESTRING*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_BYTESTRING))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_DATA_VALUE):{
				ishidaopcua_encode_DATA_VALUE(ipdata_buffer, (ishidaopcua_DATA_VALUE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DATA_VALUE))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_DATE_TIME):{
				ishidaopcua_encode_DATE_TIME(ipdata_buffer, (ishidaopcua_DATE_TIME*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DATE_TIME))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO):{
				ishidaopcua_encode_DIAGNOSTIC_INFO(ipdata_buffer, (ishidaopcua_DIAGNOSTIC_INFO*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DIAGNOSTIC_INFO))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_DOUBLE):{
				ishidaopcua_encode_DOUBLE(ipdata_buffer, (ishidaopcua_DOUBLE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DOUBLE))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_DURATION):{
				ishidaopcua_encode_DURATION(ipdata_buffer, (ishidaopcua_DURATION*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_DURATION))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_EXPANDED_NODE_ID):{
				ishidaopcua_encode_EXPANDED_NODE_ID(ipdata_buffer, (ishidaopcua_EXPANDED_NODE_ID*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_EXPANDED_NODE_ID))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_EXTENSION_OBJECT):{
				ishidaopcua_encode_EXTENSION_OBJECT(ipdata_buffer, (ishidaopcua_EXTENSION_OBJECT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_EXTENSION_OBJECT))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_FLOAT):{
				ishidaopcua_encode_FLOAT(ipdata_buffer, (ishidaopcua_FLOAT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_FLOAT))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_GUID):{
				ishidaopcua_encode_GUID(ipdata_buffer, (ishidaopcua_GUID*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_GUID))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_INT16):{
				ishidaopcua_encode_INT16(ipdata_buffer, (ishidaopcua_INT16*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_INT16))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_INT32):{
				ishidaopcua_encode_INT32(ipdata_buffer, (ishidaopcua_INT32*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_INT32))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_INT64):{
				ishidaopcua_encode_INT64(ipdata_buffer, (ishidaopcua_INT64*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_INT64))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_LOCALIZED_TEXT):{
				ishidaopcua_encode_LOCALIZED_TEXT(ipdata_buffer, (ishidaopcua_LOCALIZED_TEXT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_LOCALIZED_TEXT))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_NODE_ID):{
				ishidaopcua_encode_NODE_ID(ipdata_buffer, (ishidaopcua_NODE_ID*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_NODE_ID))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_QUALIFIED_NAME):{
				ishidaopcua_encode_QUALIFIED_NAME(ipdata_buffer, (ishidaopcua_QUALIFIED_NAME*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_QUALIFIED_NAME))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_SBYTE):{
				ishidaopcua_encode_SBYTE(ipdata_buffer, (ishidaopcua_SBYTE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_SBYTE))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_STATUS_CODE):{
				ishidaopcua_encode_STATUS_CODE(ipdata_buffer, (ishidaopcua_STATUS_CODE*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_STATUS_CODE))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_STRING):{
				/*puts("VARIANT ENCODER: String engage.");*/
				ishidaopcua_encode_STRING(ipdata_buffer, (ishidaopcua_STRING*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_STRING))), ipwrite_size);
				/*puts("VARIANT ENCODER: String cleared.");*/
				break;
			}
			case(ishidaopcua_TYPE_ID_UINT16):{
				ishidaopcua_encode_UINT16(ipdata_buffer, (ishidaopcua_UINT16*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_UINT16))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_UINT32):{
				/*puts("VEAL: 3");*/
				ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_UINT32))), ipwrite_size);
				/*puts("VEAL: 4");*/
				break;
			}
			case(ishidaopcua_TYPE_ID_UINT64):{
				ishidaopcua_encode_UINT64(ipdata_buffer, (ishidaopcua_UINT64*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_UINT64))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_VARIANT):{
				ishidaopcua_encode_VARIANT(ipdata_buffer, (ishidaopcua_VARIANT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_VARIANT))),ishidaopcua_get_variant_type_id((ishidaopcua_VARIANT*)(((unsigned int) ipvariant->value))), ipwrite_size);
				break;
			}
			case(ishidaopcua_TYPE_ID_XML_ELEMENT):{
				ishidaopcua_encode_XML_ELEMENT(ipdata_buffer, (ishidaopcua_XML_ELEMENT*)(((unsigned int) ipvariant->value) + (i*sizeof(ishidaopcua_XML_ELEMENT))), ipwrite_size);
				break;
			}
			default:{
				break;
			}
		}
	}

	if((ipvariant->encoding_mask  & ((ishidaopcua_BYTE) 0x40)) == ((ishidaopcua_BYTE) 0x40)){
		/*puts("VEAL: 5");*/
		ishidaopcua_encode_ARRAY(ipdata_buffer, ipvariant->array_dimensions, ishidaopcua_TYPE_ID_INT32, 0, ipwrite_size);
		/*puts("VEAL: 6");*/
	}
}

void ishidaopcua_decode_DATA_VALUE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DATA_VALUE* ipdata_value,ishidaopcua_UINT32* ipparsing_offset)
{
	/*puts("DDV1");*/
	ishidaopcua_decode_BYTE(ipdata_buffer, &(ipdata_value->encoding_mask ), ipparsing_offset);
	/*puts("DDV2");*/
	if((ipdata_value->encoding_mask & 0x01) == 0x01){
		ipdata_value->value = ishidaopcua_init_variant();
		ishidaopcua_decode_VARIANT(ipdata_buffer, ipdata_value->value, ipparsing_offset);
	}
	/*puts("DDV3");*/
	if((ipdata_value->encoding_mask & 0x02) == 0x02){
		ishidaopcua_decode_UINT32(ipdata_buffer, &(ipdata_value->status_code), ipparsing_offset);
	}
	/*puts("DDV4");*/
	if((ipdata_value->encoding_mask & 0x04) == 0x04){
		ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, ipdata_value->psource_timestamp, ipparsing_offset);
	}
	/*puts("DDV5");*/
	if((ipdata_value->encoding_mask & 0x10) == 0x10){
		ishidaopcua_decode_UINT16(ipdata_buffer, &(ipdata_value->source_pico_seconds), ipparsing_offset);
	}
	/*puts("DDV6");*/
	if((ipdata_value->encoding_mask & 0x08) == 0x08){
		ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, ipdata_value->pserver_timestamp, ipparsing_offset);
	}
	/*puts("DDV7");*/
	if((ipdata_value->encoding_mask & 0x20) == 0x20){
		ishidaopcua_decode_UINT16(ipdata_buffer, &(ipdata_value->server_pico_seconds), ipparsing_offset);
	}
	/*puts("DDV8");*/
}

void ishidaopcua_encode_DATA_VALUE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DATA_VALUE* ipdata_value,ishidaopcua_UINT32* ipwrite_size)
{
	int initial_write_size = *ipwrite_size;
	ishidaopcua_BYTE encoding_mask = 0x00;

	*ipwrite_size = *ipwrite_size + 1;

	if(ipdata_value->value != 0){
		encoding_mask = encoding_mask | 0x01;
		ishidaopcua_encode_VARIANT(ipdata_buffer, ipdata_value->value, ishidaopcua_get_variant_type_id(ipdata_value->value),ipwrite_size);
	}

	if(ipdata_value->status_code != ishidaopcua_STATUS_CODE_SEVERITY_GOOD){
		encoding_mask = encoding_mask | 0x02;
		ishidaopcua_encode_UINT32(ipdata_buffer, &(ipdata_value->status_code), ipwrite_size);
	}

	if(ipdata_value->psource_timestamp != 0){
		encoding_mask = encoding_mask | 0x04;
		ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipdata_value->psource_timestamp, ipwrite_size);
	}

	if(ipdata_value->source_pico_seconds != 0){
		encoding_mask = encoding_mask | 0x10;
		ishidaopcua_encode_UINT16(ipdata_buffer, &(ipdata_value->source_pico_seconds), ipwrite_size);
	}

	if(ipdata_value->pserver_timestamp != 0){
		encoding_mask = encoding_mask | 0x08;
		ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipdata_value->pserver_timestamp, ipwrite_size);
	}

	if(ipdata_value->server_pico_seconds != 0){
		encoding_mask = encoding_mask | 0x20;
		ishidaopcua_encode_UINT16(ipdata_buffer, &(ipdata_value->server_pico_seconds), ipwrite_size);
	}

	ipdata_buffer[initial_write_size] = encoding_mask;
}

void ishidaopcua_encode_REFERENCE_DESCRIPTION(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_REFERENCE_DESCRIPTION* ipreference_description, ishidaopcua_UINT32 *ipwrite_size)
{
	/*puts("REFERENCE DESCRIPTION ENCODER: inside REFERENCE DESCRIPTION encoder");*/
	ishidaopcua_encode_NODE_ID(ipdata_buffer, ipreference_description->preference_type_id, ipwrite_size);
	/*puts("REFERENCE DESCRIPTION ENCODER: pass 0");*/
	ishidaopcua_encode_BOOLEAN(ipdata_buffer, &(ipreference_description->is_forward), ipwrite_size);
	/*puts("REFERENCE DESCRIPTION ENCODER: pass 1");*/
	ishidaopcua_encode_EXPANDED_NODE_ID(ipdata_buffer, ipreference_description->pnode_id, ipwrite_size);
	/*puts("REFERENCE DESCRIPTION ENCODER: pass 2");*/
	ishidaopcua_encode_QUALIFIED_NAME(ipdata_buffer, ipreference_description->pbrowse_name, ipwrite_size);
	/*puts("REFERENCE DESCRIPTION ENCODER: pass 3");*/
	ishidaopcua_encode_LOCALIZED_TEXT(ipdata_buffer, ipreference_description->pdisplay_name, ipwrite_size);
	/*puts("REFERENCE DESCRIPTION ENCODER: pass 4");*/
	ishidaopcua_encode_UINT32(ipdata_buffer, &(ipreference_description->node_class), ipwrite_size);
	/*puts("REFERENCE DESCRIPTION ENCODER: pass 5");*/
	ishidaopcua_encode_EXPANDED_NODE_ID(ipdata_buffer, ipreference_description->ptype_definition, ipwrite_size);
	/*puts("REFERENCE DESCRIPTION ENCODER: pass 6");*/
}

void ishidaopcua_decode_REFERENCE_DESCRIPTION(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_REFERENCE_DESCRIPTION* ipreference_description, ishidaopcua_UINT32 *ipwrite_size)
{
	/*puts("REFERENCE DESCRIPTION DECODER: inside REFERENCE DESCRIPTION encoder");*/
	ipreference_description->preference_type_id = ishidaopcua_init_node_id();
	ishidaopcua_decode_NODE_ID(ipdata_buffer, ipreference_description->preference_type_id, ipwrite_size);

	/*puts("REFERENCE DESCRIPTION DECODER: pass 0");*/
	ishidaopcua_decode_BOOLEAN(ipdata_buffer, &(ipreference_description->is_forward), ipwrite_size);

	/*puts("REFERENCE DESCRIPTION DECODER: pass 1");*/
	ipreference_description->pnode_id = ishidaopcua_init_expanded_node_id();
	ishidaopcua_decode_EXPANDED_NODE_ID(ipdata_buffer, ipreference_description->pnode_id, ipwrite_size);

	/*puts("REFERENCE DESCRIPTION DECODER: pass 2");*/
	ipreference_description->pbrowse_name = ishidaopcua_init_qualified_name();
	ishidaopcua_decode_QUALIFIED_NAME(ipdata_buffer, ipreference_description->pbrowse_name, ipwrite_size);

	/*puts("REFERENCE DESCRIPTION DECODER: pass 3");*/
	ipreference_description->pdisplay_name = ishidaopcua_init_localized_text();
	ishidaopcua_decode_LOCALIZED_TEXT(ipdata_buffer, ipreference_description->pdisplay_name, ipwrite_size);

	/*puts("REFERENCE DESCRIPTION DECODER: pass 4");*/
	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipreference_description->node_class), ipwrite_size);

	/*puts("REFERENCE DESCRIPTION DECODER: pass 5");*/
	ipreference_description->ptype_definition = ishidaopcua_init_expanded_node_id();
	ishidaopcua_decode_EXPANDED_NODE_ID(ipdata_buffer, ipreference_description->ptype_definition, ipwrite_size);
	/*puts("REFERENCE DESCRIPTION DECODER: pass 6");*/
}

void ishidaopcua_decode_BROWSE_RESULT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_BROWSE_RESULT* ipbrowse_result,ishidaopcua_UINT32* ipwrite_size)
{
	/*puts("BROWSE RESULT DECODER: inside BROWSE RESULT encoder");*/
	ishidaopcua_decode_STATUS_CODE(ipdata_buffer, &(ipbrowse_result->status_code),ipwrite_size);

	/*puts("BROWSE RESULT DECODER: pass 0");*/
	ipbrowse_result->pcontinuation_point = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipbrowse_result->pcontinuation_point,ipwrite_size);

	/*puts("BROWSE RESULT DECODER: pass 1");*/
	ipbrowse_result->preference_descriptions = ishidaopcua_init_array();
	ishidaopcua_decode_ARRAY(ipdata_buffer, ipbrowse_result->preference_descriptions, ishidaopcua_TYPE_ID_REFERENCE_DESCRIPTION, 0, ipwrite_size);
	/*puts("BROWSE RESULT DECODER: pass 2");*/
}

void ishidaopcua_encode_BROWSE_RESULT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_BROWSE_RESULT* ipbrowse_result,ishidaopcua_UINT32* ipwrite_size)
{
	/*puts("BROWSE RESULT ENCODER: inside BROWSE RESULT encoder");*/
	ishidaopcua_encode_STATUS_CODE(ipdata_buffer, &(ipbrowse_result->status_code),ipwrite_size);

	/*puts("BROWSE RESULT ENCODER: pass 0");*/
	ishidaopcua_encode_STRING(ipdata_buffer, ipbrowse_result->pcontinuation_point,ipwrite_size);

	/*puts("BROWSE RESULT ENCODER: pass 1");*/
	ishidaopcua_encode_ARRAY(ipdata_buffer, ipbrowse_result->preference_descriptions, ishidaopcua_TYPE_ID_REFERENCE_DESCRIPTION, 0, ipwrite_size);
	/*puts("BROWSE RESULT ENCODER: pass 2");*/
}

void ishidaopcua_decode_DIAGNOSTIC_INFO(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DIAGNOSTIC_INFO* ipdiagnostic_info,ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_BYTE* pencoding_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, 1);
	ishidaopcua_decode_BYTE(ipdata_buffer, pencoding_mask, ipparsing_offset);
	ipdiagnostic_info->encoding_mask = (ishidaopcua_BYTE) (*pencoding_mask);

	if((ipdiagnostic_info->encoding_mask & ishidaopcua_DIAGNOSTIC_INFO_ENCODE_SYMBOLIC_ID) == ishidaopcua_DIAGNOSTIC_INFO_ENCODE_SYMBOLIC_ID){
		ishidaopcua_INT32* psymbolic_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
		ishidaopcua_decode_INT32(ipdata_buffer, psymbolic_id, ipparsing_offset);
		ipdiagnostic_info->symbolic_id = *psymbolic_id;
	}

	if((ipdiagnostic_info->encoding_mask & ishidaopcua_DIAGNOSTIC_INFO_ENCODE_NAMESPACE_URI) == ishidaopcua_DIAGNOSTIC_INFO_ENCODE_NAMESPACE_URI){
		ishidaopcua_INT32* pnamespace_uri = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
		ishidaopcua_decode_INT32(ipdata_buffer, pnamespace_uri, ipparsing_offset);
		ipdiagnostic_info->namespace_uri = *pnamespace_uri;
	}

	if((ipdiagnostic_info->encoding_mask & ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALIZED_TEXT) == ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALIZED_TEXT){
		ishidaopcua_INT32* plocalized_text = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
		ishidaopcua_decode_INT32(ipdata_buffer, plocalized_text, ipparsing_offset);
		ipdiagnostic_info->localized_text = *plocalized_text;
	}

	if((ipdiagnostic_info->encoding_mask & ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALE) == ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALE){
		ishidaopcua_INT32* plocale = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32, 1);
		ishidaopcua_decode_INT32(ipdata_buffer, plocale, ipparsing_offset);
		ipdiagnostic_info->locale = *plocale;
	}

	if((ipdiagnostic_info->encoding_mask & ishidaopcua_DIAGNOSTIC_INFO_ENCODE_ADDITIONAL_INFO) == ishidaopcua_DIAGNOSTIC_INFO_ENCODE_ADDITIONAL_INFO){
		ipdiagnostic_info->additional_info = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING, 1);
		ishidaopcua_decode_STRING(ipdata_buffer, ipdiagnostic_info->additional_info, ipparsing_offset);
	}

	if((ipdiagnostic_info->encoding_mask & ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_STATUS_CODE) == ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_STATUS_CODE){
		ishidaopcua_STATUS_CODE* pinner_status_code = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STATUS_CODE, 1);
		ishidaopcua_decode_STATUS_CODE(ipdata_buffer, pinner_status_code, ipparsing_offset);
		ipdiagnostic_info->inner_status_code = *pinner_status_code;
	}

	if((ipdiagnostic_info->encoding_mask & ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_DIAGNOSTIC_INFO) == ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_DIAGNOSTIC_INFO){
		ipdiagnostic_info->additional_info = ishidaopcua_malloc(ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO, 1);
		ishidaopcua_decode_DIAGNOSTIC_INFO(ipdata_buffer, ipdiagnostic_info->inner_diagnostic_info, ipparsing_offset);
	}
}

void ishidaopcua_encode_DIAGNOSTIC_INFO(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DIAGNOSTIC_INFO* ipdiagnostic_info,ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_UINT32 initial_write_size = *ipwrite_size;
	ipdiagnostic_info->encoding_mask = ((ishidaopcua_BYTE) 0x00);

	*ipwrite_size += 1;

	if(ipdiagnostic_info->symbolic_id > 0){
		ipdiagnostic_info->encoding_mask |= ((ishidaopcua_BYTE)ishidaopcua_DIAGNOSTIC_INFO_ENCODE_SYMBOLIC_ID);
		ishidaopcua_encode_INT32(ipdata_buffer, &(ipdiagnostic_info->symbolic_id), ipwrite_size);
	}

	if(ipdiagnostic_info->namespace_uri > 0){
		ipdiagnostic_info->encoding_mask |= ((ishidaopcua_BYTE)ishidaopcua_DIAGNOSTIC_INFO_ENCODE_NAMESPACE_URI);
		ishidaopcua_encode_INT32(ipdata_buffer, &(ipdiagnostic_info->namespace_uri), ipwrite_size);
	}

	if(ipdiagnostic_info->localized_text > 0){
		ipdiagnostic_info->encoding_mask |= ((ishidaopcua_BYTE)ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALIZED_TEXT);
		ishidaopcua_encode_INT32(ipdata_buffer, &(ipdiagnostic_info->localized_text), ipwrite_size);
	}

	if(ipdiagnostic_info->locale > 0){
		ipdiagnostic_info->encoding_mask |= ((ishidaopcua_BYTE)ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALE);
		ishidaopcua_encode_INT32(ipdata_buffer, &(ipdiagnostic_info->locale), ipwrite_size);
	}

	if(ipdiagnostic_info->additional_info != 0){
		ipdiagnostic_info->encoding_mask |= ((ishidaopcua_BYTE)ishidaopcua_DIAGNOSTIC_INFO_ENCODE_ADDITIONAL_INFO);
		ishidaopcua_encode_STRING(ipdata_buffer, ipdiagnostic_info->additional_info, ipwrite_size);
	}

	if(ipdiagnostic_info->inner_status_code > 0){
		ipdiagnostic_info->encoding_mask |= ((ishidaopcua_BYTE)ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_STATUS_CODE);
		ishidaopcua_encode_STATUS_CODE(ipdata_buffer, &(ipdiagnostic_info->inner_status_code), ipwrite_size);
	}

	if(ipdiagnostic_info->inner_diagnostic_info != 0){
		ipdiagnostic_info->encoding_mask |= ((ishidaopcua_BYTE)ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_DIAGNOSTIC_INFO);
		ishidaopcua_encode_DIAGNOSTIC_INFO(ipdata_buffer, ipdiagnostic_info->inner_diagnostic_info, ipwrite_size);
	}

	ishidaopcua_encode_BYTE(ipdata_buffer, &(ipdiagnostic_info->encoding_mask), &initial_write_size);
}

void ishidaopcua_decode_MESSAGE_SECURITY_MODE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_MESSAGE_SECURITY_MODE* ipmessage_security_mode, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer,ipmessage_security_mode,ipparsing_offset);
}

void ishidaopcua_encode_MESSAGE_SECURITY_MODE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_MESSAGE_SECURITY_MODE* ipmessage_security_mode, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_UINT32(ipdata_buffer,ipmessage_security_mode,ipwrite_size);
}

void ishidaopcua_decode_USER_IDENTITY_TOKEN_TYPE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_IDENTITY_TOKEN_TYPE* ipuser_identity_token, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer,ipuser_identity_token,ipparsing_offset);
}

void ishidaopcua_encode_USER_IDENTITY_TOKEN_TYPE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_IDENTITY_TOKEN_TYPE* ipuser_identity_token, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_UINT32(ipdata_buffer,ipuser_identity_token,ipwrite_size);
}

void ishidaopcua_decode_UTC_TIMESTAMP_MS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME *iputc_time, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UTC_TIMESTAMP_NS(ipdata_buffer, iputc_time, ipparsing_offset);
	/*ishidaeutz_LARGE_INT* pquotient;
	ishidaeutz_LARGE_INT* preminder;
	divide_ishidaeutz_LARGE_INTs(iputc_time, pishidaopcua_MILLI_SECONDS_TO_100_NANO_SECONDS, &pquotient, &preminder);
	iputc_time->double_byte_array = pquotient->double_byte_array;
	iputc_time->int_byte_array = pquotient->int_byte_array;
	iputc_time->int_bytearray_size = pquotient->int_bytearray_size;
	iputc_time->number_bytestring = pquotient->number_bytestring;
	iputc_time->number_bytestring_size = pquotient->number_bytestring_size;
	iputc_time->number_sign = pquotient->number_sign;
	*/
	stretch_ishidaeutz_LARGE_INT(iputc_time, (iputc_time->number_bytestring_size - 4));
}

void ishidaopcua_encode_UTC_TIMESTAMP_MS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME *iputc_time, ishidaopcua_UINT32* ipwrite_size)
{
	/*ishidaeutz_LARGE_INT* pproduct = multiply_ishidaeutz_LARGE_INTs(iputc_time, pishidaopcua_MILLI_SECONDS_TO_100_NANO_SECONDS);*/
	stretch_ishidaeutz_LARGE_INT(iputc_time, (iputc_time->number_bytestring_size + 4));
	ishidaopcua_encode_UTC_TIMESTAMP_NS(ipdata_buffer, iputc_time, ipwrite_size);
}

void ishidaopcua_decode_UTC_TIMESTAMP_NS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME* iputc_time, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaeutz_LARGE_INT_SIGN archived_sign;
	ishidaeutz_LARGE_INT* psum ;

	ishidaopcua_decode_UINT64(ipdata_buffer, iputc_time, ipparsing_offset);

	archived_sign = pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS->number_sign;
	pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS->number_sign = ishidaeutz_LARGE_INT_SIGN_NEGATIVE;
	psum = sum_ishidaeutz_LARGE_INTs(iputc_time, pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS);
	pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS->number_sign = archived_sign;

	iputc_time->double_bytearray = psum->double_bytearray;
	iputc_time->int_bytearray = psum->int_bytearray;
	iputc_time->int_bytearray_size = psum->int_bytearray_size;
	iputc_time->number_bytestring = psum->number_bytestring;
	iputc_time->number_bytestring_size = psum->number_bytestring_size;
	iputc_time->number_sign = psum->number_sign;
}

void ishidaopcua_encode_UTC_TIMESTAMP_NS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME *iputc_time, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaeutz_LARGE_INT_SIGN archived_sign;
	ishidaeutz_LARGE_INT* psum;

	archived_sign = pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS->number_sign;
	pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS->number_sign = ishidaeutz_LARGE_INT_SIGN_POSITIVE;
	psum = sum_ishidaeutz_LARGE_INTs(iputc_time, pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS);
	pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS->number_sign = archived_sign;

	ishidaopcua_encode_UINT64(ipdata_buffer, psum, ipwrite_size);
}

void ishidaopcua_decode_DURATION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_DURATION* ipduration, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_DOUBLE(ipdata_buffer,ipduration,ipparsing_offset);
}

void ishidaopcua_encode_DURATION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_DURATION* ipduration, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_DOUBLE(ipdata_buffer,ipduration,ipwrite_size);
}

void ishidaopcua_decode_REQUEST_HEADER(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_REQUEST_HEADER* iprequest_header,ishidaopcua_UINT32* ipparsing_offset)
{
	iprequest_header->authentication_token = ishidaopcua_init_expanded_node_id();
	ishidaopcua_decode_EXPANDED_NODE_ID(ipdata_buffer,iprequest_header->authentication_token,ipparsing_offset);

	iprequest_header->ptimestamp = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UTC_TIME, 1);
	ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, iprequest_header->ptimestamp, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(iprequest_header->request_handle), ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(iprequest_header->return_diagnostics), ipparsing_offset);

	iprequest_header->audit_entry_id = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, iprequest_header->audit_entry_id, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(iprequest_header->timeout_hint), ipparsing_offset);

	iprequest_header->additional_header = ishidaopcua_init_extension_object();
	ishidaopcua_decode_EXTENSION_OBJECT(ipdata_buffer, iprequest_header->additional_header, ipparsing_offset);
}

void ishidaopcua_encode_REQUEST_HEADER(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_REQUEST_HEADER* iprequest_header,ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_EXPANDED_NODE_ID(ipdata_buffer,iprequest_header->authentication_token,ipwrite_size);

	ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, iprequest_header->ptimestamp, ipwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &(iprequest_header->request_handle), ipwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &(iprequest_header->return_diagnostics), ipwrite_size);

	ishidaopcua_encode_STRING(ipdata_buffer, iprequest_header->audit_entry_id, ipwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &(iprequest_header->timeout_hint), ipwrite_size);

	ishidaopcua_encode_EXTENSION_OBJECT(ipdata_buffer, iprequest_header->additional_header, ipwrite_size);
}

void ishidaopcua_decode_RESPONSE_HEADER(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_RESPONSE_HEADER* ipresponse_header, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UINT64(ipdata_buffer, ipresponse_header->ptimestamp, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipresponse_header->request_handle), ipparsing_offset);

	ishidaopcua_decode_STATUS_CODE(ipdata_buffer, &(ipresponse_header->service_result), ipparsing_offset);

	ipresponse_header->service_diagnostics = ishidaopcua_init_diagnostic_info();
	ishidaopcua_decode_DIAGNOSTIC_INFO(ipdata_buffer, ipresponse_header->service_diagnostics, ipparsing_offset);

	ipresponse_header->string_table = ishidaopcua_init_array();
	ishidaopcua_decode_ARRAY(ipdata_buffer, ipresponse_header->string_table, ishidaopcua_TYPE_ID_STRING, 0, ipparsing_offset);

	ipresponse_header->additional_header = ishidaopcua_init_extension_object();
	ishidaopcua_decode_EXTENSION_OBJECT(ipdata_buffer, ipresponse_header->additional_header, ipparsing_offset);
}

void ishidaopcua_encode_RESPONSE_HEADER(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_RESPONSE_HEADER* ipresponse_header, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipresponse_header->ptimestamp, ipwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &(ipresponse_header->request_handle), ipwrite_size);

	ishidaopcua_encode_STATUS_CODE(ipdata_buffer, &(ipresponse_header->service_result), ipwrite_size);

	ishidaopcua_encode_DIAGNOSTIC_INFO(ipdata_buffer, ipresponse_header->service_diagnostics, ipwrite_size);

	/*printf("Response header string table size %u\n",ipresponse_header->string_table->size);*/
	ishidaopcua_encode_ARRAY(ipdata_buffer, ipresponse_header->string_table, ishidaopcua_TYPE_ID_STRING, 0, ipwrite_size);

	ishidaopcua_encode_EXTENSION_OBJECT(ipdata_buffer, ipresponse_header->additional_header, ipwrite_size);
}

void ishidaopcua_decode_SECURITY_TOKEN(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SECURITY_TOKEN* ipsecurity_token, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipsecurity_token->secure_channel_id), ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipsecurity_token->token_id), ipparsing_offset);

	ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, ipsecurity_token->pcreated_at, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipsecurity_token->revised_lifetime), ipparsing_offset);
}

void ishidaopcua_encode_SECURITY_TOKEN(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SECURITY_TOKEN* ipsecurity_token, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) &(ipsecurity_token->secure_channel_id), ipwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &(ipsecurity_token->token_id), ipwrite_size);

	ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipsecurity_token->pcreated_at, ipwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &(ipsecurity_token->revised_lifetime), ipwrite_size);
}

void ishidaopcua_decode_SIGNATURE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNATURE* ipsignature, ishidaopcua_UINT32* ipparsing_offset)
{
	ipsignature->algorithm = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipsignature->algorithm, ipparsing_offset);

	ipsignature->signature = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipsignature->signature, ipparsing_offset);
}

void ishidaopcua_encode_SIGNATURE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNATURE* ipsignature, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_STRING(ipdata_buffer, ipsignature->algorithm, ipwrite_size);
	ishidaopcua_encode_STRING(ipdata_buffer, ipsignature->signature, ipwrite_size);
}

void ishidaopcua_decode_SIGNED_SOFTWARE_CERTIFICATE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* ipsigned_software_certificate, ishidaopcua_UINT32* ipparsing_offset)
{
	ipsigned_software_certificate->certificate_data = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipsigned_software_certificate->certificate_data, ipparsing_offset);

	ipsigned_software_certificate->signature = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipsigned_software_certificate->signature, ipparsing_offset);
}

void ishidaopcua_encode_SIGNED_SOFTWARE_CERTIFICATE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* ipsigned_software_certificate, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_STRING(ipdata_buffer, ipsigned_software_certificate->certificate_data, ipwrite_size);
	ishidaopcua_encode_STRING(ipdata_buffer, ipsigned_software_certificate->signature, ipwrite_size);
}

void ishidaopcua_decode_APPLICATION_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_APPLICATION_DESCRIPTION* ipapplication_description,ishidaopcua_UINT32* ipparsing_offset)
{
	ipapplication_description->application_uri = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipapplication_description->application_uri, ipparsing_offset);

	ipapplication_description->product_uri = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipapplication_description->product_uri, ipparsing_offset);

	ipapplication_description->application_name = ishidaopcua_init_localized_text();
	ishidaopcua_decode_LOCALIZED_TEXT(ipdata_buffer, ipapplication_description->application_name, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipapplication_description->application_type), ipparsing_offset);

	ipapplication_description->gateway_server_uri = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipapplication_description->gateway_server_uri, ipparsing_offset);

	ipapplication_description->discovery_profile_uri = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, ipapplication_description->discovery_profile_uri, ipparsing_offset);

	ipapplication_description->discovery_urls = ishidaopcua_init_array();
	ishidaopcua_decode_ARRAY(ipdata_buffer, ipapplication_description->discovery_urls, ishidaopcua_TYPE_ID_STRING, 0, ipparsing_offset);
}

void ishidaopcua_encode_APPLICATION_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_APPLICATION_DESCRIPTION* ipapplication_description,ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_STRING(ipdata_buffer, ipapplication_description->application_uri, ipwrite_size);

	ishidaopcua_encode_STRING(ipdata_buffer, ipapplication_description->product_uri, ipwrite_size);

	ishidaopcua_encode_LOCALIZED_TEXT(ipdata_buffer, ipapplication_description->application_name, ipwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &(ipapplication_description->application_type), ipwrite_size);

	ishidaopcua_encode_STRING(ipdata_buffer, ipapplication_description->gateway_server_uri, ipwrite_size);

	ishidaopcua_encode_STRING(ipdata_buffer, ipapplication_description->discovery_profile_uri, ipwrite_size);

	ishidaopcua_encode_ARRAY(ipdata_buffer, ipapplication_description->discovery_urls, ishidaopcua_TYPE_ID_STRING, 0, ipwrite_size);
}

void ishidaopcua_decode_STRUCTURE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STRUCTURE* ipstructure,ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipstructure->type_id), ipparsing_offset);

	ishidaopcua_decode_BYTE(ipdata_buffer, &(ipstructure->encoding), ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipstructure->length), ipparsing_offset);

	ishidaopcua_decode_X_ARRAY(ipdata_buffer, ipstructure->data, ishidaopcua_TYPE_ID_BYTE, ipstructure->length, ipparsing_offset);
}

void ishidaopcua_encode_STRUCTURE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STRUCTURE* ipstructure,ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_UINT32(ipdata_buffer,&(ipstructure->type_id), ipwrite_size);

	ishidaopcua_encode_BYTE(ipdata_buffer, &(ipstructure->encoding), ipwrite_size);

	ishidaopcua_encode_X_ARRAY(ipdata_buffer, ipstructure->data, ishidaopcua_TYPE_ID_BYTE, ipstructure->length, ipwrite_size);
}

void ishidaopcua_decode_APPLICATION_INSTANCE_CERTIFICATE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* ipapplication_instance_certificate,ishidaopcua_UINT32* ipparsing_offset)
{
	ipapplication_instance_certificate->version = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING, 1);
	ishidaopcua_decode_STRING(ipdata_buffer,ipapplication_instance_certificate->version, ipparsing_offset);

	ipapplication_instance_certificate->serial_no = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTESTRING, 1);
	ishidaopcua_decode_BYTESTRING(ipdata_buffer,ipapplication_instance_certificate->serial_no, ipparsing_offset);

	ipapplication_instance_certificate->signature_algorithm = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING, 1);
	ishidaopcua_decode_STRING(ipdata_buffer,ipapplication_instance_certificate->signature_algorithm, ipparsing_offset);

	ipapplication_instance_certificate->signature = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTESTRING, 1);
	ishidaopcua_decode_BYTESTRING(ipdata_buffer,ipapplication_instance_certificate->signature, ipparsing_offset);

	ipapplication_instance_certificate->issuer = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRUCTURE, 1);
	ishidaopcua_decode_STRUCTURE(ipdata_buffer,ipapplication_instance_certificate->issuer, ipparsing_offset);

	ipapplication_instance_certificate->pvalid_from = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UTC_TIME, 1);
	ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, ipapplication_instance_certificate->pvalid_from, ipparsing_offset);

	ipapplication_instance_certificate->pvalid_to = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UTC_TIME, 1);
	ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, ipapplication_instance_certificate->pvalid_to, ipparsing_offset);

	ipapplication_instance_certificate->subject = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRUCTURE, 1);
	ishidaopcua_decode_STRUCTURE(ipdata_buffer,ipapplication_instance_certificate->subject, ipparsing_offset);

	ipapplication_instance_certificate->application_uri = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING, 1);
	ishidaopcua_decode_STRING(ipdata_buffer,ipapplication_instance_certificate->application_uri, ipparsing_offset);

	ipapplication_instance_certificate->hostnames = ishidaopcua_malloc(ishidaopcua_TYPE_ID_ARRAY, 1);
	ishidaopcua_decode_ARRAY(ipdata_buffer, ipapplication_instance_certificate->hostnames, ishidaopcua_TYPE_ID_STRING, 0, ipparsing_offset);

	ipapplication_instance_certificate->public_key = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTESTRING, 1);
	ishidaopcua_decode_BYTESTRING(ipdata_buffer,ipapplication_instance_certificate->public_key, ipparsing_offset);

	ipapplication_instance_certificate->key_usage = ishidaopcua_malloc(ishidaopcua_TYPE_ID_ARRAY, 1);
	ishidaopcua_decode_ARRAY(ipdata_buffer, ipapplication_instance_certificate->key_usage, ishidaopcua_TYPE_ID_STRING, 0, ipparsing_offset);
}

void ishidaopcua_encode_APPLICATION_INSTANCE_CERTIFICATE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* ipapplication_instance_certificate, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_UINT32 written_size;
	ishidaopcua_UINT32 initial_write_size = *ipwrite_size;

	*ipwrite_size += 4;

	ishidaopcua_encode_STRING(ipdata_buffer,ipapplication_instance_certificate->version, ipwrite_size);

	ishidaopcua_encode_BYTESTRING(ipdata_buffer,ipapplication_instance_certificate->serial_no, ipwrite_size);

	ishidaopcua_encode_STRING(ipdata_buffer,ipapplication_instance_certificate->signature_algorithm, ipwrite_size);

	ishidaopcua_encode_BYTESTRING(ipdata_buffer,ipapplication_instance_certificate->signature, ipwrite_size);

	ishidaopcua_encode_STRUCTURE(ipdata_buffer,ipapplication_instance_certificate->issuer, ipwrite_size);

	ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipapplication_instance_certificate->pvalid_from, ipwrite_size);

	ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipapplication_instance_certificate->pvalid_to, ipwrite_size);

	ishidaopcua_encode_STRUCTURE(ipdata_buffer,ipapplication_instance_certificate->subject, ipwrite_size);

	ishidaopcua_encode_STRING(ipdata_buffer,ipapplication_instance_certificate->application_uri, ipwrite_size);

	ishidaopcua_encode_ARRAY(ipdata_buffer, ipapplication_instance_certificate->hostnames, ishidaopcua_TYPE_ID_STRING, 0, ipwrite_size);

	ishidaopcua_encode_BYTESTRING(ipdata_buffer,ipapplication_instance_certificate->public_key, ipwrite_size);

	ishidaopcua_encode_ARRAY(ipdata_buffer, ipapplication_instance_certificate->key_usage, ishidaopcua_TYPE_ID_STRING, 0, ipwrite_size);

	written_size = (*ipwrite_size) - initial_write_size;

	ishidaopcua_encode_UINT32(ipdata_buffer, &(written_size), &(initial_write_size));
}

void ishidaopcua_decode_USER_TOKEN_POLICY(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_TOKEN_POLICY* ipuser_token_policy, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_UINT32* ptoken_type = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);

	ishidaopcua_decode_STRING(ipdata_buffer, ipuser_token_policy->policy_id, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, ptoken_type, ipparsing_offset);
	ipuser_token_policy->token_type = *ptoken_type;

	ishidaopcua_decode_STRING(ipdata_buffer, ipuser_token_policy->issued_token_type, ipparsing_offset);
	ishidaopcua_decode_STRING(ipdata_buffer, ipuser_token_policy->issuer_endpoint_url, ipparsing_offset);
	ishidaopcua_decode_STRING(ipdata_buffer, ipuser_token_policy->security_policy_uri, ipparsing_offset);
}

void ishidaopcua_encode_USER_TOKEN_POLICY(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_TOKEN_POLICY* ipuser_token_policy, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_encode_STRING(ipdata_buffer, ipuser_token_policy->policy_id, ipparsing_offset);
	ishidaopcua_encode_UINT32(ipdata_buffer, &ipuser_token_policy->token_type, ipparsing_offset);
	ishidaopcua_encode_STRING(ipdata_buffer, ipuser_token_policy->issued_token_type, ipparsing_offset);
	ishidaopcua_encode_STRING(ipdata_buffer, ipuser_token_policy->issuer_endpoint_url, ipparsing_offset);
	ishidaopcua_encode_STRING(ipdata_buffer, ipuser_token_policy->security_policy_uri, ipparsing_offset);
}

void ishidaopcua_decode_ENDPOINT_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_UINT32* psecurity_mode = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
	ishidaopcua_BYTE* psecurity_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, 1);

	ishidaopcua_decode_STRING(ipdata_buffer, iendpoint_description->endpoint_url, ipparsing_offset);
	ishidaopcua_decode_APPLICATION_DESCRIPTION(ipdata_buffer, iendpoint_description->server, ipparsing_offset);
	ishidaopcua_decode_BYTESTRING(ipdata_buffer, iendpoint_description->server_certificate, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, psecurity_mode, ipparsing_offset);
	iendpoint_description->security_mode = *psecurity_mode;

	ishidaopcua_decode_STRING(ipdata_buffer, iendpoint_description->security_policy_uri, ipparsing_offset);
	ishidaopcua_decode_ARRAY(ipdata_buffer, iendpoint_description->user_identity_tokens, ishidaopcua_TYPE_ID_USER_TOKEN_POLICY, 0, ipparsing_offset);
	ishidaopcua_decode_STRING(ipdata_buffer, iendpoint_description->transport_profile_uri, ipparsing_offset);

	ishidaopcua_decode_BYTE(ipdata_buffer, psecurity_level, ipparsing_offset);
	iendpoint_description->security_level = *psecurity_level;
}

void ishidaopcua_encode_ENDPOINT_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_encode_STRING(ipdata_buffer, iendpoint_description->endpoint_url, ipparsing_offset);

	ishidaopcua_encode_APPLICATION_DESCRIPTION(ipdata_buffer, iendpoint_description->server, ipparsing_offset);

	ishidaopcua_encode_BYTESTRING(ipdata_buffer, iendpoint_description->server_certificate, ipparsing_offset);

	ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) &(iendpoint_description->security_mode), ipparsing_offset);

	ishidaopcua_encode_STRING(ipdata_buffer, iendpoint_description->security_policy_uri, ipparsing_offset);

	ishidaopcua_encode_ARRAY(ipdata_buffer, iendpoint_description->user_identity_tokens, ishidaopcua_TYPE_ID_USER_TOKEN_POLICY, 0, ipparsing_offset);

	ishidaopcua_encode_STRING(ipdata_buffer, iendpoint_description->transport_profile_uri, ipparsing_offset);

	ishidaopcua_encode_BYTE(ipdata_buffer, (ishidaopcua_BYTE*) &(iendpoint_description->security_level), ipparsing_offset);
}

void ishidaopcua_decode_READ_VALUE_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_READ_VALUE_ID* iread_value_id, ishidaopcua_UINT32* ipparsing_offset)
{
	iread_value_id->node_id = ishidaopcua_init_node_id();
	ishidaopcua_decode_NODE_ID(ipdata_buffer, iread_value_id->node_id, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(iread_value_id->attribute_id), ipparsing_offset);

	iread_value_id->index_range = ishidaopcua_init_string();
	ishidaopcua_decode_STRING(ipdata_buffer, iread_value_id->index_range, ipparsing_offset);

	iread_value_id->data_encoding = ishidaopcua_init_qualified_name();
	ishidaopcua_decode_QUALIFIED_NAME(ipdata_buffer, iread_value_id->data_encoding, ipparsing_offset);
}

void ishidaopcua_encode_READ_VALUE_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_READ_VALUE_ID* iread_value_id, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_encode_NODE_ID(ipdata_buffer, iread_value_id->node_id, ipparsing_offset);

	ishidaopcua_encode_UINT32(ipdata_buffer, &iread_value_id->attribute_id, ipparsing_offset);

	ishidaopcua_encode_STRING(ipdata_buffer, iread_value_id->index_range, ipparsing_offset);
	ishidaopcua_encode_QUALIFIED_NAME(ipdata_buffer, iread_value_id->data_encoding, ipparsing_offset);
}

void ishidaopcua_decode_VIEW_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_VIEW_DESCRIPTION* ipview_description, ishidaopcua_UINT32* ipparsing_offset)
{
	ipview_description->pview_id = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE_ID));
	ishidaopcua_decode_NODE_ID(ipdata_buffer, ipview_description->pview_id, ipparsing_offset);

	ipview_description->ptimestamp = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UTC_TIME, 1);
	ishidaopcua_decode_UTC_TIMESTAMP_MS(ipdata_buffer, ipview_description->ptimestamp, ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipview_description->view_version), ipparsing_offset);
}

void ishidaopcua_encode_VIEW_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_VIEW_DESCRIPTION* ipview_description, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_encode_NODE_ID(ipdata_buffer, ipview_description->pview_id, ipwrite_size);
	ishidaopcua_encode_UTC_TIMESTAMP_MS(ipdata_buffer, ipview_description->ptimestamp,  ipwrite_size);
	ishidaopcua_encode_UINT32(ipdata_buffer, &ipview_description->view_version, ipwrite_size);
}

void ishidaopcua_decode_COUNTER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_COUNTER* ipcounter, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) ipcounter, ipparsing_offset);
}

void ishidaopcua_encode_COUNTER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_COUNTER* ipcounter, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) ipcounter, ipparsing_offset);
}

void ishidaopcua_decode_BROWSE_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_UINT32 *ipparsing_offset)
{
	ipbrowse_description->pnode_id = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE_ID));
	ishidaopcua_decode_NODE_ID(ipdata_buffer, ipbrowse_description->pnode_id, ipparsing_offset);


	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipbrowse_description->browse_direction), ipparsing_offset);

	ipbrowse_description->preference_type_id = ishidaeutz_malloc(1, sizeof(ishidaopcua_NODE_ID));
	ishidaopcua_decode_NODE_ID(ipdata_buffer, ipbrowse_description->preference_type_id, ipparsing_offset);

	ishidaopcua_decode_BOOLEAN(ipdata_buffer, &(ipbrowse_description->include_subtypes), ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipbrowse_description->node_class_mask), ipparsing_offset);

	ishidaopcua_decode_UINT32(ipdata_buffer, &(ipbrowse_description->result_class_mask), ipparsing_offset);
}

void ishidaopcua_encode_BROWSE_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_UINT32 *iwrite_size)
{
	ishidaopcua_encode_NODE_ID(ipdata_buffer, ipbrowse_description->pnode_id, iwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &ipbrowse_description->browse_direction, iwrite_size);

	ishidaopcua_encode_NODE_ID(ipdata_buffer, ipbrowse_description->preference_type_id, iwrite_size);

	ishidaopcua_encode_BOOLEAN(ipdata_buffer, &ipbrowse_description->include_subtypes, iwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &ipbrowse_description->node_class_mask, iwrite_size);

	ishidaopcua_encode_UINT32(ipdata_buffer, &ipbrowse_description->result_class_mask, iwrite_size);
}

void ishidaopcua_decode_INTEGER_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_INTEGER_ID* iinteger_id, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) iinteger_id, ipparsing_offset);
}

void ishidaopcua_encode_INTEGER_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_INTEGER_ID* iinteger_id, ishidaopcua_UINT32 *iwrite_size)
{
	ishidaopcua_encode_UINT32(ipdata_buffer, (ishidaopcua_UINT32*) iinteger_id, iwrite_size);
}

void ishidaopcua_decode_NUMERIC_RANGE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_NUMERIC_RANGE* inumeric_range, ishidaopcua_UINT32 *ipparsing_offset)
{
	ishidaopcua_decode_STRING(ipdata_buffer, inumeric_range, ipparsing_offset);
}

void ishidaopcua_encode_NUMERIC_RANGE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_NUMERIC_RANGE* inumeric_range, ishidaopcua_UINT32 *iwrite_size)
{
	ishidaopcua_encode_STRING(ipdata_buffer, inumeric_range, iwrite_size);
}

void ishidaopcua_decode_WRITE_VALUE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_WRITE_VALUE* iwrite_value, ishidaopcua_UINT32* ipparsing_offset)
{
	/*puts("DWV1");*/
	iwrite_value->node_id = ishidaopcua_init_node_id();
	ishidaopcua_decode_NODE_ID(ipdata_buffer, iwrite_value->node_id, ipparsing_offset);
	/*puts("DWV3");*/
	ishidaopcua_decode_INTEGER_ID(ipdata_buffer, &(iwrite_value->attribute_id), ipparsing_offset);
	/*puts("DWV3");*/
	iwrite_value->index_range = ishidaopcua_init_string();
	ishidaopcua_decode_NUMERIC_RANGE(ipdata_buffer, iwrite_value->index_range, ipparsing_offset);
	/*puts("DWV4");*/
	iwrite_value->value = ishidaopcua_init_data_value();
	ishidaopcua_decode_DATA_VALUE(ipdata_buffer, iwrite_value->value, ipparsing_offset);
	/*puts("DWV5");*/
}

void ishidaopcua_encode_WRITE_VALUE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_WRITE_VALUE* iwrite_value, ishidaopcua_UINT32* iwrite_size)
{
	ishidaopcua_encode_NODE_ID(ipdata_buffer, iwrite_value->node_id, iwrite_size);
	ishidaopcua_encode_INTEGER_ID(ipdata_buffer, &iwrite_value->attribute_id, iwrite_size);
	ishidaopcua_encode_NUMERIC_RANGE(ipdata_buffer, iwrite_value->index_range, iwrite_size);
	ishidaopcua_encode_DATA_VALUE(ipdata_buffer, iwrite_value->value, iwrite_size);
}

void ishidaopcua_decode_TCP_MESSAGE_HEADER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_TCP_MESSAGE_HEADER* imessage_header, ishidaopcua_UINT32* ipparsing_offset)
{
	ishidaopcua_decode_X_ARRAY(ipdata_buffer, &(imessage_header->message_type[0]), ishidaopcua_TYPE_ID_BYTE, 3, ipparsing_offset);
	ishidaopcua_decode_BYTE(ipdata_buffer, &imessage_header->reserved , ipparsing_offset);
	ishidaopcua_decode_UINT32(ipdata_buffer, &imessage_header->message_size, ipparsing_offset);
}

void ishidaopcua_encode_TCP_MESSAGE_HEADER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_TCP_MESSAGE_HEADER* ipmessage_header, ishidaopcua_UINT32* ipwrite_size)
{
	ishidaopcua_UINT32* pwrite_size = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32, 1);
	ishidaopcua_BYTE* pdata_buffer_temp = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE, ISHIDAOPCUA_MSG_MAXBUFFERSIZE);

	*pwrite_size = 0;

	ishidaopcua_encode_X_ARRAY(pdata_buffer_temp, &(ipmessage_header->message_type[0]), ishidaopcua_TYPE_ID_BYTE, 3, pwrite_size);

	ishidaopcua_encode_BYTE(pdata_buffer_temp, &(ipmessage_header->reserved), pwrite_size);

	ishidaopcua_encode_UINT32(pdata_buffer_temp, &(ipmessage_header->message_size), pwrite_size);

	ishidaeutz_shift_byte_array((char*)ipdata_buffer, *pwrite_size, *ipwrite_size);

	ishidaeutz_copy_byte_array((char*)pdata_buffer_temp, (char*)ipdata_buffer, 0, 0, *pwrite_size);

	*ipwrite_size += *pwrite_size;
}
/*************************************** ISHIDAOPCUA_ENCODE_DECODER_PAIRS END ***************************************/

/*************************************** ISHIDAOPCUA_BINARY_SERVER START ***************************************/
static int sock;
static  struct sockaddr_in socket_address;

static int ishidaopcua_binary_server_create_sock(const char *iip_address_string, unsigned short iport)
{
	memset(&sock, 0, sizeof(int));
	sock = socket (AF_INET, SOCK_STREAM, 0);

	if (sock < 0)
	{
		puts("SERVER: SOCKET ERROR");
		ishidaeu_exit(-2001);
	}

	memset(&socket_address, 0, sizeof(struct sockaddr_in));
	socket_address.sin_family = AF_INET;
	socket_address.sin_port = htons (iport);
	socket_address.sin_addr.s_addr = inet_addr(iip_address_string);

	if (bind (sock, (struct sockaddr *) &socket_address, sizeof (socket_address)) < 0)
	{
		puts("SERVER: BIND ERROR");
		ishidaeu_exit(-2002);
	}

	return sock;
}

static ishidaopcua_BYTE input_buffer[ISHIDAOPCUA_MSG_MAXBUFFERSIZE];
static ishidaopcua_BYTE output_buffer[ISHIDAOPCUA_MSG_MAXBUFFERSIZE];
static ishidaopcua_UINT32 bytes_read;
static ishidaopcua_UINT32 bytes_write;
static ishidaopcua_BYTE close_socket;
static int written_bytes;

static int ishidaopcua_binary_server_read_client (ishidaopcua_BINARY_SERVER_CONNECTION_ARGS iconnection_args)
{
		memset(&input_buffer[0], 0, ISHIDAOPCUA_MSG_MAXBUFFERSIZE);
		memset(&bytes_read, 0, sizeof(ishidaopcua_UINT32));
		bytes_read = read (iconnection_args.file_descriptor, &input_buffer[0], ISHIDAOPCUA_MSG_MAXBUFFERSIZE);

		if (bytes_read < 0)
		{
		  puts("SERVER: READ ERROR");
		  ishidaeu_exit(-2005);
		}
		else if (bytes_read == 0)
		{
		  return -1;
		}
		else{
			ishidaopcua_BINARY_SERVER_CLIENT_ARGS* pclient_args = ishidaeutz_malloc(1, sizeof(ishidaopcua_BINARY_SERVER_CLIENT_ARGS));
			
			printf("SERVER: RECEIVED MESSAGE: %s\n", &input_buffer[0]);

			memset(&bytes_write, 0, sizeof(ishidaopcua_UINT32));

			memset(&close_socket, 0, sizeof(ishidaopcua_BYTE));

			memset(&output_buffer[0], 0, ISHIDAOPCUA_MSG_MAXBUFFERSIZE);
			
			pclient_args->client_details = iconnection_args.client_details;

			ishidaeutz_mtrack_resume();
			ishidaopcua_decode_msg(&input_buffer[0], bytes_read, &output_buffer[0], &bytes_write, pclient_args, &close_socket);
			ishidaeutz_mtrack_pause();
			ishidaeutz_mtrack_reset();

			if(close_socket == 0 && bytes_write > 0)
			{
				memset(&written_bytes, 0, sizeof(int));
				written_bytes = write(iconnection_args.file_descriptor, &output_buffer[0], bytes_write);
				printf("SERVER: SENT MESSAGE: %s of size %u written being %d\n", &output_buffer[0], bytes_write, written_bytes);
			}
			else if(close_socket != 0){
			  close(iconnection_args.file_descriptor);
			  printf("SERVER: CLOSED CHANNEL\n");
			  return -2;
			}
		}

		return 0;
}

static int close_connection = -1;
static size_t client_details_size;

static void ishidaopcua_binary_server_connection(ishidaopcua_BINARY_SERVER_CONNECTION_ARGS ivargp)
{
	memset(&close_connection, 0, sizeof(int));
	close_connection = -1;

	memset(&close_connection, 0, sizeof(int));
	client_details_size =  (size_t) sizeof(*(ivargp.client_details));
	ivargp.size = &client_details_size;
	ivargp.file_descriptor = accept(ivargp.sock,((struct sockaddr *) ivargp.client_details), ivargp.size);

	if (ivargp.file_descriptor < 0)
	{
		puts("SERVER: ACCEPT ERROR");
		ishidaeu_exit(-2003);
	}

	printf("SERVER: CONNECTION FROM IP ADDRESS %s, PORT %hd.\n",inet_ntoa (ivargp.client_details->sin_addr),ntohs (ivargp.client_details->sin_port));
	
	while((close_connection = ishidaopcua_binary_server_read_client (ivargp)) >= 0)

	if(close_connection == -1){
		close(ivargp.file_descriptor);
	}
}


static int sock;
static struct sockaddr_in clientname;
static size_t connection_size;
static int wait_size;
static ishidaopcua_BINARY_SERVER_CONNECTION_ARGS connection_args;

void ishidaopcua_binary_server_init(ishidaopcua_BINARY_SERVER_ARGS ibinary_server_args)
{
	memset(&wait_size, 0, sizeof(int));
	wait_size = 32;

	memset(&sock, 0, sizeof(int));
	sock = ishidaopcua_binary_server_create_sock (ibinary_server_args.server_ip_addr, ibinary_server_args.server_port);
	
	if (listen (sock,wait_size) < 0)
	{
		puts("SERVER: LISTEN:");
		ishidaeu_exit(-2004);
	}

	while (1)
	{
		memset(&connection_args, 0, sizeof(ishidaopcua_BINARY_SERVER_CONNECTION_ARGS));
		connection_args.sock = sock;
		memset(&clientname, 0, sizeof(struct sockaddr_in));
		connection_args.client_details = &clientname;
		memset(&connection_size, 0, sizeof(size_t));
		connection_args.size = &connection_size;

		ishidaopcua_binary_server_connection(connection_args);
	}
}
/*************************************** ISHIDAOPCUA_BINARY_SERVER END ***************************************/
