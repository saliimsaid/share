#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_16(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* Boolean ********/


case 1 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 1;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Boolean", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Boolean", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is either TRUE or FALSE.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 1 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_1_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_1_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_1_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_1_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_1_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_1_retrieved_reference_45_inverse_0);
*Node_1_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_1_retrieved_reference_45_inverse_0_typed_id,Node_1_ref_node_target_id_45_inverse_0);
*Node_1_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_1_retrieved_reference_45_inverse_true_0_is_inverse, Node_1_ref_node_target_id_45_inverse_0);
*Node_1_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_1_retrieved_reference_45_inverse_0_target_id, Node_1_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_1_retrieved_reference_45_inverse_0->references,"1",Node_1_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DateTime ********/


case 13 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 13;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DateTime", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DateTime", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a Gregorian calender date and time.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 13 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_13_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_13_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_13_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_13_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_13_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_13_retrieved_reference_45_inverse_0);
*Node_13_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_13_retrieved_reference_45_inverse_0_typed_id,Node_13_ref_node_target_id_45_inverse_0);
*Node_13_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_13_retrieved_reference_45_inverse_true_0_is_inverse, Node_13_ref_node_target_id_45_inverse_0);
*Node_13_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_13_retrieved_reference_45_inverse_0_target_id, Node_13_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_13_retrieved_reference_45_inverse_0->references,"1",Node_13_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_13_retrieved_reference_45_0;
ishidaopcua_NODE* Node_13_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_13_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_13_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_13_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_13_retrieved_reference_45_0);
*Node_13_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_13_retrieved_reference_45_0_typed_id,Node_13_ref_node_target_id_45_0);
*Node_13_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_13_retrieved_reference_45_false_0_is_inverse, Node_13_ref_node_target_id_45_0);
*Node_13_retrieved_reference_45_0_target_id = 294;
ishidaopcua_node_set_target_id(Node_13_retrieved_reference_45_0_target_id, Node_13_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_13_retrieved_reference_45_0->references,"1",Node_13_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_13_retrieved_reference_45_1;
ishidaopcua_NODE* Node_13_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_13_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_13_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_13_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_13_retrieved_reference_45_1);
*Node_13_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_13_retrieved_reference_45_1_typed_id,Node_13_ref_node_target_id_45_1);
*Node_13_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_13_retrieved_reference_45_false_1_is_inverse, Node_13_ref_node_target_id_45_1);
*Node_13_retrieved_reference_45_1_target_id = 293;
ishidaopcua_node_set_target_id(Node_13_retrieved_reference_45_1_target_id, Node_13_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_13_retrieved_reference_45_1->references,"2",Node_13_ref_node_target_id_45_1);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ByteString ********/


case 15 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 15;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ByteString", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ByteString", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a sequence of bytes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 15 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_15_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_15_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_15_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_15_retrieved_reference_45_inverse_0);
*Node_15_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_15_retrieved_reference_45_inverse_0_typed_id,Node_15_ref_node_target_id_45_inverse_0);
*Node_15_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_15_retrieved_reference_45_inverse_true_0_is_inverse, Node_15_ref_node_target_id_45_inverse_0);
*Node_15_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_15_retrieved_reference_45_inverse_0_target_id, Node_15_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_15_retrieved_reference_45_inverse_0->references,"1",Node_15_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_15_retrieved_reference_45_0;
ishidaopcua_NODE* Node_15_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_15_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_15_retrieved_reference_45_0);
*Node_15_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_15_retrieved_reference_45_0_typed_id,Node_15_ref_node_target_id_45_0);
*Node_15_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_15_retrieved_reference_45_false_0_is_inverse, Node_15_ref_node_target_id_45_0);
*Node_15_retrieved_reference_45_0_target_id = 30;
ishidaopcua_node_set_target_id(Node_15_retrieved_reference_45_0_target_id, Node_15_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_15_retrieved_reference_45_0->references,"1",Node_15_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_15_retrieved_reference_45_1;
ishidaopcua_NODE* Node_15_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_15_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_15_retrieved_reference_45_1);
*Node_15_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_15_retrieved_reference_45_1_typed_id,Node_15_ref_node_target_id_45_1);
*Node_15_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_15_retrieved_reference_45_false_1_is_inverse, Node_15_ref_node_target_id_45_1);
*Node_15_retrieved_reference_45_1_target_id = 16307;
ishidaopcua_node_set_target_id(Node_15_retrieved_reference_45_1_target_id, Node_15_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_15_retrieved_reference_45_1->references,"2",Node_15_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_15_retrieved_reference_45_2;
ishidaopcua_NODE* Node_15_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_15_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_15_retrieved_reference_45_2);
*Node_15_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_15_retrieved_reference_45_2_typed_id,Node_15_ref_node_target_id_45_2);
*Node_15_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_15_retrieved_reference_45_false_2_is_inverse, Node_15_ref_node_target_id_45_2);
*Node_15_retrieved_reference_45_2_target_id = 311;
ishidaopcua_node_set_target_id(Node_15_retrieved_reference_45_2_target_id, Node_15_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_15_retrieved_reference_45_2->references,"3",Node_15_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_15_retrieved_reference_45_3;
ishidaopcua_NODE* Node_15_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_15_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_15_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_15_retrieved_reference_45_3);
*Node_15_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_15_retrieved_reference_45_3_typed_id,Node_15_ref_node_target_id_45_3);
*Node_15_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_15_retrieved_reference_45_false_3_is_inverse, Node_15_ref_node_target_id_45_3);
*Node_15_retrieved_reference_45_3_target_id = 521;
ishidaopcua_node_set_target_id(Node_15_retrieved_reference_45_3_target_id, Node_15_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_15_retrieved_reference_45_3->references,"4",Node_15_ref_node_target_id_45_3);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* DataValue ********/


case 23 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 23;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("DataValue", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("DataValue", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a structure containing a value, a status code and timestamps.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 23 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_23_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_23_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_23_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_23_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_23_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_23_retrieved_reference_45_inverse_0);
*Node_23_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_23_retrieved_reference_45_inverse_0_typed_id,Node_23_ref_node_target_id_45_inverse_0);
*Node_23_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_23_retrieved_reference_45_inverse_true_0_is_inverse, Node_23_ref_node_target_id_45_inverse_0);
*Node_23_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_23_retrieved_reference_45_inverse_0_target_id, Node_23_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_23_retrieved_reference_45_inverse_0->references,"1",Node_23_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BaseDataType ********/


case 24 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 24;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BaseDataType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BaseDataType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that can have any valid DataType.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 24 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_0;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_0);
*Node_24_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_0_typed_id,Node_24_ref_node_target_id_45_0);
*Node_24_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_0_is_inverse, Node_24_ref_node_target_id_45_0);
*Node_24_retrieved_reference_45_0_target_id = 26;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_0_target_id, Node_24_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_0->references,"1",Node_24_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_1;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_1);
*Node_24_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_1_typed_id,Node_24_ref_node_target_id_45_1);
*Node_24_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_1_is_inverse, Node_24_ref_node_target_id_45_1);
*Node_24_retrieved_reference_45_1_target_id = 29;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_1_target_id, Node_24_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_1->references,"2",Node_24_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_2;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_2);
*Node_24_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_2_typed_id,Node_24_ref_node_target_id_45_2);
*Node_24_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_2_is_inverse, Node_24_ref_node_target_id_45_2);
*Node_24_retrieved_reference_45_2_target_id = 1;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_2_target_id, Node_24_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_2->references,"3",Node_24_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_3;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_3);
*Node_24_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_3_typed_id,Node_24_ref_node_target_id_45_3);
*Node_24_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_3_is_inverse, Node_24_ref_node_target_id_45_3);
*Node_24_retrieved_reference_45_3_target_id = 12;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_3_target_id, Node_24_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_3->references,"4",Node_24_ref_node_target_id_45_3);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_4;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_4);
*Node_24_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_4_typed_id,Node_24_ref_node_target_id_45_4);
*Node_24_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_4_is_inverse, Node_24_ref_node_target_id_45_4);
*Node_24_retrieved_reference_45_4_target_id = 13;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_4_target_id, Node_24_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_4->references,"5",Node_24_ref_node_target_id_45_4);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_5;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_5);
*Node_24_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_5_typed_id,Node_24_ref_node_target_id_45_5);
*Node_24_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_5_is_inverse, Node_24_ref_node_target_id_45_5);
*Node_24_retrieved_reference_45_5_target_id = 14;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_5_target_id, Node_24_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_5->references,"6",Node_24_ref_node_target_id_45_5);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_6;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_6);
*Node_24_retrieved_reference_45_6_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_6_typed_id,Node_24_ref_node_target_id_45_6);
*Node_24_retrieved_reference_45_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_6_is_inverse, Node_24_ref_node_target_id_45_6);
*Node_24_retrieved_reference_45_6_target_id = 15;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_6_target_id, Node_24_ref_node_target_id_45_6);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_6->references,"7",Node_24_ref_node_target_id_45_6);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_7;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_7);
*Node_24_retrieved_reference_45_7_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_7_typed_id,Node_24_ref_node_target_id_45_7);
*Node_24_retrieved_reference_45_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_7_is_inverse, Node_24_ref_node_target_id_45_7);
*Node_24_retrieved_reference_45_7_target_id = 16;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_7_target_id, Node_24_ref_node_target_id_45_7);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_7->references,"8",Node_24_ref_node_target_id_45_7);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_8;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_8);
*Node_24_retrieved_reference_45_8_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_8_typed_id,Node_24_ref_node_target_id_45_8);
*Node_24_retrieved_reference_45_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_8_is_inverse, Node_24_ref_node_target_id_45_8);
*Node_24_retrieved_reference_45_8_target_id = 17;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_8_target_id, Node_24_ref_node_target_id_45_8);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_8->references,"9",Node_24_ref_node_target_id_45_8);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_9;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_9);
*Node_24_retrieved_reference_45_9_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_9_typed_id,Node_24_ref_node_target_id_45_9);
*Node_24_retrieved_reference_45_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_9_is_inverse, Node_24_ref_node_target_id_45_9);
*Node_24_retrieved_reference_45_9_target_id = 18;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_9_target_id, Node_24_ref_node_target_id_45_9);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_9->references,"10",Node_24_ref_node_target_id_45_9);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_10;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_10);
*Node_24_retrieved_reference_45_10_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_10_typed_id,Node_24_ref_node_target_id_45_10);
*Node_24_retrieved_reference_45_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_10_is_inverse, Node_24_ref_node_target_id_45_10);
*Node_24_retrieved_reference_45_10_target_id = 19;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_10_target_id, Node_24_ref_node_target_id_45_10);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_10->references,"11",Node_24_ref_node_target_id_45_10);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_11;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_11);
*Node_24_retrieved_reference_45_11_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_11_typed_id,Node_24_ref_node_target_id_45_11);
*Node_24_retrieved_reference_45_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_11_is_inverse, Node_24_ref_node_target_id_45_11);
*Node_24_retrieved_reference_45_11_target_id = 20;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_11_target_id, Node_24_ref_node_target_id_45_11);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_11->references,"12",Node_24_ref_node_target_id_45_11);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_12;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_12 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_12_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_12_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_12_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_12);
*Node_24_retrieved_reference_45_12_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_12_typed_id,Node_24_ref_node_target_id_45_12);
*Node_24_retrieved_reference_45_false_12_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_12_is_inverse, Node_24_ref_node_target_id_45_12);
*Node_24_retrieved_reference_45_12_target_id = 21;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_12_target_id, Node_24_ref_node_target_id_45_12);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_12->references,"13",Node_24_ref_node_target_id_45_12);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_13;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_13 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_13_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_13_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_13_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_13);
*Node_24_retrieved_reference_45_13_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_13_typed_id,Node_24_ref_node_target_id_45_13);
*Node_24_retrieved_reference_45_false_13_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_13_is_inverse, Node_24_ref_node_target_id_45_13);
*Node_24_retrieved_reference_45_13_target_id = 22;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_13_target_id, Node_24_ref_node_target_id_45_13);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_13->references,"14",Node_24_ref_node_target_id_45_13);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_14;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_14 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_14_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_14_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_14_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_14);
*Node_24_retrieved_reference_45_14_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_14_typed_id,Node_24_ref_node_target_id_45_14);
*Node_24_retrieved_reference_45_false_14_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_14_is_inverse, Node_24_ref_node_target_id_45_14);
*Node_24_retrieved_reference_45_14_target_id = 23;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_14_target_id, Node_24_ref_node_target_id_45_14);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_14->references,"15",Node_24_ref_node_target_id_45_14);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_45_15;
ishidaopcua_NODE* Node_24_ref_node_target_id_45_15 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_45_15_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_false_15_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_45_15_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_24_retrieved_reference_45_15);
*Node_24_retrieved_reference_45_15_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_45_15_typed_id,Node_24_ref_node_target_id_45_15);
*Node_24_retrieved_reference_45_false_15_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_45_false_15_is_inverse, Node_24_ref_node_target_id_45_15);
*Node_24_retrieved_reference_45_15_target_id = 25;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_45_15_target_id, Node_24_ref_node_target_id_45_15);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_45_15->references,"16",Node_24_ref_node_target_id_45_15);

}if(references_flag != 2){

ishidaopcua_NODE* Node_24_retrieved_reference_35_inverse_0;
ishidaopcua_NODE* Node_24_ref_node_target_id_35_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_24_retrieved_reference_35_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_35_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_24_retrieved_reference_35_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"35_inverse",&Node_24_retrieved_reference_35_inverse_0);
*Node_24_retrieved_reference_35_inverse_0_typed_id =35;
ishidaopcua_node_set_reference_type_id(Node_24_retrieved_reference_35_inverse_0_typed_id,Node_24_ref_node_target_id_35_inverse_0);
*Node_24_retrieved_reference_35_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_24_retrieved_reference_35_inverse_true_0_is_inverse, Node_24_ref_node_target_id_35_inverse_0);
*Node_24_retrieved_reference_35_inverse_0_target_id = 90;
ishidaopcua_node_set_target_id(Node_24_retrieved_reference_35_inverse_0_target_id, Node_24_ref_node_target_id_35_inverse_0);
ishidaeutz_put_hashmap(Node_24_retrieved_reference_35_inverse_0->references,"1",Node_24_ref_node_target_id_35_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Image ********/


case 30 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 30;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Image", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Image", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an image encoded as a string of bytes.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 30 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_30_retrieved_reference_45_inverse_0);
*Node_30_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_inverse_0_typed_id,Node_30_ref_node_target_id_45_inverse_0);
*Node_30_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_inverse_true_0_is_inverse, Node_30_ref_node_target_id_45_inverse_0);
*Node_30_retrieved_reference_45_inverse_0_target_id = 15;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_inverse_0_target_id, Node_30_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_inverse_0->references,"1",Node_30_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_0;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_0);
*Node_30_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_0_typed_id,Node_30_ref_node_target_id_45_0);
*Node_30_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_0_is_inverse, Node_30_ref_node_target_id_45_0);
*Node_30_retrieved_reference_45_0_target_id = 2000;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_0_target_id, Node_30_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_0->references,"1",Node_30_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_1;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_1);
*Node_30_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_1_typed_id,Node_30_ref_node_target_id_45_1);
*Node_30_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_1_is_inverse, Node_30_ref_node_target_id_45_1);
*Node_30_retrieved_reference_45_1_target_id = 2001;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_1_target_id, Node_30_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_1->references,"2",Node_30_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_2;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_2);
*Node_30_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_2_typed_id,Node_30_ref_node_target_id_45_2);
*Node_30_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_2_is_inverse, Node_30_ref_node_target_id_45_2);
*Node_30_retrieved_reference_45_2_target_id = 2002;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_2_target_id, Node_30_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_2->references,"3",Node_30_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_30_retrieved_reference_45_3;
ishidaopcua_NODE* Node_30_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_30_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_30_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_30_retrieved_reference_45_3);
*Node_30_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_30_retrieved_reference_45_3_typed_id,Node_30_ref_node_target_id_45_3);
*Node_30_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_30_retrieved_reference_45_false_3_is_inverse, Node_30_ref_node_target_id_45_3);
*Node_30_retrieved_reference_45_3_target_id = 2003;
ishidaopcua_node_set_target_id(Node_30_retrieved_reference_45_3_target_id, Node_30_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_30_retrieved_reference_45_3->references,"4",Node_30_ref_node_target_id_45_3);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* NamingRuleType ********/


case 120 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 120;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("NamingRuleType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("NamingRuleType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that specifies the significance of the BrowseName for an instance declaration.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 120 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_120_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_120_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_120_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_120_retrieved_reference_45_inverse_0);
*Node_120_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_120_retrieved_reference_45_inverse_0_typed_id,Node_120_ref_node_target_id_45_inverse_0);
*Node_120_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_120_retrieved_reference_45_inverse_true_0_is_inverse, Node_120_ref_node_target_id_45_inverse_0);
*Node_120_retrieved_reference_45_inverse_0_target_id = 29;
ishidaopcua_node_set_target_id(Node_120_retrieved_reference_45_inverse_0_target_id, Node_120_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_120_retrieved_reference_45_inverse_0->references,"1",Node_120_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_120_retrieved_reference_46_0;
ishidaopcua_NODE* Node_120_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_120_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_120_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_120_retrieved_reference_46_0);
*Node_120_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_120_retrieved_reference_46_0_typed_id,Node_120_ref_node_target_id_46_0);
*Node_120_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_120_retrieved_reference_46_false_0_is_inverse, Node_120_ref_node_target_id_46_0);
*Node_120_retrieved_reference_46_0_target_id = 12169;
ishidaopcua_node_set_target_id(Node_120_retrieved_reference_46_0_target_id, Node_120_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_120_retrieved_reference_46_0->references,"1",Node_120_ref_node_target_id_46_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* UtcTime ********/


case 294 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 294;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("UtcTime", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("UtcTime", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A date/time value specified in Universal Coordinated Time (UTC).", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 294 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_294_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_294_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_294_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_294_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_294_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_294_retrieved_reference_45_inverse_0);
*Node_294_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_294_retrieved_reference_45_inverse_0_typed_id,Node_294_ref_node_target_id_45_inverse_0);
*Node_294_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_294_retrieved_reference_45_inverse_true_0_is_inverse, Node_294_ref_node_target_id_45_inverse_0);
*Node_294_retrieved_reference_45_inverse_0_target_id = 13;
ishidaopcua_node_set_target_id(Node_294_retrieved_reference_45_inverse_0_target_id, Node_294_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_294_retrieved_reference_45_inverse_0->references,"1",Node_294_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* EnumValues ********/


case 12169 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 12169;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("EnumValues", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("EnumValues", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7594;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = 1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 12169 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_12169_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_12169_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12169_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12169_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12169_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_12169_retrieved_reference_46_inverse_0);
*Node_12169_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_12169_retrieved_reference_46_inverse_0_typed_id,Node_12169_ref_node_target_id_46_inverse_0);
*Node_12169_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_12169_retrieved_reference_46_inverse_true_0_is_inverse, Node_12169_ref_node_target_id_46_inverse_0);
*Node_12169_retrieved_reference_46_inverse_0_target_id = 120;
ishidaopcua_node_set_target_id(Node_12169_retrieved_reference_46_inverse_0_target_id, Node_12169_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_12169_retrieved_reference_46_inverse_0->references,"1",Node_12169_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_12169_retrieved_reference_40_0;
ishidaopcua_NODE* Node_12169_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12169_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12169_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12169_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_12169_retrieved_reference_40_0);
*Node_12169_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_12169_retrieved_reference_40_0_typed_id,Node_12169_ref_node_target_id_40_0);
*Node_12169_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12169_retrieved_reference_40_false_0_is_inverse, Node_12169_ref_node_target_id_40_0);
*Node_12169_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_12169_retrieved_reference_40_0_target_id, Node_12169_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_12169_retrieved_reference_40_0->references,"1",Node_12169_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_12169_retrieved_reference_37_0;
ishidaopcua_NODE* Node_12169_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12169_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12169_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12169_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_12169_retrieved_reference_37_0);
*Node_12169_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_12169_retrieved_reference_37_0_typed_id,Node_12169_ref_node_target_id_37_0);
*Node_12169_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12169_retrieved_reference_37_false_0_is_inverse, Node_12169_ref_node_target_id_37_0);
*Node_12169_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_12169_retrieved_reference_37_0_target_id, Node_12169_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_12169_retrieved_reference_37_0->references,"1",Node_12169_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/
