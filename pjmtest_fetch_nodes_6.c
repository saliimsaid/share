#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_6(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* RedundancySupport ********/


case 2035 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2035;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("RedundancySupport", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("RedundancySupport", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Indicates what style of redundancy is supported by the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 851;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2035 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2035_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2035_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2035_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2035_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2035_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2035_retrieved_reference_46_inverse_0);
*Node_2035_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2035_retrieved_reference_46_inverse_0_typed_id,Node_2035_ref_node_target_id_46_inverse_0);
*Node_2035_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2035_retrieved_reference_46_inverse_true_0_is_inverse, Node_2035_ref_node_target_id_46_inverse_0);
*Node_2035_retrieved_reference_46_inverse_0_target_id = 2034;
ishidaopcua_node_set_target_id(Node_2035_retrieved_reference_46_inverse_0_target_id, Node_2035_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2035_retrieved_reference_46_inverse_0->references,"1",Node_2035_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2035_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2035_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2035_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2035_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2035_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2035_retrieved_reference_40_0);
*Node_2035_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2035_retrieved_reference_40_0_typed_id,Node_2035_ref_node_target_id_40_0);
*Node_2035_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2035_retrieved_reference_40_false_0_is_inverse, Node_2035_ref_node_target_id_40_0);
*Node_2035_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2035_retrieved_reference_40_0_target_id, Node_2035_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2035_retrieved_reference_40_0->references,"1",Node_2035_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2035_retrieved_reference_37_0;
ishidaopcua_NODE* Node_2035_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2035_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2035_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2035_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_2035_retrieved_reference_37_0);
*Node_2035_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_2035_retrieved_reference_37_0_typed_id,Node_2035_ref_node_target_id_37_0);
*Node_2035_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2035_retrieved_reference_37_false_0_is_inverse, Node_2035_ref_node_target_id_37_0);
*Node_2035_retrieved_reference_37_0_target_id = 78;
ishidaopcua_node_set_target_id(Node_2035_retrieved_reference_37_0_target_id, Node_2035_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_2035_retrieved_reference_37_0->references,"1",Node_2035_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* NamespaceArray ********/


case 2255 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2255;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("NamespaceArray", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("NamespaceArray", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The list of namespace URIs used by the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 12;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = 1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

 variant = ishidaopcua_init_variant();

 universal_variable_string = ishidaopcua_malloc(ishidaopcua_TYPE_ID_STRING,2);
ishidaopcua_set_string("http://opcfoundation.org/UA/",&universal_variable_string[0]);
ishidaopcua_set_string("ishida:opc-server:uncofigured:namespace-array",&universal_variable_string[1]);

variant->array_length = 2;
variant->value = universal_variable_string;
variant->encoding_mask = ishidaopcua_TYPE_ID_STRING;

ishidaopcua_node_set_value(variant, universal_node);

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 


universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(1000);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2255 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2255_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2255_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2255_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2255_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2255_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2255_retrieved_reference_46_inverse_0);
*Node_2255_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2255_retrieved_reference_46_inverse_0_typed_id,Node_2255_ref_node_target_id_46_inverse_0);
*Node_2255_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2255_retrieved_reference_46_inverse_true_0_is_inverse, Node_2255_ref_node_target_id_46_inverse_0);
*Node_2255_retrieved_reference_46_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_2255_retrieved_reference_46_inverse_0_target_id, Node_2255_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2255_retrieved_reference_46_inverse_0->references,"1",Node_2255_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2255_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2255_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2255_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2255_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2255_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2255_retrieved_reference_40_0);
*Node_2255_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2255_retrieved_reference_40_0_typed_id,Node_2255_ref_node_target_id_40_0);
*Node_2255_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2255_retrieved_reference_40_false_0_is_inverse, Node_2255_ref_node_target_id_40_0);
*Node_2255_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2255_retrieved_reference_40_0_target_id, Node_2255_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2255_retrieved_reference_40_0->references,"1",Node_2255_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxBrowseContinuationPoints ********/


case 2735 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2735;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxBrowseContinuationPoints", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxBrowseContinuationPoints", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of continuation points for Browse operations per session.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 5;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2735 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2735_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2735_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2735_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2735_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2735_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2735_retrieved_reference_46_inverse_0);
*Node_2735_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2735_retrieved_reference_46_inverse_0_typed_id,Node_2735_ref_node_target_id_46_inverse_0);
*Node_2735_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2735_retrieved_reference_46_inverse_true_0_is_inverse, Node_2735_ref_node_target_id_46_inverse_0);
*Node_2735_retrieved_reference_46_inverse_0_target_id = 2268;
ishidaopcua_node_set_target_id(Node_2735_retrieved_reference_46_inverse_0_target_id, Node_2735_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2735_retrieved_reference_46_inverse_0->references,"1",Node_2735_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2735_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2735_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2735_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2735_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2735_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2735_retrieved_reference_40_0);
*Node_2735_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2735_retrieved_reference_40_0_typed_id,Node_2735_ref_node_target_id_40_0);
*Node_2735_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2735_retrieved_reference_40_false_0_is_inverse, Node_2735_ref_node_target_id_40_0);
*Node_2735_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2735_retrieved_reference_40_0_target_id, Node_2735_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2735_retrieved_reference_40_0->references,"1",Node_2735_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Auditing ********/


case 2994 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2994;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Auditing", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Auditing", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A flag indicating whether the server is currently generating audit events.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 1;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 


universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(1000);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

puts("finished adding node >> 2994 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2994_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_2994_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2994_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2994_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2994_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_2994_retrieved_reference_46_inverse_0);
*Node_2994_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2994_retrieved_reference_46_inverse_0_typed_id,Node_2994_ref_node_target_id_46_inverse_0);
*Node_2994_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2994_retrieved_reference_46_inverse_true_0_is_inverse, Node_2994_ref_node_target_id_46_inverse_0);
*Node_2994_retrieved_reference_46_inverse_0_target_id = 2253;
ishidaopcua_node_set_target_id(Node_2994_retrieved_reference_46_inverse_0_target_id, Node_2994_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_2994_retrieved_reference_46_inverse_0->references,"1",Node_2994_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_2994_retrieved_reference_40_0;
ishidaopcua_NODE* Node_2994_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2994_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2994_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2994_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_2994_retrieved_reference_40_0);
*Node_2994_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_2994_retrieved_reference_40_0_typed_id,Node_2994_ref_node_target_id_40_0);
*Node_2994_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2994_retrieved_reference_40_false_0_is_inverse, Node_2994_ref_node_target_id_40_0);
*Node_2994_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_2994_retrieved_reference_40_0_target_id, Node_2994_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_2994_retrieved_reference_40_0->references,"1",Node_2994_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerRead ********/


case 11565 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11565;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerRead", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerRead", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single Read request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11565 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11565_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11565_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11565_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11565_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11565_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11565_retrieved_reference_46_inverse_0);
*Node_11565_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11565_retrieved_reference_46_inverse_0_typed_id,Node_11565_ref_node_target_id_46_inverse_0);
*Node_11565_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11565_retrieved_reference_46_inverse_true_0_is_inverse, Node_11565_ref_node_target_id_46_inverse_0);
*Node_11565_retrieved_reference_46_inverse_0_target_id = 11564;
ishidaopcua_node_set_target_id(Node_11565_retrieved_reference_46_inverse_0_target_id, Node_11565_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11565_retrieved_reference_46_inverse_0->references,"1",Node_11565_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11565_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11565_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11565_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11565_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11565_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11565_retrieved_reference_40_0);
*Node_11565_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11565_retrieved_reference_40_0_typed_id,Node_11565_ref_node_target_id_40_0);
*Node_11565_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11565_retrieved_reference_40_false_0_is_inverse, Node_11565_ref_node_target_id_40_0);
*Node_11565_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11565_retrieved_reference_40_0_target_id, Node_11565_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11565_retrieved_reference_40_0->references,"1",Node_11565_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11565_retrieved_reference_37_0;
ishidaopcua_NODE* Node_11565_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11565_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11565_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11565_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_11565_retrieved_reference_37_0);
*Node_11565_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_11565_retrieved_reference_37_0_typed_id,Node_11565_ref_node_target_id_37_0);
*Node_11565_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11565_retrieved_reference_37_false_0_is_inverse, Node_11565_ref_node_target_id_37_0);
*Node_11565_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_11565_retrieved_reference_37_0_target_id, Node_11565_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_11565_retrieved_reference_37_0->references,"1",Node_11565_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerWrite ********/


case 11567 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11567;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerWrite", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerWrite", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single Write request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11567 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11567_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11567_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11567_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11567_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11567_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11567_retrieved_reference_46_inverse_0);
*Node_11567_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11567_retrieved_reference_46_inverse_0_typed_id,Node_11567_ref_node_target_id_46_inverse_0);
*Node_11567_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11567_retrieved_reference_46_inverse_true_0_is_inverse, Node_11567_ref_node_target_id_46_inverse_0);
*Node_11567_retrieved_reference_46_inverse_0_target_id = 11564;
ishidaopcua_node_set_target_id(Node_11567_retrieved_reference_46_inverse_0_target_id, Node_11567_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11567_retrieved_reference_46_inverse_0->references,"1",Node_11567_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11567_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11567_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11567_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11567_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11567_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11567_retrieved_reference_40_0);
*Node_11567_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11567_retrieved_reference_40_0_typed_id,Node_11567_ref_node_target_id_40_0);
*Node_11567_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11567_retrieved_reference_40_false_0_is_inverse, Node_11567_ref_node_target_id_40_0);
*Node_11567_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11567_retrieved_reference_40_0_target_id, Node_11567_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11567_retrieved_reference_40_0->references,"1",Node_11567_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11567_retrieved_reference_37_0;
ishidaopcua_NODE* Node_11567_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11567_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11567_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11567_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_11567_retrieved_reference_37_0);
*Node_11567_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_11567_retrieved_reference_37_0_typed_id,Node_11567_ref_node_target_id_37_0);
*Node_11567_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11567_retrieved_reference_37_false_0_is_inverse, Node_11567_ref_node_target_id_37_0);
*Node_11567_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_11567_retrieved_reference_37_0_target_id, Node_11567_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_11567_retrieved_reference_37_0->references,"1",Node_11567_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerMethodCall ********/


case 11569 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11569;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerMethodCall", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerMethodCall", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single Call request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11569 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11569_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11569_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11569_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11569_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11569_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11569_retrieved_reference_46_inverse_0);
*Node_11569_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11569_retrieved_reference_46_inverse_0_typed_id,Node_11569_ref_node_target_id_46_inverse_0);
*Node_11569_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11569_retrieved_reference_46_inverse_true_0_is_inverse, Node_11569_ref_node_target_id_46_inverse_0);
*Node_11569_retrieved_reference_46_inverse_0_target_id = 11564;
ishidaopcua_node_set_target_id(Node_11569_retrieved_reference_46_inverse_0_target_id, Node_11569_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11569_retrieved_reference_46_inverse_0->references,"1",Node_11569_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11569_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11569_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11569_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11569_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11569_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11569_retrieved_reference_40_0);
*Node_11569_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11569_retrieved_reference_40_0_typed_id,Node_11569_ref_node_target_id_40_0);
*Node_11569_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11569_retrieved_reference_40_false_0_is_inverse, Node_11569_ref_node_target_id_40_0);
*Node_11569_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11569_retrieved_reference_40_0_target_id, Node_11569_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11569_retrieved_reference_40_0->references,"1",Node_11569_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11569_retrieved_reference_37_0;
ishidaopcua_NODE* Node_11569_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11569_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11569_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11569_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_11569_retrieved_reference_37_0);
*Node_11569_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_11569_retrieved_reference_37_0_typed_id,Node_11569_ref_node_target_id_37_0);
*Node_11569_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11569_retrieved_reference_37_false_0_is_inverse, Node_11569_ref_node_target_id_37_0);
*Node_11569_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_11569_retrieved_reference_37_0_target_id, Node_11569_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_11569_retrieved_reference_37_0->references,"1",Node_11569_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerWrite ********/


case 11707 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11707;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerWrite", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerWrite", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single Write request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11707 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11707_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11707_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11707_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11707_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11707_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11707_retrieved_reference_46_inverse_0);
*Node_11707_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11707_retrieved_reference_46_inverse_0_typed_id,Node_11707_ref_node_target_id_46_inverse_0);
*Node_11707_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11707_retrieved_reference_46_inverse_true_0_is_inverse, Node_11707_ref_node_target_id_46_inverse_0);
*Node_11707_retrieved_reference_46_inverse_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_11707_retrieved_reference_46_inverse_0_target_id, Node_11707_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11707_retrieved_reference_46_inverse_0->references,"1",Node_11707_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11707_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11707_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11707_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11707_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11707_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11707_retrieved_reference_40_0);
*Node_11707_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11707_retrieved_reference_40_0_typed_id,Node_11707_ref_node_target_id_40_0);
*Node_11707_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11707_retrieved_reference_40_false_0_is_inverse, Node_11707_ref_node_target_id_40_0);
*Node_11707_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11707_retrieved_reference_40_0_target_id, Node_11707_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11707_retrieved_reference_40_0->references,"1",Node_11707_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerMethodCall ********/


case 11709 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11709;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerMethodCall", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerMethodCall", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single Call request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11709 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11709_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11709_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11709_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11709_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11709_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11709_retrieved_reference_46_inverse_0);
*Node_11709_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11709_retrieved_reference_46_inverse_0_typed_id,Node_11709_ref_node_target_id_46_inverse_0);
*Node_11709_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11709_retrieved_reference_46_inverse_true_0_is_inverse, Node_11709_ref_node_target_id_46_inverse_0);
*Node_11709_retrieved_reference_46_inverse_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_11709_retrieved_reference_46_inverse_0_target_id, Node_11709_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11709_retrieved_reference_46_inverse_0->references,"1",Node_11709_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11709_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11709_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11709_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11709_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11709_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11709_retrieved_reference_40_0);
*Node_11709_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11709_retrieved_reference_40_0_typed_id,Node_11709_ref_node_target_id_40_0);
*Node_11709_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11709_retrieved_reference_40_false_0_is_inverse, Node_11709_ref_node_target_id_40_0);
*Node_11709_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11709_retrieved_reference_40_0_target_id, Node_11709_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11709_retrieved_reference_40_0->references,"1",Node_11709_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* MaxNodesPerBrowse ********/


case 11710 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11710;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_VARIABLE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("MaxNodesPerBrowse", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("MaxNodesPerBrowse", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The maximum number of operations in a single Browse request.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

data_type_node_id->namespace_index = 0;
data_type_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
data_type_node_id->identifier.numeric = 7;
ishidaopcua_node_set_data_type(data_type_node_id, universal_node); 
universal_value_rank = ishidaopcua_malloc(ishidaopcua_TYPE_ID_INT32,1);
*universal_value_rank = -1;
ishidaopcua_node_set_value_rank(universal_value_rank, universal_node); 

universal_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_access_level = 1;
ishidaopcua_node_set_access_level(universal_access_level, universal_node); 

universal_user_access_level = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BYTE,1);
*universal_user_access_level = 1;
ishidaopcua_node_set_user_access_level(universal_user_access_level, universal_node); 

universal_minimum_sampling_interval = create_ishidaeutz_LARGE_INT_from_int(0);
ishidaopcua_node_set_minimum_sampling_interval(universal_minimum_sampling_interval, universal_node); 

universal_historizing = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_historizing = 0;
ishidaopcua_node_set_historizing(universal_historizing, universal_node);

variant = ishidaopcua_init_variant(); 

universal_UINT32 = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 

*universal_UINT32 = 0; 

variant->value = universal_UINT32; 
variant->encoding_mask = 7; 
ishidaopcua_node_set_value(variant, universal_node);

puts("finished adding node >> 11710 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11710_retrieved_reference_46_inverse_0;
ishidaopcua_NODE* Node_11710_ref_node_target_id_46_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11710_retrieved_reference_46_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11710_retrieved_reference_46_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11710_retrieved_reference_46_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46_inverse",&Node_11710_retrieved_reference_46_inverse_0);
*Node_11710_retrieved_reference_46_inverse_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11710_retrieved_reference_46_inverse_0_typed_id,Node_11710_ref_node_target_id_46_inverse_0);
*Node_11710_retrieved_reference_46_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11710_retrieved_reference_46_inverse_true_0_is_inverse, Node_11710_ref_node_target_id_46_inverse_0);
*Node_11710_retrieved_reference_46_inverse_0_target_id = 11704;
ishidaopcua_node_set_target_id(Node_11710_retrieved_reference_46_inverse_0_target_id, Node_11710_ref_node_target_id_46_inverse_0);
ishidaeutz_put_hashmap(Node_11710_retrieved_reference_46_inverse_0->references,"1",Node_11710_ref_node_target_id_46_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11710_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11710_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11710_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11710_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11710_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11710_retrieved_reference_40_0);
*Node_11710_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11710_retrieved_reference_40_0_typed_id,Node_11710_ref_node_target_id_40_0);
*Node_11710_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11710_retrieved_reference_40_false_0_is_inverse, Node_11710_ref_node_target_id_40_0);
*Node_11710_retrieved_reference_40_0_target_id = 68;
ishidaopcua_node_set_target_id(Node_11710_retrieved_reference_40_0_target_id, Node_11710_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11710_retrieved_reference_40_0->references,"1",Node_11710_ref_node_target_id_40_0); 
} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/
