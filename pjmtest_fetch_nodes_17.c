#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_17(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* UInt16 ********/


case 5 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 5;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("UInt16", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("UInt16", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between 0 and 65535.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 5 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_5_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_5_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_5_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_5_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_5_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_5_retrieved_reference_45_inverse_0);
*Node_5_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_5_retrieved_reference_45_inverse_0_typed_id,Node_5_ref_node_target_id_45_inverse_0);
*Node_5_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_5_retrieved_reference_45_inverse_true_0_is_inverse, Node_5_ref_node_target_id_45_inverse_0);
*Node_5_retrieved_reference_45_inverse_0_target_id = 28;
ishidaopcua_node_set_target_id(Node_5_retrieved_reference_45_inverse_0_target_id, Node_5_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_5_retrieved_reference_45_inverse_0->references,"1",Node_5_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_5_retrieved_reference_45_0;
ishidaopcua_NODE* Node_5_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_5_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_5_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_5_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_5_retrieved_reference_45_0);
*Node_5_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_5_retrieved_reference_45_0_typed_id,Node_5_ref_node_target_id_45_0);
*Node_5_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_5_retrieved_reference_45_false_0_is_inverse, Node_5_ref_node_target_id_45_0);
*Node_5_retrieved_reference_45_0_target_id = 15904;
ishidaopcua_node_set_target_id(Node_5_retrieved_reference_45_0_target_id, Node_5_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_5_retrieved_reference_45_0->references,"1",Node_5_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_5_retrieved_reference_45_1;
ishidaopcua_NODE* Node_5_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_5_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_5_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_5_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_5_retrieved_reference_45_1);
*Node_5_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_5_retrieved_reference_45_1_typed_id,Node_5_ref_node_target_id_45_1);
*Node_5_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_5_retrieved_reference_45_false_1_is_inverse, Node_5_ref_node_target_id_45_1);
*Node_5_retrieved_reference_45_1_target_id = 94;
ishidaopcua_node_set_target_id(Node_5_retrieved_reference_45_1_target_id, Node_5_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_5_retrieved_reference_45_1->references,"2",Node_5_ref_node_target_id_45_1);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* UInt32 ********/


case 7 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("UInt32", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("UInt32", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between 0 and 4,294,967,295.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 7 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_7_retrieved_reference_45_inverse_0);
*Node_7_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_inverse_0_typed_id,Node_7_ref_node_target_id_45_inverse_0);
*Node_7_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_inverse_true_0_is_inverse, Node_7_ref_node_target_id_45_inverse_0);
*Node_7_retrieved_reference_45_inverse_0_target_id = 28;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_inverse_0_target_id, Node_7_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_inverse_0->references,"1",Node_7_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_0;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_0);
*Node_7_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_0_typed_id,Node_7_ref_node_target_id_45_0);
*Node_7_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_0_is_inverse, Node_7_ref_node_target_id_45_0);
*Node_7_retrieved_reference_45_0_target_id = 15583;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_0_target_id, Node_7_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_0->references,"1",Node_7_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_1;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_1);
*Node_7_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_1_typed_id,Node_7_ref_node_target_id_45_1);
*Node_7_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_1_is_inverse, Node_7_ref_node_target_id_45_1);
*Node_7_retrieved_reference_45_1_target_id = 15642;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_1_target_id, Node_7_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_1->references,"2",Node_7_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_2;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_2);
*Node_7_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_2_typed_id,Node_7_ref_node_target_id_45_2);
*Node_7_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_2_is_inverse, Node_7_ref_node_target_id_45_2);
*Node_7_retrieved_reference_45_2_target_id = 15646;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_2_target_id, Node_7_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_2->references,"3",Node_7_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_3;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_3);
*Node_7_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_3_typed_id,Node_7_ref_node_target_id_45_3);
*Node_7_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_3_is_inverse, Node_7_ref_node_target_id_45_3);
*Node_7_retrieved_reference_45_3_target_id = 15654;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_3_target_id, Node_7_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_3->references,"4",Node_7_ref_node_target_id_45_3);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_4;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_4);
*Node_7_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_4_typed_id,Node_7_ref_node_target_id_45_4);
*Node_7_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_4_is_inverse, Node_7_ref_node_target_id_45_4);
*Node_7_retrieved_reference_45_4_target_id = 15658;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_4_target_id, Node_7_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_4->references,"5",Node_7_ref_node_target_id_45_4);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_5;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_5);
*Node_7_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_5_typed_id,Node_7_ref_node_target_id_45_5);
*Node_7_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_5_is_inverse, Node_7_ref_node_target_id_45_5);
*Node_7_retrieved_reference_45_5_target_id = 15406;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_5_target_id, Node_7_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_5->references,"6",Node_7_ref_node_target_id_45_5);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_6;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_6);
*Node_7_retrieved_reference_45_6_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_6_typed_id,Node_7_ref_node_target_id_45_6);
*Node_7_retrieved_reference_45_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_6_is_inverse, Node_7_ref_node_target_id_45_6);
*Node_7_retrieved_reference_45_6_target_id = 15033;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_6_target_id, Node_7_ref_node_target_id_45_6);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_6->references,"7",Node_7_ref_node_target_id_45_6);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_7;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_7);
*Node_7_retrieved_reference_45_7_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_7_typed_id,Node_7_ref_node_target_id_45_7);
*Node_7_retrieved_reference_45_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_7_is_inverse, Node_7_ref_node_target_id_45_7);
*Node_7_retrieved_reference_45_7_target_id = 95;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_7_target_id, Node_7_ref_node_target_id_45_7);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_7->references,"8",Node_7_ref_node_target_id_45_7);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_8;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_8);
*Node_7_retrieved_reference_45_8_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_8_typed_id,Node_7_ref_node_target_id_45_8);
*Node_7_retrieved_reference_45_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_8_is_inverse, Node_7_ref_node_target_id_45_8);
*Node_7_retrieved_reference_45_8_target_id = 288;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_8_target_id, Node_7_ref_node_target_id_45_8);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_8->references,"9",Node_7_ref_node_target_id_45_8);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_9;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_9);
*Node_7_retrieved_reference_45_9_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_9_typed_id,Node_7_ref_node_target_id_45_9);
*Node_7_retrieved_reference_45_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_9_is_inverse, Node_7_ref_node_target_id_45_9);
*Node_7_retrieved_reference_45_9_target_id = 20998;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_9_target_id, Node_7_ref_node_target_id_45_9);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_9->references,"10",Node_7_ref_node_target_id_45_9);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_10;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_10);
*Node_7_retrieved_reference_45_10_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_10_typed_id,Node_7_ref_node_target_id_45_10);
*Node_7_retrieved_reference_45_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_10_is_inverse, Node_7_ref_node_target_id_45_10);
*Node_7_retrieved_reference_45_10_target_id = 347;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_10_target_id, Node_7_ref_node_target_id_45_10);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_10->references,"11",Node_7_ref_node_target_id_45_10);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7_retrieved_reference_45_11;
ishidaopcua_NODE* Node_7_ref_node_target_id_45_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7_retrieved_reference_45_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7_retrieved_reference_45_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7_retrieved_reference_45_11);
*Node_7_retrieved_reference_45_11_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7_retrieved_reference_45_11_typed_id,Node_7_ref_node_target_id_45_11);
*Node_7_retrieved_reference_45_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7_retrieved_reference_45_false_11_is_inverse, Node_7_ref_node_target_id_45_11);
*Node_7_retrieved_reference_45_11_target_id = 289;
ishidaopcua_node_set_target_id(Node_7_retrieved_reference_45_11_target_id, Node_7_ref_node_target_id_45_11);
ishidaeutz_put_hashmap(Node_7_retrieved_reference_45_11->references,"12",Node_7_ref_node_target_id_45_11);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* UInt64 ********/


case 9 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 9;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("UInt64", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("UInt64", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is an integer between 0 and 18,446,744,073,709,551,615.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 9 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_9_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_9_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_9_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_9_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_9_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_9_retrieved_reference_45_inverse_0);
*Node_9_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_9_retrieved_reference_45_inverse_0_typed_id,Node_9_ref_node_target_id_45_inverse_0);
*Node_9_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_9_retrieved_reference_45_inverse_true_0_is_inverse, Node_9_ref_node_target_id_45_inverse_0);
*Node_9_retrieved_reference_45_inverse_0_target_id = 28;
ishidaopcua_node_set_target_id(Node_9_retrieved_reference_45_inverse_0_target_id, Node_9_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_9_retrieved_reference_45_inverse_0->references,"1",Node_9_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_9_retrieved_reference_45_0;
ishidaopcua_NODE* Node_9_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_9_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_9_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_9_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_9_retrieved_reference_45_0);
*Node_9_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_9_retrieved_reference_45_0_typed_id,Node_9_ref_node_target_id_45_0);
*Node_9_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_9_retrieved_reference_45_false_0_is_inverse, Node_9_ref_node_target_id_45_0);
*Node_9_retrieved_reference_45_0_target_id = 11737;
ishidaopcua_node_set_target_id(Node_9_retrieved_reference_45_0_target_id, Node_9_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_9_retrieved_reference_45_0->references,"1",Node_9_ref_node_target_id_45_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* String ********/


case 12 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 12;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("String", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("String", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a sequence of printable Unicode characters.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 12 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_12_retrieved_reference_45_inverse_0);
*Node_12_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_inverse_0_typed_id,Node_12_ref_node_target_id_45_inverse_0);
*Node_12_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_inverse_true_0_is_inverse, Node_12_ref_node_target_id_45_inverse_0);
*Node_12_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_inverse_0_target_id, Node_12_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_inverse_0->references,"1",Node_12_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_0;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_0);
*Node_12_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_0_typed_id,Node_12_ref_node_target_id_45_0);
*Node_12_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_0_is_inverse, Node_12_ref_node_target_id_45_0);
*Node_12_retrieved_reference_45_0_target_id = 12877;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_0_target_id, Node_12_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_0->references,"1",Node_12_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_1;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_1);
*Node_12_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_1_typed_id,Node_12_ref_node_target_id_45_1);
*Node_12_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_1_is_inverse, Node_12_ref_node_target_id_45_1);
*Node_12_retrieved_reference_45_1_target_id = 12878;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_1_target_id, Node_12_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_1->references,"2",Node_12_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_2;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_2);
*Node_12_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_2_typed_id,Node_12_ref_node_target_id_45_2);
*Node_12_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_2_is_inverse, Node_12_ref_node_target_id_45_2);
*Node_12_retrieved_reference_45_2_target_id = 12879;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_2_target_id, Node_12_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_2->references,"3",Node_12_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_3;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_3);
*Node_12_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_3_typed_id,Node_12_ref_node_target_id_45_3);
*Node_12_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_3_is_inverse, Node_12_ref_node_target_id_45_3);
*Node_12_retrieved_reference_45_3_target_id = 12880;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_3_target_id, Node_12_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_3->references,"4",Node_12_ref_node_target_id_45_3);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_4;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_4);
*Node_12_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_4_typed_id,Node_12_ref_node_target_id_45_4);
*Node_12_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_4_is_inverse, Node_12_ref_node_target_id_45_4);
*Node_12_retrieved_reference_45_4_target_id = 12881;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_4_target_id, Node_12_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_4->references,"5",Node_12_ref_node_target_id_45_4);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_5;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_5);
*Node_12_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_5_typed_id,Node_12_ref_node_target_id_45_5);
*Node_12_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_5_is_inverse, Node_12_ref_node_target_id_45_5);
*Node_12_retrieved_reference_45_5_target_id = 295;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_5_target_id, Node_12_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_5->references,"6",Node_12_ref_node_target_id_45_5);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_6;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_6);
*Node_12_retrieved_reference_45_6_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_6_typed_id,Node_12_ref_node_target_id_45_6);
*Node_12_retrieved_reference_45_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_6_is_inverse, Node_12_ref_node_target_id_45_6);
*Node_12_retrieved_reference_45_6_target_id = 291;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_6_target_id, Node_12_ref_node_target_id_45_6);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_6->references,"7",Node_12_ref_node_target_id_45_6);

}if(references_flag != 2){

ishidaopcua_NODE* Node_12_retrieved_reference_45_7;
ishidaopcua_NODE* Node_12_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_12_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_12_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_12_retrieved_reference_45_7);
*Node_12_retrieved_reference_45_7_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_12_retrieved_reference_45_7_typed_id,Node_12_ref_node_target_id_45_7);
*Node_12_retrieved_reference_45_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_12_retrieved_reference_45_false_7_is_inverse, Node_12_ref_node_target_id_45_7);
*Node_12_retrieved_reference_45_7_target_id = 292;
ishidaopcua_node_set_target_id(Node_12_retrieved_reference_45_7_target_id, Node_12_ref_node_target_id_45_7);
ishidaeutz_put_hashmap(Node_12_retrieved_reference_45_7->references,"8",Node_12_ref_node_target_id_45_7);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* StatusCode ********/


case 19 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 19;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("StatusCode", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("StatusCode", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a code representing the outcome of an operation by a Server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 19 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_19_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_19_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_19_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_19_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_19_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_19_retrieved_reference_45_inverse_0);
*Node_19_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_19_retrieved_reference_45_inverse_0_typed_id,Node_19_ref_node_target_id_45_inverse_0);
*Node_19_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_19_retrieved_reference_45_inverse_true_0_is_inverse, Node_19_ref_node_target_id_45_inverse_0);
*Node_19_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_19_retrieved_reference_45_inverse_0_target_id, Node_19_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_19_retrieved_reference_45_inverse_0->references,"1",Node_19_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* QualifiedName ********/


case 20 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 20;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("QualifiedName", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("QualifiedName", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is a name qualified by a namespace.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 20 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_20_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_20_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_20_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_20_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_20_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_20_retrieved_reference_45_inverse_0);
*Node_20_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_20_retrieved_reference_45_inverse_0_typed_id,Node_20_ref_node_target_id_45_inverse_0);
*Node_20_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_20_retrieved_reference_45_inverse_true_0_is_inverse, Node_20_ref_node_target_id_45_inverse_0);
*Node_20_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_20_retrieved_reference_45_inverse_0_target_id, Node_20_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_20_retrieved_reference_45_inverse_0->references,"1",Node_20_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Structure ********/


case 22 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 22;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Structure", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Structure", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes a value that is any type of structure that can be described with a data encoding.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 22 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_22_retrieved_reference_45_inverse_0);
*Node_22_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_inverse_0_typed_id,Node_22_ref_node_target_id_45_inverse_0);
*Node_22_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_inverse_true_0_is_inverse, Node_22_ref_node_target_id_45_inverse_0);
*Node_22_retrieved_reference_45_inverse_0_target_id = 24;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_inverse_0_target_id, Node_22_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_inverse_0->references,"1",Node_22_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_0;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_0);
*Node_22_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_0_typed_id,Node_22_ref_node_target_id_45_0);
*Node_22_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_0_is_inverse, Node_22_ref_node_target_id_45_0);
*Node_22_retrieved_reference_45_0_target_id = 14533;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_0_target_id, Node_22_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_0->references,"1",Node_22_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_1;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_1);
*Node_22_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_1_typed_id,Node_22_ref_node_target_id_45_1);
*Node_22_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_1_is_inverse, Node_22_ref_node_target_id_45_1);
*Node_22_retrieved_reference_45_1_target_id = 15528;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_1_target_id, Node_22_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_1->references,"2",Node_22_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_2;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_2);
*Node_22_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_2_typed_id,Node_22_ref_node_target_id_45_2);
*Node_22_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_2_is_inverse, Node_22_ref_node_target_id_45_2);
*Node_22_retrieved_reference_45_2_target_id = 15634;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_2_target_id, Node_22_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_2->references,"3",Node_22_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_3;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_3);
*Node_22_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_3_typed_id,Node_22_ref_node_target_id_45_3);
*Node_22_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_3_is_inverse, Node_22_ref_node_target_id_45_3);
*Node_22_retrieved_reference_45_3_target_id = 12554;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_3_target_id, Node_22_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_3->references,"4",Node_22_ref_node_target_id_45_3);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_4;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_4);
*Node_22_retrieved_reference_45_4_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_4_typed_id,Node_22_ref_node_target_id_45_4);
*Node_22_retrieved_reference_45_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_4_is_inverse, Node_22_ref_node_target_id_45_4);
*Node_22_retrieved_reference_45_4_target_id = 15534;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_4_target_id, Node_22_ref_node_target_id_45_4);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_4->references,"5",Node_22_ref_node_target_id_45_4);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_5;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_5);
*Node_22_retrieved_reference_45_5_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_5_typed_id,Node_22_ref_node_target_id_45_5);
*Node_22_retrieved_reference_45_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_5_is_inverse, Node_22_ref_node_target_id_45_5);
*Node_22_retrieved_reference_45_5_target_id = 14525;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_5_target_id, Node_22_ref_node_target_id_45_5);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_5->references,"6",Node_22_ref_node_target_id_45_5);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_6;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_6);
*Node_22_retrieved_reference_45_6_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_6_typed_id,Node_22_ref_node_target_id_45_6);
*Node_22_retrieved_reference_45_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_6_is_inverse, Node_22_ref_node_target_id_45_6);
*Node_22_retrieved_reference_45_6_target_id = 14524;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_6_target_id, Node_22_ref_node_target_id_45_6);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_6->references,"7",Node_22_ref_node_target_id_45_6);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_7;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_7);
*Node_22_retrieved_reference_45_7_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_7_typed_id,Node_22_ref_node_target_id_45_7);
*Node_22_retrieved_reference_45_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_7_is_inverse, Node_22_ref_node_target_id_45_7);
*Node_22_retrieved_reference_45_7_target_id = 14593;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_7_target_id, Node_22_ref_node_target_id_45_7);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_7->references,"8",Node_22_ref_node_target_id_45_7);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_8;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_8);
*Node_22_retrieved_reference_45_8_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_8_typed_id,Node_22_ref_node_target_id_45_8);
*Node_22_retrieved_reference_45_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_8_is_inverse, Node_22_ref_node_target_id_45_8);
*Node_22_retrieved_reference_45_8_target_id = 15578;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_8_target_id, Node_22_ref_node_target_id_45_8);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_8->references,"9",Node_22_ref_node_target_id_45_8);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_9;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_9);
*Node_22_retrieved_reference_45_9_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_9_typed_id,Node_22_ref_node_target_id_45_9);
*Node_22_retrieved_reference_45_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_9_is_inverse, Node_22_ref_node_target_id_45_9);
*Node_22_retrieved_reference_45_9_target_id = 15580;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_9_target_id, Node_22_ref_node_target_id_45_9);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_9->references,"10",Node_22_ref_node_target_id_45_9);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_10;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_10);
*Node_22_retrieved_reference_45_10_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_10_typed_id,Node_22_ref_node_target_id_45_10);
*Node_22_retrieved_reference_45_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_10_is_inverse, Node_22_ref_node_target_id_45_10);
*Node_22_retrieved_reference_45_10_target_id = 14273;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_10_target_id, Node_22_ref_node_target_id_45_10);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_10->references,"11",Node_22_ref_node_target_id_45_10);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_11;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_11);
*Node_22_retrieved_reference_45_11_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_11_typed_id,Node_22_ref_node_target_id_45_11);
*Node_22_retrieved_reference_45_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_11_is_inverse, Node_22_ref_node_target_id_45_11);
*Node_22_retrieved_reference_45_11_target_id = 15597;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_11_target_id, Node_22_ref_node_target_id_45_11);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_11->references,"12",Node_22_ref_node_target_id_45_11);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_12;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_12 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_12_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_12_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_12_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_12);
*Node_22_retrieved_reference_45_12_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_12_typed_id,Node_22_ref_node_target_id_45_12);
*Node_22_retrieved_reference_45_false_12_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_12_is_inverse, Node_22_ref_node_target_id_45_12);
*Node_22_retrieved_reference_45_12_target_id = 15598;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_12_target_id, Node_22_ref_node_target_id_45_12);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_12->references,"13",Node_22_ref_node_target_id_45_12);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_13;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_13 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_13_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_13_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_13_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_13);
*Node_22_retrieved_reference_45_13_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_13_typed_id,Node_22_ref_node_target_id_45_13);
*Node_22_retrieved_reference_45_false_13_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_13_is_inverse, Node_22_ref_node_target_id_45_13);
*Node_22_retrieved_reference_45_13_target_id = 15605;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_13_target_id, Node_22_ref_node_target_id_45_13);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_13->references,"14",Node_22_ref_node_target_id_45_13);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_14;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_14 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_14_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_14_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_14_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_14);
*Node_22_retrieved_reference_45_14_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_14_typed_id,Node_22_ref_node_target_id_45_14);
*Node_22_retrieved_reference_45_false_14_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_14_is_inverse, Node_22_ref_node_target_id_45_14);
*Node_22_retrieved_reference_45_14_target_id = 15609;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_14_target_id, Node_22_ref_node_target_id_45_14);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_14->references,"15",Node_22_ref_node_target_id_45_14);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_15;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_15 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_15_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_15_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_15_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_15);
*Node_22_retrieved_reference_45_15_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_15_typed_id,Node_22_ref_node_target_id_45_15);
*Node_22_retrieved_reference_45_false_15_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_15_is_inverse, Node_22_ref_node_target_id_45_15);
*Node_22_retrieved_reference_45_15_target_id = 15611;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_15_target_id, Node_22_ref_node_target_id_45_15);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_15->references,"16",Node_22_ref_node_target_id_45_15);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_16;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_16 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_16_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_16_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_16_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_16);
*Node_22_retrieved_reference_45_16_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_16_typed_id,Node_22_ref_node_target_id_45_16);
*Node_22_retrieved_reference_45_false_16_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_16_is_inverse, Node_22_ref_node_target_id_45_16);
*Node_22_retrieved_reference_45_16_target_id = 15616;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_16_target_id, Node_22_ref_node_target_id_45_16);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_16->references,"17",Node_22_ref_node_target_id_45_16);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_17;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_17 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_17_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_17_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_17_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_17);
*Node_22_retrieved_reference_45_17_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_17_typed_id,Node_22_ref_node_target_id_45_17);
*Node_22_retrieved_reference_45_false_17_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_17_is_inverse, Node_22_ref_node_target_id_45_17);
*Node_22_retrieved_reference_45_17_target_id = 15617;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_17_target_id, Node_22_ref_node_target_id_45_17);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_17->references,"18",Node_22_ref_node_target_id_45_17);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_18;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_18 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_18_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_18_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_18_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_18);
*Node_22_retrieved_reference_45_18_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_18_typed_id,Node_22_ref_node_target_id_45_18);
*Node_22_retrieved_reference_45_false_18_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_18_is_inverse, Node_22_ref_node_target_id_45_18);
*Node_22_retrieved_reference_45_18_target_id = 15618;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_18_target_id, Node_22_ref_node_target_id_45_18);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_18->references,"19",Node_22_ref_node_target_id_45_18);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_19;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_19 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_19_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_19_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_19_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_19);
*Node_22_retrieved_reference_45_19_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_19_typed_id,Node_22_ref_node_target_id_45_19);
*Node_22_retrieved_reference_45_false_19_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_19_is_inverse, Node_22_ref_node_target_id_45_19);
*Node_22_retrieved_reference_45_19_target_id = 15502;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_19_target_id, Node_22_ref_node_target_id_45_19);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_19->references,"20",Node_22_ref_node_target_id_45_19);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_20;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_20 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_20_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_20_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_20_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_20);
*Node_22_retrieved_reference_45_20_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_20_typed_id,Node_22_ref_node_target_id_45_20);
*Node_22_retrieved_reference_45_false_20_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_20_is_inverse, Node_22_ref_node_target_id_45_20);
*Node_22_retrieved_reference_45_20_target_id = 15621;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_20_target_id, Node_22_ref_node_target_id_45_20);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_20->references,"21",Node_22_ref_node_target_id_45_20);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_21;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_21 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_21_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_21_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_21_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_21);
*Node_22_retrieved_reference_45_21_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_21_typed_id,Node_22_ref_node_target_id_45_21);
*Node_22_retrieved_reference_45_false_21_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_21_is_inverse, Node_22_ref_node_target_id_45_21);
*Node_22_retrieved_reference_45_21_target_id = 15622;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_21_target_id, Node_22_ref_node_target_id_45_21);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_21->references,"22",Node_22_ref_node_target_id_45_21);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_22;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_22 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_22_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_22_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_22_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_22);
*Node_22_retrieved_reference_45_22_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_22_typed_id,Node_22_ref_node_target_id_45_22);
*Node_22_retrieved_reference_45_false_22_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_22_is_inverse, Node_22_ref_node_target_id_45_22);
*Node_22_retrieved_reference_45_22_target_id = 15623;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_22_target_id, Node_22_ref_node_target_id_45_22);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_22->references,"23",Node_22_ref_node_target_id_45_22);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_23;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_23 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_23_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_23_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_23_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_23);
*Node_22_retrieved_reference_45_23_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_23_typed_id,Node_22_ref_node_target_id_45_23);
*Node_22_retrieved_reference_45_false_23_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_23_is_inverse, Node_22_ref_node_target_id_45_23);
*Node_22_retrieved_reference_45_23_target_id = 15628;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_23_target_id, Node_22_ref_node_target_id_45_23);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_23->references,"24",Node_22_ref_node_target_id_45_23);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_24;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_24 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_24_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_24_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_24_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_24);
*Node_22_retrieved_reference_45_24_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_24_typed_id,Node_22_ref_node_target_id_45_24);
*Node_22_retrieved_reference_45_false_24_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_24_is_inverse, Node_22_ref_node_target_id_45_24);
*Node_22_retrieved_reference_45_24_target_id = 15629;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_24_target_id, Node_22_ref_node_target_id_45_24);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_24->references,"25",Node_22_ref_node_target_id_45_24);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_25;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_25 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_25_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_25_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_25_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_25);
*Node_22_retrieved_reference_45_25_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_25_typed_id,Node_22_ref_node_target_id_45_25);
*Node_22_retrieved_reference_45_false_25_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_25_is_inverse, Node_22_ref_node_target_id_45_25);
*Node_22_retrieved_reference_45_25_target_id = 15630;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_25_target_id, Node_22_ref_node_target_id_45_25);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_25->references,"26",Node_22_ref_node_target_id_45_25);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_26;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_26 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_26_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_26_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_26_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_26);
*Node_22_retrieved_reference_45_26_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_26_typed_id,Node_22_ref_node_target_id_45_26);
*Node_22_retrieved_reference_45_false_26_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_26_is_inverse, Node_22_ref_node_target_id_45_26);
*Node_22_retrieved_reference_45_26_target_id = 14744;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_26_target_id, Node_22_ref_node_target_id_45_26);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_26->references,"27",Node_22_ref_node_target_id_45_26);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_27;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_27 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_27_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_27_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_27_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_27);
*Node_22_retrieved_reference_45_27_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_27_typed_id,Node_22_ref_node_target_id_45_27);
*Node_22_retrieved_reference_45_false_27_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_27_is_inverse, Node_22_ref_node_target_id_45_27);
*Node_22_retrieved_reference_45_27_target_id = 15530;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_27_target_id, Node_22_ref_node_target_id_45_27);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_27->references,"28",Node_22_ref_node_target_id_45_27);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_28;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_28 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_28_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_28_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_28_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_28);
*Node_22_retrieved_reference_45_28_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_28_typed_id,Node_22_ref_node_target_id_45_28);
*Node_22_retrieved_reference_45_false_28_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_28_is_inverse, Node_22_ref_node_target_id_45_28);
*Node_22_retrieved_reference_45_28_target_id = 96;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_28_target_id, Node_22_ref_node_target_id_45_28);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_28->references,"29",Node_22_ref_node_target_id_45_28);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_29;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_29 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_29_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_29_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_29_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_29);
*Node_22_retrieved_reference_45_29_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_29_typed_id,Node_22_ref_node_target_id_45_29);
*Node_22_retrieved_reference_45_false_29_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_29_is_inverse, Node_22_ref_node_target_id_45_29);
*Node_22_retrieved_reference_45_29_target_id = 97;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_29_target_id, Node_22_ref_node_target_id_45_29);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_29->references,"30",Node_22_ref_node_target_id_45_29);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_30;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_30 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_30_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_30_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_30_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_30);
*Node_22_retrieved_reference_45_30_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_30_typed_id,Node_22_ref_node_target_id_45_30);
*Node_22_retrieved_reference_45_false_30_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_30_is_inverse, Node_22_ref_node_target_id_45_30);
*Node_22_retrieved_reference_45_30_target_id = 101;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_30_target_id, Node_22_ref_node_target_id_45_30);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_30->references,"31",Node_22_ref_node_target_id_45_30);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_31;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_31 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_31_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_31_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_31_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_31);
*Node_22_retrieved_reference_45_31_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_31_typed_id,Node_22_ref_node_target_id_45_31);
*Node_22_retrieved_reference_45_false_31_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_31_is_inverse, Node_22_ref_node_target_id_45_31);
*Node_22_retrieved_reference_45_31_target_id = 296;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_31_target_id, Node_22_ref_node_target_id_45_31);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_31->references,"32",Node_22_ref_node_target_id_45_31);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_32;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_32 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_32_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_32_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_32_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_32);
*Node_22_retrieved_reference_45_32_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_32_typed_id,Node_22_ref_node_target_id_45_32);
*Node_22_retrieved_reference_45_false_32_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_32_is_inverse, Node_22_ref_node_target_id_45_32);
*Node_22_retrieved_reference_45_32_target_id = 7594;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_32_target_id, Node_22_ref_node_target_id_45_32);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_32->references,"33",Node_22_ref_node_target_id_45_32);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_33;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_33 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_33_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_33_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_33_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_33);
*Node_22_retrieved_reference_45_33_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_33_typed_id,Node_22_ref_node_target_id_45_33);
*Node_22_retrieved_reference_45_false_33_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_33_is_inverse, Node_22_ref_node_target_id_45_33);
*Node_22_retrieved_reference_45_33_target_id = 12755;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_33_target_id, Node_22_ref_node_target_id_45_33);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_33->references,"34",Node_22_ref_node_target_id_45_33);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_34;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_34 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_34_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_34_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_34_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_34);
*Node_22_retrieved_reference_45_34_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_34_typed_id,Node_22_ref_node_target_id_45_34);
*Node_22_retrieved_reference_45_false_34_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_34_is_inverse, Node_22_ref_node_target_id_45_34);
*Node_22_retrieved_reference_45_34_target_id = 12756;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_34_target_id, Node_22_ref_node_target_id_45_34);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_34->references,"35",Node_22_ref_node_target_id_45_34);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_35;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_35 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_35_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_35_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_35_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_35);
*Node_22_retrieved_reference_45_35_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_35_typed_id,Node_22_ref_node_target_id_45_35);
*Node_22_retrieved_reference_45_false_35_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_35_is_inverse, Node_22_ref_node_target_id_45_35);
*Node_22_retrieved_reference_45_35_target_id = 8912;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_35_target_id, Node_22_ref_node_target_id_45_35);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_35->references,"36",Node_22_ref_node_target_id_45_35);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_36;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_36 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_36_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_36_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_36_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_36);
*Node_22_retrieved_reference_45_36_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_36_typed_id,Node_22_ref_node_target_id_45_36);
*Node_22_retrieved_reference_45_false_36_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_36_is_inverse, Node_22_ref_node_target_id_45_36);
*Node_22_retrieved_reference_45_36_target_id = 308;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_36_target_id, Node_22_ref_node_target_id_45_36);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_36->references,"37",Node_22_ref_node_target_id_45_36);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_37;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_37 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_37_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_37_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_37_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_37);
*Node_22_retrieved_reference_45_37_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_37_typed_id,Node_22_ref_node_target_id_45_37);
*Node_22_retrieved_reference_45_false_37_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_37_is_inverse, Node_22_ref_node_target_id_45_37);
*Node_22_retrieved_reference_45_37_target_id = 12189;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_37_target_id, Node_22_ref_node_target_id_45_37);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_37->references,"38",Node_22_ref_node_target_id_45_37);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_38;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_38 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_38_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_38_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_38_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_38);
*Node_22_retrieved_reference_45_38_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_38_typed_id,Node_22_ref_node_target_id_45_38);
*Node_22_retrieved_reference_45_false_38_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_38_is_inverse, Node_22_ref_node_target_id_45_38);
*Node_22_retrieved_reference_45_38_target_id = 304;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_38_target_id, Node_22_ref_node_target_id_45_38);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_38->references,"39",Node_22_ref_node_target_id_45_38);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_39;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_39 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_39_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_39_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_39_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_39);
*Node_22_retrieved_reference_45_39_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_39_typed_id,Node_22_ref_node_target_id_45_39);
*Node_22_retrieved_reference_45_false_39_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_39_is_inverse, Node_22_ref_node_target_id_45_39);
*Node_22_retrieved_reference_45_39_target_id = 312;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_39_target_id, Node_22_ref_node_target_id_45_39);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_39->references,"40",Node_22_ref_node_target_id_45_39);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_40;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_40 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_40_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_40_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_40_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_40);
*Node_22_retrieved_reference_45_40_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_40_typed_id,Node_22_ref_node_target_id_45_40);
*Node_22_retrieved_reference_45_false_40_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_40_is_inverse, Node_22_ref_node_target_id_45_40);
*Node_22_retrieved_reference_45_40_target_id = 432;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_40_target_id, Node_22_ref_node_target_id_45_40);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_40->references,"41",Node_22_ref_node_target_id_45_40);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_41;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_41 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_41_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_41_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_41_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_41);
*Node_22_retrieved_reference_45_41_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_41_typed_id,Node_22_ref_node_target_id_45_41);
*Node_22_retrieved_reference_45_false_41_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_41_is_inverse, Node_22_ref_node_target_id_45_41);
*Node_22_retrieved_reference_45_41_target_id = 12890;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_41_target_id, Node_22_ref_node_target_id_45_41);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_41->references,"42",Node_22_ref_node_target_id_45_41);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_42;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_42 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_42_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_42_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_42_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_42);
*Node_22_retrieved_reference_45_42_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_42_typed_id,Node_22_ref_node_target_id_45_42);
*Node_22_retrieved_reference_45_false_42_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_42_is_inverse, Node_22_ref_node_target_id_45_42);
*Node_22_retrieved_reference_45_42_target_id = 344;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_42_target_id, Node_22_ref_node_target_id_45_42);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_42->references,"43",Node_22_ref_node_target_id_45_42);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_43;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_43 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_43_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_43_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_43_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_43);
*Node_22_retrieved_reference_45_43_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_43_typed_id,Node_22_ref_node_target_id_45_43);
*Node_22_retrieved_reference_45_false_43_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_43_is_inverse, Node_22_ref_node_target_id_45_43);
*Node_22_retrieved_reference_45_43_target_id = 316;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_43_target_id, Node_22_ref_node_target_id_45_43);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_43->references,"44",Node_22_ref_node_target_id_45_43);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_44;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_44 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_44_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_44_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_44_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_44);
*Node_22_retrieved_reference_45_44_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_44_typed_id,Node_22_ref_node_target_id_45_44);
*Node_22_retrieved_reference_45_false_44_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_44_is_inverse, Node_22_ref_node_target_id_45_44);
*Node_22_retrieved_reference_45_44_target_id = 376;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_44_target_id, Node_22_ref_node_target_id_45_44);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_44->references,"45",Node_22_ref_node_target_id_45_44);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_45;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_45 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_45_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_45_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_45_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_45);
*Node_22_retrieved_reference_45_45_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_45_typed_id,Node_22_ref_node_target_id_45_45);
*Node_22_retrieved_reference_45_false_45_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_45_is_inverse, Node_22_ref_node_target_id_45_45);
*Node_22_retrieved_reference_45_45_target_id = 379;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_45_target_id, Node_22_ref_node_target_id_45_45);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_45->references,"46",Node_22_ref_node_target_id_45_45);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_46;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_46 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_46_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_46_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_46_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_46);
*Node_22_retrieved_reference_45_46_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_46_typed_id,Node_22_ref_node_target_id_45_46);
*Node_22_retrieved_reference_45_false_46_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_46_is_inverse, Node_22_ref_node_target_id_45_46);
*Node_22_retrieved_reference_45_46_target_id = 382;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_46_target_id, Node_22_ref_node_target_id_45_46);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_46->references,"47",Node_22_ref_node_target_id_45_46);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_47;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_47 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_47_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_47_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_47_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_47);
*Node_22_retrieved_reference_45_47_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_47_typed_id,Node_22_ref_node_target_id_45_47);
*Node_22_retrieved_reference_45_false_47_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_47_is_inverse, Node_22_ref_node_target_id_45_47);
*Node_22_retrieved_reference_45_47_target_id = 385;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_47_target_id, Node_22_ref_node_target_id_45_47);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_47->references,"48",Node_22_ref_node_target_id_45_47);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_48;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_48 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_48_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_48_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_48_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_48);
*Node_22_retrieved_reference_45_48_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_48_typed_id,Node_22_ref_node_target_id_45_48);
*Node_22_retrieved_reference_45_false_48_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_48_is_inverse, Node_22_ref_node_target_id_45_48);
*Node_22_retrieved_reference_45_48_target_id = 537;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_48_target_id, Node_22_ref_node_target_id_45_48);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_48->references,"49",Node_22_ref_node_target_id_45_48);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_49;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_49 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_49_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_49_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_49_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_49);
*Node_22_retrieved_reference_45_49_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_49_typed_id,Node_22_ref_node_target_id_45_49);
*Node_22_retrieved_reference_45_false_49_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_49_is_inverse, Node_22_ref_node_target_id_45_49);
*Node_22_retrieved_reference_45_49_target_id = 540;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_49_target_id, Node_22_ref_node_target_id_45_49);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_49->references,"50",Node_22_ref_node_target_id_45_49);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_50;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_50 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_50_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_50_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_50_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_50);
*Node_22_retrieved_reference_45_50_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_50_typed_id,Node_22_ref_node_target_id_45_50);
*Node_22_retrieved_reference_45_false_50_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_50_is_inverse, Node_22_ref_node_target_id_45_50);
*Node_22_retrieved_reference_45_50_target_id = 331;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_50_target_id, Node_22_ref_node_target_id_45_50);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_50->references,"51",Node_22_ref_node_target_id_45_50);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_51;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_51 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_51_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_51_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_51_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_51);
*Node_22_retrieved_reference_45_51_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_51_typed_id,Node_22_ref_node_target_id_45_51);
*Node_22_retrieved_reference_45_false_51_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_51_is_inverse, Node_22_ref_node_target_id_45_51);
*Node_22_retrieved_reference_45_51_target_id = 583;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_51_target_id, Node_22_ref_node_target_id_45_51);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_51->references,"52",Node_22_ref_node_target_id_45_51);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_52;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_52 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_52_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_52_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_52_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_52);
*Node_22_retrieved_reference_45_52_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_52_typed_id,Node_22_ref_node_target_id_45_52);
*Node_22_retrieved_reference_45_false_52_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_52_is_inverse, Node_22_ref_node_target_id_45_52);
*Node_22_retrieved_reference_45_52_target_id = 586;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_52_target_id, Node_22_ref_node_target_id_45_52);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_52->references,"53",Node_22_ref_node_target_id_45_52);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_53;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_53 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_53_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_53_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_53_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_53);
*Node_22_retrieved_reference_45_53_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_53_typed_id,Node_22_ref_node_target_id_45_53);
*Node_22_retrieved_reference_45_false_53_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_53_is_inverse, Node_22_ref_node_target_id_45_53);
*Node_22_retrieved_reference_45_53_target_id = 589;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_53_target_id, Node_22_ref_node_target_id_45_53);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_53->references,"54",Node_22_ref_node_target_id_45_53);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_54;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_54 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_54_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_54_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_54_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_54);
*Node_22_retrieved_reference_45_54_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_54_typed_id,Node_22_ref_node_target_id_45_54);
*Node_22_retrieved_reference_45_false_54_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_54_is_inverse, Node_22_ref_node_target_id_45_54);
*Node_22_retrieved_reference_45_54_target_id = 659;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_54_target_id, Node_22_ref_node_target_id_45_54);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_54->references,"55",Node_22_ref_node_target_id_45_54);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_55;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_55 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_55_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_55_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_55_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_55);
*Node_22_retrieved_reference_45_55_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_55_typed_id,Node_22_ref_node_target_id_45_55);
*Node_22_retrieved_reference_45_false_55_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_55_is_inverse, Node_22_ref_node_target_id_45_55);
*Node_22_retrieved_reference_45_55_target_id = 719;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_55_target_id, Node_22_ref_node_target_id_45_55);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_55->references,"56",Node_22_ref_node_target_id_45_55);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_56;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_56 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_56_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_56_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_56_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_56);
*Node_22_retrieved_reference_45_56_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_56_typed_id,Node_22_ref_node_target_id_45_56);
*Node_22_retrieved_reference_45_false_56_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_56_is_inverse, Node_22_ref_node_target_id_45_56);
*Node_22_retrieved_reference_45_56_target_id = 948;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_56_target_id, Node_22_ref_node_target_id_45_56);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_56->references,"57",Node_22_ref_node_target_id_45_56);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_57;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_57 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_57_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_57_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_57_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_57);
*Node_22_retrieved_reference_45_57_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_57_typed_id,Node_22_ref_node_target_id_45_57);
*Node_22_retrieved_reference_45_false_57_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_57_is_inverse, Node_22_ref_node_target_id_45_57);
*Node_22_retrieved_reference_45_57_target_id = 920;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_57_target_id, Node_22_ref_node_target_id_45_57);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_57->references,"58",Node_22_ref_node_target_id_45_57);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_58;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_58 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_58_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_58_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_58_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_58);
*Node_22_retrieved_reference_45_58_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_58_typed_id,Node_22_ref_node_target_id_45_58);
*Node_22_retrieved_reference_45_false_58_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_58_is_inverse, Node_22_ref_node_target_id_45_58);
*Node_22_retrieved_reference_45_58_target_id = 338;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_58_target_id, Node_22_ref_node_target_id_45_58);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_58->references,"59",Node_22_ref_node_target_id_45_58);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_59;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_59 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_59_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_59_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_59_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_59);
*Node_22_retrieved_reference_45_59_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_59_typed_id,Node_22_ref_node_target_id_45_59);
*Node_22_retrieved_reference_45_false_59_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_59_is_inverse, Node_22_ref_node_target_id_45_59);
*Node_22_retrieved_reference_45_59_target_id = 853;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_59_target_id, Node_22_ref_node_target_id_45_59);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_59->references,"60",Node_22_ref_node_target_id_45_59);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_60;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_60 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_60_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_60_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_60_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_60);
*Node_22_retrieved_reference_45_60_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_60_typed_id,Node_22_ref_node_target_id_45_60);
*Node_22_retrieved_reference_45_false_60_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_60_is_inverse, Node_22_ref_node_target_id_45_60);
*Node_22_retrieved_reference_45_60_target_id = 11943;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_60_target_id, Node_22_ref_node_target_id_45_60);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_60->references,"61",Node_22_ref_node_target_id_45_60);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_61;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_61 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_61_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_61_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_61_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_61);
*Node_22_retrieved_reference_45_61_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_61_typed_id,Node_22_ref_node_target_id_45_61);
*Node_22_retrieved_reference_45_false_61_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_61_is_inverse, Node_22_ref_node_target_id_45_61);
*Node_22_retrieved_reference_45_61_target_id = 11944;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_61_target_id, Node_22_ref_node_target_id_45_61);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_61->references,"62",Node_22_ref_node_target_id_45_61);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_62;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_62 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_62_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_62_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_62_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_62);
*Node_22_retrieved_reference_45_62_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_62_typed_id,Node_22_ref_node_target_id_45_62);
*Node_22_retrieved_reference_45_false_62_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_62_is_inverse, Node_22_ref_node_target_id_45_62);
*Node_22_retrieved_reference_45_62_target_id = 856;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_62_target_id, Node_22_ref_node_target_id_45_62);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_62->references,"63",Node_22_ref_node_target_id_45_62);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_63;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_63 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_63_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_63_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_63_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_63);
*Node_22_retrieved_reference_45_63_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_63_typed_id,Node_22_ref_node_target_id_45_63);
*Node_22_retrieved_reference_45_false_63_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_63_is_inverse, Node_22_ref_node_target_id_45_63);
*Node_22_retrieved_reference_45_63_target_id = 859;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_63_target_id, Node_22_ref_node_target_id_45_63);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_63->references,"64",Node_22_ref_node_target_id_45_63);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_64;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_64 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_64_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_64_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_64_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_64);
*Node_22_retrieved_reference_45_64_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_64_typed_id,Node_22_ref_node_target_id_45_64);
*Node_22_retrieved_reference_45_false_64_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_64_is_inverse, Node_22_ref_node_target_id_45_64);
*Node_22_retrieved_reference_45_64_target_id = 862;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_64_target_id, Node_22_ref_node_target_id_45_64);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_64->references,"65",Node_22_ref_node_target_id_45_64);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_65;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_65 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_65_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_65_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_65_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_65);
*Node_22_retrieved_reference_45_65_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_65_typed_id,Node_22_ref_node_target_id_45_65);
*Node_22_retrieved_reference_45_false_65_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_65_is_inverse, Node_22_ref_node_target_id_45_65);
*Node_22_retrieved_reference_45_65_target_id = 865;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_65_target_id, Node_22_ref_node_target_id_45_65);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_65->references,"66",Node_22_ref_node_target_id_45_65);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_66;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_66 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_66_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_66_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_66_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_66);
*Node_22_retrieved_reference_45_66_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_66_typed_id,Node_22_ref_node_target_id_45_66);
*Node_22_retrieved_reference_45_false_66_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_66_is_inverse, Node_22_ref_node_target_id_45_66);
*Node_22_retrieved_reference_45_66_target_id = 868;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_66_target_id, Node_22_ref_node_target_id_45_66);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_66->references,"67",Node_22_ref_node_target_id_45_66);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_67;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_67 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_67_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_67_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_67_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_67);
*Node_22_retrieved_reference_45_67_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_67_typed_id,Node_22_ref_node_target_id_45_67);
*Node_22_retrieved_reference_45_false_67_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_67_is_inverse, Node_22_ref_node_target_id_45_67);
*Node_22_retrieved_reference_45_67_target_id = 871;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_67_target_id, Node_22_ref_node_target_id_45_67);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_67->references,"68",Node_22_ref_node_target_id_45_67);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_68;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_68 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_68_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_68_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_68_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_68);
*Node_22_retrieved_reference_45_68_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_68_typed_id,Node_22_ref_node_target_id_45_68);
*Node_22_retrieved_reference_45_false_68_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_68_is_inverse, Node_22_ref_node_target_id_45_68);
*Node_22_retrieved_reference_45_68_target_id = 299;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_68_target_id, Node_22_ref_node_target_id_45_68);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_68->references,"69",Node_22_ref_node_target_id_45_68);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_69;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_69 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_69_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_69_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_69_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_69);
*Node_22_retrieved_reference_45_69_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_69_typed_id,Node_22_ref_node_target_id_45_69);
*Node_22_retrieved_reference_45_false_69_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_69_is_inverse, Node_22_ref_node_target_id_45_69);
*Node_22_retrieved_reference_45_69_target_id = 874;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_69_target_id, Node_22_ref_node_target_id_45_69);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_69->references,"70",Node_22_ref_node_target_id_45_69);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_70;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_70 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_70_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_70_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_70_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_70);
*Node_22_retrieved_reference_45_70_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_70_typed_id,Node_22_ref_node_target_id_45_70);
*Node_22_retrieved_reference_45_false_70_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_70_is_inverse, Node_22_ref_node_target_id_45_70);
*Node_22_retrieved_reference_45_70_target_id = 877;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_70_target_id, Node_22_ref_node_target_id_45_70);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_70->references,"71",Node_22_ref_node_target_id_45_70);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_71;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_71 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_71_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_71_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_71_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_71);
*Node_22_retrieved_reference_45_71_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_71_typed_id,Node_22_ref_node_target_id_45_71);
*Node_22_retrieved_reference_45_false_71_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_71_is_inverse, Node_22_ref_node_target_id_45_71);
*Node_22_retrieved_reference_45_71_target_id = 897;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_71_target_id, Node_22_ref_node_target_id_45_71);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_71->references,"72",Node_22_ref_node_target_id_45_71);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_72;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_72 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_72_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_72_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_72_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_72);
*Node_22_retrieved_reference_45_72_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_72_typed_id,Node_22_ref_node_target_id_45_72);
*Node_22_retrieved_reference_45_false_72_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_72_is_inverse, Node_22_ref_node_target_id_45_72);
*Node_22_retrieved_reference_45_72_target_id = 884;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_72_target_id, Node_22_ref_node_target_id_45_72);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_72->references,"73",Node_22_ref_node_target_id_45_72);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_73;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_73 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_73_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_73_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_73_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_73);
*Node_22_retrieved_reference_45_73_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_73_typed_id,Node_22_ref_node_target_id_45_73);
*Node_22_retrieved_reference_45_false_73_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_73_is_inverse, Node_22_ref_node_target_id_45_73);
*Node_22_retrieved_reference_45_73_target_id = 887;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_73_target_id, Node_22_ref_node_target_id_45_73);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_73->references,"74",Node_22_ref_node_target_id_45_73);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_74;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_74 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_74_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_74_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_74_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_74);
*Node_22_retrieved_reference_45_74_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_74_typed_id,Node_22_ref_node_target_id_45_74);
*Node_22_retrieved_reference_45_false_74_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_74_is_inverse, Node_22_ref_node_target_id_45_74);
*Node_22_retrieved_reference_45_74_target_id = 12171;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_74_target_id, Node_22_ref_node_target_id_45_74);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_74->references,"75",Node_22_ref_node_target_id_45_74);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_75;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_75 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_75_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_75_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_75_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_75);
*Node_22_retrieved_reference_45_75_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_75_typed_id,Node_22_ref_node_target_id_45_75);
*Node_22_retrieved_reference_45_false_75_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_75_is_inverse, Node_22_ref_node_target_id_45_75);
*Node_22_retrieved_reference_45_75_target_id = 12172;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_75_target_id, Node_22_ref_node_target_id_45_75);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_75->references,"76",Node_22_ref_node_target_id_45_75);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_76;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_76 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_76_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_76_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_76_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_76);
*Node_22_retrieved_reference_45_76_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_76_typed_id,Node_22_ref_node_target_id_45_76);
*Node_22_retrieved_reference_45_false_76_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_76_is_inverse, Node_22_ref_node_target_id_45_76);
*Node_22_retrieved_reference_45_76_target_id = 12079;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_76_target_id, Node_22_ref_node_target_id_45_76);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_76->references,"77",Node_22_ref_node_target_id_45_76);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_77;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_77 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_77_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_77_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_77_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_77);
*Node_22_retrieved_reference_45_77_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_77_typed_id,Node_22_ref_node_target_id_45_77);
*Node_22_retrieved_reference_45_false_77_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_77_is_inverse, Node_22_ref_node_target_id_45_77);
*Node_22_retrieved_reference_45_77_target_id = 12080;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_77_target_id, Node_22_ref_node_target_id_45_77);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_77->references,"78",Node_22_ref_node_target_id_45_77);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_78;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_78 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_78_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_78_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_78_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_78);
*Node_22_retrieved_reference_45_78_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_78_typed_id,Node_22_ref_node_target_id_45_78);
*Node_22_retrieved_reference_45_false_78_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_78_is_inverse, Node_22_ref_node_target_id_45_78);
*Node_22_retrieved_reference_45_78_target_id = 894;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_78_target_id, Node_22_ref_node_target_id_45_78);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_78->references,"79",Node_22_ref_node_target_id_45_78);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_79;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_79 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_79_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_79_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_79_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_79);
*Node_22_retrieved_reference_45_79_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_79_typed_id,Node_22_ref_node_target_id_45_79);
*Node_22_retrieved_reference_45_false_79_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_79_is_inverse, Node_22_ref_node_target_id_45_79);
*Node_22_retrieved_reference_45_79_target_id = 15396;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_79_target_id, Node_22_ref_node_target_id_45_79);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_79->references,"80",Node_22_ref_node_target_id_45_79);

}if(references_flag != 2){

ishidaopcua_NODE* Node_22_retrieved_reference_45_80;
ishidaopcua_NODE* Node_22_ref_node_target_id_45_80 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_22_retrieved_reference_45_80_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_false_80_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_22_retrieved_reference_45_80_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_22_retrieved_reference_45_80);
*Node_22_retrieved_reference_45_80_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_22_retrieved_reference_45_80_typed_id,Node_22_ref_node_target_id_45_80);
*Node_22_retrieved_reference_45_false_80_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_22_retrieved_reference_45_false_80_is_inverse, Node_22_ref_node_target_id_45_80);
*Node_22_retrieved_reference_45_80_target_id = 891;
ishidaopcua_node_set_target_id(Node_22_retrieved_reference_45_80_target_id, Node_22_ref_node_target_id_45_80);
ishidaeutz_put_hashmap(Node_22_retrieved_reference_45_80->references,"81",Node_22_ref_node_target_id_45_80);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* LocaleId ********/


case 295 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 295;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("LocaleId", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("LocaleId", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("An identifier for a user locale.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 295 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_295_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_295_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_295_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_295_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_295_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_295_retrieved_reference_45_inverse_0);
*Node_295_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_295_retrieved_reference_45_inverse_0_typed_id,Node_295_ref_node_target_id_45_inverse_0);
*Node_295_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_295_retrieved_reference_45_inverse_true_0_is_inverse, Node_295_ref_node_target_id_45_inverse_0);
*Node_295_retrieved_reference_45_inverse_0_target_id = 12;
ishidaopcua_node_set_target_id(Node_295_retrieved_reference_45_inverse_0_target_id, Node_295_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_295_retrieved_reference_45_inverse_0->references,"1",Node_295_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* Argument ********/


case 296 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 296;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("Argument", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("Argument", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("An argument for a method.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 296 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_296_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_296_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_296_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_296_retrieved_reference_45_inverse_0);
*Node_296_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_296_retrieved_reference_45_inverse_0_typed_id,Node_296_ref_node_target_id_45_inverse_0);
*Node_296_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_296_retrieved_reference_45_inverse_true_0_is_inverse, Node_296_ref_node_target_id_45_inverse_0);
*Node_296_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_296_retrieved_reference_45_inverse_0_target_id, Node_296_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_296_retrieved_reference_45_inverse_0->references,"1",Node_296_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_296_retrieved_reference_38_0;
ishidaopcua_NODE* Node_296_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_296_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_296_retrieved_reference_38_0);
*Node_296_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_296_retrieved_reference_38_0_typed_id,Node_296_ref_node_target_id_38_0);
*Node_296_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_296_retrieved_reference_38_false_0_is_inverse, Node_296_ref_node_target_id_38_0);
*Node_296_retrieved_reference_38_0_target_id = 298;
ishidaopcua_node_set_target_id(Node_296_retrieved_reference_38_0_target_id, Node_296_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_296_retrieved_reference_38_0->references,"1",Node_296_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_296_retrieved_reference_38_1;
ishidaopcua_NODE* Node_296_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_296_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_296_retrieved_reference_38_1);
*Node_296_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_296_retrieved_reference_38_1_typed_id,Node_296_ref_node_target_id_38_1);
*Node_296_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_296_retrieved_reference_38_false_1_is_inverse, Node_296_ref_node_target_id_38_1);
*Node_296_retrieved_reference_38_1_target_id = 297;
ishidaopcua_node_set_target_id(Node_296_retrieved_reference_38_1_target_id, Node_296_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_296_retrieved_reference_38_1->references,"2",Node_296_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_296_retrieved_reference_38_2;
ishidaopcua_NODE* Node_296_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_296_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_296_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_296_retrieved_reference_38_2);
*Node_296_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_296_retrieved_reference_38_2_typed_id,Node_296_ref_node_target_id_38_2);
*Node_296_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_296_retrieved_reference_38_false_2_is_inverse, Node_296_ref_node_target_id_38_2);
*Node_296_retrieved_reference_38_2_target_id = 15081;
ishidaopcua_node_set_target_id(Node_296_retrieved_reference_38_2_target_id, Node_296_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_296_retrieved_reference_38_2->references,"3",Node_296_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BuildInfo ********/


case 338 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 338;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BuildInfo", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BuildInfo", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 338 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_338_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_338_retrieved_reference_45_inverse_0);
*Node_338_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_45_inverse_0_typed_id,Node_338_ref_node_target_id_45_inverse_0);
*Node_338_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_45_inverse_true_0_is_inverse, Node_338_ref_node_target_id_45_inverse_0);
*Node_338_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_45_inverse_0_target_id, Node_338_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_45_inverse_0->references,"1",Node_338_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_38_0;
ishidaopcua_NODE* Node_338_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_338_retrieved_reference_38_0);
*Node_338_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_38_0_typed_id,Node_338_ref_node_target_id_38_0);
*Node_338_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_38_false_0_is_inverse, Node_338_ref_node_target_id_38_0);
*Node_338_retrieved_reference_38_0_target_id = 340;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_38_0_target_id, Node_338_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_38_0->references,"1",Node_338_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_38_1;
ishidaopcua_NODE* Node_338_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_338_retrieved_reference_38_1);
*Node_338_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_38_1_typed_id,Node_338_ref_node_target_id_38_1);
*Node_338_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_38_false_1_is_inverse, Node_338_ref_node_target_id_38_1);
*Node_338_retrieved_reference_38_1_target_id = 339;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_38_1_target_id, Node_338_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_38_1->references,"2",Node_338_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_38_2;
ishidaopcua_NODE* Node_338_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_338_retrieved_reference_38_2);
*Node_338_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_38_2_typed_id,Node_338_ref_node_target_id_38_2);
*Node_338_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_38_false_2_is_inverse, Node_338_ref_node_target_id_38_2);
*Node_338_retrieved_reference_38_2_target_id = 15361;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_38_2_target_id, Node_338_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_38_2->references,"3",Node_338_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/