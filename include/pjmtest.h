#ifndef PJMTEST_H_
#define PJMTEST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include <stdarg.h>
#include <math.h>


/************** start DMU INCLUDES *****************
#include "dbg.h"
#include <local.h>
#include <socket.h>

/************** end DMU INCLUDES *****************/

/************** start ubuntu INCLUDES *****************/

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
/*
#include <valgrind/valgrind.h>
#include <valgrind/memcheck.h>
*/

/************** end ubuntu INCLUDES *****************/

/*************************************** ISHIDAEUTZ_MEMORY_MANAGER START ***************************************/
extern char predef_mem_ephemeral[600000];
extern unsigned int predef_mem_ephemeral_count;
extern unsigned int predef_mem_ephemeral_total_count;

extern char predef_mem_chronic[300000];
extern unsigned int predef_mem_chronic_count;
extern unsigned int predef_mem_chronic_total_count;

typedef struct predef_mem_index_element{
	void* ptr_index;
	unsigned int size;
	unsigned char occupied;
} predef_mem_index_element;

extern predef_mem_index_element predef_mem_ephemeral_index_element_array[6000];
extern unsigned int predef_mem_ephemeral_index_element_array_count;
extern unsigned int predef_mem_ephemeral_index_element_array_total_count;

extern predef_mem_index_element predef_mem_chronic_index_element_array[1500];
extern unsigned int predef_mem_chronic_index_element_array_count;
extern unsigned int predef_mem_chronic_index_element_array_total_count;

void initiate_memblock(unsigned int iclean);

void reset_memblock();

void* memblock_allocate_smartly(unsigned int isize, unsigned int iclean_on_reset, unsigned int idebug_reset);

unsigned int get_memblock_ephemeral_size(unsigned int imem_index);

void memblock_free(void* iptr);
/*************************************** ISHIDAEUTZ_MEMORY_MANAGER END ***************************************/

/*************************************** ISHIDAEUTZ_LARGE_NUMBER START ***************************************/
typedef char ishidaeutz_LARGE_INT_ENDIANNESS;
#define ishidaeutz_LARGE_INT_ENDIANNESS_BIG_ENDIAN 1
#define ishidaeutz_LARGE_INT_ENDIANNESS_LITTLE_ENDIAN -1

extern ishidaeutz_LARGE_INT_ENDIANNESS ishidaopcua_SERVER_LARGE_INT_ENDIANNESS;

typedef char ishidaeutz_LARGE_INT_SIGN;
#define ishidaeutz_LARGE_INT_SIGN_POSITIVE 1
#define ishidaeutz_LARGE_INT_SIGN_NEGATIVE -1

#define ishidaeutz_LARGE_INT_DEFAULT_MAX_BYTE_ARRAY_SIZE 8

typedef struct ishidaeutz_LARGE_INT
{
	unsigned char* number_bytestring;
	unsigned char* int_bytearray;
	unsigned char* double_bytearray;
	unsigned int double_bytearray_size;
	unsigned int number_bytestring_size;
	unsigned int int_bytearray_size;
	ishidaeutz_LARGE_INT_SIGN number_sign;
} ishidaeutz_LARGE_INT;

/* void Message(0,const char* text, ...); */
void ishidaeu_exit(int ierror_code);
unsigned char convert_base10_single_digit_unsigned_string_char_to_unsigned_byte_char(const char* ipstring_char);
const char* convert_base10_single_digit_unsigned_byte_char_to_unsigned_string_char(unsigned char ibyte_char);
ishidaeutz_LARGE_INT* create_ishidaeutz_LARGE_INT_from_int(int inumber_int);
ishidaeutz_LARGE_INT* create_ishidaeutz_LARGE_INT_from_unsigned_int(unsigned int inumber_int, ishidaeutz_LARGE_INT_SIGN isign);
void stretch_ishidaeutz_LARGE_INT(ishidaeutz_LARGE_INT* iplarge_int, unsigned char istretch_scale);
ishidaeutz_LARGE_INT* create_ishidaeutz_LARGE_INT_from_ASCII_STRING(const char* ipnumber_string, ishidaeutz_LARGE_INT_SIGN inumber_sign);
const char* get_ishidaeutz_LARGE_INT_as_ASCII_string_array(ishidaeutz_LARGE_INT* iplarge_int);
unsigned int get_ishidaeutz_LARGE_INT_size(ishidaeutz_LARGE_INT* iplarge_int);
unsigned int get_ishidaeutz_LARGE_INT_yielded_int_byte_array_size(ishidaeutz_LARGE_INT* iplarge_int);
ishidaeutz_LARGE_INT_SIGN get_ishidaeutz_LARGE_INT_sign(ishidaeutz_LARGE_INT* iplarge_int);
char compare_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2, char icompare_absolute);
ishidaeutz_LARGE_INT* sum_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2);
void divide_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2, ishidaeutz_LARGE_INT** ippquotient, ishidaeutz_LARGE_INT** ippreminder);
ishidaeutz_LARGE_INT* multiply_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1, ishidaeutz_LARGE_INT* iplarge_int_2);
void yield_ishidaeutz_LARGE_INT_int_byte_array(ishidaeutz_LARGE_INT* iplarge_int, ishidaeutz_LARGE_INT_ENDIANNESS iexpected_endianness, unsigned char expected_byte_array_size);
int ishidaeutz_unsigned_int_to_ASCII_HEX_string_array(unsigned int num,char* buff);
const char* get_ishidaeutz_LARGE_INT_as_ASCII_HEX_string_array(ishidaeutz_LARGE_INT* iplarge_int);
void yield_ishidaeutz_LARGE_INT_IEEE754_double_precision_byte_array(ishidaeutz_LARGE_INT* iplarge_int);
const char* get_ishidaeutz_LARGE_INT_as_ASCII_IEE754_double_precision_HEX_string_array(ishidaeutz_LARGE_INT* iplarge_int);
void extract_ishidaeutz_LARGE_INT_from_unsigned_int_byte_array(ishidaeutz_LARGE_INT* iplarge_int, const unsigned char* ipbyte_array, unsigned int ioffset, unsigned char ibytearray_size);
ishidaeutz_LARGE_INT* surface_copy_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1);
ishidaeutz_LARGE_INT* deep_copy_ishidaeutz_LARGE_INTs(ishidaeutz_LARGE_INT* iplarge_int_1);
void extract_ishidaeutz_LARGE_INT_from_IEEE754_dp_byte_array(ishidaeutz_LARGE_INT* iplarge_int, const unsigned char* ipIEEE754_dp_byte_array, unsigned int ioffset);
/*************************************** ISHIDAEUTZ_LARGE_NUMBER END ***************************************/

/*************************************** ISHIDAEU_UTILS START ***************************************/
time_t ishidaeutz_get_current_unix_timestamp_in_seconds();

unsigned int ishidaeutz_get_current_unix_timestamp_in_seconds_uint32();

ishidaeutz_LARGE_INT* ishidaeutz_get_current_unix_timestamp_in_milliseconds_uint64();

void ishidauetz_convert_uint16_to_byte_array_le(unsigned short num, unsigned char* ibyte_array,int ioffset);

void ishidaopcua_convert_uint16_to_byte_array_be(unsigned short num, unsigned char* ibyte_array,int ioffset);

void ishidaeutz_convert_int16_to_byte_array_be(short num, unsigned char* ibyte_array,int ioffset);

short ishidaeutz_convert_byte_array_to_int16_le(unsigned int ioffset,unsigned char* ibyte_array);

short ishidaeutz_convert_byte_array_to_int16_be(unsigned int ioffset,unsigned char* ibyte_array);

unsigned short ishidaeutz_convert_byte_array_to_uint16_le(unsigned int ioffset,unsigned char* ibyte_array);

unsigned short ishidaeutz_convert_byte_array_to_uint16_be(unsigned int ioffset,unsigned char* ibyte_array);

void ishidaeutz_convert_uint32_to_byte_array_be(unsigned int inum, unsigned char* ibyte_array,int ioffset);

void ishidaeutz_convert_uint32_to_byte_array_le(unsigned int inum, unsigned char* ibyte_array,int ioffset);

void ishidaeutz_convert_uint64_to_byte_array_be(unsigned long inum, unsigned char* ibyte_array,int ioffset);

unsigned int ishidaeutz_convert_byte_array_to_uint32_be(unsigned int ioffset,unsigned char* ibyte_array);

unsigned int ishidaeutz_convert_byte_array_to_uint32_le(unsigned int ioffset,unsigned char* ibyte_array);

int ishidaeutz_convert_byte_array_to_int32_be(unsigned int ioffset,unsigned char* ibyte_array);

int ishidaeutz_convert_byte_array_to_int32_le(unsigned int ioffset,unsigned char* ibyte_array);

void ishidaeutz_copy_byte_array(char *isource_byte_array, char *idestination_byte_array, unsigned int isource_offset, unsigned int idestination_offset, unsigned int icopy_length);

void ishidaeutz_shift_byte_array(char *i_byte_array, unsigned int ioffset, unsigned int ishift_size);

unsigned int ishidaeutz_system_is_little_endian();

void* ishidaeutz_malloc(size_t inum_blocks, size_t isize_of_single_block);

char* ishidaeutz_unsigned_int_to_string(unsigned int inumber);

void ishidaeutz_convert_double_to_byte_array(double idouble, unsigned char *idata_buffer, unsigned int iwrite_size);

double ishidaeutz_convert_byte_array_to_double(unsigned char *idata_buffer, unsigned int iparsing_offset);

extern unsigned int ishidaeutz_mem_track_array_clean_on_reset;

void ishidaeutz_mtrack_init();

void ishidaeutz_mtrack_pause();

void ishidaeutz_mtrack_resume();

void ishidaeutz_mtrack_reset();

unsigned int ishidaeutz_mtrack_state();
/*************************************** ISHIDAEU_UTILS END ***************************************/

/*************************************** ISHIDAEU_HASHMAP START ***************************************/
typedef struct ishidaeutz_HASHMAP_BUCKET_ELEMENT{
	const char *key;
	void *value;
	struct ishidaeutz_HASHMAP_BUCKET_ELEMENT *next;
} ishidaeutz_HASHMAP_BUCKET_ELEMENT;

typedef struct ishidaeutz_HASHMAP_KEYS_ELEMENT{
	const char *name;
} ishidaeutz_HASHMAP_KEYS_ELEMENT;

typedef struct ishidaeutz_HASHMAP{
	ishidaeutz_HASHMAP_BUCKET_ELEMENT *buckets;
	ishidaeutz_HASHMAP_KEYS_ELEMENT *keys;
	unsigned int actual_size;
	unsigned int key_size;
	unsigned int expected_size;
	unsigned int persistent_memory_elements;
} ishidaeutz_HASHMAP;

/** Returns SDBM hash **/
unsigned long ishidaeutz_get_SDBM_hash(const char *ipraw_string);

ishidaeutz_HASHMAP* ishidaeutz_init_hashmap(unsigned int isize);

unsigned int ishidaopcua_get_hashmap_size(ishidaeutz_HASHMAP *iphashmap);

void ishidaeutz_put_hashmap(ishidaeutz_HASHMAP *iphashmap, const char *ipkey, void *ipvalue);

void ishidaeutz_get_hashmap(ishidaeutz_HASHMAP *iphashmap, const char *ipkey, const void **ippvalue);
/*************************************** ISHIDAEU_HASHMAP END ***************************************/

/*************************************** ISHIDAOPCUA_CONSTANTS START ***************************************/
#define ISHIDAOPCUA_MSG_MAXBUFFERSIZE  8192
#define ISHIDAOPCUA_SECURITY_POLICY_URI_NONE "http://opcfoundation.org/UA/SecurityPolicy#None"
#define ISHIDAOPCUA_OPCUA_BINARY_TRANSPORT_PROFILE "http://opcfoundation.org/UA-Profile/Transport/uatcp-uasc-uabinary"
#define ISHIDAOPCUA_SERVER_IP "192.168.43.7"
#define ISHIDAOPCUA_SERVER_PORT 9998
#define ISHIDAOPCUA_MAIN_URL "opc.tcp://192.168.43.7:9998"
#define ISHIDAOPCUA_SERVER_PROTOCOL_VERSION 1
#define ISHIDAOPCUA_SERVER_SEND_RECEIVE_BUFFER_SIZE 8192
#define ISHIDAOPCUA_MAX_CHUNK_COUNT 1
#define ISHIDAOPCUA_MAX_CLIENT_CONNECTIONS 1
#define ISHIDAOPCUA_TRANSPORT_PROFILE_STANDARD "Standard UA Server Profile"
#define ISHIDAOPCUA_APPLICATION_NAME "ISHIDA EUROPE OPC UA"
#define ISHIDAOPCUA_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS 116444736000000000L
#define ISHIDAOPCUA_MILLI_SECONDS_TO_100_NANO_SECONDS 10000L
#define ISHIDAOPCUA_SECONDS_TO_100_NANO_SECONDS 10000000L

#define ISHIDAOPCUA_NODE_NODE_ID_ATTRIBUTE_KEY 1
#define ISHIDAOPCUA_NODE_NODE_CLASS_ATTRIBUTE_KEY 2
#define ISHIDAOPCUA_NODE_BROWSE_NAME_ATTRIBUTE_KEY 3
#define ISHIDAOPCUA_NODE_DISPLAY_NAME_ATTRIBUTE_KEY 4
#define ISHIDAOPCUA_NODE_DESCRIPTION_ATTRIBUTE_KEY 5
#define ISHIDAOPCUA_NODE_WRITE_MASK_ATTRIBUTE_KEY 6
#define ISHIDAOPCUA_NODE_USER_WRITE_MASK_ATTRIBUTE_KEY 7
#define ISHIDAOPCUA_NODE_IS_ABSTRACT_ATTRIBUTE_KEY 8
#define ISHIDAOPCUA_NODE_SYMMETRIC_ATTRIBUTE_KEY 9
#define ISHIDAOPCUA_NODE_INVERSE_NAME_ATTRIBUTE_KEY 10
#define ISHIDAOPCUA_NODE_CONTAINS_NO_LOOPS_ATTRIBUTE_KEY 11
#define ISHIDAOPCUA_NODE_EVENT_NOTIFIER_ATTRIBUTE_KEY 12
#define ISHIDAOPCUA_NODE_VALUE_ATTRIBUTE_KEY 13
#define ISHIDAOPCUA_NODE_DATA_TYPE_ATTRIBUTE_KEY 14
#define ISHIDAOPCUA_NODE_VALUE_RANK_ATTRIBUTE_KEY 15
#define ISHIDAOPCUA_NODE_ARRAY_DIMENSIONS_ATTRIBUTE_KEY 16
#define ISHIDAOPCUA_NODE_ACCESS_LEVEL_ATTRIBUTE_KEY 17
#define ISHIDAOPCUA_NODE_USER_ACCESS_LEVEL_ATTRIBUTE_KEY 18
#define ISHIDAOPCUA_NODE_MINIMUM_SAMPLING_INTERVAL_ATTRIBUTE_KEY 19
#define ISHIDAOPCUA_NODE_HISTORIZING_ATTRIBUTE_KEY 20
#define ISHIDAOPCUA_NODE_EXECUTABLE_ATTRIBUTE_KEY 21
#define ISHIDAOPCUA_NODE_USER_EXECUTABLE_ATTRIBUTE_KEY 22

#define ISHIDAOPCUA_REFERENCE_TYPE_ID_ATTRIBUTE_KEY "23"
#define ISHIDAOPCUA_IS_INVERSE_ATTRIBUTE_KEY "24"
#define ISHIDAOPCUA_TARGET_ID_ATTRIBUTE_KEY "25"

#define ISHIDAOPCUA_REFERENCES 31

#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES 33
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_EVENT_SOURCE_REFERENCE 36
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_NOTIFIER_REFERENCE 48
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_CHILD 34
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_SUB_TYPE_REFERENCE 45
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_AGGREGATES_REFERENCE 44
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_PROPERTY_REFERENCE 46
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_COMPONENT_REFERENCE 47
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_HAS_ORDERED_COMPONENT_REFERENCE 49
#define ISHIDAOPCUA_HIERARCHICAL_REFERENCES_ORGANIZES_REFERENCE 35

#define ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES 32
#define ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES_HAS_TYPE_DEFINITION_REFERENCE 40
#define ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES_HAS_ENCODING_REFERENCE 38
#define ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES_HAS_MODELLING_RULE_REFERENCE 37
#define ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES_HAS_DESCRIPTION_REFERENCE 39
#define ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES_GENERATES_EVENTS_REFERENCE 41
#define ISHIDAOPCUA_NON_HIERARCHICAL_REFERENCES_ALWAYS_GENERATES_EVENTS_REFERENCE 3065

extern ishidaeutz_LARGE_INT* pishidaopcua_UINT64_MIN;
extern ishidaeutz_LARGE_INT* pishidaopcua_UINT64_MAX;
extern ishidaeutz_LARGE_INT* pishidaopcua_DATE_TIME_MIN;
extern ishidaeutz_LARGE_INT* pishidaopcua_DATE_TIME_MAX;
extern ishidaeutz_LARGE_INT* pishidaopcua_INT64_MIN;
extern ishidaeutz_LARGE_INT* pishidaopcua_INT64_MAX;
extern ishidaeutz_LARGE_INT* pishidaopcua_NANO_SECONDS_BETWEEN_WIN_AND_UNIX_EPOCHS;
extern ishidaeutz_LARGE_INT* pishidaopcua_MILLI_SECONDS_TO_100_NANO_SECONDS;
extern ishidaeutz_LARGE_INT* pishidaopcua_SECONDS_TO_100_NANO_SECONDS;
extern ishidaeutz_LARGE_INT* pishidaopcua_SECONDS_TO_MILLI_SECONDS;
/*************************************** ISHIDAOPCUA_CONSTANTS END ***************************************/

/*************************************** ISHIDAOPCUA_TYPES START ***************************************/
typedef char ishidaopcua_SBYTE;
#define ishidaopcua_SBYTE_MIN (-128)
#define ishidaopcua_SBYTE_MAX 127

typedef unsigned char ishidaopcua_BYTE;
#define ishidaopcua_BYTE_MIN 0
#define ishidaopcua_BYTE_MAX 255

typedef enum ishidaopcua_TYPE_ID
{
	ishidaopcua_TYPE_ID_BOOLEAN = 1,
	ishidaopcua_TYPE_ID_SBYTE = 2,
	ishidaopcua_TYPE_ID_BYTE= 3,
	ishidaopcua_TYPE_ID_INT16 = 4,
	ishidaopcua_TYPE_ID_UINT16 = 5,
	ishidaopcua_TYPE_ID_INT32 = 6,
	ishidaopcua_TYPE_ID_UINT32 = 7,
	ishidaopcua_TYPE_ID_INT64 = 8,
	ishidaopcua_TYPE_ID_UINT64 = 9,
	ishidaopcua_TYPE_ID_FLOAT = 10,
	ishidaopcua_TYPE_ID_DOUBLE = 11,
	ishidaopcua_TYPE_ID_STRING = 12,
	ishidaopcua_TYPE_ID_DATE_TIME = 13,
	ishidaopcua_TYPE_ID_GUID = 14,
	ishidaopcua_TYPE_ID_BYTESTRING = 15,
	ishidaopcua_TYPE_ID_XML_ELEMENT = 16,
	ishidaopcua_TYPE_ID_NODE_ID = 17,
	ishidaopcua_TYPE_ID_EXPANDED_NODE_ID = 18,
	ishidaopcua_TYPE_ID_STATUS_CODE = 19,
	ishidaopcua_TYPE_ID_QUALIFIED_NAME = 20,
	ishidaopcua_TYPE_ID_LOCALIZED_TEXT = 21,
	ishidaopcua_TYPE_ID_EXTENSION_OBJECT = 22,
	ishidaopcua_TYPE_ID_DATA_VALUE = 23,
	ishidaopcua_TYPE_ID_VARIANT = 24,
	ishidaopcua_TYPE_ID_DIAGNOSTIC_INFO = 25,
	ishidaopcua_TYPE_ID_BROWSE_DESCRIPTION = 101,
	ishidaopcua_TYPE_ID_USER_TOKEN_POLICY = 102,
	ishidaopcua_TYPE_ID_MESSAGE_SECURITY_MODE = 103,
	ishidaopcua_TYPE_ID_USER_IDENTITY_TOKEN_TYPE = 104,
	ishidaopcua_TYPE_ID_UTC_TIME = 105,
	ishidaopcua_TYPE_ID_DURATION= 106,
	ishidaopcua_TYPE_ID_REQUEST_HEADER = 107,
	ishidaopcua_TYPE_ID_RESPONSE_HEADER = 108,
	ishidaopcua_TYPE_ID_SECURITY_TOKEN = 109,
	ishidaopcua_TYPE_ID_SIGNATURE = 110,
	ishidaopcua_TYPE_ID_SIGNED_SOFTWARE_CERTIFICATE= 111,
	ishidaopcua_TYPE_ID_APPLICATION_DESCRIPTION = 112,
	ishidaopcua_TYPE_ID_STRUCTURE = 113,
	ishidaopcua_TYPE_ID_APPLICATION_INSTANCE_CERTIFICATE = 114,
	ishidaopcua_TYPE_ID_ENDPOINT_DESCRIPTION = 115,
	ishidaopcua_TYPE_ID_READ_VALUE_ID = 116,
	ishidaopcua_TYPE_ID_NODE = 117,
	ishidaopcua_TYPE_ID_VIEW_DESCRIPTION = 118,
	ishidaopcua_TYPE_ID_COUNTER = 119,
	ishidaopcua_TYPE_ID_INTEGER_ID = 120,
	ishidaopcua_TYPE_ID_NUMERIC_RANGE = 121,
	ishidaopcua_TYPE_ID_WRITE_VALUE = 122,
	ishidaopcua_TYPE_ID_TCP_MESSAGE_HEADER = 123,
	ishidaopcua_TYPE_ID_ARRAY = 124,
	ishidaopcua_TYPE_ID_BROWSE_RESULT = 125,
	ishidaopcua_TYPE_ID_REFERENCE_DESCRIPTION = 126,
	ishidaopcua_TYPE_ID_UINT32_ARRAY = 10002
} ishidaopcua_TYPE_ID;

typedef ishidaopcua_BYTE ishidaopcua_BOOLEAN;
#define ishidaopcua_BOOLEAN_TRUE 1
#define ishidaopcua_BOOLEAN_FALSE 0

typedef short ishidaopcua_INT16;
#define ishidaopcua_INT16_MIN (-32768)
#define ishidaopcua_INT16_MAX 32767

typedef unsigned short ishidaopcua_UINT16;
#define ishidaopcua_UINT16_MIN 0
#define ishidaopcua_UINT16_MAX 65535

typedef int ishidaopcua_INT32;
#define ishidaopcua_INT32_MIN (-2147483648)
#define ishidaopcua_INT32_MAX 2147483647

typedef unsigned int ishidaopcua_UINT32;
#define ishidaopcua_UINT32_MIN 0
#define ishidaopcua_UINT32_MAX 4294967295

typedef ishidaeutz_LARGE_INT ishidaopcua_INT64;

typedef ishidaeutz_LARGE_INT ishidaopcua_UINT64;

typedef float ishidaopcua_FLOAT;

typedef ishidaeutz_LARGE_INT ishidaopcua_DOUBLE;

typedef struct ishidaopcua_ARRAY
{
    ishidaopcua_UINT32 size;
    ishidaopcua_TYPE_ID type;
    void* elements;
} ishidaopcua_ARRAY;

typedef struct ishidaopcua_STRING
{
    ishidaopcua_UINT32 length;
    ishidaopcua_BYTE* text;
} ishidaopcua_STRING;

typedef ishidaeutz_LARGE_INT ishidaopcua_DATE_TIME;

typedef struct ishidaopcua_GUID
{
	ishidaopcua_UINT32 data1;
    ishidaopcua_UINT16 data2;
    ishidaopcua_UINT16 data3;
    ishidaopcua_BYTE   data4[8];
} ishidaopcua_GUID;

typedef ishidaopcua_STRING ishidaopcua_BYTESTRING;

typedef ishidaopcua_STRING ishidaopcua_XML_ELEMENT;

extern ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_NUMERIC_TWO_BYTE;
extern ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_NUMERIC_FOUR_BYTE;
extern ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_NUMERIC_OVER_FOUR_BYTE;
extern ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_STRING;
extern ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_GUID;
extern ishidaopcua_BYTE ishidaopcua_NODE_ID_ENCODING_BYTESTRING;

typedef struct ishidaopcua_NODE_ID
{
	ishidaopcua_BYTE encoding_mask;
    ishidaopcua_UINT16 namespace_index;
    enum ishidaopcua_UINT32
    {
        ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC		= 0,
        ishidaopcua_NODE_ID_IDENTIFIER_TYPE_STRING     	= 3,
        ishidaopcua_NODE_ID_IDENTIFIER_TYPE_GUID       	= 4,
        ishidaopcua_NODE_ID_IDENTIFIER_TYPE_BYTESTRING 	= 5
    } identifier_type;
    union ishidaopuca_NODE_IDENTIFIER
    {
        ishidaopcua_UINT32      numeric;
        ishidaopcua_STRING*     string;
        ishidaopcua_GUID*       guid;
        ishidaopcua_BYTESTRING* bytestring;
    } identifier;
} ishidaopcua_NODE_ID;

typedef struct ishidaopcua_EXPANDED_NODE_ID{
    ishidaopcua_NODE_ID* node_id;
    ishidaopcua_STRING* namespace_uri;
    ishidaopcua_UINT32 server_index;
} ishidaopcua_EXPANDED_NODE_ID;

typedef enum
{
	ishidaopcua_STATUS_CODE_SEVERITY_GOOD 					= 0,
	ishidaopcua_STATUS_CODE_SEVERITY_UNCERTAIN 				= 1,
	ishidaopcua_STATUS_CODE_SEVERITY_BAD_FAILURE 			= 2,
	ishidaopcua_STATUS_CODE_SEVERITY_BAD_INVALID_ATTRIBUTE 	= 2150957056u,
	ishidaopcua_STATUS_CODE_BAD_NODE_ID_UNKNOWN = 0x80340000,
	ishidaopcua_STATUS_CODE_BAD_REFERENCE_TYPE_ID_INVALID = 0x804C0000,
	ishidaopcua_STATUS_CODE_BAD_SERVICE_UNSUPPORTED = 0x800B0000,
} ishidaopcua_STATUS_CODE;

typedef struct ishidaopcua_QUALIFIED_NAME
{
    ishidaopcua_UINT16 namespace_index;
    ishidaopcua_STRING* name;
} ishidaopcua_QUALIFIED_NAME;

extern ishidaopcua_BYTE ishidaopcua_LOCALIZED_TEXT_ENCODE_LOCALE;
extern ishidaopcua_BYTE ishidaopcua_LOCALIZED_TEXT_ENCODE_TEXT;

typedef struct ishidaopcua_LOCALIZED_TEXT
{
	ishidaopcua_BYTE encoding;
    ishidaopcua_STRING* locale;
    ishidaopcua_STRING* text;
} ishidaopcua_LOCALIZED_TEXT;

extern ishidaopcua_BYTE ishidaopcua_EXTENSION_OBJECT_ENCODE_NO_BODY;
extern ishidaopcua_BYTE ishidaopcua_EXTENSION_OBJECT_ENCODE_BYTESTRING;
extern ishidaopcua_BYTE ishidaopcua_EXTENSION_OBJECT_ENCODE_XML;

typedef struct ishidaopcua_EXTENSION_OBJECT
{
	ishidaopcua_EXPANDED_NODE_ID* typed_id;
	ishidaopcua_BYTE encoding;
	ishidaopcua_UINT32 length;
	ishidaopcua_BYTE* body;
} ishidaopcua_EXTENSION_OBJECT;

typedef struct ishidaopcua_VARIANT
{
	ishidaopcua_BYTE encoding_mask;
	ishidaopcua_UINT32 array_length;
	void* value;
	ishidaopcua_ARRAY* array_dimensions;
} ishidaopcua_VARIANT;

typedef struct ishidaopcua_DATA_VALUE
{
	ishidaopcua_BYTE encoding_mask;
	ishidaopcua_VARIANT* value;
    ishidaopcua_STATUS_CODE status_code;
    ishidaopcua_DATE_TIME* psource_timestamp;
    ishidaopcua_UINT16 source_pico_seconds;
    ishidaopcua_DATE_TIME* pserver_timestamp;
    ishidaopcua_UINT16 server_pico_seconds;
} ishidaopcua_DATA_VALUE;

extern ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_SYMBOLIC_ID;
extern ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_NAMESPACE_URI;
extern ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALIZED_TEXT;
extern ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_LOCALE;
extern ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_ADDITIONAL_INFO;
extern ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_STATUS_CODE;
extern ishidaopcua_BYTE ishidaopcua_DIAGNOSTIC_INFO_ENCODE_INNER_DIAGNOSTIC_INFO;

typedef struct ishidaopcua_DIAGNOSTIC_INFO
{
	ishidaopcua_BYTE encoding_mask;
    ishidaopcua_INT32      symbolic_id;
    ishidaopcua_INT32      namespace_uri;
    ishidaopcua_INT32      localized_text;
    ishidaopcua_INT32      locale;
    ishidaopcua_STRING*     additional_info;
    ishidaopcua_STATUS_CODE inner_status_code;
    struct ishidaopcua_DIAGNOSTIC_INFO* inner_diagnostic_info;
} ishidaopcua_DIAGNOSTIC_INFO;

ishidaopcua_ARRAY* ishidaopcua_init_array();

void ishidaopcua_set_array_size(ishidaopcua_UINT32 isize, ishidaopcua_ARRAY* iarray);

void ishidaopcua_set_array_type(ishidaopcua_UINT16 itype, ishidaopcua_ARRAY* iarray);

void ishidaopcua_set_array_elements(void* ielements, ishidaopcua_ARRAY* iarray);

void ishidaopcua_set_array(ishidaopcua_UINT32 isize, ishidaopcua_UINT16 itype, void* ielements, ishidaopcua_ARRAY* iarray);

ishidaopcua_UINT32 ishidaopcua_get_array_size(ishidaopcua_ARRAY* iarray);

ishidaopcua_UINT16 ishidaopcua_get_array_type(ishidaopcua_ARRAY* iarray);

void* ishidaopcua_get_array_elements(ishidaopcua_ARRAY* iarray);

ishidaopcua_STRING* ishidaopcua_init_string();

void ishidaopcua_set_string(ishidaopcua_BYTE* itext, ishidaopcua_STRING* istring);

ishidaopcua_UINT32 ishidaopcua_get_string_length(ishidaopcua_STRING* istring);

ishidaopcua_BYTE* ishidaopcua_get_string_text(ishidaopcua_STRING* istring);

ishidaopcua_GUID* ishidaopcua_init_guid();

void ishidaopcua_set_guid_data1(ishidaopcua_UINT32 idata1, ishidaopcua_GUID* iguid);

void ishidaopcua_set_guid_data2(ishidaopcua_UINT16 idata2, ishidaopcua_GUID* iguid);

void ishidaopcua_set_guid_data3(ishidaopcua_UINT16 idata3, ishidaopcua_GUID* iguid);

void ishidaopcua_set_guid_data4(ishidaopcua_BYTE idata4[8], ishidaopcua_GUID* iguid);

void ishidaopcua_set_guid(ishidaopcua_UINT32 idata1, ishidaopcua_UINT16 idata2, ishidaopcua_UINT16 idata3, ishidaopcua_BYTE idata4[8], ishidaopcua_GUID* iguid);

ishidaopcua_UINT32 ishidaopcua_get_guid_data1(ishidaopcua_GUID* iguid);

ishidaopcua_UINT16 ishidaopcua_get_guid_data2(ishidaopcua_GUID* iguid);

ishidaopcua_UINT16 ishidaopcua_get_guid_data3(ishidaopcua_GUID* iguid);

ishidaopcua_BYTE* ishidaopcua_get_guid_data4(ishidaopcua_GUID* iguid);

ishidaopcua_NODE_ID* ishidaopcua_init_node_id();

void ishidaopcua_set_node_id_namespace_index(ishidaopcua_UINT16 inamespace_index, ishidaopcua_NODE_ID* inode_id);

void ishidaopcua_set_node_id_identifier_numeric(ishidaopcua_UINT32 iidentifier_numeric, ishidaopcua_NODE_ID* inode_id);

void ishidaopcua_set_node_id_identifier_string(ishidaopcua_STRING* iidentifier_string, ishidaopcua_NODE_ID* inode_id);

void ishidaopcua_set_node_id_identifier_guid(ishidaopcua_GUID* iidentifier_guid, ishidaopcua_NODE_ID* inode_id);

void ishidaopcua_set_node_id_identifier_bytestring(ishidaopcua_BYTESTRING* iidentifier_bytestring, ishidaopcua_NODE_ID* inode_id);

ishidaopcua_BYTE ishidaopcua_get_node_id_encoding_mask(ishidaopcua_NODE_ID* inode_id);

ishidaopcua_UINT16 ishidaopcua_get_node_id_namespace_index(ishidaopcua_NODE_ID* inode_id);

ishidaopcua_UINT32 ishidaopcua_get_node_id_identifier_type(ishidaopcua_NODE_ID* inode_id);

ishidaopcua_UINT32 ishidaopcua_get_node_id_identifier_numeric(ishidaopcua_NODE_ID* inode_id);

ishidaopcua_STRING* ishidaopcua_get_node_id_identifier_string(ishidaopcua_NODE_ID* inode_id);

ishidaopcua_GUID* ishidaopcua_get_node_id_identifier_guid(ishidaopcua_NODE_ID* inode_id);

ishidaopcua_BYTESTRING* ishidaopcua_get_node_id_identifier_bytestring(ishidaopcua_NODE_ID* inode_id);

ishidaopcua_EXPANDED_NODE_ID* ishidaopcua_init_expanded_node_id();

void ishidaopcua_set_expanded_node_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id);

void ishidaopcua_set_namespace_uri(ishidaopcua_STRING* inamespace_uri, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id);

void ishidaopcua_set_server_index(ishidaopcua_UINT32 iserver_index, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id);

void ishidaopcua_set_expanded_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_STRING* inamespace_uri, ishidaopcua_UINT32 iserver_index, ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id);

ishidaopcua_NODE_ID* ishidaopcua_get_node_id(ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id);

ishidaopcua_STRING* ishidaopcua_get_namespace_uri(ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id);

ishidaopcua_UINT32 ishidaopcua_get_server_index(ishidaopcua_EXPANDED_NODE_ID* iexpanded_node_id);

ishidaopcua_QUALIFIED_NAME* ishidaopcua_init_qualified_name();

void ishidaopcua_set_qualified_name_namespace_index(ishidaopcua_UINT16 inamespace_index, ishidaopcua_QUALIFIED_NAME* iqualified_name);

void ishidaopcua_set_qualified_name_name(ishidaopcua_STRING* iname, ishidaopcua_QUALIFIED_NAME* iqualified_name);

void ishidaopcua_set_qualified_name(ishidaopcua_UINT16 inamespace_index, ishidaopcua_STRING* iname, ishidaopcua_QUALIFIED_NAME* iqualified_name);

ishidaopcua_UINT16 ishidaopcua_get_qualified_name_namespace_index(ishidaopcua_QUALIFIED_NAME* iqualified_name);

ishidaopcua_STRING* ishidaopcua_get_qualified_name_name(ishidaopcua_QUALIFIED_NAME* iqualified_name);

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_init_localized_text();

void ishidaopcua_set_localized_text_locale(ishidaopcua_STRING* ilocale, ishidaopcua_LOCALIZED_TEXT* ilocalized_text);

void ishidaopcua_set_localized_text_text(ishidaopcua_STRING* itext, ishidaopcua_LOCALIZED_TEXT* ilocalized_text);

void ishidaopcua_set_localized_text(ishidaopcua_STRING* ilocale, ishidaopcua_STRING* itext, ishidaopcua_LOCALIZED_TEXT* ilocalized_text);

ishidaopcua_BYTE ishidaopcua_get_localized_text_encoding(ishidaopcua_LOCALIZED_TEXT* ilocalized_text);

ishidaopcua_STRING* ishidaopcua_get_localized_text_locale(ishidaopcua_LOCALIZED_TEXT* ilocalized_text);

ishidaopcua_STRING* ishidaopcua_get_localized_text_text(ishidaopcua_LOCALIZED_TEXT* ilocalized_text);

ishidaopcua_EXTENSION_OBJECT* ishidaopcua_init_extension_object();

void ishidaopcua_set_extension_object_type_id(ishidaopcua_EXPANDED_NODE_ID* ityped_id, ishidaopcua_EXTENSION_OBJECT* iextension_object);

void ishidaopcua_set_extension_object_length(ishidaopcua_UINT32 ilength, ishidaopcua_EXTENSION_OBJECT* iextension_object);

void ishidaopcua_set_extension_object_body(ishidaopcua_BYTE iencoding, ishidaopcua_BYTE* ibody, ishidaopcua_EXTENSION_OBJECT* iextension_object);

void ishidaopcua_set_extension_object(ishidaopcua_EXPANDED_NODE_ID* ityped_id, ishidaopcua_UINT32 ilength, ishidaopcua_BYTE iencoding, ishidaopcua_BYTE* ibody, ishidaopcua_EXTENSION_OBJECT* iextension_object);

ishidaopcua_EXPANDED_NODE_ID* ishidaopcua_get_extension_object_type_id(ishidaopcua_EXTENSION_OBJECT* iextension_object);

ishidaopcua_BYTE ishidaopcua_get_extension_object_encoding(ishidaopcua_EXTENSION_OBJECT* iextension_object);

ishidaopcua_UINT32 ishidaopcua_get_extension_object_length(ishidaopcua_EXTENSION_OBJECT* iextension_object);

ishidaopcua_BYTE* ishidaopcua_get_extension_object_body(ishidaopcua_EXTENSION_OBJECT* iextension_object);

ishidaopcua_VARIANT* ishidaopcua_init_variant();

void ishidaopcua_set_variant_array_length(ishidaopcua_UINT32 iarray_length, ishidaopcua_VARIANT* ivariant);

void ishidaopcua_set_variant_type_id(ishidaopcua_TYPE_ID itype_id, ishidaopcua_VARIANT* ivariant);

void ishidaopcua_set_variant_value(ishidaopcua_TYPE_ID itype_id, void* ivalue, ishidaopcua_VARIANT* ivariant);

void ishidaopcua_set_variant_array_dimensions(ishidaopcua_ARRAY* iarray_dimensions, ishidaopcua_VARIANT* ivariant);

void ishidaopcua_set_variant(ishidaopcua_UINT32 iarray_length,ishidaopcua_ARRAY* iarray_dimensions, ishidaopcua_TYPE_ID itype_id, void* ivalue, ishidaopcua_VARIANT* ivariant);

ishidaopcua_BYTE ishidaopcua_get_variant_encoding_mask(ishidaopcua_VARIANT* ivariant);

ishidaopcua_UINT32 ishidaopcua_get_variant_array_length(ishidaopcua_VARIANT* ivariant);

void* ishidaopcua_get_variant_value(ishidaopcua_VARIANT* ivariant);

ishidaopcua_ARRAY* ishidaopcua_get_variant_array_dimensions(ishidaopcua_VARIANT* ivariant);

ishidaopcua_TYPE_ID ishidaopcua_get_variant_type_id(ishidaopcua_VARIANT* ivariant);

ishidaopcua_DATA_VALUE* ishidaopcua_init_data_value();

void ishidaopcua_set_data_value_value(ishidaopcua_VARIANT* ivalue, ishidaopcua_DATA_VALUE* idata_value);

void ishidaopcua_set_data_value_status_code(ishidaopcua_STATUS_CODE istatus_code, ishidaopcua_DATA_VALUE* idata_value);

void ishidaopcua_set_data_value_source_timestamp(ishidaopcua_DATE_TIME* ipsource_timestamp, ishidaopcua_DATA_VALUE* idata_value);

void ishidaopcua_set_data_value_source_pico_seconds(ishidaopcua_UINT16 isource_pico_seconds, ishidaopcua_DATA_VALUE* idata_value);

void ishidaopcua_set_data_value_server_timestamp(ishidaopcua_DATE_TIME* ipserver_timestamp, ishidaopcua_DATA_VALUE* idata_value);

void ishidaopcua_set_data_value_server_pico_seconds(ishidaopcua_UINT16 iserver_pico_seconds, ishidaopcua_DATA_VALUE* idata_value);

ishidaopcua_STATUS_CODE ishidaopcua_get_data_value_status_code(ishidaopcua_DATA_VALUE* idata_value);

ishidaopcua_DATE_TIME* ishidaopcua_get_data_value_source_timestamp(ishidaopcua_DATA_VALUE* idata_value);

ishidaopcua_UINT16 ishidaopcua_get_data_value_source_pico_seconds(ishidaopcua_DATA_VALUE* idata_value);

ishidaopcua_DATE_TIME* ishidaopcua_get_data_value_server_timestamp(ishidaopcua_DATA_VALUE* idata_value);

ishidaopcua_UINT16 ishidaopcua_get_data_value_server_pico_seconds(ishidaopcua_DATA_VALUE* idata_value);

ishidaopcua_DIAGNOSTIC_INFO* ishidaopcua_init_diagnostic_info();

void ishidaopcua_set_diagnostic_info_symbolic_id(ishidaopcua_UINT32 isymbolic_id, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

void ishidaopcua_set_diagnostic_info_namespace_uri(ishidaopcua_UINT32 inamespace_uri, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

void ishidaopcua_set_diagnostic_info_localized_text(ishidaopcua_UINT32 ilocalized_text, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

void ishidaopcua_set_diagnostic_info_locale(ishidaopcua_UINT32 ilocale, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

void ishidaopcua_set_diagnostic_info_additional_info(ishidaopcua_STRING* iadditional_info, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

void ishidaopcua_set_diagnostic_info_inner_status_code(ishidaopcua_STATUS_CODE iinner_status_code, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

void ishidaopcua_set_diagnostic_info_inner_diagnostic_info(ishidaopcua_DIAGNOSTIC_INFO* iinner_diagnostic_info, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

void ishidaopcua_set_diagnostic_info(ishidaopcua_UINT32 isymbolic_id, ishidaopcua_UINT32 inamespace_uri,
								ishidaopcua_UINT32 ilocalized_text, ishidaopcua_UINT32 ilocale, ishidaopcua_STRING* iadditional_info,
								ishidaopcua_STATUS_CODE iinner_status_code, ishidaopcua_DIAGNOSTIC_INFO* iinner_diagnostic_info, ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_symbolic_id(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_namespace_uri(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_localized_text(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

ishidaopcua_UINT32 ishidaopcua_get_diagnostic_info_locale(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

ishidaopcua_STRING* ishidaopcua_get_diagnostic_info_additional_info(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

ishidaopcua_STATUS_CODE ishidaopcua_get_diagnostic_info_inner_status_code(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);

ishidaopcua_DIAGNOSTIC_INFO* ishidaopcua_get_diagnostic_info_inner_diagnostic_info(ishidaopcua_DIAGNOSTIC_INFO* idiagnostic_info);
/*************************************** ISHIDAOPCUA_TYPES END ***************************************/

/*************************************** ISHIDAOPCUA_CUSTOM_TYPES START ***************************************/
typedef enum ishidaopcua_MESSAGE_SECURITY_MODE
{
	ishidaopcua_MESSAGE_SECURITY_MODE_INVALID = 0,
	ishidaopcua_MESSAGE_SECURITY_MODE_NONE = 1,
	ishidaopcua_MESSAGE_SECURITY_MODE_SIGN = 2,
	ishidaopcua_MESSAGE_SECURITY_MODE_SIGN_AND_ENCRYPT = 3
} ishidaopcua_MESSAGE_SECURITY_MODE;

typedef enum ishidaopcua_USER_IDENTITY_TOKEN_TYPE
{
	ishidaopcua_USER_IDENTITY_TOKEN_TYPE_ANONYMOUS = 0,
	ishidaopcua_USER_IDENTITY_TOKEN_TYPE_USERNAME = 1,
	ishidaopcua_USER_IDENTITY_TOKEN_TYPE_CERTIFICATE = 2,
	ishidaopcua_USER_IDENTITY_TOKEN_TYPE_ISSUED_TOKEN = 3
} ishidaopcua_USER_IDENTITY_TOKEN_TYPE;

typedef ishidaeutz_LARGE_INT ishidaopcua_UTC_TIME;

typedef ishidaopcua_DOUBLE ishidaopcua_DURATION;

typedef struct ishidaopcua_REQUEST_HEADER
{
	ishidaopcua_EXPANDED_NODE_ID* authentication_token;
	ishidaopcua_UTC_TIME* ptimestamp;
	ishidaopcua_UINT32 request_handle;
	ishidaopcua_UINT32 return_diagnostics;
	ishidaopcua_STRING* audit_entry_id;
	ishidaopcua_UINT32 timeout_hint;
	ishidaopcua_EXTENSION_OBJECT* additional_header;

} ishidaopcua_REQUEST_HEADER;

typedef struct ishidaopcua_RESPONSE_HEADER
{
	ishidaopcua_UTC_TIME* ptimestamp;
	ishidaopcua_UINT32 request_handle;
	ishidaopcua_STATUS_CODE service_result;
	ishidaopcua_DIAGNOSTIC_INFO* service_diagnostics;
	ishidaopcua_ARRAY* string_table;
	ishidaopcua_EXTENSION_OBJECT* additional_header;

} ishidaopcua_RESPONSE_HEADER;

typedef struct ishidaopcua_SECURITY_TOKEN
{
    ishidaopcua_UINT32 secure_channel_id;
    ishidaopcua_UINT32 token_id;
    ishidaopcua_UTC_TIME* pcreated_at;
    ishidaopcua_UINT32 revised_lifetime;
} ishidaopcua_SECURITY_TOKEN;

typedef struct ishidaopcua_SIGNATURE
{
    ishidaopcua_STRING* algorithm;
    ishidaopcua_STRING* signature;
} ishidaopcua_SIGNATURE;

typedef struct ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE
{
    ishidaopcua_STRING* certificate_data;
    ishidaopcua_STRING* signature;
} ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE;

typedef struct ishidaopcua_APPLICATION_DESCRIPTION
{
    ishidaopcua_STRING* application_uri;
    ishidaopcua_STRING* product_uri;
    ishidaopcua_LOCALIZED_TEXT* application_name;
    enum{
    	ishidaopcua_APPLICATION_DESCRIPTION_APPLICATION_TYPE_SERVER = 0,
		ishidaopcua_APPLICATION_DESCRIPTION_APPLICATION_TYPE_CLIENT = 1,
		ishidaopcua_APPLICATION_DESCRIPTION_APPLICATION_TYPE_CLIENTANDSERVER = 2,
		ishidaopcua_APPLICATION_DESCRIPTION_APPLICATION_TYPE_DISCOVERYSERVER = 3
    } application_type;
    ishidaopcua_STRING* gateway_server_uri;
    ishidaopcua_STRING* discovery_profile_uri;
    ishidaopcua_ARRAY* discovery_urls;
} ishidaopcua_APPLICATION_DESCRIPTION;

typedef struct ishidaopcua_STRUCTURE
{
    ishidaopcua_UINT32 type_id;
    ishidaopcua_BYTE encoding;
    ishidaopcua_UINT32 length;
    ishidaopcua_BYTE* data;
} ishidaopcua_STRUCTURE;

typedef struct ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE
{
    ishidaopcua_STRING* version;
    ishidaopcua_BYTESTRING* serial_no;
    ishidaopcua_STRING* signature_algorithm;
    ishidaopcua_BYTESTRING* signature;
    ishidaopcua_STRUCTURE* issuer;
    ishidaopcua_UTC_TIME* pvalid_from;
    ishidaopcua_UTC_TIME* pvalid_to;
    ishidaopcua_STRUCTURE* subject;
    ishidaopcua_STRING* application_uri;
    ishidaopcua_ARRAY* hostnames;
    ishidaopcua_BYTESTRING* public_key;
    ishidaopcua_ARRAY* key_usage;
} ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE;

typedef struct ishidaopcua_USER_TOKEN_POLICY
{
    ishidaopcua_STRING* policy_id;
    ishidaopcua_UINT32 token_type;
    ishidaopcua_STRING* issued_token_type;
    ishidaopcua_STRING* issuer_endpoint_url;
    ishidaopcua_STRING* security_policy_uri;
} ishidaopcua_USER_TOKEN_POLICY;

typedef struct ishidaopcua_ENDPOINT_DESCRIPTION
{
    ishidaopcua_STRING* endpoint_url;
    ishidaopcua_APPLICATION_DESCRIPTION* server;
    ishidaopcua_BYTESTRING* server_certificate;
    ishidaopcua_UINT32 security_mode;
    ishidaopcua_STRING* security_policy_uri;
    ishidaopcua_ARRAY* user_identity_tokens;
    ishidaopcua_STRING* transport_profile_uri;
    ishidaopcua_BYTE security_level;
} ishidaopcua_ENDPOINT_DESCRIPTION;

typedef struct ishidaopcua_READ_VALUE_ID
{
	ishidaopcua_NODE_ID* node_id;
	ishidaopcua_UINT32 attribute_id;
	ishidaopcua_STRING* index_range;
	ishidaopcua_QUALIFIED_NAME* data_encoding;
} ishidaopcua_READ_VALUE_ID;

typedef struct ishidaopcua_VIEW_DESCRIPTION
{
    ishidaopcua_NODE_ID* pview_id;
    ishidaopcua_UTC_TIME* ptimestamp;
    ishidaopcua_UINT32 view_version;
} ishidaopcua_VIEW_DESCRIPTION;

typedef ishidaopcua_UINT32 ishidaopcua_COUNTER;

typedef struct ishidaopcua_BROWSE_DESCRIPTION
{
    ishidaopcua_NODE_ID* pnode_id;
    enum
    {
    	ishidaopcua_BROWSE_DIRECTION_FORWARD = 0,
    	ishidaopcua_BROWSE_DIRECTION_INVERSE = 1,
    	ishidaopcua_BROWSE_DIRECTION_BOTH = 2
    } browse_direction;
    ishidaopcua_NODE_ID* preference_type_id;
    ishidaopcua_BOOLEAN include_subtypes;
    ishidaopcua_UINT32 node_class_mask;
    ishidaopcua_UINT32 result_class_mask;
} ishidaopcua_BROWSE_DESCRIPTION;

typedef enum
{
	ishidaopcua_NODE_CLASS_OBJECT = 1,
	ishidaopcua_NODE_CLASS_VARIABLE = 2,
	ishidaopcua_NODE_CLASS_METHOD = 4,
	ishidaopcua_NODE_CLASS_OBJECT_TYPE = 8,
	ishidaopcua_NODE_CLASS_VARIABLE_TYPE = 16,
	ishidaopcua_NODE_CLASS_REFERENCE_TYPE = 32,
	ishidaopcua_NODE_CLASS_DATA_TYPE = 64,
	ishidaopcua_NODE_CLASS_VIEW = 128
} ishidaopcua_NODE_CLASS;

typedef struct ishidaopcua_REFERENCE_DESCRIPTION
{
	ishidaopcua_NODE_ID* preference_type_id;
	ishidaopcua_BOOLEAN is_forward;
	ishidaopcua_EXPANDED_NODE_ID* pnode_id;
	ishidaopcua_QUALIFIED_NAME* pbrowse_name;
	ishidaopcua_LOCALIZED_TEXT* pdisplay_name;
	ishidaopcua_NODE_CLASS node_class;
	ishidaopcua_EXPANDED_NODE_ID* ptype_definition;

} ishidaopcua_REFERENCE_DESCRIPTION;

typedef struct ishidaopcua_BROWSE_RESULT
{
    ishidaopcua_STATUS_CODE status_code;
    ishidaopcua_STRING* pcontinuation_point;
    ishidaopcua_ARRAY* preference_descriptions;
} ishidaopcua_BROWSE_RESULT;

typedef ishidaopcua_UINT32 ishidaopcua_INTEGER_ID;
typedef ishidaopcua_STRING ishidaopcua_NUMERIC_RANGE;

typedef struct ishidaopcua_WRITE_VALUE
{
	ishidaopcua_NODE_ID* node_id;
	ishidaopcua_INTEGER_ID attribute_id;
	ishidaopcua_NUMERIC_RANGE* index_range;
	ishidaopcua_DATA_VALUE* value;
} ishidaopcua_WRITE_VALUE;

typedef struct ishidaopcua_NODE
{
	ishidaeutz_HASHMAP* attributes;
	ishidaeutz_HASHMAP* references;
	ishidaeutz_HASHMAP* standard_properties;
} ishidaopcua_NODE;

typedef struct ishidaopcua_TCP_MESSAGE_HEADER
{
    ishidaopcua_BYTE   message_type[4];
    ishidaopcua_BYTE   reserved;
    ishidaopcua_UINT32   message_size;
} ishidaopcua_TCP_MESSAGE_HEADER;

typedef struct ishidaopcua_UINT32_ARRAY
{
    ishidaopcua_UINT32 size; /* The length of the string */
    ishidaopcua_UINT32* numbers; /* The content (not null-terminated) */
} ishidaopcua_UINT32_ARRAY;

ishidaopcua_REQUEST_HEADER* ishidaopcua_init_request_header();

void ishidaopcua_set_request_header_authentication_token(ishidaopcua_EXPANDED_NODE_ID* iauthentication_token, ishidaopcua_REQUEST_HEADER* irequest_header);

void ishidaopcua_set_request_header_timestamp(ishidaopcua_UTC_TIME* iptimestamp, ishidaopcua_REQUEST_HEADER* irequest_header);

void ishidaopcua_set_request_header_request_handle(ishidaopcua_UINT32 irequest_handle, ishidaopcua_REQUEST_HEADER* irequest_header);

void ishidaopcua_set_request_header_return_diagnostics(ishidaopcua_UINT32 ireturn_diagnostics, ishidaopcua_REQUEST_HEADER* irequest_header);

void ishidaopcua_set_request_header_audit_entry_id(ishidaopcua_STRING* iaudit_entry_id, ishidaopcua_REQUEST_HEADER* irequest_header);

void ishidaopcua_set_request_header_timeout_hint(ishidaopcua_UINT32 itimeout_hint, ishidaopcua_REQUEST_HEADER* irequest_header);

void ishidaopcua_set_request_header_additional_header(ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_REQUEST_HEADER* irequest_header);

void ishidaopcua_set_request_header(ishidaopcua_EXPANDED_NODE_ID* iauthentication_token, ishidaopcua_UTC_TIME* iptimestamp, ishidaopcua_UINT32 irequest_handle,
									ishidaopcua_UINT32 ireturn_diagnostics, ishidaopcua_STRING* iaudit_entry_id,
									ishidaopcua_UINT32 itimeout_hint, ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_EXPANDED_NODE_ID* ishidaopcua_get_request_header_authentication_token(ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_UTC_TIME* ishidaopcua_get_request_header_timestamp(ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_UINT32 ishidaopcua_get_request_header_request_handle(ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_UINT32 ishidaopcua_get_request_header_return_diagnostics(ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_STRING* ishidaopcua_get_request_header_audit_entry_id(ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_UINT32 ishidaopcua_get_request_header_timeout_hint(ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_EXTENSION_OBJECT* ishidaopcua_get_request_header_additional_header(ishidaopcua_REQUEST_HEADER* irequest_header);

ishidaopcua_RESPONSE_HEADER* ishidaopcua_init_response_header();

void ishidaopcua_set_response_header_timestamp(ishidaeutz_LARGE_INT* iptimestamp, ishidaopcua_RESPONSE_HEADER* iresponse_header);

void ishidaopcua_set_response_header_request_handle(ishidaopcua_UINT32 irequest_handle, ishidaopcua_RESPONSE_HEADER* iresponse_header);

void ishidaopcua_set_response_header_service_result(ishidaopcua_STATUS_CODE iservice_result, ishidaopcua_RESPONSE_HEADER* iresponse_header);

void ishidaopcua_set_response_header_service_diagnostics(ishidaopcua_DIAGNOSTIC_INFO* iservice_diagnostics, ishidaopcua_RESPONSE_HEADER* iresponse_header);

void ishidaopcua_set_response_header_string_table(ishidaopcua_ARRAY* istring_table, ishidaopcua_RESPONSE_HEADER* iresponse_header);

void ishidaopcua_set_response_header_additional_header(ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_RESPONSE_HEADER* iresponse_header);

void ishidaopcua_set_response_header(ishidaeutz_LARGE_INT* iptimestamp, ishidaopcua_UINT32 irequest_handle, ishidaopcua_STATUS_CODE iservice_result,
										ishidaopcua_DIAGNOSTIC_INFO* iservice_diagnostics, ishidaopcua_ARRAY* istring_table,
										ishidaopcua_EXTENSION_OBJECT* iadditional_header, ishidaopcua_RESPONSE_HEADER* iresponse_header);

ishidaeutz_LARGE_INT*ishidaopcua_get_response_header_timestamp(ishidaopcua_RESPONSE_HEADER* iresponse_header);

ishidaopcua_UINT32 ishidaopcua_get_response_header_request_handle(ishidaopcua_RESPONSE_HEADER* iresponse_header);

ishidaopcua_STATUS_CODE ishidaopcua_get_response_header_service_result(ishidaopcua_RESPONSE_HEADER* iresponse_header);

ishidaopcua_DIAGNOSTIC_INFO* ishidaopcua_get_response_header_service_diagnostics(ishidaopcua_RESPONSE_HEADER* iresponse_header);

ishidaopcua_ARRAY* ishidaopcua_get_response_header_string_table(ishidaopcua_RESPONSE_HEADER* iresponse_header);

ishidaopcua_EXTENSION_OBJECT* ishidaopcua_get_response_header_additional_header(ishidaopcua_RESPONSE_HEADER* iresponse_header);

ishidaopcua_SECURITY_TOKEN* ishidaopcua_init_security_token();

void ishidaopcua_set_security_token_secure_channel_id(ishidaopcua_UINT32 isecure_channel_id, ishidaopcua_SECURITY_TOKEN* isecurity_token);

void ishidaopcua_set_security_token_token_id(ishidaopcua_UINT32 itoken_id, ishidaopcua_SECURITY_TOKEN* isecurity_token);

void ishidaopcua_set_security_token_created_at(ishidaopcua_UTC_TIME* ipcreated_at, ishidaopcua_SECURITY_TOKEN* isecurity_token);

void ishidaopcua_set_security_token_revised_lifetime(ishidaopcua_UINT32 irevised_lifetime, ishidaopcua_SECURITY_TOKEN* isecurity_token);

void ishidaopcua_set_security_token(ishidaopcua_UINT32 isecure_channel_id, ishidaopcua_UINT32 itoken_id, ishidaopcua_UTC_TIME* ipcreated_at,
									ishidaopcua_UINT32 irevised_lifetime, ishidaopcua_SECURITY_TOKEN* isecurity_token);

ishidaopcua_UINT32 ishidaopcua_get_security_token_secure_channel_id(ishidaopcua_SECURITY_TOKEN* isecurity_token);

ishidaopcua_UINT32 ishidaopcua_get_security_token_token_id(ishidaopcua_SECURITY_TOKEN* isecurity_token);

ishidaopcua_UTC_TIME* ishidaopcua_get_security_token_created_at(ishidaopcua_SECURITY_TOKEN* isecurity_token);

ishidaopcua_UINT32 ishidaopcua_get_security_token_revised_lifetime(ishidaopcua_SECURITY_TOKEN* isecurity_token);

ishidaopcua_SIGNATURE* ishidaopcua_init_signature();

void ishidaopcua_set_signature_algorithm(ishidaopcua_STRING* ialgorithm, ishidaopcua_SIGNATURE* isignature);

void ishidaopcua_set_signature_signature(ishidaopcua_STRING* isignature_string, ishidaopcua_SIGNATURE* isignature);

void ishidaopcua_set_signature(ishidaopcua_STRING* ialgorithm, ishidaopcua_STRING* isignature_string, ishidaopcua_SIGNATURE* isignature);

ishidaopcua_STRING* ishidaopcua_get_signature_algorithm(ishidaopcua_SIGNATURE* isignature);

ishidaopcua_STRING* ishidaopcua_get_signature_signature(ishidaopcua_SIGNATURE* isignature);

ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* ishidaopcua_init_signed_software_certificate();

void ishidaopcua_set_signed_software_certificate_certificate_data(ishidaopcua_STRING* icertificate_data, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate);

void ishidaopcua_set_signed_software_certificate_signature(ishidaopcua_STRING* isignature, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate);

void ishidaopcua_set_signed_software_certificate(ishidaopcua_STRING* icertificate_data, ishidaopcua_STRING* isignature, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate);

ishidaopcua_STRING* ishidaopcua_get_signed_software_certificate_certificate_data(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate);

ishidaopcua_STRING* ishidaopcua_get_signed_software_certificate_signature(ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* isigned_software_certificate);

ishidaopcua_APPLICATION_DESCRIPTION* ishidaopcua_init_application_description();

void ishidaopcua_set_application_description_application_uri(ishidaopcua_STRING* iapplication_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

void ishidaopcua_set_application_description_product_uri(ishidaopcua_STRING* iproduct_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

void ishidaopcua_set_application_description_application_name(ishidaopcua_LOCALIZED_TEXT* iapplication_name, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

void ishidaopcua_set_application_description_application_type(ishidaopcua_UINT32 iapplication_type, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

void ishidaopcua_set_application_description_gateway_server_uri(ishidaopcua_STRING* igateway_server_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

void ishidaopcua_set_application_description_discovery_profile_uri(ishidaopcua_STRING* idiscovery_profile_uri, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

void ishidaopcua_set_application_description_discovery_urls(ishidaopcua_UINT32 idiscovery_urls_size, ishidaopcua_STRING* idiscovery_urls, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

void ishidaopcua_set_application_description(ishidaopcua_STRING* iapplication_uri, ishidaopcua_STRING* iproduct_uri, ishidaopcua_LOCALIZED_TEXT* iapplication_name,
												ishidaopcua_UINT32 iapplication_type, ishidaopcua_STRING* igateway_server_uri, ishidaopcua_STRING* idiscovery_profile_uri,
												ishidaopcua_UINT32 idiscovery_urls_size, ishidaopcua_STRING* idiscovery_urls, ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_STRING* ishidaopcua_get_application_description_application_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_STRING* ishidaopcua_get_application_description_product_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_get_application_description_application_name(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_UINT32 ishidaopcua_get_application_description_application_type(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_STRING* ishidaopcua_get_application_description_gateway_server_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_STRING* ishidaopcua_get_application_description_discovery_profile_uri(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_ARRAY* ishidaopcua_get_application_description_discovery_urls(ishidaopcua_APPLICATION_DESCRIPTION* iapplication_description);

ishidaopcua_STRUCTURE* ishidaopcua_init_structure();

void ishidaopcua_set_structure_type_id(ishidaopcua_UINT32 itype_id, ishidaopcua_STRUCTURE* istructure);

void ishidaopcua_set_structure_encoding(ishidaopcua_BYTE iencoding, ishidaopcua_STRUCTURE* istructure);

void ishidaopcua_set_structure_length(ishidaopcua_UINT32 ilength, ishidaopcua_STRUCTURE* istructure);

void ishidaopcua_set_structure_data(ishidaopcua_BYTE* idata, ishidaopcua_STRUCTURE* istructure);

void ishidaopcua_set_structure(ishidaopcua_UINT32 itype_id, ishidaopcua_BYTE iencoding, ishidaopcua_UINT32 ilength,
								ishidaopcua_BYTE* idata, ishidaopcua_STRUCTURE* istructure);

ishidaopcua_UINT32 ishidaopcua_get_structure_type_id(ishidaopcua_STRUCTURE* istructure);

ishidaopcua_BYTE ishidaopcua_get_structure_encoding(ishidaopcua_STRUCTURE* istructure);

ishidaopcua_UINT32 ishidaopcua_get_structure_length(ishidaopcua_STRUCTURE* istructure);

ishidaopcua_BYTE* ishidaopcua_get_structure_data(ishidaopcua_STRUCTURE* istructure);

ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* ishidaopcua_init_application_instance_certificate();

void ishidaopcua_set_application_instance_certificate_version(ishidaopcua_STRING* iversion, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_serial_no(ishidaopcua_BYTESTRING* iserial_no, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_signature_algorithm(ishidaopcua_STRING* isignature_algorithm, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_signature(ishidaopcua_BYTESTRING* isignature, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_issuer(ishidaopcua_STRUCTURE* iissuer, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_valid_from(ishidaopcua_UTC_TIME* ipvalid_from, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_valid_to(ishidaopcua_UTC_TIME* ipvalid_to, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_subject(ishidaopcua_STRUCTURE* isubject, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_application_uri(ishidaopcua_STRING* iapplication_uri, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_hostnames(ishidaopcua_UINT32 ihostnames_size, ishidaopcua_STRING* ihostnames, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_public_key(ishidaopcua_BYTESTRING* ipublic_key, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate_key_usage(ishidaopcua_UINT32 ikey_usage_size, ishidaopcua_STRING* ikey_usage, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

void ishidaopcua_set_application_instance_certificate(ishidaopcua_STRING* iversion, ishidaopcua_BYTESTRING* iserial_no, ishidaopcua_STRING* isignature_algorithm, ishidaopcua_BYTESTRING* isignature,
														ishidaopcua_STRUCTURE* iissuer, ishidaopcua_UTC_TIME* ipvalid_from, ishidaopcua_UTC_TIME* ipvalid_to, ishidaopcua_STRUCTURE* isubject, ishidaopcua_STRING* iapplication_uri,
														ishidaopcua_UINT32 ihostnames_size, ishidaopcua_STRING* ihostnames, ishidaopcua_BYTESTRING* ipublic_key,
														ishidaopcua_UINT32 ikey_usage_size, ishidaopcua_STRING* ikey_usage, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_STRING* ishidaopcua_get_application_instance_certificate_version(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_BYTESTRING* ishidaopcua_get_application_instance_certificate_serial_no(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_STRING* ishidaopcua_get_application_instance_certificate_signature_algorithm(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_BYTESTRING* ishidaopcua_get_application_instance_certificate_signature(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_STRUCTURE* ishidaopcua_get_application_instance_certificate_issuer(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_UTC_TIME* ishidaopcua_get_application_instance_certificate_valid_from(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_UTC_TIME* ishidaopcua_get_application_instance_certificate_valid_to(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_STRUCTURE* ishidaopcua_get_application_instance_certificate_subject(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_STRING* ishidaopcua_get_application_instance_certificate_application_uri(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_ARRAY* ishidaopcua_get_application_instance_certificate_hostnames(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_BYTESTRING* ishidaopcua_get_application_instance_certificate_public_key(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_ARRAY* ishidaopcua_get_application_instance_certificate_key_usage(ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* iapplication_instance_certificate);

ishidaopcua_USER_TOKEN_POLICY* ishidaopcua_init_user_token_policy();

void ishidaopcua_set_user_token_policy_policy_id(ishidaopcua_STRING* ipolicy_id, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

void ishidaopcua_set_user_token_policy_token_type(ishidaopcua_UINT32 itoken_type, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

void ishidaopcua_set_user_token_policy_issued_token_type(ishidaopcua_STRING* iissued_token_type, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

void ishidaopcua_set_user_token_policy_issuer_endpoint_url(ishidaopcua_STRING* iissuer_endpoint_url, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

void ishidaopcua_set_user_token_policy_security_policy_uri(ishidaopcua_STRING* isecurity_policy_uri, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

void ishidaopcua_set_user_token_policy(ishidaopcua_STRING* ipolicy_id, ishidaopcua_UINT32 itoken_type, ishidaopcua_STRING* iissued_token_type, ishidaopcua_STRING* iissuer_endpoint_url,
										ishidaopcua_STRING* isecurity_policy_uri, ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_policy_id(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

ishidaopcua_UINT32 ishidaopcua_get_user_token_policy_token_type(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_issued_token_type(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_issuer_endpoint_url(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

ishidaopcua_STRING* ishidaopcua_get_user_token_policy_security_policy_uri(ishidaopcua_USER_TOKEN_POLICY* iuser_token_policy);

ishidaopcua_ENDPOINT_DESCRIPTION* ishidaopcua_init_endpoint_description();

void ishidaopcua_set_endpoint_description_endpoint_url(ishidaopcua_STRING* iendpoint_url, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

void ishidaopcua_set_endpoint_description_server(ishidaopcua_APPLICATION_DESCRIPTION* iserver, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

void ishidaopcua_set_endpoint_description_server_certificate(ishidaopcua_BYTESTRING* iserver_certificate, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

void ishidaopcua_set_endpoint_description_security_mode(ishidaopcua_UINT32 isecurity_mode, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

void ishidaopcua_set_endpoint_description_security_policy_uri(ishidaopcua_STRING* isecurity_policy_uri, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

void ishidaopcua_set_endpoint_description_user_identity_tokens(ishidaopcua_UINT32 iuser_identity_tokens_size, ishidaopcua_USER_TOKEN_POLICY* iuser_identity_tokens, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

void ishidaopcua_set_endpoint_description_transport_profile_uri(ishidaopcua_STRING* itransport_profile_uri, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

void ishidaopcua_set_endpoint_description_security_level(ishidaopcua_BYTE isecurity_level, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_STRING* ishidaopcua_get_endpoint_description_endpoint_url(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_APPLICATION_DESCRIPTION* ishidaopcua_get_endpoint_description_server(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_BYTESTRING* ishidaopcua_get_endpoint_description_server_certificate(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_UINT32 ishidaopcua_get_endpoint_description_security_mode(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_STRING* ishidaopcua_get_endpoint_description_security_policy_uri(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_ARRAY* ishidaopcua_get_endpoint_description_user_identity_tokens(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_STRING* ishidaopcua_get_endpoint_description_transport_profile_uri(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_BYTE ishidaopcua_get_endpoint_description_security_level(ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description);

ishidaopcua_READ_VALUE_ID* ishidaopcua_init_read_value_id();

void ishidaopcua_set_read_value_id_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_READ_VALUE_ID* iread_value_id);

void ishidaopcua_set_read_value_id_attribute_id(ishidaopcua_UINT32 iattribute_id, ishidaopcua_READ_VALUE_ID* iread_value_id);

void ishidaopcua_set_read_value_id_index_range(ishidaopcua_STRING* iindex_range, ishidaopcua_READ_VALUE_ID* iread_value_id);

void ishidaopcua_set_read_value_id_data_encoding(ishidaopcua_QUALIFIED_NAME* idata_encoding, ishidaopcua_READ_VALUE_ID* iread_value_id);

void ishidaopcua_set_read_value_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_UINT32 iattribute_id, ishidaopcua_STRING* iindex_range,
									ishidaopcua_QUALIFIED_NAME* idata_encoding, ishidaopcua_READ_VALUE_ID* iread_value_id);

ishidaopcua_NODE_ID* ishidaopcua_get_read_value_id_node_id(ishidaopcua_READ_VALUE_ID* iread_value_id);

ishidaopcua_UINT32 ishidaopcua_get_read_value_id_attribute_id(ishidaopcua_READ_VALUE_ID* iread_value_id);

ishidaopcua_STRING* ishidaopcua_get_read_value_id_index_range(ishidaopcua_READ_VALUE_ID* iread_value_id);

ishidaopcua_QUALIFIED_NAME* ishidaopcua_get_read_value_id_data_encoding(ishidaopcua_READ_VALUE_ID* iread_value_id);

ishidaopcua_NODE* ishidaopcua_node_init(ishidaopcua_UINT32 inum_attributes, ishidaopcua_UINT32 inum_references, ishidaopcua_UINT32 inum_standard_properties);

void ishidaopcua_node_set_value(ishidaopcua_VARIANT* ivalue, ishidaopcua_NODE* inode);

ishidaopcua_VARIANT* ishidaopcua_node_get_value(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_data_type(ishidaopcua_NODE_ID* idata_type, ishidaopcua_NODE* inode);

ishidaopcua_NODE_ID* ishidaopcua_node_get_data_type(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_NODE* inode);

ishidaopcua_NODE_ID* ishidaopcua_node_get_node_id(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_node_class(ishidaopcua_NODE_CLASS* inode_class, ishidaopcua_NODE* inode);

ishidaopcua_NODE_CLASS* ishidaopcua_node_get_node_class(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_browse_name(ishidaopcua_QUALIFIED_NAME* ibrowse_name, ishidaopcua_NODE* inode);

ishidaopcua_QUALIFIED_NAME* ishidaopcua_node_get_browse_name(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_display_name(ishidaopcua_LOCALIZED_TEXT* idisplay_name, ishidaopcua_NODE* inode);

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_node_get_display_name(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_description(ishidaopcua_LOCALIZED_TEXT* idescription, ishidaopcua_NODE* inode);

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_node_get_description(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_write_mask(ishidaopcua_UINT32* iwrite_mask, ishidaopcua_NODE* inode);

ishidaopcua_UINT32* ishidaopcua_node_get_write_mask(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_user_write_mask(ishidaopcua_UINT32* iuser_write_mask, ishidaopcua_NODE* inode);

ishidaopcua_UINT32* ishidaopcua_node_get_user_write_mask(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_is_abstract(ishidaopcua_BOOLEAN* iis_abstract, ishidaopcua_NODE* inode);

ishidaopcua_BOOLEAN* ishidaopcua_node_get_is_abstract(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_symmetric(ishidaopcua_BOOLEAN* isymmetric, ishidaopcua_NODE* inode);

ishidaopcua_BOOLEAN* ishidaopcua_node_get_symmetric(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_inverse_name(ishidaopcua_LOCALIZED_TEXT* iinverse_name, ishidaopcua_NODE* inode);

ishidaopcua_LOCALIZED_TEXT* ishidaopcua_node_get_inverse_name(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_contains_no_loops(ishidaopcua_BOOLEAN* icontains_no_loops, ishidaopcua_NODE* inode);

ishidaopcua_BOOLEAN* ishidaopcua_node_get_contains_no_loops(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_event_notifier(ishidaopcua_BYTE* ievent_notifier, ishidaopcua_NODE* inode);

ishidaopcua_BYTE* ishidaopcua_node_get_event_notifier(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_value_rank(ishidaopcua_INT32* ivalue_rank, ishidaopcua_NODE* inode);

ishidaopcua_INT32* ishidaopcua_node_get_value_rank(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_array_dimensions(ishidaopcua_ARRAY* iparray_dimensions, ishidaopcua_NODE* inode);

ishidaopcua_ARRAY* ishidaopcua_node_get_array_dimensions(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_access_level(ishidaopcua_BYTE* iaccess_level, ishidaopcua_NODE* inode);

ishidaopcua_BYTE* ishidaopcua_node_get_access_level(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_user_access_level(ishidaopcua_BYTE* iuser_access_level, ishidaopcua_NODE* inode);

ishidaopcua_BYTE* ishidaopcua_node_get_user_access_level(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_minimum_sampling_interval(ishidaopcua_DURATION* iminimum_sampling_interval, ishidaopcua_NODE* inode);

ishidaopcua_DURATION* ishidaopcua_node_get_minimum_sampling_interval(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_historizing(ishidaopcua_BOOLEAN* ihistorizing, ishidaopcua_NODE* inode);

ishidaopcua_BOOLEAN* ishidaopcua_node_get_historizing(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_executable(ishidaopcua_BOOLEAN* iexecutable, ishidaopcua_NODE* inode);

ishidaopcua_BOOLEAN* ishidaopcua_node_get_executable(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_user_executable(ishidaopcua_BOOLEAN* iuser_executable, ishidaopcua_NODE* inode);

ishidaopcua_BOOLEAN* ishidaopcua_node_get_user_executable(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_reference_type_id(ishidaopcua_UINT32* ireference_type_id, ishidaopcua_NODE* inode);

ishidaopcua_UINT32* ishidaopcua_node_get_reference_type_id(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_target_id(ishidaopcua_UINT32* itarget_id, ishidaopcua_NODE* inode);

ishidaopcua_UINT32* ishidaopcua_node_get_target_id(ishidaopcua_NODE* inode);

void ishidaopcua_node_set_is_inverse(ishidaopcua_UINT32* iis_inverse, ishidaopcua_NODE* inode);

ishidaopcua_UINT32* ishidaopcua_node_get_is_inverse(ishidaopcua_NODE* inode);

ishidaopcua_VIEW_DESCRIPTION* ishidaopcua_init_view_description();

void ishidaopcua_set_view_description_pview_id(ishidaopcua_NODE_ID* ipview_id, ishidaopcua_VIEW_DESCRIPTION* iview_description);

void ishidaopcua_set_view_description_timestamp(ishidaopcua_UTC_TIME* iptimestamp, ishidaopcua_VIEW_DESCRIPTION* iview_description);

void ishidaopcua_set_view_description_view_version(ishidaopcua_UINT32 iview_version, ishidaopcua_VIEW_DESCRIPTION* iview_description);

void ishidaopcua_set_view_description(ishidaopcua_NODE_ID* ipview_id, ishidaopcua_UTC_TIME* iptimestamp,  ishidaopcua_UINT32 iview_version, ishidaopcua_VIEW_DESCRIPTION* iview_description);

ishidaopcua_NODE_ID* ishidaopcua_get_view_description_pview_id(ishidaopcua_VIEW_DESCRIPTION* iview_description);

ishidaopcua_UTC_TIME* ishidaopcua_get_view_description_timestamp(ishidaopcua_VIEW_DESCRIPTION* iview_description);

ishidaopcua_UINT32 ishidaopcua_get_view_description_view_version(ishidaopcua_VIEW_DESCRIPTION* iview_description);

ishidaopcua_BROWSE_DESCRIPTION* ishidaopcua_init_browse_description();

void ishidaopcua_set_browse_description_pnode_id(ishidaopcua_NODE_ID* ipnode_id, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

void ishidaopcua_set_browse_description_browse_direction(ishidaopcua_UINT32 ibrowse_direction, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

void ishidaopcua_set_browse_description_preference_type_id(ishidaopcua_NODE_ID* ipreference_type_id, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

void ishidaopcua_set_browse_description_include_subtypes(ishidaopcua_BOOLEAN iinclude_subtypes, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

void ishidaopcua_set_browse_description_node_class_mask(ishidaopcua_UINT32 inode_class_mask, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

void ishidaopcua_set_browse_description_result_class_mask(ishidaopcua_UINT32 iresult_class_mask, ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

void ishidaopcua_set_browse_description(ishidaopcua_NODE_ID* ipnode_id, ishidaopcua_UINT32 ibrowse_direction, ishidaopcua_NODE_ID* ipreference_type_id, ishidaopcua_BOOLEAN iinclude_subtypes,
										ishidaopcua_UINT32 inode_class_mask, ishidaopcua_UINT32 iresult_class_mask,  ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

ishidaopcua_NODE_ID* ishidaopcua_get_view_description_pnode_id(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

ishidaopcua_UINT32 ishidaopcua_get_view_description_browse_direction(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

ishidaopcua_NODE_ID* ishidaopcua_get_view_description_preference_type_id(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

ishidaopcua_BOOLEAN ishidaopcua_get_view_description_include_subtypes(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

ishidaopcua_UINT32 ishidaopcua_get_view_description_node_class_mask(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

ishidaopcua_UINT32 ishidaopcua_get_view_description_result_class_mask(ishidaopcua_BROWSE_DESCRIPTION* ibrowse_description);

ishidaopcua_BROWSE_RESULT* ishidaopcua_init_browse_result();

ishidaopcua_WRITE_VALUE* ishidaopcua_init_write_value();

void ishidaopcua_set_write_value_node_id(ishidaopcua_NODE_ID* inode_id, ishidaopcua_WRITE_VALUE* iwrite_value);

void ishidaopcua_set_write_value_attribute_id(ishidaopcua_INTEGER_ID iattribute_id, ishidaopcua_WRITE_VALUE* iwrite_value);

void ishidaopcua_set_write_value_index_range(ishidaopcua_NUMERIC_RANGE* iindex_range, ishidaopcua_WRITE_VALUE* iwrite_value);

void ishidaopcua_set_write_value_value(ishidaopcua_DATA_VALUE* ivalue, ishidaopcua_WRITE_VALUE* iwrite_value);

void ishidaopcua_set_write_value(ishidaopcua_NODE_ID* inode_id, ishidaopcua_INTEGER_ID iattribute_id, ishidaopcua_NUMERIC_RANGE* iindex_range,
									ishidaopcua_DATA_VALUE* ivalue, ishidaopcua_WRITE_VALUE* iwrite_value);

ishidaopcua_NODE_ID* ishidaopcua_get_write_value_node_id(ishidaopcua_WRITE_VALUE* iwrite_value);

ishidaopcua_INTEGER_ID ishidaopcua_get_write_value_attribute_id(ishidaopcua_WRITE_VALUE* iwrite_value);

ishidaopcua_NUMERIC_RANGE* ishidaopcua_get_write_value_index_range(ishidaopcua_WRITE_VALUE* iwrite_value);

ishidaopcua_DATA_VALUE* ishidaopcua_get_write_value_value(ishidaopcua_WRITE_VALUE* iwrite_value);

ishidaopcua_REFERENCE_DESCRIPTION* ishidaopcua_init_reference_description();

ishidaopcua_TCP_MESSAGE_HEADER* ishidaopcua_init_tcp_message_header();

void ishidaopcua_set_tcp_message_header_message_type(ishidaopcua_BYTE imessage_type[3], ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header);

void ishidaopcua_set_tcp_message_header_reserved(ishidaopcua_BYTE ireserved, ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header);

void ishidaopcua_set_tcp_message_header_message_size(ishidaopcua_UINT32 imessage_size, ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header);

void ishidaopcua_set_tcp_message_header(ishidaopcua_BYTE imessage_type[3], ishidaopcua_BYTE ireserved, ishidaopcua_UINT32 imessage_size, ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header);

ishidaopcua_BYTE* ishidaopcua_get_tcp_message_header_message_type(ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header);

ishidaopcua_BYTE ishidaopcua_get_tcp_message_header_reserved(ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header);

ishidaopcua_UINT32 ishidaopcua_get_tcp_message_header_message_size(ishidaopcua_TCP_MESSAGE_HEADER* itcp_message_header);

void* ishidaopcua_malloc(ishidaopcua_TYPE_ID itype_id, ishidaopcua_UINT32 isize_factor);
/*************************************** ISHIDAOPCUA_CUSTOM_TYPES END ***************************************/

/*************************************** ISHIDAOPCUA_ENCODE_DECODER_PAIRS START ***************************************/
void ishidaopcua_decode_BYTE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BYTE* ipbyte, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_BYTE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_BYTE* ipbyte, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_SBYTE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SBYTE* ipsbyte, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_SBYTE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SBYTE* ipsbyte, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_BOOLEAN(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BOOLEAN* ipboolean, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_BOOLEAN(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BOOLEAN* ipboolean, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_UINT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT16* ipuint16, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_UINT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT16* ipuint16, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_INT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT16* ipint16, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_INT16(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT16* ipint16, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_UINT32(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UINT32* ipuint32, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_UINT32(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UINT32* ipuint32, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_INT32(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT32* ipint32, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_INT32(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_INT32* ipint32, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_UINT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT64* ipuint64, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_UINT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_UINT64* ipuint64, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_INT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT64* ipint64, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_INT64(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_INT64* ipint64, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_FLOAT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_FLOAT* ipfloat, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_FLOAT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_FLOAT* ipfloat, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_DOUBLE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DOUBLE* ipdouble, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_DOUBLE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DOUBLE* ipdouble, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_X_ARRAY(ishidaopcua_BYTE *ipdata_buffer, void* ipx_array, ishidaopcua_TYPE_ID ix_array_type_id, ishidaopcua_UINT32 ix_array_target_size, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_X_ARRAY(ishidaopcua_BYTE *ipdata_buffer, void* ipx_array, ishidaopcua_TYPE_ID ix_array_type_id, ishidaopcua_UINT32 ix_array_target_size, ishidaopcua_UINT32 *iwrite_size);

void ishidaopcua_decode_ARRAY(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_ARRAY* iparray, ishidaopcua_TYPE_ID iarray_type_id, ishidaopcua_UINT32 iarray_target_size, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_ARRAY(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_ARRAY* iparray, ishidaopcua_TYPE_ID iarray_type_id, ishidaopcua_UINT32 iarray_target_size, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_STRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_STRING* ipstring, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_STRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_STRING* ipstring, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_DATE_TIME(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DATE_TIME* ipdate_time, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_DATE_TIME(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_DATE_TIME* ipdate_time, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_GUID(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_GUID* ipguid, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_GUID(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_GUID* ipguid, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_BYTESTRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_BYTESTRING* ipbytestring, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_BYTESTRING(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_BYTESTRING* ipbytestring, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_XML_ELEMENT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_XML_ELEMENT* ipxml_element, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_XML_ELEMENT(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_XML_ELEMENT* ipxml_element, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_NODE_ID* ipnode_id,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_NODE_ID* ipnode_id,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_EXPANDED_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXPANDED_NODE_ID* ipexpanded_node_id,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_EXPANDED_NODE_ID(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXPANDED_NODE_ID* ipexpanded_node_id,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_STATUS_CODE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STATUS_CODE* ipstatus_code,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_STATUS_CODE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STATUS_CODE* ipstatus_code,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_QUALIFIED_NAME(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_QUALIFIED_NAME* ipqualified_name,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_QUALIFIED_NAME(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_QUALIFIED_NAME* ipqualified_name,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_LOCALIZED_TEXT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_LOCALIZED_TEXT* iplocalized_text,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_LOCALIZED_TEXT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_LOCALIZED_TEXT* iplocalized_text,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_EXTENSION_OBJECT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXTENSION_OBJECT* ipextension_object,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_EXTENSION_OBJECT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_EXTENSION_OBJECT* ipextension_object,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_VARIANT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_VARIANT* ipvariant, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_VARIANT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_VARIANT* ipvariant, ishidaopcua_TYPE_ID itype_id, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_DATA_VALUE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DATA_VALUE* ipdata_value,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_DATA_VALUE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DATA_VALUE* ipdata_value,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_encode_REFERENCE_DESCRIPTION(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_REFERENCE_DESCRIPTION* ipreference_description, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_REFERENCE_DESCRIPTION(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_REFERENCE_DESCRIPTION* ipreference_description, ishidaopcua_UINT32 *ipwrite_size);

void ishidaopcua_decode_BROWSE_RESULT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_BROWSE_RESULT* ipbrowse_result,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_encode_BROWSE_RESULT(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_BROWSE_RESULT* ipbrowse_result,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_DIAGNOSTIC_INFO(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DIAGNOSTIC_INFO* ipdiagnostic_info,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_DIAGNOSTIC_INFO(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_DIAGNOSTIC_INFO* ipdiagnostic_info,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_MESSAGE_SECURITY_MODE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_MESSAGE_SECURITY_MODE* ipmessage_security_mode, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_MESSAGE_SECURITY_MODE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_MESSAGE_SECURITY_MODE* ipmessage_security_mode, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_USER_IDENTITY_TOKEN_TYPE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_IDENTITY_TOKEN_TYPE* ipuser_identity_token, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_USER_IDENTITY_TOKEN_TYPE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_IDENTITY_TOKEN_TYPE* ipuser_identity_token, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_UTC_TIMESTAMP_MS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME *iputc_time, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_UTC_TIMESTAMP_MS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME *iputc_time, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_UTC_TIMESTAMP_NS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME* iputc_time, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_UTC_TIMESTAMP_NS(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_UTC_TIME *iputc_time, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_DURATION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_DURATION* ipduration, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_DURATION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_DURATION* ipduration, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_REQUEST_HEADER(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_REQUEST_HEADER* iprequest_header,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_REQUEST_HEADER(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_REQUEST_HEADER* iprequest_header,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_RESPONSE_HEADER(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_RESPONSE_HEADER* ipresponse_header, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_RESPONSE_HEADER(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_RESPONSE_HEADER* ipresponse_header, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_SECURITY_TOKEN(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SECURITY_TOKEN* ipsecurity_token, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_SECURITY_TOKEN(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SECURITY_TOKEN* ipsecurity_token, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_SIGNATURE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNATURE* ipsignature, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_SIGNATURE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNATURE* ipsignature, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_SIGNED_SOFTWARE_CERTIFICATE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* ipsigned_software_certificate, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_SIGNED_SOFTWARE_CERTIFICATE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_SIGNED_SOFTWARE_CERTIFICATE* ipsigned_software_certificate, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_APPLICATION_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_APPLICATION_DESCRIPTION* ipapplication_description,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_APPLICATION_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_APPLICATION_DESCRIPTION* ipapplication_description,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_STRUCTURE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STRUCTURE* ipstructure,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_STRUCTURE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_STRUCTURE* ipstructure,ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_APPLICATION_INSTANCE_CERTIFICATE(ishidaopcua_BYTE* ipdata_buffer,ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* ipapplication_instance_certificate,ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_APPLICATION_INSTANCE_CERTIFICATE(ishidaopcua_BYTE *ipdata_buffer, ishidaopcua_APPLICATION_INSTANCE_CERTIFICATE* ipapplication_instance_certificate, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_USER_TOKEN_POLICY(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_TOKEN_POLICY* ipuser_token_policy, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_USER_TOKEN_POLICY(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_USER_TOKEN_POLICY* ipuser_token_policy, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_decode_ENDPOINT_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_ENDPOINT_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_ENDPOINT_DESCRIPTION* iendpoint_description, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_decode_READ_VALUE_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_READ_VALUE_ID* iread_value_id, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_READ_VALUE_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_READ_VALUE_ID* iread_value_id, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_decode_VIEW_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_VIEW_DESCRIPTION* ipview_description, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_VIEW_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_VIEW_DESCRIPTION* ipview_description, ishidaopcua_UINT32* ipwrite_size);

void ishidaopcua_decode_COUNTER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_COUNTER* ipcounter, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_COUNTER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_COUNTER* ipcounter, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_decode_BROWSE_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_BROWSE_DESCRIPTION(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_UINT32 *iwrite_size);

void ishidaopcua_decode_INTEGER_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_INTEGER_ID* iinteger_id, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_INTEGER_ID(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_INTEGER_ID* iinteger_id, ishidaopcua_UINT32 *iwrite_size);

void ishidaopcua_decode_NUMERIC_RANGE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_NUMERIC_RANGE* inumeric_range, ishidaopcua_UINT32 *ipparsing_offset);

void ishidaopcua_encode_NUMERIC_RANGE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_NUMERIC_RANGE* inumeric_range, ishidaopcua_UINT32 *iwrite_size);

void ishidaopcua_decode_WRITE_VALUE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_WRITE_VALUE* iwrite_value, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_WRITE_VALUE(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_WRITE_VALUE* iwrite_value, ishidaopcua_UINT32* iwrite_size);

void ishidaopcua_decode_TCP_MESSAGE_HEADER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_TCP_MESSAGE_HEADER* imessage_header, ishidaopcua_UINT32* ipparsing_offset);

void ishidaopcua_encode_TCP_MESSAGE_HEADER(ishidaopcua_BYTE* ipdata_buffer, ishidaopcua_TCP_MESSAGE_HEADER* ipmessage_header, ishidaopcua_UINT32* ipwrite_size);
/*************************************** ISHIDAOPCUA_ENCODE_DECODER_PAIRS END ***************************************/

/*************************************** ISHIDAOPCUA_BINAR_SERVER START ***************************************/
typedef struct ishidaopcua_BINARY_SERVER_CLIENT_ARGS ishidaopcua_BINARY_SERVER_CLIENT_ARGS;
struct ishidaopcua_BINARY_SERVER_CLIENT_ARGS{
	struct sockaddr_in *client_details;
	ishidaopcua_UINT32 request_id;
	ishidaopcua_UINT32 sequence_number;
	ishidaopcua_UINT32 session_id;
	ishidaopcua_UINT64* session_authentication_token;
	ishidaopcua_SECURITY_TOKEN* security_token;
};
typedef struct ishidaopcua_BINARY_SERVER_CONNECTION_ARGS ishidaopcua_BINARY_SERVER_CONNECTION_ARGS;
struct ishidaopcua_BINARY_SERVER_CONNECTION_ARGS{
	int sock;
	struct sockaddr_in *client_details;
	size_t *size;
	const char *ip_address;
	int file_descriptor;
};

typedef struct ishidaopcua_BINARY_SERVER_ARGS ishidaopcua_BINARY_SERVER_ARGS;
struct ishidaopcua_BINARY_SERVER_ARGS{
	const char *server_ip_addr;
	int server_port;
};

void ishidaopcua_binary_server_init(ishidaopcua_BINARY_SERVER_ARGS ibinary_server_args);
/*************************************** ISHIDAOPCUA_BINAR_SERVER END ***************************************/

/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
extern ishidaeutz_HASHMAP* ishidaopcua_client_session_hmap;

ishidaopcua_NODE* fetch_node_1(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_2(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_3(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_4(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_5(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_6(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_7(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_8(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_9(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_10(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_11(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_12(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_13(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_14(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_15(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_16(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_17(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_18(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_19(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node_20(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
ishidaopcua_NODE* fetch_node(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag);
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/

/*************************************** ISHIDAOPCUA_MAIN START ***************************************/
ishidaopcua_BOOLEAN qualifies_node_class_mask(ishidaopcua_UINT32* pinode_class_mask,ishidaopcua_UINT32* pinode_class);

void ishidaopcua_prepare_response(ishidaopcua_BYTE* ioutput_buffer, ishidaopcua_BYTE* imessage_type, ishidaopcua_BINARY_SERVER_CLIENT_ARGS* iclient_args, ishidaopcua_UINT32* iwrite_size);

ishidaopcua_VARIANT* ishidaopcua_get_attribute(ishidaopcua_READ_VALUE_ID iread_value_id, ishidaeutz_HASHMAP* ipcache);

ishidaopcua_BOOLEAN is_hierarchichal_reference(ishidaopcua_UINT32 reference_type);

char* ishidaopcua_convert_reference_description(ishidaopcua_UINT32 reference_type_id, ishidaopcua_UINT32 direction);

ishidaopcua_UINT32 is_forward_browse_direction(char* key_name);

void process_hiearchical_references(ishidaopcua_NODE* pnode_from_address_space, ishidaopcua_UINT32* pstatus_code, ishidaopcua_ARRAY* preference_description_array,ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_BOOLEAN hierarchy_filter, ishidaeutz_HASHMAP* ipcache);

ishidaopcua_ARRAY* ishidaopcua_get_reference_description(ishidaopcua_BROWSE_DESCRIPTION* ipbrowse_description, ishidaopcua_STATUS_CODE* ipstatus_code, ishidaeutz_HASHMAP* ipcache);

ishidaopcua_STATUS_CODE ishidaopcua_set_attribute(ishidaopcua_WRITE_VALUE iwrite_value);

void ishidaopcua_decode_msg(ishidaopcua_BYTE* ipinput_buffer, ishidaopcua_UINT32 iread_size,ishidaopcua_BYTE* ipoutput_buffer, ishidaopcua_UINT32* ipwrite_size,ishidaopcua_BINARY_SERVER_CLIENT_ARGS* iclient_args, ishidaopcua_BYTE* ipclose_socket);

ishidaopcua_BOOLEAN qualifies_node_class_mask(ishidaopcua_UINT32* pinode_class_mask,ishidaopcua_UINT32* pinode_class);

void initialize_reference_nodes(ishidaopcua_NODE* node);
/*************************************** ISHIDAOPCUA_MAIN END ***************************************/
#endif /* PJMTEST_H_ */
