#include "pjmtest.h"
/*************************************** ISHIDAOPCUA_SERVER_NODES START ***************************************/
ishidaopcua_NODE* fetch_node_2(ishidaopcua_UINT32 nodeid, ishidaopcua_UINT32 references_flag)
{
	ishidaopcua_VARIANT* variant = ishidaopcua_init_variant();
	ishidaopcua_NODE_ID* data_type_node_id = ishidaopcua_init_node_id();

	ishidaopcua_NODE* unsset_node = NULL;

	
	ishidaopcua_NODE* universal_node;
ishidaopcua_NODE_ID* universal_node_id; 
ishidaopcua_NODE_CLASS* universal_node_class; 
ishidaopcua_QUALIFIED_NAME* universal_browse_name;
ishidaopcua_STRING* universal_browse_name_string;
ishidaopcua_LOCALIZED_TEXT* universal_display_name;
ishidaopcua_STRING* universal_display_name_locale; 
ishidaopcua_STRING* universal_display_name_text;
ishidaopcua_LOCALIZED_TEXT* universal_description;
ishidaopcua_STRING* universal_description_locale ;
ishidaopcua_STRING* universal_description_text ;
ishidaopcua_UINT32* universal_write_mask ; 
ishidaopcua_UINT32* universal_user_write_mask ;

/*********** fields for reference node ************************/
ishidaopcua_BOOLEAN* universal_symetric;
ishidaopcua_LOCALIZED_TEXT* universal_inverse_name;
ishidaopcua_STRING *universal_inverse_name_locale;
ishidaopcua_STRING *universal_inverse_name_text;

/*********** fields for reference node ************************/

/*********** fields for objecttype node ************************/
ishidaopcua_BOOLEAN* universal_is_abstract;

/*********** fields for objecttype node ************************/

/*********** fields for variable node ************************/
ishidaopcua_INT32* universal_value_rank;
ishidaopcua_BOOLEAN* universal_historizing;
ishidaopcua_DURATION* universal_minimum_sampling_interval ;
ishidaopcua_BYTE* universal_user_access_level ;
ishidaopcua_BYTE* universal_access_level ;
ishidaopcua_STRING* universal_variable_string;
ishidaopcua_UINT32* universal_UINT32;

/*********** fields for variable node ************************/

/*********** fields for method node ************************/
ishidaopcua_BOOLEAN* universal_executable;
ishidaopcua_BOOLEAN* universal_user_executable;

/*********** fields for method node ************************/
switch(nodeid) 
{/******* FromState ********/


case 51 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 51;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("FromState", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("FromState", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for a reference to the state before a transition.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("ToTransition", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 51 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_51_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_51_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_51_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_51_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_51_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_51_retrieved_reference_45_inverse_0);
*Node_51_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_51_retrieved_reference_45_inverse_0_typed_id,Node_51_ref_node_target_id_45_inverse_0);
*Node_51_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_51_retrieved_reference_45_inverse_true_0_is_inverse, Node_51_ref_node_target_id_45_inverse_0);
*Node_51_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_51_retrieved_reference_45_inverse_0_target_id, Node_51_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_51_retrieved_reference_45_inverse_0->references,"1",Node_51_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ToState ********/


case 52 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 52;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ToState", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ToState", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for a reference to the state after a transition.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("FromTransition", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 52 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_52_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_52_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_52_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_52_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_52_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_52_retrieved_reference_45_inverse_0);
*Node_52_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_52_retrieved_reference_45_inverse_0_typed_id,Node_52_ref_node_target_id_45_inverse_0);
*Node_52_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_52_retrieved_reference_45_inverse_true_0_is_inverse, Node_52_ref_node_target_id_45_inverse_0);
*Node_52_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_52_retrieved_reference_45_inverse_0_target_id, Node_52_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_52_retrieved_reference_45_inverse_0->references,"1",Node_52_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasCause ********/


case 53 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 53;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasCause", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasCause", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for a reference to a method that can cause a transition to occur.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("MayBeCausedBy", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 53 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_53_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_53_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_53_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_53_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_53_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_53_retrieved_reference_45_inverse_0);
*Node_53_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_53_retrieved_reference_45_inverse_0_typed_id,Node_53_ref_node_target_id_45_inverse_0);
*Node_53_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_53_retrieved_reference_45_inverse_true_0_is_inverse, Node_53_ref_node_target_id_45_inverse_0);
*Node_53_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_53_retrieved_reference_45_inverse_0_target_id, Node_53_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_53_retrieved_reference_45_inverse_0->references,"1",Node_53_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasEffect ********/


case 54 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 54;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasEffect", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasEffect", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for a reference to an event that may be raised when a transition occurs.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("MayBeEffectedBy", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 54 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_54_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_54_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_54_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_54_retrieved_reference_45_inverse_0);
*Node_54_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_54_retrieved_reference_45_inverse_0_typed_id,Node_54_ref_node_target_id_45_inverse_0);
*Node_54_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_54_retrieved_reference_45_inverse_true_0_is_inverse, Node_54_ref_node_target_id_45_inverse_0);
*Node_54_retrieved_reference_45_inverse_0_target_id = 32;
ishidaopcua_node_set_target_id(Node_54_retrieved_reference_45_inverse_0_target_id, Node_54_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_54_retrieved_reference_45_inverse_0->references,"1",Node_54_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_54_retrieved_reference_45_0;
ishidaopcua_NODE* Node_54_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_54_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_54_retrieved_reference_45_0);
*Node_54_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_54_retrieved_reference_45_0_typed_id,Node_54_ref_node_target_id_45_0);
*Node_54_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_54_retrieved_reference_45_false_0_is_inverse, Node_54_ref_node_target_id_45_0);
*Node_54_retrieved_reference_45_0_target_id = 17276;
ishidaopcua_node_set_target_id(Node_54_retrieved_reference_45_0_target_id, Node_54_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_54_retrieved_reference_45_0->references,"1",Node_54_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_54_retrieved_reference_45_1;
ishidaopcua_NODE* Node_54_ref_node_target_id_45_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_54_retrieved_reference_45_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_54_retrieved_reference_45_1);
*Node_54_retrieved_reference_45_1_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_54_retrieved_reference_45_1_typed_id,Node_54_ref_node_target_id_45_1);
*Node_54_retrieved_reference_45_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_54_retrieved_reference_45_false_1_is_inverse, Node_54_ref_node_target_id_45_1);
*Node_54_retrieved_reference_45_1_target_id = 17983;
ishidaopcua_node_set_target_id(Node_54_retrieved_reference_45_1_target_id, Node_54_ref_node_target_id_45_1);
ishidaeutz_put_hashmap(Node_54_retrieved_reference_45_1->references,"2",Node_54_ref_node_target_id_45_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_54_retrieved_reference_45_2;
ishidaopcua_NODE* Node_54_ref_node_target_id_45_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_54_retrieved_reference_45_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_54_retrieved_reference_45_2);
*Node_54_retrieved_reference_45_2_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_54_retrieved_reference_45_2_typed_id,Node_54_ref_node_target_id_45_2);
*Node_54_retrieved_reference_45_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_54_retrieved_reference_45_false_2_is_inverse, Node_54_ref_node_target_id_45_2);
*Node_54_retrieved_reference_45_2_target_id = 17984;
ishidaopcua_node_set_target_id(Node_54_retrieved_reference_45_2_target_id, Node_54_ref_node_target_id_45_2);
ishidaeutz_put_hashmap(Node_54_retrieved_reference_45_2->references,"3",Node_54_ref_node_target_id_45_2);

}if(references_flag != 2){

ishidaopcua_NODE* Node_54_retrieved_reference_45_3;
ishidaopcua_NODE* Node_54_ref_node_target_id_45_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_54_retrieved_reference_45_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_54_retrieved_reference_45_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_54_retrieved_reference_45_3);
*Node_54_retrieved_reference_45_3_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_54_retrieved_reference_45_3_typed_id,Node_54_ref_node_target_id_45_3);
*Node_54_retrieved_reference_45_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_54_retrieved_reference_45_false_3_is_inverse, Node_54_ref_node_target_id_45_3);
*Node_54_retrieved_reference_45_3_target_id = 17985;
ishidaopcua_node_set_target_id(Node_54_retrieved_reference_45_3_target_id, Node_54_ref_node_target_id_45_3);
ishidaeutz_put_hashmap(Node_54_retrieved_reference_45_3->references,"4",Node_54_ref_node_target_id_45_3);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* HasHistoricalConfiguration ********/


case 56 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 56;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_REFERENCE_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("HasHistoricalConfiguration", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("HasHistoricalConfiguration", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("The type for a reference to the historical configuration for a data variable.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

universal_symetric = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_symetric = 0;
ishidaopcua_node_set_symmetric(universal_symetric, universal_node);

universal_inverse_name = ishidaopcua_init_localized_text();
universal_inverse_name_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_inverse_name_locale));
universal_inverse_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("HistoricalConfigurationOf", (universal_inverse_name_text));
ishidaopcua_set_localized_text(universal_inverse_name_locale, universal_inverse_name_text, universal_inverse_name);
ishidaopcua_node_set_inverse_name(universal_inverse_name, universal_node);
puts("finished adding node >> 56 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_56_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_56_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_56_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_56_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_56_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_56_retrieved_reference_45_inverse_0);
*Node_56_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_56_retrieved_reference_45_inverse_0_typed_id,Node_56_ref_node_target_id_45_inverse_0);
*Node_56_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_56_retrieved_reference_45_inverse_true_0_is_inverse, Node_56_ref_node_target_id_45_inverse_0);
*Node_56_retrieved_reference_45_inverse_0_target_id = 44;
ishidaopcua_node_set_target_id(Node_56_retrieved_reference_45_inverse_0_target_id, Node_56_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_56_retrieved_reference_45_inverse_0->references,"1",Node_56_ref_node_target_id_45_inverse_0);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* BuildInfo ********/


case 338 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 338;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("BuildInfo", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("BuildInfo", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 338 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_338_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_338_retrieved_reference_45_inverse_0);
*Node_338_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_45_inverse_0_typed_id,Node_338_ref_node_target_id_45_inverse_0);
*Node_338_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_45_inverse_true_0_is_inverse, Node_338_ref_node_target_id_45_inverse_0);
*Node_338_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_45_inverse_0_target_id, Node_338_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_45_inverse_0->references,"1",Node_338_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_38_0;
ishidaopcua_NODE* Node_338_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_338_retrieved_reference_38_0);
*Node_338_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_38_0_typed_id,Node_338_ref_node_target_id_38_0);
*Node_338_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_38_false_0_is_inverse, Node_338_ref_node_target_id_38_0);
*Node_338_retrieved_reference_38_0_target_id = 340;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_38_0_target_id, Node_338_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_38_0->references,"1",Node_338_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_38_1;
ishidaopcua_NODE* Node_338_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_338_retrieved_reference_38_1);
*Node_338_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_38_1_typed_id,Node_338_ref_node_target_id_38_1);
*Node_338_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_38_false_1_is_inverse, Node_338_ref_node_target_id_38_1);
*Node_338_retrieved_reference_38_1_target_id = 339;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_38_1_target_id, Node_338_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_38_1->references,"2",Node_338_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_338_retrieved_reference_38_2;
ishidaopcua_NODE* Node_338_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_338_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_338_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_338_retrieved_reference_38_2);
*Node_338_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_338_retrieved_reference_38_2_typed_id,Node_338_ref_node_target_id_38_2);
*Node_338_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_338_retrieved_reference_38_false_2_is_inverse, Node_338_ref_node_target_id_38_2);
*Node_338_retrieved_reference_38_2_target_id = 15361;
ishidaopcua_node_set_target_id(Node_338_retrieved_reference_38_2_target_id, Node_338_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_338_retrieved_reference_38_2->references,"3",Node_338_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* ServerCapabilitiesType ********/


case 2013 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 2013;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("ServerCapabilitiesType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("ServerCapabilitiesType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Describes the capabilities supported by the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

puts("finished adding node >> 2013 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_2013_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_2013_retrieved_reference_45_inverse_0);
*Node_2013_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_45_inverse_0_typed_id,Node_2013_ref_node_target_id_45_inverse_0);
*Node_2013_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_45_inverse_true_0_is_inverse, Node_2013_ref_node_target_id_45_inverse_0);
*Node_2013_retrieved_reference_45_inverse_0_target_id = 58;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_45_inverse_0_target_id, Node_2013_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_45_inverse_0->references,"1",Node_2013_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_0;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_0);
*Node_2013_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_0_typed_id,Node_2013_ref_node_target_id_46_0);
*Node_2013_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_0_is_inverse, Node_2013_ref_node_target_id_46_0);
*Node_2013_retrieved_reference_46_0_target_id = 2014;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_0_target_id, Node_2013_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_0->references,"1",Node_2013_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_1;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_1);
*Node_2013_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_1_typed_id,Node_2013_ref_node_target_id_46_1);
*Node_2013_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_1_is_inverse, Node_2013_ref_node_target_id_46_1);
*Node_2013_retrieved_reference_46_1_target_id = 2016;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_1_target_id, Node_2013_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_1->references,"2",Node_2013_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_2;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_2);
*Node_2013_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_2_typed_id,Node_2013_ref_node_target_id_46_2);
*Node_2013_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_2_is_inverse, Node_2013_ref_node_target_id_46_2);
*Node_2013_retrieved_reference_46_2_target_id = 2017;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_2_target_id, Node_2013_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_2->references,"3",Node_2013_ref_node_target_id_46_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_3;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_3);
*Node_2013_retrieved_reference_46_3_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_3_typed_id,Node_2013_ref_node_target_id_46_3);
*Node_2013_retrieved_reference_46_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_3_is_inverse, Node_2013_ref_node_target_id_46_3);
*Node_2013_retrieved_reference_46_3_target_id = 2732;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_3_target_id, Node_2013_ref_node_target_id_46_3);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_3->references,"4",Node_2013_ref_node_target_id_46_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_4;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_4);
*Node_2013_retrieved_reference_46_4_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_4_typed_id,Node_2013_ref_node_target_id_46_4);
*Node_2013_retrieved_reference_46_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_4_is_inverse, Node_2013_ref_node_target_id_46_4);
*Node_2013_retrieved_reference_46_4_target_id = 2733;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_4_target_id, Node_2013_ref_node_target_id_46_4);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_4->references,"5",Node_2013_ref_node_target_id_46_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_5;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_5);
*Node_2013_retrieved_reference_46_5_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_5_typed_id,Node_2013_ref_node_target_id_46_5);
*Node_2013_retrieved_reference_46_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_5_is_inverse, Node_2013_ref_node_target_id_46_5);
*Node_2013_retrieved_reference_46_5_target_id = 2734;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_5_target_id, Node_2013_ref_node_target_id_46_5);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_5->references,"6",Node_2013_ref_node_target_id_46_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_6;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_6);
*Node_2013_retrieved_reference_46_6_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_6_typed_id,Node_2013_ref_node_target_id_46_6);
*Node_2013_retrieved_reference_46_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_6_is_inverse, Node_2013_ref_node_target_id_46_6);
*Node_2013_retrieved_reference_46_6_target_id = 3049;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_6_target_id, Node_2013_ref_node_target_id_46_6);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_6->references,"7",Node_2013_ref_node_target_id_46_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_7;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_7);
*Node_2013_retrieved_reference_46_7_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_7_typed_id,Node_2013_ref_node_target_id_46_7);
*Node_2013_retrieved_reference_46_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_7_is_inverse, Node_2013_ref_node_target_id_46_7);
*Node_2013_retrieved_reference_46_7_target_id = 11549;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_7_target_id, Node_2013_ref_node_target_id_46_7);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_7->references,"8",Node_2013_ref_node_target_id_46_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_8;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_8);
*Node_2013_retrieved_reference_46_8_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_8_typed_id,Node_2013_ref_node_target_id_46_8);
*Node_2013_retrieved_reference_46_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_8_is_inverse, Node_2013_ref_node_target_id_46_8);
*Node_2013_retrieved_reference_46_8_target_id = 11550;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_8_target_id, Node_2013_ref_node_target_id_46_8);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_8->references,"9",Node_2013_ref_node_target_id_46_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_46_9;
ishidaopcua_NODE* Node_2013_ref_node_target_id_46_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_46_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_2013_retrieved_reference_46_9);
*Node_2013_retrieved_reference_46_9_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_46_9_typed_id,Node_2013_ref_node_target_id_46_9);
*Node_2013_retrieved_reference_46_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_46_false_9_is_inverse, Node_2013_ref_node_target_id_46_9);
*Node_2013_retrieved_reference_46_9_target_id = 12910;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_46_9_target_id, Node_2013_ref_node_target_id_46_9);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_46_9->references,"10",Node_2013_ref_node_target_id_46_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_47_0;
ishidaopcua_NODE* Node_2013_ref_node_target_id_47_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2013_retrieved_reference_47_0);
*Node_2013_retrieved_reference_47_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_47_0_typed_id,Node_2013_ref_node_target_id_47_0);
*Node_2013_retrieved_reference_47_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_47_false_0_is_inverse, Node_2013_ref_node_target_id_47_0);
*Node_2013_retrieved_reference_47_0_target_id = 11551;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_47_0_target_id, Node_2013_ref_node_target_id_47_0);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_47_0->references,"1",Node_2013_ref_node_target_id_47_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_47_1;
ishidaopcua_NODE* Node_2013_ref_node_target_id_47_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2013_retrieved_reference_47_1);
*Node_2013_retrieved_reference_47_1_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_47_1_typed_id,Node_2013_ref_node_target_id_47_1);
*Node_2013_retrieved_reference_47_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_47_false_1_is_inverse, Node_2013_ref_node_target_id_47_1);
*Node_2013_retrieved_reference_47_1_target_id = 2019;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_47_1_target_id, Node_2013_ref_node_target_id_47_1);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_47_1->references,"2",Node_2013_ref_node_target_id_47_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_47_2;
ishidaopcua_NODE* Node_2013_ref_node_target_id_47_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2013_retrieved_reference_47_2);
*Node_2013_retrieved_reference_47_2_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_47_2_typed_id,Node_2013_ref_node_target_id_47_2);
*Node_2013_retrieved_reference_47_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_47_false_2_is_inverse, Node_2013_ref_node_target_id_47_2);
*Node_2013_retrieved_reference_47_2_target_id = 2754;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_47_2_target_id, Node_2013_ref_node_target_id_47_2);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_47_2->references,"3",Node_2013_ref_node_target_id_47_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_47_3;
ishidaopcua_NODE* Node_2013_ref_node_target_id_47_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2013_retrieved_reference_47_3);
*Node_2013_retrieved_reference_47_3_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_47_3_typed_id,Node_2013_ref_node_target_id_47_3);
*Node_2013_retrieved_reference_47_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_47_false_3_is_inverse, Node_2013_ref_node_target_id_47_3);
*Node_2013_retrieved_reference_47_3_target_id = 11562;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_47_3_target_id, Node_2013_ref_node_target_id_47_3);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_47_3->references,"4",Node_2013_ref_node_target_id_47_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_2013_retrieved_reference_47_4;
ishidaopcua_NODE* Node_2013_ref_node_target_id_47_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_2013_retrieved_reference_47_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47",&Node_2013_retrieved_reference_47_4);
*Node_2013_retrieved_reference_47_4_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_2013_retrieved_reference_47_4_typed_id,Node_2013_ref_node_target_id_47_4);
*Node_2013_retrieved_reference_47_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_2013_retrieved_reference_47_false_4_is_inverse, Node_2013_ref_node_target_id_47_4);
*Node_2013_retrieved_reference_47_4_target_id = 16295;
ishidaopcua_node_set_target_id(Node_2013_retrieved_reference_47_4_target_id, Node_2013_ref_node_target_id_47_4);
ishidaeutz_put_hashmap(Node_2013_retrieved_reference_47_4->references,"5",Node_2013_ref_node_target_id_47_4); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* EnumValueType ********/


case 7594 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 7594;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_DATA_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("EnumValueType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("EnumValueType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("A mapping between a value of an enumerated type and a name and description.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 7594 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_7594_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_7594_retrieved_reference_45_inverse_0);
*Node_7594_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_45_inverse_0_typed_id,Node_7594_ref_node_target_id_45_inverse_0);
*Node_7594_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_45_inverse_true_0_is_inverse, Node_7594_ref_node_target_id_45_inverse_0);
*Node_7594_retrieved_reference_45_inverse_0_target_id = 22;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_45_inverse_0_target_id, Node_7594_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_45_inverse_0->references,"1",Node_7594_ref_node_target_id_45_inverse_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_45_0;
ishidaopcua_NODE* Node_7594_ref_node_target_id_45_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_45_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45",&Node_7594_retrieved_reference_45_0);
*Node_7594_retrieved_reference_45_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_45_0_typed_id,Node_7594_ref_node_target_id_45_0);
*Node_7594_retrieved_reference_45_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_45_false_0_is_inverse, Node_7594_ref_node_target_id_45_0);
*Node_7594_retrieved_reference_45_0_target_id = 102;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_45_0_target_id, Node_7594_ref_node_target_id_45_0);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_45_0->references,"1",Node_7594_ref_node_target_id_45_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_38_0;
ishidaopcua_NODE* Node_7594_ref_node_target_id_38_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_7594_retrieved_reference_38_0);
*Node_7594_retrieved_reference_38_0_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_38_0_typed_id,Node_7594_ref_node_target_id_38_0);
*Node_7594_retrieved_reference_38_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_38_false_0_is_inverse, Node_7594_ref_node_target_id_38_0);
*Node_7594_retrieved_reference_38_0_target_id = 8251;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_38_0_target_id, Node_7594_ref_node_target_id_38_0);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_38_0->references,"1",Node_7594_ref_node_target_id_38_0);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_38_1;
ishidaopcua_NODE* Node_7594_ref_node_target_id_38_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_7594_retrieved_reference_38_1);
*Node_7594_retrieved_reference_38_1_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_38_1_typed_id,Node_7594_ref_node_target_id_38_1);
*Node_7594_retrieved_reference_38_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_38_false_1_is_inverse, Node_7594_ref_node_target_id_38_1);
*Node_7594_retrieved_reference_38_1_target_id = 7616;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_38_1_target_id, Node_7594_ref_node_target_id_38_1);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_38_1->references,"2",Node_7594_ref_node_target_id_38_1);

}if(references_flag != 2){

ishidaopcua_NODE* Node_7594_retrieved_reference_38_2;
ishidaopcua_NODE* Node_7594_ref_node_target_id_38_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_7594_retrieved_reference_38_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"38",&Node_7594_retrieved_reference_38_2);
*Node_7594_retrieved_reference_38_2_typed_id =38;
ishidaopcua_node_set_reference_type_id(Node_7594_retrieved_reference_38_2_typed_id,Node_7594_ref_node_target_id_38_2);
*Node_7594_retrieved_reference_38_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_7594_retrieved_reference_38_false_2_is_inverse, Node_7594_ref_node_target_id_38_2);
*Node_7594_retrieved_reference_38_2_target_id = 15082;
ishidaopcua_node_set_target_id(Node_7594_retrieved_reference_38_2_target_id, Node_7594_ref_node_target_id_38_2);
ishidaeutz_put_hashmap(Node_7594_retrieved_reference_38_2->references,"3",Node_7594_ref_node_target_id_38_2);

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* OperationLimits ********/


case 11551 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11551;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("OperationLimits", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("OperationLimits", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Defines the limits supported by the server for different operations.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

puts("finished adding node >> 11551 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11551_retrieved_reference_47_inverse_0;
ishidaopcua_NODE* Node_11551_ref_node_target_id_47_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11551_retrieved_reference_47_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11551_retrieved_reference_47_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11551_retrieved_reference_47_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"47_inverse",&Node_11551_retrieved_reference_47_inverse_0);
*Node_11551_retrieved_reference_47_inverse_0_typed_id =47;
ishidaopcua_node_set_reference_type_id(Node_11551_retrieved_reference_47_inverse_0_typed_id,Node_11551_ref_node_target_id_47_inverse_0);
*Node_11551_retrieved_reference_47_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11551_retrieved_reference_47_inverse_true_0_is_inverse, Node_11551_ref_node_target_id_47_inverse_0);
*Node_11551_retrieved_reference_47_inverse_0_target_id = 2013;
ishidaopcua_node_set_target_id(Node_11551_retrieved_reference_47_inverse_0_target_id, Node_11551_ref_node_target_id_47_inverse_0);
ishidaeutz_put_hashmap(Node_11551_retrieved_reference_47_inverse_0->references,"1",Node_11551_ref_node_target_id_47_inverse_0); 
}

} {
ishidaopcua_NODE* Node_11551_retrieved_reference_40_0;
ishidaopcua_NODE* Node_11551_ref_node_target_id_40_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11551_retrieved_reference_40_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11551_retrieved_reference_40_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11551_retrieved_reference_40_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"40",&Node_11551_retrieved_reference_40_0);
*Node_11551_retrieved_reference_40_0_typed_id =40;
ishidaopcua_node_set_reference_type_id(Node_11551_retrieved_reference_40_0_typed_id,Node_11551_ref_node_target_id_40_0);
*Node_11551_retrieved_reference_40_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11551_retrieved_reference_40_false_0_is_inverse, Node_11551_ref_node_target_id_40_0);
*Node_11551_retrieved_reference_40_0_target_id = 11564;
ishidaopcua_node_set_target_id(Node_11551_retrieved_reference_40_0_target_id, Node_11551_ref_node_target_id_40_0);
ishidaeutz_put_hashmap(Node_11551_retrieved_reference_40_0->references,"1",Node_11551_ref_node_target_id_40_0); 
}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11551_retrieved_reference_37_0;
ishidaopcua_NODE* Node_11551_ref_node_target_id_37_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11551_retrieved_reference_37_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11551_retrieved_reference_37_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11551_retrieved_reference_37_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"37",&Node_11551_retrieved_reference_37_0);
*Node_11551_retrieved_reference_37_0_typed_id =37;
ishidaopcua_node_set_reference_type_id(Node_11551_retrieved_reference_37_0_typed_id,Node_11551_ref_node_target_id_37_0);
*Node_11551_retrieved_reference_37_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11551_retrieved_reference_37_false_0_is_inverse, Node_11551_ref_node_target_id_37_0);
*Node_11551_retrieved_reference_37_0_target_id = 80;
ishidaopcua_node_set_target_id(Node_11551_retrieved_reference_37_0_target_id, Node_11551_ref_node_target_id_37_0);
ishidaeutz_put_hashmap(Node_11551_retrieved_reference_37_0->references,"1",Node_11551_ref_node_target_id_37_0); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
/******* OperationLimitsType ********/


case 11564 : 
{ 

if(references_flag == 1) {
universal_node = ishidaopcua_node_init(12,12,0);
}
else
{ universal_node = ishidaopcua_node_init(12,1,0);
} 

universal_node_id = ishidaopcua_init_node_id();
universal_node_id->identifier_type = ishidaopcua_NODE_ID_IDENTIFIER_TYPE_NUMERIC;
universal_node_id->identifier.numeric = 11564;
ishidaopcua_node_set_node_id(universal_node_id, universal_node);

universal_node_class = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_node_class = ishidaopcua_NODE_CLASS_OBJECT_TYPE;
ishidaopcua_node_set_node_class(universal_node_class, universal_node);

universal_browse_name = ishidaopcua_init_qualified_name();
universal_browse_name_string = ishidaopcua_init_string(); 
ishidaopcua_set_string("OperationLimitsType", universal_browse_name_string);
ishidaopcua_set_qualified_name(0,universal_browse_name_string, universal_browse_name);
ishidaopcua_node_set_browse_name(universal_browse_name, universal_node);

universal_display_name = ishidaopcua_init_localized_text();
universal_display_name_locale = ishidaopcua_init_string();
universal_display_name_text = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_display_name_locale));
ishidaopcua_set_string("OperationLimitsType", (universal_display_name_text));
ishidaopcua_set_localized_text(universal_display_name_locale, universal_display_name_text, universal_display_name);
ishidaopcua_node_set_display_name(universal_display_name, universal_node);

universal_description = ishidaopcua_init_localized_text();
universal_description_locale = ishidaopcua_init_string();
ishidaopcua_set_string("en-UK", (universal_description_locale));
universal_description_text = ishidaopcua_init_string();
ishidaopcua_set_string("Identifies the operation limits imposed by the server.", (universal_description_text));
ishidaopcua_set_localized_text(universal_description_locale, universal_description_text, universal_description);
ishidaopcua_node_set_description(universal_description, universal_node);

universal_write_mask = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_write_mask = 0;
ishidaopcua_node_set_write_mask(universal_write_mask,universal_node);

universal_user_write_mask =ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1);
*universal_user_write_mask = 0;
ishidaopcua_node_set_user_write_mask(universal_user_write_mask, universal_node);

universal_is_abstract = ishidaopcua_malloc(ishidaopcua_TYPE_ID_BOOLEAN,1);
*universal_is_abstract = 0;
ishidaopcua_node_set_is_abstract(universal_is_abstract , universal_node);

puts("finished adding node >> 11564 \n");




if(references_flag > 0)
{
initialize_reference_nodes(universal_node);

if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_45_inverse_0;
ishidaopcua_NODE* Node_11564_ref_node_target_id_45_inverse_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_45_inverse_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_45_inverse_true_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_45_inverse_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"45_inverse",&Node_11564_retrieved_reference_45_inverse_0);
*Node_11564_retrieved_reference_45_inverse_0_typed_id =45;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_45_inverse_0_typed_id,Node_11564_ref_node_target_id_45_inverse_0);
*Node_11564_retrieved_reference_45_inverse_true_0_is_inverse = 1;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_45_inverse_true_0_is_inverse, Node_11564_ref_node_target_id_45_inverse_0);
*Node_11564_retrieved_reference_45_inverse_0_target_id = 61;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_45_inverse_0_target_id, Node_11564_ref_node_target_id_45_inverse_0);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_45_inverse_0->references,"1",Node_11564_ref_node_target_id_45_inverse_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_0;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_0 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_0_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_0_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_0_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_0);
*Node_11564_retrieved_reference_46_0_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_0_typed_id,Node_11564_ref_node_target_id_46_0);
*Node_11564_retrieved_reference_46_false_0_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_0_is_inverse, Node_11564_ref_node_target_id_46_0);
*Node_11564_retrieved_reference_46_0_target_id = 11565;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_0_target_id, Node_11564_ref_node_target_id_46_0);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_0->references,"1",Node_11564_ref_node_target_id_46_0); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_1;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_1 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_1_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_1_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_1_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_1);
*Node_11564_retrieved_reference_46_1_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_1_typed_id,Node_11564_ref_node_target_id_46_1);
*Node_11564_retrieved_reference_46_false_1_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_1_is_inverse, Node_11564_ref_node_target_id_46_1);
*Node_11564_retrieved_reference_46_1_target_id = 12161;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_1_target_id, Node_11564_ref_node_target_id_46_1);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_1->references,"2",Node_11564_ref_node_target_id_46_1); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_2;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_2 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_2_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_2_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_2_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_2);
*Node_11564_retrieved_reference_46_2_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_2_typed_id,Node_11564_ref_node_target_id_46_2);
*Node_11564_retrieved_reference_46_false_2_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_2_is_inverse, Node_11564_ref_node_target_id_46_2);
*Node_11564_retrieved_reference_46_2_target_id = 12162;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_2_target_id, Node_11564_ref_node_target_id_46_2);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_2->references,"3",Node_11564_ref_node_target_id_46_2); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_3;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_3 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_3_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_3_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_3_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_3);
*Node_11564_retrieved_reference_46_3_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_3_typed_id,Node_11564_ref_node_target_id_46_3);
*Node_11564_retrieved_reference_46_false_3_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_3_is_inverse, Node_11564_ref_node_target_id_46_3);
*Node_11564_retrieved_reference_46_3_target_id = 11567;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_3_target_id, Node_11564_ref_node_target_id_46_3);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_3->references,"4",Node_11564_ref_node_target_id_46_3); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_4;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_4 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_4_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_4_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_4_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_4);
*Node_11564_retrieved_reference_46_4_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_4_typed_id,Node_11564_ref_node_target_id_46_4);
*Node_11564_retrieved_reference_46_false_4_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_4_is_inverse, Node_11564_ref_node_target_id_46_4);
*Node_11564_retrieved_reference_46_4_target_id = 12163;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_4_target_id, Node_11564_ref_node_target_id_46_4);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_4->references,"5",Node_11564_ref_node_target_id_46_4); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_5;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_5 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_5_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_5_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_5_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_5);
*Node_11564_retrieved_reference_46_5_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_5_typed_id,Node_11564_ref_node_target_id_46_5);
*Node_11564_retrieved_reference_46_false_5_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_5_is_inverse, Node_11564_ref_node_target_id_46_5);
*Node_11564_retrieved_reference_46_5_target_id = 12164;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_5_target_id, Node_11564_ref_node_target_id_46_5);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_5->references,"6",Node_11564_ref_node_target_id_46_5); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_6;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_6 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_6_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_6_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_6_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_6);
*Node_11564_retrieved_reference_46_6_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_6_typed_id,Node_11564_ref_node_target_id_46_6);
*Node_11564_retrieved_reference_46_false_6_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_6_is_inverse, Node_11564_ref_node_target_id_46_6);
*Node_11564_retrieved_reference_46_6_target_id = 11569;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_6_target_id, Node_11564_ref_node_target_id_46_6);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_6->references,"7",Node_11564_ref_node_target_id_46_6); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_7;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_7 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_7_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_7_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_7_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_7);
*Node_11564_retrieved_reference_46_7_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_7_typed_id,Node_11564_ref_node_target_id_46_7);
*Node_11564_retrieved_reference_46_false_7_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_7_is_inverse, Node_11564_ref_node_target_id_46_7);
*Node_11564_retrieved_reference_46_7_target_id = 11570;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_7_target_id, Node_11564_ref_node_target_id_46_7);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_7->references,"8",Node_11564_ref_node_target_id_46_7); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_8;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_8 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_8_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_8_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_8_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_8);
*Node_11564_retrieved_reference_46_8_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_8_typed_id,Node_11564_ref_node_target_id_46_8);
*Node_11564_retrieved_reference_46_false_8_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_8_is_inverse, Node_11564_ref_node_target_id_46_8);
*Node_11564_retrieved_reference_46_8_target_id = 11571;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_8_target_id, Node_11564_ref_node_target_id_46_8);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_8->references,"9",Node_11564_ref_node_target_id_46_8); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_9;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_9 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_9_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_9_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_9_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_9);
*Node_11564_retrieved_reference_46_9_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_9_typed_id,Node_11564_ref_node_target_id_46_9);
*Node_11564_retrieved_reference_46_false_9_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_9_is_inverse, Node_11564_ref_node_target_id_46_9);
*Node_11564_retrieved_reference_46_9_target_id = 11572;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_9_target_id, Node_11564_ref_node_target_id_46_9);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_9->references,"10",Node_11564_ref_node_target_id_46_9); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_10;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_10 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_10_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_10_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_10_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_10);
*Node_11564_retrieved_reference_46_10_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_10_typed_id,Node_11564_ref_node_target_id_46_10);
*Node_11564_retrieved_reference_46_false_10_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_10_is_inverse, Node_11564_ref_node_target_id_46_10);
*Node_11564_retrieved_reference_46_10_target_id = 11573;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_10_target_id, Node_11564_ref_node_target_id_46_10);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_10->references,"11",Node_11564_ref_node_target_id_46_10); 
}

}if(references_flag != 2){

{
ishidaopcua_NODE* Node_11564_retrieved_reference_46_11;
ishidaopcua_NODE* Node_11564_ref_node_target_id_46_11 = ishidaopcua_node_init(3,0,0);
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_11_typed_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_false_11_is_inverse = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaopcua_UINT32* Node_11564_retrieved_reference_46_11_target_id = ishidaopcua_malloc(ishidaopcua_TYPE_ID_UINT32,1); 
ishidaeutz_get_hashmap(universal_node->references,"46",&Node_11564_retrieved_reference_46_11);
*Node_11564_retrieved_reference_46_11_typed_id =46;
ishidaopcua_node_set_reference_type_id(Node_11564_retrieved_reference_46_11_typed_id,Node_11564_ref_node_target_id_46_11);
*Node_11564_retrieved_reference_46_false_11_is_inverse = 0;
ishidaopcua_node_set_is_inverse(Node_11564_retrieved_reference_46_false_11_is_inverse, Node_11564_ref_node_target_id_46_11);
*Node_11564_retrieved_reference_46_11_target_id = 11574;
ishidaopcua_node_set_target_id(Node_11564_retrieved_reference_46_11_target_id, Node_11564_ref_node_target_id_46_11);
ishidaeutz_put_hashmap(Node_11564_retrieved_reference_46_11->references,"12",Node_11564_ref_node_target_id_46_11); 
}

} 

} /* ---- if match ---- */ 


return universal_node; 
break;
} /* ---------- match case ---------------- */ 
} /* --- match switch --- */

	
	return NULL;
}
/*************************************** ISHIDAOPCUA_SERVER_NODES END ***************************************/